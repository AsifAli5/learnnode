const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
const fileUpload = require("express-fileupload");
const rootPath = path.join(__dirname, '..');
const api = require('./api2');
const cors = require('cors');
const helmet = require('helmet');
const xss = require('xss-clean');
const config = require('../config');
const mysqlConfig = require('./config/dbConfig');
const { validate, ValidationError, Joi } = require('express-validation');
// require('../server/config/mongodb.config');
var mysql = require('mysql');

var whitelist = ['http://localhost:4200'];


app.use(fileUpload());

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
//to prevent xss.
app.use(xss());
//Helmet is a collection of middleware functions.n
app.use(helmet());
//app.use(express.static(path.join(rootPath, 'ang')));

//sending connection to child routes. 
var pool = mysql.createPool(mysqlConfig, { multipleStatements: true });

const poolFunc = function (req, res, next) {
//work like onSave person
    pool.getConnection(function (err, conn) {
        if (err) {
            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            }
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            }
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            }
        }
        req.conn = conn;
        req.pool = pool;
        next();
        
    });

};

app.use('/api2',poolFunc, api);

app.use((error, req, res, next) => {
    if (error instanceof ValidationError) {
        return res.status(error.statusCode).json(error)
      }
    res.status(error.status || 500);
    res.json({
        message: error.message
       
    });
});

/*app.get('*', (req, res) => {
    res.sendFile(path.join(rootPath, 'ang/index.html'));
});*/

const port = '3001';//config.server.PORT || '3000';	
const server = http.createServer(app);
server.listen(port, () => console.log(`Running on localhost:${port}`));
