var Database = require('../../../config/database');
var db = new Database();
const config = require('../../../../config');

class OrgChartMultiline {

  constructor() { }

  async employees(filters = '') {

    let records = await this.multilineReportingQuery(filters);
    let data = this.sortEmployeeData(records, records.length);
    let reportLineData = await this.multilineArrowQuery(filters);
    let processedNodeData = this.employeeDataBuilder(data);

    let response = {
      nodes: processedNodeData,
      conn: this.employeeLinkBuilder(reportLineData, processedNodeData),
      raw_data: data
    };


    return response;

  }

  sortEmployeeData(data, total_records, sortedArray = [], counter = 0) {

    if (total_records > 0 && counter < total_records) {

      if (typeof data[counter] !== 'undefined' && data[counter].parent_id == 0) {

        sortedArray.push(data.splice(0, 1)[0]);

        this.sortEmployeeData(data, total_records, sortedArray, 0);

      }
      else {
        var strippedArray = 0;
        var removedNodes = [];

        for (var i = 0; i < data.length; i++) {

          if (data[i].parent_id == sortedArray[counter].id &&
            typeof sortedArray[counter] !== 'undefined') {

            sortedArray.push(data[i]);

            var index = data.indexOf(i);

            if (index > -1) {

              data.splice(index, 1);

            }

          }

        }

      }

      this.sortEmployeeData(data, total_records, sortedArray, ++counter);
    }

    const filteredArr = sortedArray.reduce((acc, current) => {
      const x = acc.find(item => item.id === current.id);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);

    return filteredArr;
  }

  removeNodes(dataArray, nodesArray) {

    for (var i = 0; i < nodesArray.length; i++) {

      dataArray.splice(nodesArray[i], 1);

    }

    return dataArray;

  }

  employeeDataBuilder(data) {

    let response = [];
	
	console.log( config.site.SITE_URL + config.site.PPM_URL );

    for (let i = 0; i < data.length; i++) {

      let image = '';
      if (data[i].picture != '' || data[i].picture != null || data[i].picture != 'undefined') {
        image = config.site.SITE_URL + config.site.PPM_URL + "emp_pictures/" + data[i].picture;
      }
      else {

        let placeHolder = '';

        if (data[i].name_salute.toLowerCase() == "mr.") {

          placeHolder = config.site.SITE_URL + config.site.PPM_URL + 'assets/orgchart/img/male.png';

        }
        else {

          placeHolder = config.site.SITE_URL + config.site.PPM_URL + 'assets/orgchart/img/female.png';

        }

        image = placeHolder;

      }

      let nodes = {
        id: i.toString(), // department id
        label: data[i].name, // employee name
        data: {
          band: data[i].band_desc, // band description
          department: data[i].companyname, // department name
          colorCode: '#e80000', // color code
          role: data[i].designation_name, // designation name
          img: image,
          emp_id: data[i].id.toString(),
          lm_lvl: data[i].main_lm.toString()
        }
      };

      response.push(nodes);

    }

    return response;

  }

  employeeLinkBuilder(data, nodeData) {

    let response = [];

    for (let i = 0; i < data.length; i++) {

      let links = {
        parent_id: data[i].parent_id, // department id
        emp_id: data[i].id, // reporting to id
        counter_id: i,
        data: {
          strokeColor: '#fdd8aa', // line color
          linkText: 'Manager of', // manager of
          strokeDash: 4,
          currentLevel: 1,
          expectedLevel: 2
        }
      };

      response.push(links);

    }

    return this.employeeIdChange(response, nodeData);

  }

  employeeIdChange(data, nodeData) {

    let response = [];

    for (let i = 0; i < data.length; i++) {

      let links = {};

      // if(data[i].parent_id == '0') {

      //     links = {
      //         source: data[i].counter_id.toString(), // reporting to id
      //         target:  data[i].counter_id.toString(), // reporting to id
      //         data: {
      //             strokeColor: '#fdd8aa', // line color
      //             linkText: 'Manager of', // manager of
      //             emp_id: data[i].emp_id, // employee id
      //             strokeDash: 4,
      //             currentLevel: 1,
      //             expectedLevel: 2
      //         }
      //     };

      // }
      // else {
      if (data[i].parent_id != '0') {
        links = {
          source: this.getParentVirtualId(data[i].parent_id, nodeData), // reporting to id
          target: this.getParentVirtualId(data[i].emp_id, nodeData), // reporting to id
          data: {
            strokeColor: '#fdd8aa', // line color
            // linkText: 'Manager of', // manager of
            emp_id: data[i].emp_id, // employee id
            strokeDash: 0,
            // currentLevel: 1,
            // expectedLevel: 2
          }
        };
        response.push(links);
      }
      // }


    }

    return response;

  }

  getParentVirtualId(currentNode, dataArray) {

    var parentId = 0;

    for (var i = 0; i < dataArray.length; i++) {

      if (currentNode == dataArray[i].data.emp_id) {

        parentId = dataArray[i].id;

      }

    }

    return parentId.toString();

  }

  multilineReportingQuery(filters) {

    filters = this.getFilterQuery(filters);

    let query = `SELECT
                DISTINCT(pe.id), pe.name,
                ( CASE WHEN (
                SELECT
                (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id
                FROM proll_department_managers
                INNER JOIN proll_employee ON proll_employee.loginname = proll_department_managers.email
                INNER JOIN (
                SELECT
                    DISTINCT(pe.id) AS id
                    FROM proll_employee pe
                    INNER JOIN proll_department_managers pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND pc.id = '2'
                ) AS pj ON pj.id = proll_employee.id
                WHERE proll_department_managers.id IN (
                (CASE WHEN pe.reporting_to_id != 0 AND pe.reporting_to_id != pe.dept_id THEN pe.reporting_to_id ELSE pe.dept_id END)
                )
                ) = '0' THEN 0 ELSE pe.dept_id END ) AS dept_id,
                ( CASE WHEN (
                SELECT
                (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id
                FROM proll_department_managers
                INNER JOIN proll_employee ON proll_employee.loginname = proll_department_managers.email
                INNER JOIN (
                SELECT
                    DISTINCT(pe.id) AS id
                    FROM proll_employee pe
                    INNER JOIN proll_department_managers pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND pc.id = '2'
                ) AS pj ON pj.id = proll_employee.id
                WHERE proll_department_managers.id IN (
                (CASE WHEN pe.reporting_to_id != 0 AND pe.reporting_to_id != pe.dept_id THEN pe.reporting_to_id ELSE pe.dept_id END)
                )
                ) = '0' THEN 0 ELSE
                (CASE WHEN (pe.reporting_to_id IS NULL OR pe.reporting_to_id = '0')
                THEN pe.dept_id ELSE pe.reporting_to_id END ) END ) AS reporting_to_id,
                pe.name_salute,
                ( CASE WHEN pe.micro_picture IS NULL OR pe.micro_picture = '' THEN 'n' ELSE pe.micro_picture END ) AS picture,
                (
                SELECT
                (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id
                FROM proll_department_managers
                INNER JOIN proll_employee ON proll_employee.loginname = proll_department_managers.email
                INNER JOIN (
                SELECT
                    DISTINCT(pe.id) AS id
                    FROM proll_employee pe
                    INNER JOIN proll_department_managers pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND pc.id = '2'
                ) AS pj ON pj.id = proll_employee.id
                WHERE proll_department_managers.id IN (
                (CASE WHEN pe.reporting_to_id != 0 AND pe.reporting_to_id != pe.dept_id THEN pe.reporting_to_id ELSE pe.dept_id END)
                )
                )
                AS parent_id,
                eb.band_desc,
                eb.id AS band_id,
                pcd.designation_name,
                pe.main_lm,
                (SELECT department FROM proll_department_managers WHERE proll_department_managers.id = pe.dept_id) AS companyname
                FROM proll_employee pe
                INNER JOIN proll_department_managers pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                LEFT JOIN proll_client pc ON pc.id = pe.cid
                LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                WHERE pe.status = 1 AND pc.id = '2'
                ORDER BY eb.band_desc ASC, parent_id ASC, pe.id ASC`;

    return this.executeQuery(query);

  }

  multilineArrowQuery(filters) {

    filters = this.getFilterQuery(filters);

    let query = `SELECT
                    DISTINCT(pe.id),
                    pe.name,
                    pe.reporting_to_id,
                    pe.dept_id,
                    pe.name_salute,
                    ( CASE WHEN pe.micro_picture IS NULL OR pe.micro_picture = '' THEN 'n' ELSE pe.micro_picture END ) AS picture,
                    (
                SELECT
                (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id
                FROM proll_department_managers
                INNER JOIN proll_employee ON proll_employee.loginname = proll_department_managers.email
                INNER JOIN (
                SELECT
                    DISTINCT(pe.id) AS id
                    FROM proll_employee pe
                    INNER JOIN proll_department_managers pd ON pd.id = pe.reporting_to_id
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND pc.id = '2'
                ) AS pj ON pj.id = proll_employee.id
                WHERE proll_department_managers.id IN ( pe.reporting_to_id )
                )
                AS parent_id,
                eb.band_desc,
                eb.id AS band_id,
                pcd.designation_name,
                pe.main_lm,
                (SELECT department FROM proll_department_managers WHERE proll_department_managers.id = pe.dept_id) AS companyname
                FROM proll_employee pe
                INNER JOIN proll_department_managers pd ON pd.id = pe.reporting_to_id
                LEFT JOIN proll_client pc ON pc.id = pe.cid
                LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                WHERE pe.status = 1 AND pc.id = '2'

                UNION

                SELECT
                    DISTINCT(pe.id),
                    pe.name,
                    pe.reporting_to_id,
                    pe.dept_id,
                    pe.name_salute,
                    ( CASE WHEN pe.micro_picture IS NULL OR pe.micro_picture = '' THEN 'n' ELSE pe.micro_picture END ) AS picture,
                    (
                SELECT
                (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id
                FROM proll_department_managers
                INNER JOIN proll_employee ON proll_employee.loginname = proll_department_managers.email
                INNER JOIN (
                SELECT
                    DISTINCT(pe.id) AS id
                    FROM proll_employee pe
                    INNER JOIN proll_department_managers pd ON pd.id = pe.dept_id
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND pc.id = '2'
                ) AS pj ON pj.id = proll_employee.id
                WHERE proll_department_managers.id IN ( pe.dept_id )
                )
                AS parent_id,
                eb.band_desc,
                eb.id AS band_id,
                pcd.designation_name,
                pe.main_lm,
                (SELECT department FROM proll_department_managers WHERE proll_department_managers.id = pe.dept_id) AS companyname
                FROM proll_employee pe
                INNER JOIN proll_department_managers pd ON pd.id = pe.dept_id
                LEFT JOIN proll_client pc ON pc.id = pe.cid
                LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                WHERE pe.status = 1 AND pc.id = '2' AND (
                SELECT
                (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id
                FROM proll_department_managers
                INNER JOIN proll_employee ON proll_employee.loginname = proll_department_managers.email
                INNER JOIN (
                SELECT
                    DISTINCT(pe.id) AS id
                    FROM proll_employee pe
                    INNER JOIN proll_department_managers pd ON pd.id = pe.dept_id
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND pc.id = '2'
                ) AS pj ON pj.id = proll_employee.id
                WHERE proll_department_managers.id IN ( pe.dept_id )
                ) != 0
                ORDER BY band_desc ASC, parent_id ASC, id ASC`;

    return this.executeQuery(query);
  }

  getFilterQuery(filters) {

    let filter = '';

    if (typeof filters.filter != 'undefined' && filters.filter == '1') {

      if (typeof filters.country != 'undefined' && filters.country != '') {

        filter += ' AND pcl.country_id IN(' + filters.country + ') ';

      }

      if (typeof filters.city != 'undefined' && filters.city != '') {

        filter += ' AND pcl.city_id IN(' + filters.city + ') ';

      }

      if (typeof filters.branch != 'undefined' && filters.branch != '') {

        filter += ' AND pcl.loc_id IN(' + filters.branch + ') ';

      }

      if (typeof filters.group != 'undefined' && filters.group != '') {

        filter += ' AND dg2.department_group2_id IN(' + filters.group + ') ';

      }

      if (typeof filters.family != 'undefined' && filters.family != '') {

        filter += ' AND dg3.department_group3_id IN(' + filters.family + ') ';

      }

      if (typeof filters.department != 'undefined' && filters.department != '') {

        filter += ' AND pd.id IN(' + filters.department + ') ';

      }

      if (typeof filters.designation != 'undefined' && filters.designation != '') {

        filter += ' AND pcd.designation_id IN(' + filters.designation + ')';

      }

      if (typeof filters.band != 'undefined' && filters.band != '') {

        filter += ' AND eb.id IN(' + filters.band + ')';

      }

    }
    else {

      filter = '';

    }

    return filter;
  }

  executeQuery(query) {

    return new Promise((resolve, reject) => {

      db.query(query, (result, err) => {

        if (!err) {
          resolve(JSON.parse(JSON.stringify(result)));
        }
        else {
          console.log(err);
          reject(err);
        }

      });

    });

  }

}

exports.OrgChartMultiline = OrgChartMultiline;