var express = require('express');
var router = express.Router();
var OrgChartFilter = require("../model/OrgChartFilter").OrgChartFilter;
import OrgChartData from '../model/OrgChartData';


router.get("/group_one_filter", (req, res, next) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getGroupOneData().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/group_two_filter", (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getGroupTwoData().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/group_three_filter", (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getGroupThreeData().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/countries_filter", (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getCountries().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/cities_filter", (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getCities().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/branches_filter", (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getBranches().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/department_filter", (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getDepartment().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/designation_filter", (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getDesignation().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/bands_filter", (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getTotalBands().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/cities_filter", (req, res) => {

    var countryId = '';

    countryId = req.body.countryId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getCities(countryId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/cities_branches", (req, res) => {

    var cityId = '';

    cityId = req.body.cityId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getCitiesBranches(cityId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/branch_country", (req, res) => {

    var branchId = '';

    branchId = req.body.branchId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getBranchCityCountry(branchId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/get_group_three", (req, res) => {

    var groupId = '';

    groupId = req.body.groupId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getJobFamilies(groupId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/get_family_group_depart", (req, res) => {

    var familyId = '';

    familyId = req.body.familyId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getSelectedJobGroups(familyId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/get_depart_groups", (req, res) => {

    var departmentId = '';

    departmentId = req.body.departmentId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getSelectedDepartmentsGroups(departmentId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/get_approval_matrix_data", (req, res) => {
    
    let options = req.query.op;
    let id = req.query.s;

    if( typeof id === 'undefined' ) {

        id = null;

    }

    var orgChartData = new OrgChartData();

    orgChartData.getOrgChartResults(options, id, req.query).then((result) => {

        res.status(200).send(result[0]);

    }).catch((err) => {

        res.status(500).send(err);

    });

});

router.post("/get_single_employee_data", (req, res) => {

    let action = req.body.action;
    let details = req.body.details;

    var orgChartData = new OrgChartData();

    orgChartData.getSingleEmployeeData(details).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/orgchart_locations", (req, res) => {

    var orgChartData = new OrgChartData();

    orgChartData.getOrgLocations().then((result) => {

        res.status(200).send(result);

    }).catch((err) => {

        res.status(500).send(err);

    });

});

module.exports = router;