var express = require("express");
var router = express.Router();
var payroll = require("../../2.2/model/Payroll").Payroll;

var path = require("path");

router.post("/generateEmf", async (req, res) => {

    let payrollData = '';
    let data = [];
    let startDate = req.body.startDate;
    let endDate = req.body.endDate;

    /**  lwop data start */
    let payrolls = new payroll(req.conn);
    
    // let result =  await payrolls.getLwopByDateRange(startDate,endDate);
    // data.push({lwopData:req.body.lwop});
    /**  lwop data end */

    /**  Joined staff data start  */
    let joinedStaffResult =  await payrolls.getJoinedStaffByDateRange(startDate,endDate);
    data.push({joinedStaff:joinedStaffResult});
    /**  Joined staff data end  */

    /**  Leaver staff data start  */
    let leaverStaffResult =  await payrolls.getleaverStaffByDateRange(startDate,endDate);
    data.push({leaverStaff:leaverStaffResult});
    /**  Leaver staff data end  */
   
    /**  Loan staff data start  */
    let loanResult =  await payrolls.getLoanDetailsByDateRange(startDate,endDate);
    data.push({loanResults:loanResult});
    /**  Loan staff data end  */

     /**  Expense staff data start  */
     let expenseResult =  await payrolls.getExpenseDetailsByDateRange(startDate,endDate);
     data.push({expenseResults:expenseResult});
     /**  Expense staff data end  */
   
    res.status(200).json({ 'status':'success',message: "list" ,data});
  });
module.exports = router;



// return res.json(Object.keys(result).length);
// for(let i=0;i<Object.keys(result).length;i++){
//   console.log(result[i].empcode);
//   return false;
// }
