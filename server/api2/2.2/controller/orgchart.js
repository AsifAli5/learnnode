var express = require('express');
var router = express.Router();
var OrgChartFilter = require("../model/OrgChartFilter").OrgChartFilter;
const jwt = require("jsonwebtoken");
const tokenVerifier = require("../middlewares/verifyToken");
const OrgChartData = require('../model/OrgChartData').OrgChartData;
const OrgChartMultiline = require('../model/OrgChartMultiline').OrgChartMultiline;
const AESEncryption = require('../model/AESEncryption').AESEncryption;
const SeamlessLogin = require('../model/SeamlessLogin').SeamlessLogin;
var crypto=require('crypto');
var md5 = require('md5');

router.post("/login", (req, res, next) => {

    //mock user

    let uName = req.body.username;
    let uSecret = req.body.password;
    let uType = req.body.type;

    const loginRedirect = "portals";

    let password = 'lbwyBzfgzUIvXZFShJuikaWvLJhIVq36';

    const aesEncryption = new AESEncryption();

    var orgChartData = new OrgChartData();

    if(typeof uType != 'undefined' && uType != null && uType != false) {

        let decryptedName = uName;
        let decryptedPassword = md5(uSecret);

        orgChartData.checkUser(decryptedName, decryptedPassword).then( (result) => {

            if(result.response == true) {
    
                const user = {
                    id: result.id,
                    username: result.email,
                    email: result.email,
                }
            
                jwt.sign( { user }, 'secretkey', { expiresIn: '1h' }, (err, token) => {
            
                    res.json({
                        token,
                        success: true,
                        message: "user logged in successfully!",
                        result: {
                            response: true,
                            e_number: result.id,
                            e_code: result.empcode,
                            emp_name: result.emp_name,
                            destination: loginRedirect,
                            portal: false,
                        }
                    });
            
                });
    
            }
            else {
                res.json({
                    success: false,
                    result
                });
            }
    
        });

    }
    else {

        orgChartData.accessUser( uName ).then( (result) => {
            if(result.length > 0) {
                
                let username = result[0].username;
                
                let secret = result[0].secret;

                let destination = result[0].destination;

                let portal = result[0].portal;

                orgChartData.checkUser(username, secret).then( (result) => {
                    
                    if(result.response == true) {

                        const user = {
                            id: result.id,
                            username: result.email,
                            email: result.email,
                        }
                    
                        jwt.sign( { user }, 'secretkey', { expiresIn: '1h' }, (err, token) => {
                    
                            res.json({
                                token,
                                success: true,
                                message: "user logged in successfully!",
                                result: {
                                    response: true,
                                    e_number: result.id,
                                    e_code: result.empcode,
                                    emp_name: result.emp_name,
                                    destination: destination,
                                    portal: portal,
                                }
                            });
                    
                        });
            
                    }
                    else {
                        res.json({
                            success: false,
                            result
                        });
                    }

                });
            }
        });

    }

});

router.post("/verifyLogin", (req, res, next) => {

    let token = req.body.token;

    jwt.verify(token, 'secretkey', (err, authData) => {

        if( err ) {
            res.json({
                err,
                success: false,
                message: "system cannot find user loggedin",
                token: false,
            });
        }
        else {

            res.json({
                success: true,
                message: "user logged in verify",
                token: token,
            });

        }

    });

});

router.get("/group_one_filter", 
            (req, res, next) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getGroupOneData().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

    

});

router.get("/group_two_filter",
                (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getGroupTwoData().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/group_three_filter", 
            (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getGroupThreeData().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/countries_filter", 
            (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getCountries().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/cities_filter", 
            (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getCities().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/branches_filter", 
            (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getBranches().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/department_filter", 
            (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getDepartment().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/designation_filter", 
            (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getDesignation().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/bands_filter", 
            (req, res) => {

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getTotalBands().then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/cities_filter", 
            (req, res) => {

    var countryId = '';

    countryId = req.body.countryId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getCities(countryId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/cities_branches",
             (req, res) => {

    var cityId = '';

    cityId = req.body.cityId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getCitiesBranches(cityId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/branch_country", 
            (req, res) => {

    var branchId = '';

    branchId = req.body.branchId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getBranchCityCountry(branchId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/get_group_three",
             (req, res) => {

    var groupId = '';

    groupId = req.body.groupId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getJobFamilies(groupId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/get_family_group_depart",
             (req, res) => {

    var familyId = '';

    familyId = req.body.familyId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getSelectedJobGroups(familyId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.post("/get_depart_groups", 
            (req, res) => {

    var departmentId = '';

    departmentId = req.body.departmentId;

    var orgChartFilter = new OrgChartFilter();

    orgChartFilter.getSelectedDepartmentsGroups(departmentId).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/get_approval_matrix_data",
             (req, res) => {
    
    let options = req.query.op;
    let id = req.query.s;

    if( typeof id === 'undefined' ) {

        id = null;

    }

    var orgChartData = new OrgChartData();

    orgChartData.getOrgChartResults(options, id, req.query).then((result) => {

        res.status(200).send(result[0]);

    }).catch((err) => {

        res.status(500).send(err);

    });

});

router.post("/get_single_employee_data",
            (req, res) => {

    let action = req.body.action;
    let details = req.body.details;

    var orgChartData = new OrgChartData();

    orgChartData.getSingleEmployeeData(details).then( (result) => {

        res.status(200).send(result);

    })
    .catch( (err) => {

        res.status(500).send(err);

    });

});

router.get("/orgchart_locations",
            (req, res) => {

    var orgChartData = new OrgChartData();

    orgChartData.getOrgLocations(req.query).then((result) => {

        res.status(200).send(result);

    }).catch((err) => {

        res.status(500).send(err);

    });

});

router.get("/multiline_chart", (req, res) => {

    let orgChartMultiline = new OrgChartMultiline();

    orgChartMultiline.employees(req.query).then((result) => {

        res.status(200).send(result);

    }).catch((err) => {

        res.status(500).send(err);

    });

});

router.get("/total_employee_by_designation",
            (req, res) => {

    let orgChartData = new OrgChartData();

    orgChartData.getEmployeeCountByDesignation(req.query).then((result) => {

        res.status(200).send(result);

    }).catch((err) => {

        res.status(500).send(err);

    });

});

router.post("/seamless_login", (req, res) => {

    let seamlessLogin = new SeamlessLogin();

    const employeeId = (req.body.employeeId != '' && req.body.employeeId != null) ? Number(req.body.employeeId) : '';
    const source = (req.body.sourceRequest != '' && req.body.sourceRequest != null) ? req.body.sourceRequest: '';
    const destination = (req.body.destinationRequest != '' && req.body.destinationRequest != null) ? req.body.destinationRequest : '';
    const portal = (req.body.portal != '' && req.body.portal != null) ? req.body.portal : '';
    const extraParams = (req.body.extraParams != '' && req.body.extraParams != null) ? req.body.extraParams : '';
    const remoteAddr = req.connection.remoteAddress;
    const agent = req.headers['user-agent'];

    seamlessLogin.checkSeamlessLogin(employeeId).then((result) => {

        if(result == false) {

            res.status(403).send("Invalid Request Sent");

        }else {

            if(result.length == 0) {
                
                seamlessLogin.insertUserData(source, destination, portal, remoteAddr, agent,
                    employeeId, '1', 'CURRENT_TIMESTAMP', extraParams)
                    .then( (result) => {
                        
                        res.status(200).send(result);

                    });

            }
            else {

                seamlessLogin.updateUserData(source, destination, portal, remoteAddr, agent,
                        employeeId, '1', 'CURRENT_TIMESTAMP', extraParams)
                    .then( (result) => {

                        res.status(200).send(result);

                    });

            }

        }

    });

});

module.exports = router;