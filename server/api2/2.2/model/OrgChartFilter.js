var Database = require('../../../config/database');
var db = new Database();


class OrgChartFilter {

    constructor () {
    }

    getGroupOneData () { 

        var query = "SELECT dg1.department_group1_id, dg1.department_group1 FROM department_group1 dg1";

        return this.executeQuery( query );
    
    }
    
    getGroupTwoData () {
    
        var query = "SELECT dg2.department_group2_id, dg2.department_group2 FROM department_group2 dg2";
    
        return this.executeQuery( query );
     }
    
    getGroupThreeData () {
    
        var query = "SELECT dg3.department_group3_id, dg3.department_group3 FROM department_group3 dg3";
    
        return this.executeQuery( query );
     }
    
    getCountries () {
    
        var query = `SELECT DISTINCT(countries.country_id), countries.country FROM countries 
                INNER JOIN proll_client_location pcl ON pcl.country_id = countries.country_id`;
    
        return this.executeQuery( query );
     }
    
    getCities ( countriesId = '') {
    
        if(countriesId != '' && countriesId != 'undefined') {

            var query = `SELECT cities.id, cities.name AS city_name FROM cities 
            INNER JOIN proll_client_location pcl ON pcl.city_id = cities.id
            WHERE pcl.country_id IN (`+ countriesId +`)`;

        }
        else {

            var query = `SELECT cities.id, cities.name AS city_name FROM cities 
                INNER JOIN proll_client_location pcl ON pcl.city_id = cities.id`;

        }

        return this.executeQuery( query );
    
     }

    getCitiesBranches( cityId ) {

        if( cityId != '' && cityId != 'undefined') {

            var query1 = `SELECT true;
            SELECT DISTINCT(countries.country_id), countries.country FROM countries
            INNER JOIN proll_client_location pcl ON pcl.country_id = countries.country_id
            WHERE pcl.city_id IN (`+ cityId +`);`;

            var query2 = `SELECT pcl.loc_id, pcl.loc_desc FROM proll_client_location pcl 
            WHERE pcl.city_id IN (`+ cityId +`)`;

            var query = query1 + query2;

            return this.executeQuery( query );

        }
        else {

            var query = `SELECT false;
                        SELECT DISTINCT(countries.country_id), countries.country FROM countries 
                        INNER JOIN proll_client_location pcl ON pcl.country_id = countries.country_id;
                        SELECT loc_id, loc_desc FROM proll_client_location`;

            return this.executeQuery( query );

        }

    }

    getBranchCityCountry( branchId ) {

        if( branchId != '' && branchId != 'undefined') {

            var query1 = `SELECT TRUE;
            SELECT DISTINCT(countries.country_id), countries.country FROM countries
            INNER JOIN proll_client_location pcl ON pcl.country_id = countries.country_id
            WHERE pcl.loc_id IN (`+ branchId +`);`;

            var query2 = `SELECT cities.id, cities.name AS city_name FROM cities 
            INNER JOIN proll_client_location pcl ON pcl.city_id = cities.id
            WHERE pcl.loc_id IN (`+ branchId +`)`;

            var query = query1 + query2;

            return this.executeQuery( query );

        }
        else {

            var query = `SELECT FALSE;
                        SELECT DISTINCT(countries.country_id), countries.country FROM countries 
                        INNER JOIN proll_client_location pcl ON pcl.country_id = countries.country_id;
                        SELECT cities.id, cities.name AS city_name FROM cities 
                        INNER JOIN proll_client_location pcl ON pcl.city_id = cities.id`;

            return this.executeQuery( query );

        }

    }
    
    getJobFamilies( jobGroups ) {

        var returnArray = '';

        if( jobGroups != '' && jobGroups != 'undefined' ) {

            var query = `SELECT department_group3_id, department_group3 
                        FROM department_group3 WHERE department_group2_id IN (`+ jobGroups +`)`;

            return this.executeQuery(query);

        }
        else {

            return this.getGroupThreeData();

        }

    }

    getSelectedJobGroups( jobFamily ) {

        if( jobFamily != '' && jobFamily != 'undefined') {

            let query1 = `SELECT TRUE;
                        SELECT dg2.department_group2_id, dg2.department_group2
                FROM department_group3 dg3 
                INNER JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                WHERE dg3.department_group2_id IN (`+jobFamily+`);`;

            let query2 = `SELECT id, department FROM proll_department
            WHERE department_group3_id IN (`+ jobFamily +`)`;

            let query = query1 + query2;

            return this.executeQuery( query );

        }
        else {

            let query = ` SELECT FALSE;
            SELECT dg2.department_group2_id, dg2.department_group2 FROM department_group2 dg2;
            SELECT id, department FROM proll_department`;
            
            return this.executeQuery( query );

        }

    }

    getSelectedDepartmentsGroups( departmentId ) {

        if( departmentId == '' || departmentId == 'undefined' || departmentId == null) {

            let query = `SELECT FALSE;
                SELECT dg2.department_group2_id, dg2.department_group2 FROM department_group2 dg2;
                SELECT dg3.department_group3_id, dg3.department_group3 FROM department_group3 dg3`;

            return this.executeQuery( query );

        }
        else {

            let query = `SELECT TRUE;
            SELECT DISTINCT(dg2.department_group2_id), dg2.department_group2 FROM proll_department pd
            INNER JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
            INNER JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
            WHERE pd.id IN (`+departmentId+`);
            SELECT DISTINCT(dg3.department_group3_id), dg3.department_group3 FROM proll_department pd
            INNER JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
            WHERE pd.id IN (`+departmentId+`)`;

            return this.executeQuery( query );

        }

    }

    getBranches() {
    
        var query = "SELECT loc_id, loc_desc FROM proll_client_location";

        return this.executeQuery( query );
    
     }
    
    getDepartment () {
    
        var query = "SELECT id, department FROM proll_department";

        return this.executeQuery( query );
    
     }
    
    getDesignation () {
    
        var query = "SELECT designation_id, designation_name FROM proll_client_designation";

        return this.executeQuery( query );
    
     }
    
    getTotalBands () { 
    
        var query = "SELECT eb.id, eb.band_desc FROM employee_bands eb ORDER BY eb.band_desc, eb.id ASC";

        return this.executeQuery( query );
    
    }

    executeQuery( query ) {

        return new Promise( (resolve, reject) => {
    
            db.query(query, (result, err) => {
    
                if (!err) { 
                            resolve(JSON.parse(JSON.stringify(result)));
                        }
                else {
                        console.log(err);
                        reject(err);
                    }
        
            });
    
        });

    }

}

exports.OrgChartFilter = OrgChartFilter;