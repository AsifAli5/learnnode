const Database = require("../../../config/database");
const db = new Database();

const mongoose = require('mongoose');

class Menus {
  constructor() {}

  getMainMenus(id, groupPortalName) {
    let groupName = '';
    if (groupPortalName == 'em') {
      groupName = 'employee'
    } else {
      groupName = groupPortalName;
    }
    let query = "SELECT m.*, grm.status, grm.description, grm.component_name, grm.file_name FROM group_role_menu grm INNER JOIN menu m ON grm.menus_id = m.id WHERE grm.group_role_id = (SELECT id FROM group_roles WHERE id = (SELECT group_role_id FROM user_roles WHERE user_id = " +
      id +
      " AND group_role_id = (SELECT gr.id FROM group_roles gr WHERE gr.link LIKE '" + groupPortalName + "' OR gr.group_name LIKE '" + groupName + "' ))) ORDER BY m.parent_id, m.menu_order ASC;";

    return this.executeQuery(query);
  }

  getPortals(id) {
    return this.executeQuery(
      "SELECT r.*, gr.link, ur.status, gr.group_name FROM roles r INNER JOIN user_roles ur ON ur.group_role_id = r.id INNER JOIN  group_roles gr ON r.id = gr.roles_portal_id WHERE ur.user_id = " +
      id +
      ";"
    );
  }

  getMainSubMenus(pageName) {
    // return this.executeQuery("SELECT m.*, grm.status, grm.description, grm.component_name, grm.file_name FROM menu m INNER JOIN group_role_menu grm ON grm.menus_id = m.id  WHERE m.parent_id= (SELECT menus_id FROM group_role_menu WHERE component_name = '" + pageName + "') GROUP BY m.menu_title ORDER BY m.parent_id, m.menu_order ASC;")
    return this.executeQuery("SELECT m.*, grm.status, grm.description, grm.component_name, grm.file_name FROM menu m INNER JOIN group_role_menu grm ON grm.menus_id = m.id WHERE m.parent_id= ( SELECT menus_id FROM group_role_menu WHERE component_name = '" + pageName + "') AND grm.group_role_id = ( SELECT group_role_id FROM group_role_menu WHERE component_name = '" + pageName + "') GROUP BY m.menu_title ORDER BY m.parent_id, m.menu_order ASC;")
  }

  executeQuery(query) {
    return new Promise((resolve, reject) => {
      db.query(query, (result, error) => {
        if (error) return reject(error);
        return resolve(result);
      });
    });
  }
}

// ================ MongoDB
// Menus Cache Schema as per User
const menusCacheSchema = mongoose.Schema({
  e_number: Number,
  link: String,

  levelOne: Number,
  levelTwo: Number,
  levelThree: Number,
});

// Menus Schema for all portals, this will be static and will be updated when any menu portal is updated
const menus = mongoose.Schema({
  id: Number,
  menu_title: String,
  parent_id: Number,
  menu_order: Number,
  menu_type_id: Number,
  level: Number,
  client_id: Number,
  created_by: Number,
  created_at: Date,
  updated_at: String,
  icon: String,
  img: String,
  page_menu_title: String,
  status: Number,
  description: String,
  component_name: String,
  file_name: String,
});

const menusSchema = mongoose.Schema({
  strict: false
});

const MenusCacheSchema = mongoose.model('menuscaches', menusCacheSchema);
const MenusSchema = mongoose.model('menus', menusSchema);

module.exports = {
  MenusCacheSchema,
  MenusSchema,
  Menus
}
