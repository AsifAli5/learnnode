const config = require("../../../../config");
const queryObj = require("../../3.0/helper/query");

class Payroll{
    // logArray = [];
    dbConnection = '';
   
    constructor(conn) {
        this.dbConnection = conn;
    }

    async getLwopByDateRange(startDate,endDate) {
        let LwopQuery = `SELECT e.empcode,e.name,s.department,s.designation,s.lwop_days,
                                DATE_FORMAT(s.payroll_date,'%Y-%m-%d') AS payroll_date,g.lwop_deduction 
                            FROM cb_salary_details s
                            INNER JOIN proll_employee e ON e.empcode=s.emp_no
                            INNER JOIN cb_before_gross_salary g ON g.salary_details_id =s.id  
                            WHERE s.payroll_date BETWEEN '${startDate}' AND '${endDate}' AND g.lwop_deduction !=0 
                            ORDER BY s.payroll_date desc`;
                       
        return await queryObj.selectQuery(LwopQuery, this.dbConnection);
    
     }
     async getJoinedStaffByDateRange(startDate,endDate) {
        let joinedStaffQuery = `SELECT e.empcode,e.name,s.city,s.designation,s.department,d.line_manager,e.doj,s.gross_monthly_salary,s.net_pay, DATE_FORMAT(s.payroll_date,'%Y-%m-%d') AS payroll_date
                            FROM cb_salary_details s
                            INNER JOIN proll_employee e ON e.empcode=s.emp_no
                            INNER JOIN proll_department d ON (d.id=e.reporting_to_id OR d.id=e.dept_id)  
                            WHERE s.payroll_date BETWEEN '${startDate}' AND '${endDate}'
                            ORDER BY s.payroll_date desc`;
                       
        return await queryObj.selectQuery(joinedStaffQuery, this.dbConnection);
    
     }

     async getleaverStaffByDateRange(startDate,endDate) {
        let leaversQuery = `SELECT e.empcode,e.name,rf.description AS status,e.dor,ed.lastworkingdate,s.net_pay, DATE_FORMAT(s.payroll_date,'%Y-%m-%d') AS payroll_date
                            FROM cb_salary_details s
                            INNER JOIN proll_employee e ON e.empcode=s.emp_no
                            INNER JOIN proll_employee_detail ed ON ed.empid=e.id
                            INNER JOIN proll_department d ON (d.id=e.reporting_to_id OR d.id=e.dept_id)
                            INNER JOIN proll_reference_data rf ON rf.reference_key=e.job_status AND rf.ref_id=36
                            WHERE s.payroll_date BETWEEN '${startDate}' AND '${endDate}' and e.status=0
                            ORDER BY s.payroll_date desc`;
                       
        return await queryObj.selectQuery(leaversQuery, this.dbConnection);
    
     }

     async getLoanDetailsByDateRange(startDate,endDate) {
        let loanQuery = `SELECT e.empcode,e.name,lr.loan_req_id,lr.payback_tenure AS total_installments,lr.disbursement_on, DATE_ADD(lr.disbursement_on, INTERVAL lr.payback_tenure MONTH) AS tenure_end,
                                SUM(if(li.status=0, 1, 0)) AS due_instalments,d.department,cd.designation_name,
                                SUM(if(li.status=1, 1, 0)) AS paid_instalments,
                                SUM(if(li.status=1, li.monthly_installment, 0)) AS paid_amount,
                                lr.deduction_from,lr.loan_reason,lr.loan_amount AS sacnctioned_amount,lr.application_date,lr.application_type,lt.loan_type
                                FROM la_requests lr
                                INNER JOIN la_setup ls ON lr.la_setup_id=ls.id
                                INNER JOIN la_types lt ON ls.loan_type=lt.id
                                INNER JOIN proll_employee e ON lr.empid=e.id
                                LEFT JOIN la_installments li ON li.la_req_id=lr.loan_req_id
                                LEFT JOIN proll_department d ON d.id=e.dept_id
                                LEFT JOIN proll_client_designation cd ON cd.designation_id=e.designation
                                WHERE lr.application_status=2
                                GROUP BY lr.loan_req_id
                                ORDER BY lr.loan_req_id DESC
                 `;
                       
        return await queryObj.selectQuery(loanQuery, this.dbConnection);
    
     }

     async getExpenseDetailsByDateRange(startDate,endDate) {
      let expenseQuery = `SELECT emp.id,emp.loginname,emp.empcode,d.department,cd.designation_name,et.type,SUM(ed.amount) AS expense_amount
                        FROM proll_expense e
                        LEFT JOIN proll_expense_detail ed ON ed.exp_id=e.id
                        LEFT JOIN proll_expense_type et ON ed.type_id=et.id
                        LEFT JOIN proll_employee emp ON emp.id=e.eid
                        LEFT JOIN proll_department d ON d.id=emp.dept_id
                        LEFT JOIN proll_client_designation cd ON cd.designation_id=emp.designation
                        WHERE e.hr_status=2 AND e.added_on BETWEEN '${startDate}' AND  '${endDate}' 
                        GROUP BY emp.id
      
               `;
                     
      return await queryObj.selectQuery(expenseQuery, this.dbConnection);
  
   }
}

exports.Payroll = Payroll;