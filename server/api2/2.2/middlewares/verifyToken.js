const jwt = require("jsonwebtoken");

module.exports = {

    //verify token middleware

    checkToken: function ( req, res, next ) {
        //auth header value
    
        const bearerHeader = req.headers['auth'];
    
        //Check if bearer is undefined
    
        if( typeof bearerHeader !== 'undefined') {
    
            const bearer = bearerHeader.split(" ");
    
            const bearerToken = bearer[1];
    
            req.token = bearerToken;
    
            next();
    
        }
        else {
            //forbidden access
    
            res.sendStatus(403);
        }
    },

    verifyToken: function(req, res, next) {

        jwt.verify(req.token, 'secretkey', (err, authData) => {

            if( err ) {
                res.json({
                    err
                });
            }
            else {
    
                next();
    
            }
    
        });

    }

}
