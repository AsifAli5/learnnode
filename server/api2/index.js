var express = require('express');
var router = express.Router();

/*Declare routers for our project*/
router.use("/2.2", require("./2.2/controller/orgchart"));
router.use("/2.2", require('./2.2/controller/menus'));
router.use("/2.3", require("./2.3/controller/orgchart"));
router.use("/2.3", require('./2.3/controller/menus'));
router.use("/3.0", require("./3.0/controller/orgchart"));
router.use("/3.0", require('./3.0/controller/menus'));

router.use("/2.2/generateEmf", require('./2.2/controller/payroll'));

router.use("/3.0/PayrollDashboard", require('./3.0/controller/payroll/PayrollDashboard'));
router.use("/3.0/EmploymentPolicy", require('./3.0/controller/polices/EmploymentPolicy'));
router.use("/3.0/CompAndBenDashboard", require('./3.0/controller/payroll/CompAndBenDashboard'));
// router.use('/3.0/PayrollSetup/statutory', require('./3.0/controller/payrollSetup/statutorySetup'));
// router.use('/3.0/PayrollSetup/compensation', require('./3.0/controller/payrollSetup/compensationSetup'));
router.use('/3.0/PayrollSetup/filterScreen', require('./3.0/controller/payrollSetup/payrollSetup'));
router.use("/3.0/EmfUpload", require('./3.0/controller/payroll/EmfUpload'));
router.use("/3.0/PayrollValidation", require('./3.0/controller/payroll/PayrollValidation'));
router.use("/3.0/PayrollSummary", require('./3.0/controller/payroll/PayrollSummary'));


//policies routes
router.use("/3.0/EmploymentPolicy", require('./3.0/controller/polices/EmploymentPolicy'));
router.use("/3.0/CompensationPolicy",require('./3.0/controller/polices/CompensationPolicy'));
router.use("/3.0/AttendanceLeavePolicy",require('./3.0/controller/polices/AttendanceLeavePolicy'));
router.use("/3.0/ExpenseTravelPolicy",require('./3.0/controller/polices/ExpenseTravelPolicy'));
router.use("/3.0/AssetPolicy",require('./3.0/controller/polices/AssetPolicy'));
router.use("/3.0/PerformanceAppraisalPolicy",require('./3.0/controller/polices/PerformanceAppraisalPolicy'));
router.use("/3.0/IncentiveBonusPolicy",require('./3.0/controller/polices/IncentiveBonusPolicy'));


module.exports = router;
