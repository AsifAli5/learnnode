const { validate, ValidationError, Joi } = require('express-validation');
module.exports.EmfUploding = {
    body: Joi.object({
        ClintId: Joi.number()
        .required(),
        Date: Joi.string()
        .required(),
        Type: Joi.number()
        .required(),
        Discription: Joi.string()
        .required(),
        IsConfirm: Joi.number().allow(''),
        
    }),
  };
  module.exports.EmfList = {
    body: Joi.object({
        ClintId: Joi.number()
        .required(),
        Date: Joi.string()
        .required(),
        Type: Joi.number()
        .required(),  
    }),
  };
  module.exports.EmfUpdate = {
    body: Joi.object({
      EmfArray: Joi.string()
        .required(),
    }),
  };