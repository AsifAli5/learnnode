const express = require('express');
const route = express.Router();
var Database = require('../../../config/database');
var db = new Database();

function executeQuery(query) {
    try {
        return new Promise((resolve, reject) => {
            db.query(query, async function (error, result, fields) {
                if (error) { console.log(error); reject(error); }
                else {
                    resolve('success');
                }
            });
        });
    }
    catch (error) {
        reject(error);
    }
}
function executeSelectQuery(query, conn) {
    try {
        return new Promise((resolve, reject) => {
            conn.query(query, async function (error, result, fields) {
                if (error) { console.log(error); reject(error); }
                else {
                    result = JSON.parse(JSON.stringify(result));
                    resolve(result);
                }
            });
        });
    }
    catch (error) {
        reject(error);
    }
}
function executeWithPlaceholders(query, params) {
    try {
        return new Promise((resolve, reject) => {
            db.query(query, params, async function (error, result, fields) {
                if (error) { console.log(error); reject(error); }
                else {
                    result = JSON.parse(JSON.stringify(result));
                    resolve(result);
                }
            });
        });
    }
    catch (error) {
        reject(error);
    }
}
function executeInsertQuery(query,conn) {
    try {
        return new Promise((resolve, reject) => {
            conn.query(query, async function (err, result) {
                if (err) {
                    console.log(err);  reject(err);
                }
                else {
                    result = JSON.parse(JSON.stringify(result));
                    resolve(result);
                }
            });
        });
    }
    catch (err) {
        throw (err);
    }
}

function selectQuery(query, connection) {
    try {
        return new Promise((resolve, reject) => {
            connection.query(query, async function (error, result, fields) {
                if (error) {
                    console.log(error);
                    reject(error);
                } else {
                    result = JSON.parse(JSON.stringify(result));
                    resolve(result);
                }
            });
        });
    } catch (error) {
        reject(error);
    }
}

module.exports.executeQuery = executeQuery;
module.exports.executeSelectQuery = executeSelectQuery;
module.exports.executeWithPlaceholders = executeWithPlaceholders;
module.exports.executeInsertQuery = executeInsertQuery;
module.exports.selectQuery = selectQuery;
