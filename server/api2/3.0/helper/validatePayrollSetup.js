/**
 * @Author: ismail Raza
 * @Date: 04-03-2021
 */

const {Joi} = require('express-validation');

module.exports.updateSetupValidation = {
    body: Joi.object({
        payroll_setup: Joi.object({setupScreen: Joi.string().required(), setupCategory: Joi.string().required()}),
        parent_table: Joi.object({
            id: Joi.number(),
            payrollSetupId: Joi.number().required(),
            region: Joi.number(),
            province: Joi.number(),
            employeeType: Joi.number(),
            lowerLimit: Joi.number(),
            maxLimit: Joi.number(),
            maxCount: Joi.number(),
            rate: Joi.number(),
            frequency: Joi.string(),
            ssMaxAmount: Joi.number(),
            status: Joi.number(),
            contributor: Joi.number(),
            year: Joi.number().required(),
            updatedBy: Joi.number(),
            createdBy: Joi.number()
        }),
        child_table: Joi.array().items(Joi.object({
            id: Joi.number(),
            setupId: Joi.number(),
            province: Joi.number(),
            allowanceType: Joi.number(),
            allowanceId: Joi.number(),
            interestBased: Joi.number(),
            interestRate: Joi.number(),
            department: Joi.number(),
            minimumLimit: Joi.number(),
            maximumLimit: Joi.number(),
            lowerLimit: Joi.number(),
            upperLimit: Joi.number(),
            percentageRate: Joi.number(),
            maxSS: Joi.number(),
            value: Joi.number(),
            status: Joi.number(),
            calculationMethodDays: Joi.number(),
            eligibilityCriteriaMonths: Joi.number(),
            band: Joi.number(),
            taxable: Joi.number(),
            nonAverageTax: Joi.number(),
            excludeForSS: Joi.number(),
            feature: Joi.number(),
            deleted: Joi.number(),
            employeeType: Joi.number(),
            editable: Joi.number(),
            updatedBy: Joi.number().required(),
            createdBy: Joi.number(),
            contributor: Joi.number(),
            contributor_name: Joi.string(),
            startSlabRange: Joi.number(),
            rate: Joi.number(),
            maxCount: Joi.number(),
            endSlabRange: Joi.number(),
            fixedAmount: Joi.number(),
            taxRate: Joi.number(),
            gender: Joi.string(),
            ageLimit: Joi.number(),
            calculationType: Joi.number(),
            constantRate: Joi.number(),
            eligibilityCriteria: Joi.string(),
            taxPayerStatus: Joi.number(),
            lwop: Joi.number(),
            createdDate: Joi.date(),
            frequency: Joi.string(),
            hrsPerMonth: Joi.number(),
            otRate: Joi.number(),
        }))
    })
}

module.exports.searchSetupValidation = {
    body: Joi.object({
        parent: Joi.boolean(),
        payroll_setup: Joi.object({setupScreen: Joi.string().required(), setupCategory: Joi.string().required()}),
    })
}