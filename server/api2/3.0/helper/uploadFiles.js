
const fs = require("fs-extra");
var path = require('path');
var appDir = path.dirname(require.main.filename);

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}
async function uploadFile(files, folder) {
    var assetNames = [];
     //getting time & date
     var today = new Date();
     var date = today.getFullYear() + '_' + (today.getMonth() + 1) + '_' + today.getDate();
     var time = today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds();
     var dateTime = date + '_' + time;
    try {
     
        if (  files.length > 0) {
            //for multi file upload. 
            await asyncForEach(files, async (file, index) => {
              

                let fileType = file.mimetype.split("/")[1];
                fileType = fileType == "octet-stream" ? "csv" : fileType;

                let newName =
                    "EMF" +
                    "_" + date +
                    "_"+Math.floor(Math.random() * 1000000000) +
                    "." + fileType;
                let imageFile = `${appDir}/server/api2/3.0/uploads/${folder}/` + newName;

                fs.writeFileSync(imageFile, file.data);
                assetNames.push({"name":newName,"created":file.name});

            });
        } else  {
            //fore single file upload
            let fileType = files.mimetype.split("/")[1];
            fileType = fileType == "octet-stream" ? "csv" : fileType;

            let newName =
            "EMF" +
                    "_" + date +
                    "_"+Math.floor(Math.random() * 1000000000) +
                    "." + fileType;

            let imageFile = `${appDir}/server/api2/3.0/uploads/${folder}/` + newName;


            fs.writeFileSync(imageFile, files.data);
            assetNames.push({"name":newName,"created":files.name});

        }
        return { success: true, assetNames: assetNames, message: "Successfully saved images!" };

    } catch (error) {
        console.log(error);
        return { success: false, assetNames: assetNames, message: error.message };
        // return res.status(200).json({ message: "error" });
    }
}
module.exports.uploadFile = uploadFile;


