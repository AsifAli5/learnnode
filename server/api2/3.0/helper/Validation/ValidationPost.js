const { validate, ValidationError, Joi } = require('express-validation');
/////////////// EMF Validation //////////////////////
module.exports.EmfUploding = {
    body: Joi.object({
        Date: Joi.string()
        .required(),
        Type: Joi.number()
        .required(),
        Discription: Joi.string()
        .required(),
        IsConfirm: Joi.number().allow(''),
        
    }),
  };
  module.exports.EmfList = {
    body: Joi.object({
        Date: Joi.string()
        .required(),
        Type: Joi.number()
        .required(),  
    }),
  };
  module.exports.EmfUpdate = {
    body: Joi.object({
      EmfArray: Joi.string()
        .required(),
    }),
  };
  module.exports.EmfReverttoOrigin = {
    body: Joi.object({
      Date: Joi.string()
        .required(),
      Type: Joi.number()
        .required(),    
    }),
  };
////////////// payroll validation ////////////////// 
  module.exports.payrollList = {
    body: Joi.object({
        Date: Joi.string()
        .required(),
        Type: Joi.number()
        .required(),  
    }),
  };
  module.exports.PayrollUpdate = {
    body: Joi.object({
      PayrollArray: Joi.string()
        .required(),
    }),
  };
//////////// Payroll Summary /////////////////////
module.exports.parollsummary = {
  body: Joi.object({
      InfoId: Joi.number()
      .required(),
  }),
};   