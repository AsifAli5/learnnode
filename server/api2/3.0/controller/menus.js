const express = require("express");
const config = require('../../../../config');
const router = express.Router();
const {
  Menus,
  MenusCacheSchema,
  MenusSchema
} = require("../model/menus");

const PHPurlHead = config.site.SITE_URL + config.site.PPM_URL;

const menu = new Menus();

router.get("/getMenus/:portalID/:userID", (req, res) => {
  const portalID = req.params.portalID;
  const userID = req.params.userID;
  const conn = req.conn;
  const pool = req.pool;
  menu
    .getMainMenus(userID, portalID,conn,pool)
    .then((result) => {
      if (result.length > 0) {
        result = result.filter((menu) => {
          if (menu.file_name)
            return (menu.file_name = PHPurlHead + menu.file_name);
          return menu;
        });
        return res.send(result);

        // const levelOne = result.filter((menu) => {
        //   return menu.level === 1;
        // });
        // const levelTWO = result.filter((menu) => {
        //   return menu.level === 2;
        // });

        // const levelThree = result.filter((menu) => {
        //   return menu.level === 3;
        // });

        // levelOne.filter((firstLevel) => {
        //   firstLevel.subMenu = [];
        //   levelTWO.filter((secondLevel) => {
        //     secondLevel.subMenu = [];
        //     if (firstLevel.id === secondLevel.parent_id) {
        //       firstLevel.subMenu.push(secondLevel);
        //     } else {
        //       levelThree.filter((thirdLevel) => {
        //         thirdLevel.subMenu = [];
        //         if (secondLevel.id === thirdLevel.parent_id) {
        //           secondLevel.subMenu.push(thirdLevel);
        //         }
        //       });
        //     }
        //   });
        // });
        // return res.send(levelOne);
      } else {
        return res.status(401).send({
          error: "no menus"
        });
      }
    })
    .catch((error) => {
      return res.status(401).send(error);
    });
});

router.get("/getLandingSubMenus/:pageName", (req, res) => {
  const pageName = req.params.pageName;
  const conn = req.conn;
  const pool = req.pool;
  menu
    .getMainSubMenus(pageName,conn,pool)
    .then((subMenu) => {
      return res.send(subMenu);
    }).catch((error) => {
      return res.status(401).send(error);
    });
});

router.get("/getSubMenus/:pageName", (req, res) => {
  const pageName = req.params.pageName;
  const conn = req.conn;
  const pool = req.pool;
  menu
    .getMainSubMenus(pageName,conn,pool)
    .then((subMenu) => {
      const menus = subMenu.filter(map => map.level != 4);
      return res.send(menus);
    }).catch((error) => {
      return res.status(401).send(error);
    });
});

router.get("/getPortals/:userID", (req, res) => {
  const userID = req.params.userID;
  const conn = req.conn;
  const pool = req.pool;
  menu
    .getPortals(userID,conn,pool)
    .then((portals) => {
      const portalsList = [];

      for (const portal of portals) {
        portal.phpurl = PHPurlHead + portal.link + '.php';

        if (portal.link === 'ceoportal' || portal.link === 'crportal')
          portal.phpurl = PHPurlHead + 'hrportal.php';

        portalsList.push(portal);
      }

      return res.send(portalsList);
    })
    .catch((error) => {
      return res.status(401).send(error);
    });
});

// ============================ MongoDB 
router.get('/getMenusCache/:uid', (req, res) => {
  const uid = req.params.uid;

  MenusCacheSchema.find({
    userid: +uid
  }, (err, docs) => {
    if (!docs) {
      return res.status(400).send(err);
    } else {
      return res.send(docs);
    }
  });
});

router.post('/creatMenusCache', (req, res) => {
  const body = req.body;

  MenusCacheSchema.create(body)
    .then(resp => {
      return res.send(resp);
    }).catch((error) => {
      return res.status(400).send(error);
    });
});

router.post('/updateMenusCache', (req, res) => {
  const body = req.body;

  MenusCacheSchema.findOneAndUpdate({
      userid: body.uid
    }, body.updateObject)
    .then(resp => {
      return res.send(resp);
    }).catch((error) => {
      return res.status(400).send(error);
    });
});

router.get("/creatMenus/:portalID/:userID", (req, res) => {
  const portalID = req.params.portalID;
  const userID = req.params.userID;
  menu
    .getMainMenus(userID, portalID)
    .then((result) => {
      if (result.length > 0) {

        const levelOne = result.filter((menu) => {
          return menu.level === 1;
        });
        const levelTWO = result.filter((menu) => {
          return menu.level === 2;
        });

        const levelThree = result.filter((menu) => {
          return menu.level === 3;
        });

        levelOne.filter((firstLevel) => {
          firstLevel.subMenu = [];
          levelTWO.filter((secondLevel) => {
            secondLevel.subMenu = [];
            if (firstLevel.id === secondLevel.parent_id) {
              firstLevel.subMenu.push(secondLevel);
            } else {
              levelThree.filter((thirdLevel) => {
                thirdLevel.subMenu = [];
                if (secondLevel.id === thirdLevel.parent_id) {
                  secondLevel.subMenu.push(thirdLevel);
                }
              });
            }
          });
        });
        // const menus = {
        //   employee: levelOne
        // };
        // MenusSchema.create(...menus, (err, result) => {
        //   if (err) {
        //     return res.status(400).send(err);
        //   } else {
        //     return res.send(result);
        //   }
        // })

        return res.send(levelOne);
      } else {
        return res.status(401).send({
          error: "no menus"
        });
      }
    })
    .catch((error) => {
      return res.status(401).send(error);
    });
});

module.exports = router;
