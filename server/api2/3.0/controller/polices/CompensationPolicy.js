var express = require("express");
var router = express.Router();
var CompensationPolicyData = require("../../model/polices/CompensationPolicyData")
  .CompensationPolicyData;

//to get compensation policy
router.get("/getCompensationPolicy", async (req, res) => {
  try {
    let compensationPolicy = new CompensationPolicyData();

    let policy_id = req.body.policy_id;
    let conn = req.conn;
    let pool = req.pool;

    let result = await compensationPolicy.getCompensationPolicy(
      policy_id,
      conn,
      pool
    );
    let monthly_salary_breakup = [];
    let job_bands_salary_ranges = [];
    let unpaid_leave_calculation = [];
    let monthly_cutoff_time = [];
    let overtime_calculation_criteria = [];
    let terminal_benefits = [];
    let gl_dis_insurance = [];
    let gl_health_insurance = [];
    let other_benefits = [];

    //sectionize data based on their section id
    for (let i = 0; i < result.length; i++) {
      if (result[i]["policy_section_id"] === 12) {
        monthly_salary_breakup.push(result[i]);
      } else if (result[i]["policy_section_id"] === 13) {
        job_bands_salary_ranges.push(result[i]);
      } else if (result[i]["policy_section_id"] === 14) {
        unpaid_leave_calculation.push(result[i]);
      } else if (result[i]["policy_section_id"] === 15) {
        monthly_cutoff_time.push(result[i]);
      } else if (result[i]["policy_section_id"] === 16) {
        overtime_calculation_criteria.push(result[i]);
      } else if (result[i]["policy_section_id"] === 17) {
        terminal_benefits.push(result[i]);
      } else if (result[i]["policy_section_id"] === 18) {
        gl_dis_insurance.push(result[i]);
      } else if (result[i]["policy_section_id"] === 19) {
        gl_health_insurance.push(result[i]);
      } else if (result[i]["policy_section_id"] === 20) {
        other_benefits.push(result[i]);
      }
    }

    res.status(200).json({
      status: true,
      statusCode: 200,
      message: "success",
      payload: {
        monthly_salary_breakup,
        job_bands_salary_ranges,
        unpaid_leave_calculation,
        monthly_cutoff_time,
        overtime_calculation_criteria,
        terminal_benefits,
        gl_dis_insurance,
        gl_health_insurance,
        other_benefits,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "Internal failure",
      payload: null,
    });
  }
});

//to post compensation policy
router.post("/postComensationPolicy", async (req, res) => {
  try {
    //console.log(req.body);
    let compensationPolicy = new CompensationPolicyData();

    //parse request body
    let req_monthly_salary_breakup = req.body.monthly_salary_breakup;
    let req_job_bands_salary_ranges = req.body.job_bands_salary_ranges;
    let req_unpaid_leave_calculation = req.body.unpaid_leave_calculation;
    let req_monthly_cutoff_time = req.body.monthly_cutoff_time;
    let req_overtime_calculation_criteria =
      req.body.overtime_calculation_criteria;
    let req_terminal_benefits = req.body.terminal_benefits;
    let req_gl_dis_insurance = req.body.gl_dis_insurance;
    let req_gl_health_insurance = req.body.gl_health_insurance;
    let req_other_benefits = req.body.other_benefits;

    let conn = req.conn;
    let pool = req.pool;

    //console.table(req_monthly_salary_breakup);
    // console.table(req_job_bands_salary_ranges);
    //  console.table(req_gl_dis_insurance);
    //  console.table(req_gl_health_insurance);

    let monthly_salary_breakup_result = await compensationPolicy.postMonthlySalaryBreakup(
      req_monthly_salary_breakup,
      conn,
      pool
    );
    // console.log(JSON.stringify(staff_result));

    if (monthly_salary_breakup_result.affectedRowsCount > 0) {
      //For updating job bands and salary ranges details
      let job_bands_salary_ranges_result = await compensationPolicy.postJobBandsSalaryRanges(
        req_job_bands_salary_ranges,
        conn,
        pool
      );

      if (job_bands_salary_ranges_result.affectedRowsCount > 0) {
        //for updating  basis unpaid leaves calculation
        let unpaid_leave_calculation_result = await compensationPolicy.postUnpaidLeaveCalculation(
          req_unpaid_leave_calculation,
          conn,
          pool
        );

        if (unpaid_leave_calculation_result.affectedRowsCount > 0) {
          //for updating monthly cut off time
          let monthly_cutoff_time_result = await compensationPolicy.postMonthlyCutoffTime(
            req_monthly_cutoff_time,
            conn,
            pool
          );

          if (monthly_cutoff_time_result.affectedRowsCount > 0) {
            //for updating overtime calcution criteria
            let overtime_calculation_criteria_result = await compensationPolicy.postOvertimeCalculationCriteria(
              req_overtime_calculation_criteria,
              conn,
              pool
            );
            if (overtime_calculation_criteria_result.affectedRowsCount > 0) {
              //for updating terminal benefits

              let terminal_benefits_result = await compensationPolicy.postTerminalBenefits(
                req_terminal_benefits,
                conn,
                pool
              );
              if (terminal_benefits_result.affectedRowsCount > 0) {
                //for updating group life and disability insurance

                let gl_dis_insurance_result = await compensationPolicy.postGroupLifeDisInsurnace(
                  req_gl_dis_insurance,
                  conn,
                  pool
                );
                if (gl_dis_insurance_result.affectedRowsCount > 0) {
                  //for updating group life health insurance
                  let gl_health_insurance_result = await compensationPolicy.postGroupLifeHealthInsurance(
                    req_gl_health_insurance,
                    conn,
                    pool
                  );
                  if (gl_health_insurance_result.affectedRowsCount > 0) {
                    //for updating other benefits
                    let other_benefits_result = await compensationPolicy.postOtherBenefits(
                      req_other_benefits,
                      conn,
                      pool
                    );
                    if (other_benefits_result.affectedRowsCount > 0) {
                      res.status(200).json({
                        status: true,
                        statusCode: 200,
                        message: "success",
                        payload: {
                          monthly_salary_breakup_result,
                          job_bands_salary_ranges_result,
                          unpaid_leave_calculation_result,
                          monthly_cutoff_time_result,
                          overtime_calculation_criteria_result,
                          terminal_benefits_result,
                          gl_dis_insurance_result,
                          gl_health_insurance_result,
                          other_benefits_result,
                        },
                      });
                    } else {
                      console.log(
                        `Error occured at other benefits section: ${other_benefits_result}`
                      );
                      res.status(400).json({
                        status: false,
                        statusCode: 400,
                        message: "Bad Request",
                        payload: null,
                      });
                    }
                  } else {
                    console.log(
                      `Error occured at group life health insurance section: ${gl_health_insurance_result}`
                    );
                    res.status(400).json({
                      status: false,
                      statusCode: 400,
                      message: "Bad Request",
                      payload: null,
                    });
                  }
                } else {
                  console.log(
                    `Error occured at group life and disability insurance section: ${gl_dis_insurance_result}`
                  );
                  res.status(400).json({
                    status: false,
                    statusCode: 400,
                    message: "Bad Request",
                    payload: null,
                  });
                }
              } else {
                console.log(
                  `Error occured at monthly cut off time section: ${terminal_benefits_result}`
                );
                res.status(400).json({
                  status: false,
                  statusCode: 400,
                  message: "Bad Request",
                  payload: null,
                });
              }
            } else {
              console.log(
                `Error occured at monthly cut off time section: ${overtime_calculation_criteria_result}`
              );
              res.status(400).json({
                status: false,
                statusCode: 400,
                message: "Bad Request",
                payload: null,
              });
            }
          } else {
            console.log(
              `Error occured at monthly cut off time section: ${monthly_cutoff_time_result}`
            );
            res.status(400).json({
              status: false,
              statusCode: 400,
              message: "Bad Request",
              payload: null,
            });
          }
        } else {
          console.log(
            `Error occured at basis unpaid leaves calculation: ${unpaid_leave_calculation_result}`
          );
          res.status(400).json({
            status: false,
            statusCode: 400,
            message: "Bad Request",
            payload: null,
          });
        }
      } else {
        console.log(
          `Error occured at job bands and salary ranges section: ${job_bands_salary_ranges_result}`
        );
        res.status(400).json({
          status: false,
          statusCode: 400,
          message: "Bad Request",
          payload: null,
        });
      }
    } else {
      console.log(
        `Error occured at monthly salary breakup section: ${monthly_salary_breakup_result}`
      );
      res.status(400).json({
        status: false,
        statusCode: 400,
        message: "Bad Request",
        payload: null,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "internal failure",
      payload: null,
    });
  }
});

//to update compensation policy
router.put("/updateCompensationPolicy", async (req, res) => {
  try {
    //console.log(req.body);
    let compensationPolicy = new CompensationPolicyData();

    //parse request body
    let req_monthly_salary_breakup = req.body.monthly_salary_breakup;
    let req_job_bands_salary_ranges = req.body.job_bands_salary_ranges;
    let req_unpaid_leave_calculation = req.body.unpaid_leave_calculation;
    let req_monthly_cutoff_time = req.body.monthly_cutoff_time;
    let req_overtime_calculation_criteria =
      req.body.overtime_calculation_criteria;
    let req_terminal_benefits = req.body.terminal_benefits;
    let req_gl_dis_insurance = req.body.gl_dis_insurance;
    let req_gl_health_insurance = req.body.gl_health_insurance;
    let req_other_benefits = req.body.other_benefits;

    let conn = req.conn;
    let pool = req.pool;

    //console.table(req_monthly_salary_breakup);
    // console.table(req_job_bands_salary_ranges);
    //  console.table(req_gl_dis_insurance);
    //  console.table(req_gl_health_insurance);

    let monthly_salary_breakup_result = await compensationPolicy.updateMonthlySalaryBreakup(
      req_monthly_salary_breakup,
      conn,
      pool
    );
    // console.log(JSON.stringify(staff_result));

    if (monthly_salary_breakup_result.affectedRowsCount > 0) {
      //For updating job bands and salary ranges details
      let job_bands_salary_ranges_result = await compensationPolicy.updateJobBandsSalaryRanges(
        req_job_bands_salary_ranges,
        conn,
        pool
      );

      if (job_bands_salary_ranges_result.affectedRowsCount > 0) {
        //for updating  basis unpaid leaves calculation
        let unpaid_leave_calculation_result = await compensationPolicy.updateUnpaidLeaveCalculation(
          req_unpaid_leave_calculation,
          conn,
          pool
        );

        if (unpaid_leave_calculation_result.affectedRowsCount > 0) {
          //for updating monthly cut off time
          let monthly_cutoff_time_result = await compensationPolicy.updateMonthlyCutoffTime(
            req_monthly_cutoff_time,
            conn,
            pool
          );

          if (monthly_cutoff_time_result.affectedRowsCount > 0) {
            //for updating overtime calcution criteria
            let overtime_calculation_criteria_result = await compensationPolicy.updateOvertimeCalculationCriteria(
              req_overtime_calculation_criteria,
              conn,
              pool
            );
            if (overtime_calculation_criteria_result.affectedRowsCount > 0) {
              //for updating terminal benefits

              let terminal_benefits_result = await compensationPolicy.updateTerminalBenefits(
                req_terminal_benefits,
                conn,
                pool
              );
              if (terminal_benefits_result.affectedRowsCount > 0) {
                //for updating group life and disability insurance

                let gl_dis_insurance_result = await compensationPolicy.updateGroupLifeDisInsurnace(
                  req_gl_dis_insurance,
                  conn,
                  pool
                );
                if (gl_dis_insurance_result.affectedRowsCount > 0) {
                  //for updating group life health insurance
                  let gl_health_insurance_result = await compensationPolicy.updateGroupLifeHealthInsurance(
                    req_gl_health_insurance,
                    conn,
                    pool
                  );
                  if (gl_health_insurance_result.affectedRowsCount > 0) {
                    //for updating other benefits
                    let other_benefits_result = await compensationPolicy.updateOtherBenefits(
                      req_other_benefits,
                      conn,
                      pool
                    );
                    if (other_benefits_result.affectedRowsCount > 0) {
                      res.status(200).json({
                        status: true,
                        statusCode: 200,
                        message: "success",
                        payload: {
                          monthly_salary_breakup_result,
                          job_bands_salary_ranges_result,
                          unpaid_leave_calculation_result,
                          monthly_cutoff_time_result,
                          overtime_calculation_criteria_result,
                          terminal_benefits_result,
                          gl_dis_insurance_result,
                          gl_health_insurance_result,
                          other_benefits_result,
                        },
                      });
                    } else {
                      console.log(
                        `Error occured at update other benefits section: ${other_benefits_result}`
                      );
                      res.status(400).json({
                        status: false,
                        statusCode: 400,
                        message: "Bad Request",
                        payload: null,
                      });
                    }
                  } else {
                    console.log(
                      `Error occured at update group life health insurance section: ${gl_health_insurance_result}`
                    );
                    res.status(400).json({
                      status: false,
                      statusCode: 400,
                      message: "Bad Request",
                      payload: null,
                    });
                  }
                } else {
                  console.log(
                    `Error occured at update group life and disability insurance section: ${gl_dis_insurance_result}`
                  );
                  res.status(400).json({
                    status: false,
                    statusCode: 400,
                    message: "Bad Request",
                    payload: null,
                  });
                }
              } else {
                console.log(
                  `Error occured at update monthly cut off time section: ${terminal_benefits_result}`
                );
                res.status(400).json({
                  status: false,
                  statusCode: 400,
                  message: "Bad Request",
                  payload: null,
                });
              }
            } else {
              console.log(
                `Error occured at update monthly cut off time section: ${overtime_calculation_criteria_result}`
              );
              res.status(400).json({
                status: false,
                statusCode: 400,
                message: "Bad Request",
                payload: null,
              });
            }
          } else {
            console.log(
              `Error occured at update monthly cut off time section: ${monthly_cutoff_time_result}`
            );
            res.status(400).json({
              status: false,
              statusCode: 400,
              message: "Bad Request",
              payload: null,
            });
          }
        } else {
          console.log(
            `Error occured at update basis unpaid leaves calculation: ${unpaid_leave_calculation_result}`
          );
          res.status(400).json({
            status: false,
            statusCode: 400,
            message: "Bad Request",
            payload: null,
          });
        }
      } else {
        console.log(
          `Error occured at update job bands and salary ranges section: ${job_bands_salary_ranges_result}`
        );
        res.status(400).json({
          status: false,
          statusCode: 400,
          message: "Bad Request",
          payload: null,
        });
      }
    } else {
      console.log(
        `Error occured at update monthly salary breakup section: ${monthly_salary_breakup_result}`
      );
      res.status(400).json({
        status: false,
        statusCode: 400,
        message: "Bad Request",
        payload: null,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "internal failure",
      payload: null,
    });
  }
});

module.exports = router;
