var express = require("express");
var router = express.Router();

var EmploymentPolicy = require("../../model/polices/EmploymentPolicyData")
  .EmploymentPolicyData;

  //post employement policy data
router.post("/postEmploymentPolicy", async (req, res) => {
  //console.log(" post employement policy api got hit");

  try {
    //console.log(req.body);
    let employmentPolicy = new EmploymentPolicy();

    //parse request body
    let req_staff_category = req.body.staff_category;
    let req_approval_auth = req.body.approval_authority;
    let req_probation_period = req.body.probation_period;
    let req_notice_period = req.body.notice_period;
    let req_separation_process = req.body.separation_process;
    let req_resig_documents = req.body.resig_documents;
    
    
    let conn = req.conn;
    let pool = req.pool;

   // console.table(req_resig_documents);
    
    // console.log(req_staff_category);
    // console.log(req_approval_auth);
    let staff_result = await employmentPolicy.postHiringStaff(req_staff_category, conn, pool);
   // console.log(JSON.stringify(staff_result));  


    if(staff_result.affectedRowsCount>0){
     
      //For posting of Hiring Approval Authority
     let approval_result =  await employmentPolicy.postHiringApprovalAuth(req_approval_auth,conn,pool);

     if (approval_result.affectedRowsCount > 0) {
       //for posting probationary period details
       let probation_result = await employmentPolicy.postProbationPeriod(req_probation_period,conn,pool);

       if (probation_result.affectedRowsCount > 0) {
         //for posting notice period details
         let notice_period_result = await employmentPolicy.postNoticePeriod(req_notice_period,conn,pool);

         if (notice_period_result.affectedRowsCount > 0) {


           //for posting separation process for employees
           let separation_result = await employmentPolicy.postSeparationProcess(req_separation_process,conn,pool);
           if (separation_result.affectedRowsCount >0) {
             
            //for posting resignation doc details

            let resig_documents_result =  await employmentPolicy.postResigDocuments(req_resig_documents,conn,pool);
            if (resig_documents_result.affectedRowsCount > 0) {
              
            res.status(200).json({
              status: true, 
              statusCode: 200,
              message: 'success',
              payload: {
                 staff_result,
                approval_result,
                probation_result,
                notice_period_result,
                separation_result,
                resig_documents_result
              }
            });  
              
            } else {
              console.log("error occured at resignation documents section");

              
            }
             
           } else {
             console.log("error occured at separation process section");
             
           }


         } else {
           console.log("error occured at notice Period section")
         }
       } else {
         console.log("error occured at probationary period section");
         
       }

     } else {
       console.log("error happened at approval authority section");
       
     }


    }else{
      res.status(500).json({
        status: false,
        statusCode: 500,
        message: 'internal Error',
        payload: null
      });

    }
  } catch (error) {
    console.log(error);
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: 'internal Error',
      payload: null
    });
  }
});


//select  data of all sections for particular policy
router.get('/getEmploymentPolicy', async(req, res) => {
try {

  let employmentPolicy = new EmploymentPolicy();

  let policy_id = req.body.policy_id;
  let conn = req.conn;
  let pool = req.pool;


  let result = await employmentPolicy.getEmploymentPolicy(policy_id,conn,pool);
  let staff_category = [];
  let approval_authority = [];
  let probation_period = [];
  let notice_period = [];
  let separation_process = [];
  let resig_documents = [];

  //sectionize data based on their section id
  for (let i = 0; i < result.length; i++) {

    if (result[i]["policy_section_id"] === 1) {
      staff_category.push(result[i]);
      
    }else if(result[i]["policy_section_id"] === 2){
      approval_authority.push(result[i]);
    }else if (result[i]["policy_section_id"] === 3){
      probation_period.push(result[i]);
    }else if(result[i]["policy_section_id"] === 4){
      notice_period.push(result[i]);
    } else if(result[i]["policy_section_id"] === 5){
      separation_process.push(result[i]);
    } else if(result[i]["policy_section_id"] === 6){
      resig_documents.push(result[i]);
    }
    
  }

  res.status(200).json({
    status: true,
    statusCode: 200,
    message: "success",
    payload: {
      staff_category,
      approval_authority,
      probation_period,
      notice_period,
      separation_process,
      resig_documents,
    },
  });
 
} catch (error) {
  console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: 'internal Error',
      payload: null
    });

  
} 	
});


//update employment policy data
router.put("/updateEmploymentPolicy", async (req, res) => {
  //console.log("update employement policy api got hit");

  try {
    //console.log(req.body);
    let employmentPolicy = new EmploymentPolicy();

    //parse request body
    let req_staff_category = req.body.staff_category;
    let req_approval_auth = req.body.approval_authority;
    let req_probation_period = req.body.probation_period;
    let req_notice_period = req.body.notice_period;
    let req_separation_process = req.body.separation_process;
    let req_resig_documents = req.body.resig_documents;

    let conn = req.conn;
    let pool = req.pool;

  //  console.table(req_resig_documents);
    
     //console.table(req_staff_category);
    
  //   console.log(req_approval_auth);

    let staff_result = await employmentPolicy.updateHiringStaff(req_staff_category, conn, pool);
   // console.log(JSON.stringify(staff_result));  
 

    if(staff_result.affectedRowsCount>0){
     
      //to update Hiring Approval Authority data
     let approval_result =  await employmentPolicy.updateHiringApprovalAuth(req_approval_auth,conn,pool);
    // console.log(approval_result);
     
     if (approval_result.affectedRowsCount > 0) {
       //to update probationary period details
       let probation_result = await employmentPolicy.updateProbationPeriod(req_probation_period,conn,pool);
       //console.log(probation_result);
       
       if (probation_result.affectedRowsCount > 0) {
         //to update notice period details
         let notice_period_result = await employmentPolicy.updateNoticePeriod(req_notice_period,conn,pool);
         //console.log(notice_period_result);
        
         if (notice_period_result.affectedRowsCount > 0) {

           //to update separation process for employees
           let separation_result = await employmentPolicy.updateSeparationProcess(req_separation_process,conn,pool);
           //console.log(separation_result);

           if (separation_result.affectedRowsCount >0) {
             
            //to update resignation doc details

            let resig_documents_result =  await employmentPolicy.updateResigDocuments(req_resig_documents,conn,pool);
            if (resig_documents_result.affectedRowsCount > 0) {
              res.status(200).json({
                status: true,
                statusCode: 200,
                message: "success",
                payload: {
                  staff_result,
                  approval_result,
                  probation_result,
                  notice_period_result,
                  separation_result,
                  resig_documents_result,
                },
              });
              
            } else {
              console.log("error occured at resignation documents update section");
            }
             
           } else {
             console.log("error occured at separation process update section");
             
           }

         } else {
           console.log("error occured at notice Period update section")
         }
       } else {
         console.log("error occured at probationary period update section");
         
       }

     } else {
       console.log("error happened at approval authority update section");
       
     }


    }else{
      res.status(500).json({
        status: false,
        statusCode: 500,
        message: 'internal Error',
        payload: null
      });

    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: 'internal Error',
      payload: null
    });
  }
});





module.exports = router;





