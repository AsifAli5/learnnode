var express = require('express');
var router = express.Router();
var PerformanceAppraisalPolicyData = require("../../model/polices/PerformanceAppraisalPolicyData").PerformanceAppraisalPolicyData;


//to get performance appraisal policy 
router.get('/getPerformanceAppraisalPolicy', async (req, res) => {
    try {
        // console.table(req.body);
    
        let policy_id = req.body.policy_id;
        let conn = req.conn;
        let pool = req.pool;
    
        let performanceAppraisalPolicy = new PerformanceAppraisalPolicyData();
    
        let result = await performanceAppraisalPolicy.getPerformanceAppraisalPolicy(policy_id, conn, pool);

            let perform_launch_frequency = [];
            let perform_goals_authority = [];
            let perform_apprais_objectives = [];
            let eval_process_involve_emp = [];
            let staff_perform_eval_basis = [];
            let rating_system_and_def = [];
            let apprais_auth_approvals = [];
            let manage_poor_performers = [];
            let emp_perform_cycle_for_apprais = [];
            let probation_eval_policy = [];
    
    
        //sectionize data based on their section id
        for (let i = 0; i < result.length; i++) {
          if (result[i]["policy_section_id"] === 48) {
            perform_launch_frequency.push(result[i]);
          } else if (result[i]["policy_section_id"] === 49) {
            perform_goals_authority.push(result[i]);
          } else if (result[i]["policy_section_id"] === 50) {
            perform_apprais_objectives.push(result[i]);
          } else if (result[i]["policy_section_id"] === 51) {
            eval_process_involve_emp.push(result[i]);
          } else if (result[i]["policy_section_id"] === 52) {
            staff_perform_eval_basis.push(result[i]);
          } else if (result[i]["policy_section_id"] === 53) {
            rating_system_and_def.push(result[i]);
          } else if (result[i]["policy_section_id"] === 54) {
            apprais_auth_approvals.push(result[i]);
          } else if (result[i]["policy_section_id"] === 55) {
            manage_poor_performers.push(result[i]);
          }else if (result[i]["policy_section_id"] === 56) {
            emp_perform_cycle_for_apprais.push(result[i]);
          }else if (result[i]["policy_section_id"] === 57) {
            probation_eval_policy.push(result[i]);
          }
        }
    
        res.status(200).json({
          status: true,
          statusCode: 200,
          message: "success",
          payload: {
            perform_launch_frequency,
            perform_goals_authority,
            perform_apprais_objectives,
            eval_process_involve_emp,
            staff_perform_eval_basis,
            rating_system_and_def,
            apprais_auth_approvals,
            manage_poor_performers,
            emp_perform_cycle_for_apprais,
            probation_eval_policy
           
          },
        });
      } catch (error) {
        console.log(error);
        res.status(500).json({
          status: false,
          statusCode: 500,
          message: "internal failure",
          payload: null,
        });
      }


});

//to post performance appraisal policy
router.post('/postPerformanceAppraisalPolicy', async (req, res) => {
    try {
      //console.table(req.body);

      //parse request body
      let req_perform_launch_frequency = req.body.perform_launch_frequency;
      let req_perform_goals_authority = req.body.perform_goals_authority;
      let req_perform_apprais_objectives = req.body.perform_apprais_objectives;
      let req_eval_process_involve_emp = req.body.eval_process_involve_emp;
      let req_staff_perform_eval_basis = req.body.staff_perform_eval_basis;
      let req_rating_system_and_def = req.body.rating_system_and_def;
      let req_apprais_auth_approvals = req.body.apprais_auth_approvals;
      let req_manage_poor_performers = req.body.manage_poor_performers;
      let req_emp_perform_cycle_for_apprais = req.body.emp_perform_cycle_for_apprais;
      let req_probation_eval_policy = req.body.probation_eval_policy;

      

      let conn = req.conn;
      let pool = req.pool;

        // console.table(req_perform_launch_frequency);
        // console.table(req_perform_goals_authority);
        // console.table(req_perform_apprais_objectives);
        // console.table(req_eval_process_involve_emp);
     
      let performanceAppraisalPolicy= new PerformanceAppraisalPolicyData();

      //For posting performance launch frequency
      let perform_launch_frequency_result = await performanceAppraisalPolicy.postPerformLaunchFrequency(
        req_perform_launch_frequency,
        conn,
        pool
      );

      if (perform_launch_frequency_result.affectedRowsCount > 0) {
        //For posting performance goals authority
        let perform_goals_authority_result = await performanceAppraisalPolicy.postPerformGoalsAuthority(
            req_perform_goals_authority,
          conn,
          pool
        );

        if (perform_goals_authority_result.affectedRowsCount > 0) {
          //for posting performance appraisal objectives
          let perform_apprais_objectives_result = await performanceAppraisalPolicy.postPerformAppraisObjectives(
            req_perform_apprais_objectives,
            conn,
            pool
          );

          if (perform_apprais_objectives_result.affectedRowsCount > 0) {
            //for posting employees involved in evaluation process
            let eval_process_involve_emp_result = await performanceAppraisalPolicy.postEvalProcessInvolveEmp(
                req_eval_process_involve_emp,
              conn,
              pool
            );

            if (eval_process_involve_emp_result.affectedRowsCount > 0) {
              //for posting staff performance evaluation basis
              let staff_perform_eval_basis_result = await performanceAppraisalPolicy.postStaffPerformEvalBasis(
                req_staff_perform_eval_basis,
                conn,
                pool
              );

              if (staff_perform_eval_basis_result.affectedRowsCount > 0) {
                //for posting rating system and its definition
                let rating_system_and_def_result = await performanceAppraisalPolicy.postRatingSystemAndDef(
                    req_rating_system_and_def,
                  conn,
                  pool
                );
                if (rating_system_and_def_result.affectedRowsCount > 0) {
                  //for posting appraisal authorization approvals
                  let apprais_auth_approvals_result = await performanceAppraisalPolicy.postAppraisAuthApprovals(
                    req_apprais_auth_approvals,
                    conn,
                    pool
                  );

                  if (apprais_auth_approvals_result.affectedRowsCount > 0) {
                    //for posting manage poor performers
                    let manage_poor_performers_result = await performanceAppraisalPolicy.postManagePoorPerformers(
                        req_manage_poor_performers,
                      conn,
                      pool
                    );

                    if (
                      manage_poor_performers_result.affectedRowsCount > 0
                    ) {
                      //for posting employee performance cycle for appraisal
                      let emp_perform_cycle_for_apprais_result = await performanceAppraisalPolicy.postEmpPerformCycleForApprais(
                        req_emp_perform_cycle_for_apprais,
                        conn,
                        pool
                      );

                      if (emp_perform_cycle_for_apprais_result.affectedRowsCount > 0) {
                        //for posting probation evaluation policy
                        let probation_eval_policy_result = await performanceAppraisalPolicy.postProbationEvalPolicy(
                            req_probation_eval_policy,
                          conn,
                          pool
                        );

                        if (
                          probation_eval_policy_result.affectedRowsCount > 0
                        ) {
                          res.status(200).json({
                            status: true,
                            statusCode: 200,
                            message: "success",
                            payload: {
                                perform_launch_frequency_result,
                                perform_goals_authority_result,
                                perform_apprais_objectives_result,
                                eval_process_involve_emp_result,
                                staff_perform_eval_basis_result,
                                rating_system_and_def_result,
                                apprais_auth_approvals_result,
                                manage_poor_performers_result,
                                emp_perform_cycle_for_apprais_result,
                                probation_eval_policy_result
                            },
                          });
                        } else {
                          console.log(
                            `error occured at probation eval policy section: ${probation_eval_policy_result}`
                          );

                          res.status(400).json({
                            status: false,
                            statusCode: 400,
                            message: "Bad Request",
                            payload: null,
                          });
                        }
                      } else {
                        console.log(
                          `error occured at emp perform cycle for apprais section: ${emp_perform_cycle_for_apprais_result}`
                        );

                        res.status(400).json({
                          status: false,
                          statusCode: 400,
                          message: "Bad Request",
                          payload: null,
                        });
                      }
                    } else {
                      console.log(
                        `error occured at  manage poor performers section: ${manage_poor_performers_result}`
                      );

                      res.status(400).json({
                        status: false,
                        statusCode: 400,
                        message: "Bad Request",
                        payload: null,
                      });
                    }
                  } else {
                    console.log(
                      `error occured at apprais auth approvals section: ${apprais_auth_approvals_result}`
                    );

                    res.status(400).json({
                      status: false,
                      statusCode: 400,
                      message: "Bad Request",
                      payload: null,
                    });
                  }
                } else {
                  console.log(
                    `error occured at rating system and def section: ${rating_system_and_def_result}`
                  );

                  res.status(400).json({
                    status: false,
                    statusCode: 400,
                    message: "Bad Request",
                    payload: null,
                  });
                }
              } else {
                console.log(
                  `error occured at staff perform eval basis section: ${staff_perform_eval_basis_result}`
                );

                res.status(400).json({
                  status: false,
                  statusCode: 400,
                  message: "Bad Request",
                  payload: null,
                });
              }
            } else {
              console.log(
                `error occured at eval process involve emp section ${eval_process_involve_emp_result}`
              );

              res.status(400).json({
                status: false,
                statusCode: 400,
                message: "Bad Request",
                payload: null,
              });
            }
          } else {
            console.log(
              `error occured at perform apprais objectives section: ${perform_apprais_objectives_result}`
            );

            res.status(400).json({
              status: false,
              statusCode: 400,
              message: "Bad Request",
              payload: null,
            });
          }
        } else {
          console.log(
            `error occured at perform goals authority section: ${perform_goals_authority_result}`
          );

          res.status(400).json({
            status: false,
            statusCode: 400,
            message: "Bad Request",
            payload: null,
          });
        }
      } else {
        res.status(500).json({
          status: false,
          statusCode: 500,
          message: "internal Error",
          payload: null,
        });
      }
    } catch (error) {
        console.log(error);
        res.status(500).json({
          status: false,
          statusCode: 500,
          message: "internal failure",
          payload: null,
        });
      }
});


//to update performance appraisal policy
router.put('/updatePerformanceAppraisalPolicy', async (req, res) => {
    try {
        //console.table(req.body);
  
        //parse request body
        let req_perform_launch_frequency = req.body.perform_launch_frequency;
        let req_perform_goals_authority = req.body.perform_goals_authority;
        let req_perform_apprais_objectives = req.body.perform_apprais_objectives;
        let req_eval_process_involve_emp = req.body.eval_process_involve_emp;
        let req_staff_perform_eval_basis = req.body.staff_perform_eval_basis;
        let req_rating_system_and_def = req.body.rating_system_and_def;
        let req_apprais_auth_approvals = req.body.apprais_auth_approvals;
        let req_manage_poor_performers = req.body.manage_poor_performers;
        let req_emp_perform_cycle_for_apprais = req.body.emp_perform_cycle_for_apprais;
        let req_probation_eval_policy = req.body.probation_eval_policy;
  
        
  
        let conn = req.conn;
        let pool = req.pool;
  
          // console.table(req_perform_launch_frequency);
          // console.table(req_perform_goals_authority);
          // console.table(req_perform_apprais_objectives);
          // console.table(req_eval_process_involve_emp);
       
        let performanceAppraisalPolicy= new PerformanceAppraisalPolicyData();
  
        //For updating performance launch frequency
        let perform_launch_frequency_result = await performanceAppraisalPolicy.updatePerformLaunchFrequency(
          req_perform_launch_frequency,
          conn,
          pool
        );
  
        if (perform_launch_frequency_result.affectedRowsCount > 0) {
          //For updating performance goals authority
          let perform_goals_authority_result = await performanceAppraisalPolicy.updatePerformGoalsAuthority(
              req_perform_goals_authority,
            conn,
            pool
          );
  
          if (perform_goals_authority_result.affectedRowsCount > 0) {
            //for updating performance appraisal objectives
            let perform_apprais_objectives_result = await performanceAppraisalPolicy.updatePerformAppraisObjectives(
              req_perform_apprais_objectives,
              conn,
              pool
            );
  
            if (perform_apprais_objectives_result.affectedRowsCount > 0) {
              //for updating employees involved in evaluation process
              let eval_process_involve_emp_result = await performanceAppraisalPolicy.updateEvalProcessInvolveEmp(
                  req_eval_process_involve_emp,
                conn,
                pool
              );
  
              if (eval_process_involve_emp_result.affectedRowsCount > 0) {
                //for updating staff performance evaluation basis
                let staff_perform_eval_basis_result = await performanceAppraisalPolicy.updateStaffPerformEvalBasis(
                  req_staff_perform_eval_basis,
                  conn,
                  pool
                );
  
                if (staff_perform_eval_basis_result.affectedRowsCount > 0) {
                  //for updating rating system and its definition
                  let rating_system_and_def_result = await performanceAppraisalPolicy.updateRatingSystemAndDef(
                      req_rating_system_and_def,
                    conn,
                    pool
                  );
                  if (rating_system_and_def_result.affectedRowsCount > 0) {
                    //for updating appraisal authorization approvals
                    let apprais_auth_approvals_result = await performanceAppraisalPolicy.updateAppraisAuthApprovals(
                      req_apprais_auth_approvals,
                      conn,
                      pool
                    );
  
                    if (apprais_auth_approvals_result.affectedRowsCount > 0) {
                      //for updating manage poor performers
                      let manage_poor_performers_result = await performanceAppraisalPolicy.updateManagePoorPerformers(
                          req_manage_poor_performers,
                        conn,
                        pool
                      );
  
                      if (
                        manage_poor_performers_result.affectedRowsCount > 0
                      ) {
                        //for updating employee performance cycle for appraisal
                        let emp_perform_cycle_for_apprais_result = await performanceAppraisalPolicy.updateEmpPerformCycleForApprais(
                          req_emp_perform_cycle_for_apprais,
                          conn,
                          pool
                        );
  
                        if (emp_perform_cycle_for_apprais_result.affectedRowsCount > 0) {
                          //for updating probation evaluation policy
                          let probation_eval_policy_result = await performanceAppraisalPolicy.updateProbationEvalPolicy(
                              req_probation_eval_policy,
                            conn,
                            pool
                          );
  
                          if (
                            probation_eval_policy_result.affectedRowsCount > 0
                          ) {
                            res.status(200).json({
                              status: true,
                              statusCode: 200,
                              message: "success",
                              payload: {
                                  perform_launch_frequency_result,
                                  perform_goals_authority_result,
                                  perform_apprais_objectives_result,
                                  eval_process_involve_emp_result,
                                  staff_perform_eval_basis_result,
                                  rating_system_and_def_result,
                                  apprais_auth_approvals_result,
                                  manage_poor_performers_result,
                                  emp_perform_cycle_for_apprais_result,
                                  probation_eval_policy_result
                              },
                            });
                          } else {
                            console.log(
                              `error occured at update probation eval policy section: ${probation_eval_policy_result}`
                            );
  
                            res.status(400).json({
                              status: false,
                              statusCode: 400,
                              message: "Bad Request",
                              payload: null,
                            });
                          }
                        } else {
                          console.log(
                            `error occured at update emp perform cycle for apprais section: ${emp_perform_cycle_for_apprais_result}`
                          );
  
                          res.status(400).json({
                            status: false,
                            statusCode: 400,
                            message: "Bad Request",
                            payload: null,
                          });
                        }
                      } else {
                        console.log(
                          `error occured at update  manage poor performers section: ${manage_poor_performers_result}`
                        );
  
                        res.status(400).json({
                          status: false,
                          statusCode: 400,
                          message: "Bad Request",
                          payload: null,
                        });
                      }
                    } else {
                      console.log(
                        `error occured at update apprais auth approvals section: ${apprais_auth_approvals_result}`
                      );
  
                      res.status(400).json({
                        status: false,
                        statusCode: 400,
                        message: "Bad Request",
                        payload: null,
                      });
                    }
                  } else {
                    console.log(
                      `error occured at update rating system and def section: ${rating_system_and_def_result}`
                    );
  
                    res.status(400).json({
                      status: false,
                      statusCode: 400,
                      message: "Bad Request",
                      payload: null,
                    });
                  }
                } else {
                  console.log(
                    `error occured at update staff perform eval basis section: ${staff_perform_eval_basis_result}`
                  );
  
                  res.status(400).json({
                    status: false,
                    statusCode: 400,
                    message: "Bad Request",
                    payload: null,
                  });
                }
              } else {
                console.log(
                  `error occured at update eval process involve emp section ${eval_process_involve_emp_result}`
                );
  
                res.status(400).json({
                  status: false,
                  statusCode: 400,
                  message: "Bad Request",
                  payload: null,
                });
              }
            } else {
              console.log(
                `error occured at update perform apprais objectives section: ${perform_apprais_objectives_result}`
              );
  
              res.status(400).json({
                status: false,
                statusCode: 400,
                message: "Bad Request",
                payload: null,
              });
            }
          } else {
            console.log(
              `error occured at update perform goals authority section: ${perform_goals_authority_result}`
            );
  
            res.status(400).json({
              status: false,
              statusCode: 400,
              message: "Bad Request",
              payload: null,
            });
          }
        } else {
          res.status(500).json({
            status: false,
            statusCode: 500,
            message: "internal Error",
            payload: null,
          });
        }
      } catch (error) {
          console.log(error);
          res.status(500).json({
            status: false,
            statusCode: 500,
            message: "internal failure",
            payload: null,
          });
        }
});






module.exports = router;