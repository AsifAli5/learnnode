var express = require("express");
var router = express.Router();
var AttendanceLeavePolicyData = require("../../model/polices/AttendanceLeavePolicyData")
  .AttendanceLeavePolicyData;

//to get attendance and leave policy
router.get("/getAttendanceLeavePolicy", async (req, res) => {
  try {
    // console.table(req.body);

    let policy_id = req.body.policy_id;
    let conn = req.conn;
    let pool = req.pool;

    let attendanceLeavePolicy = new AttendanceLeavePolicyData();

    let result = await attendanceLeavePolicy.getAttendanceLeavePolicy(
      policy_id,
      conn,
      pool
    );
    let leave_types = [];
    let leave_auth_approvals = [];
    let leave_encash_claim = [];
    let working_days_office_timings = [];
    let emp_attendance_mark = [];
    let extra_hours_compen = [];
    let working_shifts = [];
    let treat_late_arrivals_reg_emp = [];
    let treat_late_arrivals_ros_emp = [];

    //sectionize data based on their section id
    for (let i = 0; i < result.length; i++) {
      if (result[i]["policy_section_id"] === 21) {
        leave_types.push(result[i]);
      } else if (result[i]["policy_section_id"] === 22) {
        leave_auth_approvals.push(result[i]);
      } else if (result[i]["policy_section_id"] === 23) {
        leave_encash_claim.push(result[i]);
      } else if (result[i]["policy_section_id"] === 24) {
        working_days_office_timings.push(result[i]);
      } else if (result[i]["policy_section_id"] === 25) {
        emp_attendance_mark.push(result[i]);
      } else if (result[i]["policy_section_id"] === 26) {
        extra_hours_compen.push(result[i]);
      }else if (result[i]["policy_section_id"] === 27) {
        working_shifts.push(result[i]);
      }else if (result[i]["policy_section_id"] === 28) {
        treat_late_arrivals_reg_emp.push(result[i]);
      }else if (result[i]["policy_section_id"] === 29) {
        treat_late_arrivals_ros_emp.push(result[i]);
      }
    }

    res.status(200).json({
      status: true,
      statusCode: 200,
      message: "success",
      payload: {
        leave_types,
        leave_auth_approvals,
        leave_encash_claim,
        working_days_office_timings,
        emp_attendance_mark,
        extra_hours_compen,
        working_shifts,
        treat_late_arrivals_reg_emp,
        treat_late_arrivals_ros_emp
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "internal failure",
      payload: null,
    });
  }
});

//to post attendance and leave policy
router.post("/postAttendanceLeavePolicy", async (req, res) => {
  try {
    //cosole.table(req.body);

    //parse request body
    let req_leave_types = req.body.leave_types;
    let req_leave_auth_approvals = req.body.leave_auth_approvals;
    let req_leave_encash_claim = req.body.leave_encash_claim;
    let req_working_days_office_timings = req.body.working_days_office_timings;
    let req_emp_attendance_mark = req.body.emp_attendance_mark;
    let req_extra_hours_compen = req.body.extra_hours_compen;
    let req_working_shifts = req.body.working_shifts;
    let req_treat_late_arrivals_reg_emp = req.body.treat_late_arrivals_reg_emp;
    let req_treat_late_arrivals_ros_emp = req.body.treat_late_arrivals_ros_emp;

    let conn = req.conn;
    let pool = req.pool;

    let attendanceLeavePolicy = new AttendanceLeavePolicyData();

    //console.table(req_leave_types);

    let leave_types_result = await attendanceLeavePolicy.postLeaveTypes(
      req_leave_types,
      conn,
      pool
    );

    if (leave_types_result.affectedRowsCount > 0) {
      //For posting of leave authorization approvals
      let leave_auth_approvals_result = await attendanceLeavePolicy.postLeaveAuthApprovals(
        req_leave_auth_approvals,
        conn,
        pool
      );

      if (leave_auth_approvals_result.affectedRowsCount > 0) {
        //for posting leave encashment claim
        let leave_encash_claim_result = await attendanceLeavePolicy.postLeaveEncashClaim(
          req_leave_encash_claim,
          conn,
          pool
        );

        if (leave_encash_claim_result.affectedRowsCount > 0) {
          //for posting working days office timings
          let working_days_office_timings_result = await attendanceLeavePolicy.postWorkingDaysOfficeTimings(
            req_working_days_office_timings,
            conn,
            pool
          );

          if (working_days_office_timings_result.affectedRowsCount > 0) {
            //for posting employee attendance mark
            let emp_attendance_mark_result = await attendanceLeavePolicy.postEmpAttendaceMark(
              req_emp_attendance_mark,
              conn,
              pool
            );

            if (emp_attendance_mark_result.affectedRowsCount > 0) {
              //for posting extra working hours compensatoin
              let extra_hours_compen_result = await attendanceLeavePolicy.postExtraHoursCompensation(
                req_extra_hours_compen,
                conn,
                pool
              );
              if (extra_hours_compen_result.affectedRowsCount > 0) {
                //for posting working shifts
                let working_shifts_result = await attendanceLeavePolicy.postWorkingShifts(
                  req_working_shifts,
                  conn,
                  pool
                );

                if (working_shifts_result.affectedRowsCount > 0) {
                  //for posting treat late arrivals for regular employees
                  let treat_late_arrivals_reg_emp_result = await attendanceLeavePolicy.postLateArrivalsRegEmp(
                    req_treat_late_arrivals_reg_emp,
                    conn,
                    pool
                  );

                  if (
                    treat_late_arrivals_reg_emp_result.affectedRowsCount > 0
                  ) {
                    //for posting treat late arrivals for roster employees
                    let treat_late_arrivals_ros_emp_result = await attendanceLeavePolicy.postPostLateArrivalsRosEmp(
                      req_treat_late_arrivals_ros_emp,
                      conn,
                      pool
                    );

                    if (
                      treat_late_arrivals_ros_emp_result.affectedRowsCount > 0
                    ) {
                      res.status(200).json({
                        status: true,
                        statusCode: 200,
                        message: "success",
                        payload: {
                          leave_types_result,
                          leave_auth_approvals_result, 
                          leave_encash_claim_result, 
                          working_days_office_timings_result, 
                          emp_attendance_mark_result, 
                          extra_hours_compen_result, 
                          working_shifts_result, 
                          treat_late_arrivals_reg_emp_result, 
                          treat_late_arrivals_ros_emp_result
                        },
                      });
                    } else {
                      console.log(
                        `error occured at treat late arrivals for roster employees section: ${treat_late_arrivals_ros_emp_result}`
                      );

                      res.status(400).json({
                        status: false,
                        statusCode: 400,
                        message: "Bad Request",
                        payload: null,
                      });
                    }
                  } else {
                    console.log(
                      `error occured at treat late arrivals for regular employees section: ${treat_late_arrivals_reg_emp_result}`
                    );

                    res.status(400).json({
                      status: false,
                      statusCode: 400,
                      message: "Bad Request",
                      payload: null,
                    });
                  }
                } else {
                  console.log(
                    `error occured at working shifts section: ${working_shifts_result}`
                  );

                  res.status(400).json({
                    status: false,
                    statusCode: 400,
                    message: "Bad Request",
                    payload: null,
                  });
                }
              } else {
                console.log(
                  `error occured at extra working hours compensation section: ${extra_hours_compen_result}`
                );

                res.status(400).json({
                  status: false,
                  statusCode: 400,
                  message: "Bad Request",
                  payload: null,
                });
              }
            } else {
              console.log(
                `error occured at employee attendance mark section: ${emp_attendance_mark_result}`
              );

              res.status(400).json({
                status: false,
                statusCode: 400,
                message: "Bad Request",
                payload: null,
              });
            }
          } else {
            console.log(
              `error occured at working days and office timings section ${working_days_office_timings_result}`
            );

            res.status(400).json({
              status: false,
              statusCode: 400,
              message: "Bad Request",
              payload: null,
            });
          }
        } else {
          console.log(
            `error happend at leave encashment claim section: ${leave_encash_claim_result}`
          );

          res.status(400).json({
            status: false,
            statusCode: 400,
            message: "Bad Request",
            payload: null,
          });
        }
      } else {
        console.log(
          `error happened at leave authorization approvals section: ${leave_auth_approvals_result}`
        );

        res.status(400).json({
          status: false,
          statusCode: 400,
          message: "Bad Request",
          payload: null,
        });
      }
    } else {
      res.status(500).json({
        status: false,
        statusCode: 500,
        message: "internal Error",
        payload: null,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "internal failure",
      payload: null,
    });
  }
});


//to update attendance and leave policy
router.put("/updateAttendanceLeavePolicy", async (req, res) => {
  try {
    //cosole.table(req.body);

    //parse request body
    let req_leave_types = req.body.leave_types;
    let req_leave_auth_approvals = req.body.leave_auth_approvals;
    let req_leave_encash_claim = req.body.leave_encash_claim;
    let req_working_days_office_timings = req.body.working_days_office_timings;
    let req_emp_attendance_mark = req.body.emp_attendance_mark;
    let req_extra_hours_compen = req.body.extra_hours_compen;
    let req_working_shifts = req.body.working_shifts;
    let req_treat_late_arrivals_reg_emp = req.body.treat_late_arrivals_reg_emp;
    let req_treat_late_arrivals_ros_emp = req.body.treat_late_arrivals_ros_emp;

    let conn = req.conn;
    let pool = req.pool;

    let attendanceLeavePolicy = new AttendanceLeavePolicyData();

    //console.table(req_leave_types);

    let leave_types_result = await attendanceLeavePolicy.updateLeaveTypes(
      req_leave_types,
      conn,
      pool
    );

    if (leave_types_result.affectedRowsCount > 0) {
      //For updateing of leave authorization approvals
      let leave_auth_approvals_result = await attendanceLeavePolicy.updateLeaveAuthApprovals(
        req_leave_auth_approvals,
        conn,
        pool
      );

      if (leave_auth_approvals_result.affectedRowsCount > 0) {
        //for updateing leave encashment claim
        let leave_encash_claim_result = await attendanceLeavePolicy.updateLeaveEncashClaim(
          req_leave_encash_claim,
          conn,
          pool
        );

        if (leave_encash_claim_result.affectedRowsCount > 0) {
          //for updateing working days office timings
          let working_days_office_timings_result = await attendanceLeavePolicy.updateWorkingDaysOfficeTimings(
            req_working_days_office_timings,
            conn,
            pool
          );

          if (working_days_office_timings_result.affectedRowsCount > 0) {
            //for updateing employee attendance mark
            let emp_attendance_mark_result = await attendanceLeavePolicy.updateEmpAttendaceMark(
              req_emp_attendance_mark,
              conn,
              pool
            );

            if (emp_attendance_mark_result.affectedRowsCount > 0) {
              //for updateing extra working hours compensatoin
              let extra_hours_compen_result = await attendanceLeavePolicy.updateExtraHoursCompensation(
                req_extra_hours_compen,
                conn,
                pool
              );
              if (extra_hours_compen_result.affectedRowsCount > 0) {
                //for updateing working shifts
                let working_shifts_result = await attendanceLeavePolicy.updateWorkingShifts(
                  req_working_shifts,
                  conn,
                  pool
                );

                if (working_shifts_result.affectedRowsCount > 0) {
                  //for updateing treat late arrivals for regular employees
                  let treat_late_arrivals_reg_emp_result = await attendanceLeavePolicy.updateLateArrivalsRegEmp(
                    req_treat_late_arrivals_reg_emp,
                    conn,
                    pool
                  );

                  if (
                    treat_late_arrivals_reg_emp_result.affectedRowsCount > 0
                  ) {
                    //for updateing treat late arrivals for roster employees
                    let treat_late_arrivals_ros_emp_result = await attendanceLeavePolicy.updateLateArrivalsRosEmp(
                      req_treat_late_arrivals_ros_emp,
                      conn,
                      pool
                    );

                    if (
                      treat_late_arrivals_ros_emp_result.affectedRowsCount > 0
                    ) {
                      res.status(200).json({
                        status: true,
                        statusCode: 200,
                        message: "success",
                        payload: {
                          leave_types_result,
                          leave_auth_approvals_result, 
                          leave_encash_claim_result, 
                          working_days_office_timings_result, 
                          emp_attendance_mark_result, 
                          extra_hours_compen_result, 
                          working_shifts_result, 
                          treat_late_arrivals_reg_emp_result, 
                          treat_late_arrivals_ros_emp_result
                        },
                      });
                    } else {
                      console.log(
                        `error occured at update treat late arrivals for roster employees section: ${treat_late_arrivals_ros_emp_result}`
                      );

                      res.status(400).json({
                        status: false,
                        statusCode: 400,
                        message: "Bad Request",
                        payload: null,
                      });
                    }
                  } else {
                    console.log(
                      `error occured at update treat late arrivals for regular employees section: ${treat_late_arrivals_reg_emp_result}`
                    );

                    res.status(400).json({
                      status: false,
                      statusCode: 400,
                      message: "Bad Request",
                      payload: null,
                    });
                  }
                } else {
                  console.log(
                    `error occured at update working shifts section: ${working_shifts_result}`
                  );

                  res.status(400).json({
                    status: false,
                    statusCode: 400,
                    message: "Bad Request",
                    payload: null,
                  });
                }
              } else {
                console.log(
                  `error occured at update extra working hours compensation section: ${extra_hours_compen_result}`
                );

                res.status(400).json({
                  status: false,
                  statusCode: 400,
                  message: "Bad Request",
                  payload: null,
                });
              }
            } else {
              console.log(
                `error occured at update employee attendance mark section: ${emp_attendance_mark_result}`
              );

              res.status(400).json({
                status: false,
                statusCode: 400,
                message: "Bad Request",
                payload: null,
              });
            }
          } else {
            console.log(
              `error occured at update working days and office timings section ${working_days_office_timings_result}`
            );

            res.status(400).json({
              status: false,
              statusCode: 400,
              message: "Bad Request",
              payload: null,
            });
          }
        } else {
          console.log(
            `error occured at updateleave encashment claim section: ${leave_encash_claim_result}`
          );

          res.status(400).json({
            status: false,
            statusCode: 400,
            message: "Bad Request",
            payload: null,
          });
        }
      } else {
        console.log(
          `error occured at update leave authorization approvals section: ${leave_auth_approvals_result}`
        );

        res.status(400).json({
          status: false,
          statusCode: 400,
          message: "Bad Request",
          payload: null,
        });
      }
    } else {
      res.status(500).json({
        status: false,
        statusCode: 500,
        message: "internal Error",
        payload: null,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "internal failure",
      payload: null,
    });
  }
});


module.exports = router;
