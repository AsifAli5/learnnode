var express = require("express");
var router = express.Router();
var IncentiveBonusPolicyData = require("../../model/polices/IncentiveBonusPolicyData")
  .IncentiveBonusPolicyData;

router.get("/getIncentiveBonusPolicy", async (req, res) => {
  try {
    let incentiveBonusPolicy = new IncentiveBonusPolicyData();

    //parse request body
    let policy_id = req.body.policy_id;

    let conn = req.conn;
    let pool = req.pool;

    let result = await incentiveBonusPolicy.getIncentiveBonusPolicy(
      policy_id,
      conn,
      pool
    );

    let incentives_type = [];
    let bonuses_type = [];
    let statutory_bonus = [];
    let festival_bonus = [];
    let long_service_award = [];

    for (let i = 0; i < result.length; i++) {
        const record = result[i];

        if(record["policy_section_id"] === 7){
            incentives_type.push(record);

        }else if(record["policy_section_id"] === 8){
            bonuses_type.push(record);

        }else if(record["policy_section_id"] === 9){
            statutory_bonus.push(record);
        }else if(record["policy_section_id"] === 10){
            festival_bonus.push(record);

        }else if(record["policy_section_id"] === 11){
            long_service_award.push(record);
        }
        
    }

    res.status(200).json({
      status: true,
      statusCode: 200,
      message: "success",
      payload: {
        incentives_type,
        bonuses_type,
        statutory_bonus,
        festival_bonus,
        long_service_award
      },
    });

  

  } catch (error) {
    console.log(error);
    res.status(500).json({
        status: false,
        statusCode: 500,
        message: 'internal failure',
        payload: {
            error:error.message
        }
    });
  }
});


router.post('/postIncentiveBonusPolicy', async (req, res) => {

  try {
    //console.log(req.body);
    let incentiveBonusPolicy = new IncentiveBonusPolicyData();

    //parse request body
   let req_incentives_type = req.body.incentives_type;
   let req_bonuses_type = req.body.bonuses_type;
   let req_statutory_bonus = req.body.statutory_bonus;
   let req_festival_bonus = req.body.festival_bonus;
   let req_long_service_award = req.body.long_service_award;

    
    let conn = req.conn;
    let pool = req.pool;

    // console.table(req_incentives_type);
    // console.table(req_bonuses_type);
  
    
    let incentives_type_result = await incentiveBonusPolicy.postIncentivesType(req_incentives_type, conn, pool);
   //console.log(JSON.stringify(incentives_type_result));  


    if(incentives_type_result.affectedRowsCount>0){
     
      //For posting of bonuses type section data
     let bonuses_type_result =  await incentiveBonusPolicy.postBonusesType(req_bonuses_type,conn,pool);

     if (bonuses_type_result.affectedRowsCount > 0) {
      
      //for posting statutory bonus section data
       let statutory_bonus_result = await incentiveBonusPolicy.postStatutoryBonus(req_statutory_bonus,conn,pool);
       
       if (statutory_bonus_result.affectedRowsCount > 0) {

         //for posting festival/occasional bonus data
         let festival_bonus_result = await incentiveBonusPolicy.postFestivalBonus(req_festival_bonus,conn,pool);

         if (festival_bonus_result.affectedRowsCount > 0) {


           //for posting Long service award
           let long_service_award_result = await incentiveBonusPolicy.postLongServiceAward(req_long_service_award,conn,pool);

           if (long_service_award_result.affectedRowsCount >0) {
             res.status(200).json({
               status: true,
               statusCode: 200,
               message: 'success',
               payload:{
                incentives_type_result,
                bonuses_type_result,
                statutory_bonus_result,
                festival_bonus_result,
                long_service_award_result
                 
               }
             });
             
           } else {
             console.log("error occured at posting long service award section");
             
           }


         } else {
           console.log("error occured at posting festival bonus section")
         }
       } else {
         console.log("error occured at  posting statutory bonus section");
         
       }

     } else {
       console.log("error happened at  posting bonuses type section data");
       
     }


    }else{
      res.status(500).json({
        status: false,
        statusCode: 500,
        message: 'internal Error',
        payload: null
      });

    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: 'internal Error',
      payload: error.message
    });
  }

});

router.put('/updateIncentiveBonusPolicy', async (req, res) => {
  try {
   // console.log(req.body);
    let incentiveBonusPolicy = new IncentiveBonusPolicyData();

    //parse request body
   let req_incentives_type = req.body.incentives_type;
   let req_bonuses_type = req.body.bonuses_type;
   let req_statutory_bonus = req.body.statutory_bonus;
   let req_festival_bonus = req.body.festival_bonus;
   let req_long_service_award = req.body.long_service_award;

    
    let conn = req.conn;
    let pool = req.pool;

    //  console.table(req_incentives_type);
    //  console.table(req_bonuses_type);

    let incentives_type_result = await incentiveBonusPolicy.updateIncentivesType(req_incentives_type, conn, pool);
   //console.log(JSON.stringify(incentives_type_result));  


    if(incentives_type_result.affectedRowsCount>0){
     
      //For updating of bonuses type section data
     let bonuses_type_result =  await incentiveBonusPolicy.updateBonusesType(req_bonuses_type,conn,pool);

     if (bonuses_type_result.affectedRowsCount > 0) {
    
      //for updating statutory bonus section data
       let statutory_bonus_result = await incentiveBonusPolicy.updateStatutoryBonus(req_statutory_bonus,conn,pool);
       
       if (statutory_bonus_result.affectedRowsCount > 0) {

         //for updating festival/occasional bonus data
         let festival_bonus_result =  await incentiveBonusPolicy.updateFestivalBonus(req_festival_bonus,conn,pool);

         if (festival_bonus_result.affectedRowsCount > 0) {


           //for updating Long service award
           let long_service_award_result = await incentiveBonusPolicy.updateLongServiceAward(req_long_service_award,conn,pool);

           if (long_service_award_result.affectedRowsCount >0) {
             res.status(200).json({
               status: true,
               statusCode: 200,
               message: 'success',
               payload:{
                incentives_type_result,
                bonuses_type_result,
                statutory_bonus_result,
                festival_bonus_result,
                long_service_award_result
                 
               }
             });
             
           } else {
             console.log("error occured at updating long service award section");
             
           }


         } else {
           console.log(`error occured at updating festival bonus section ${festival_bonus_result}`);
           return res.status(400).json({ 
             status: false,
             statusCode: 400,
             message: 'Bad Request',
             payload: null 
          });
         }
       } else {
         console.log("error occured at  updating statutory bonus section");
         
       }

     } else {
       console.log("error happened at updating bonuses type section data");
       
     }


    }else{
      res.status(500).json({
        status: false,
        statusCode: 500,
        message: 'internal Error',
        payload: null
      });

    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: 'internal Error',
      payload: error.message
    });
  }
});

module.exports = router;
