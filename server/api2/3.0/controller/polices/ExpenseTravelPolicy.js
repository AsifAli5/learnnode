var express = require("express");
var router = express.Router();
var ExpenseTravelPolicyData = require("../../model/polices/ExpenseTravelPolicy")
  .ExpenseTravelPolicyData;

//to get expense and travel policy
router.get("/getExpenseTravelPolicy", async (req, res) => {
  try {
    // console.table(req.body);

    let policy_id = req.body.policy_id;
    let conn = req.conn;
    let pool = req.pool;

    let expenseTravelPolicy = new ExpenseTravelPolicyData();

    let result = await expenseTravelPolicy.getExpenseTravelPolicy(
      policy_id,
      conn,
      pool
    );

    let expenses_type = [] ;
    let expense_auth_approvals = [] ;
    let travel_auth_approvals = [] ;
    let business_travel_type = [] ;
    let accommodation_type = [] ;
    let daily_allowance_pa = [] ;
    let travel_class = [] ;
    let travel_daily_allowance_limit = [] ;
    let kilometer_allowance = [] ;
    let transfer_allowances_type = [] ;

    //sectionize data based on their section id
    for (let i = 0; i < result.length; i++) {
      if (result[i]["policy_section_id"] === 30) {
        expenses_type_result.push(result[i]);
      } else if (result[i]["policy_section_id"] === 31) {
        expense_auth_approvals_result.push(result[i]);
      } else if (result[i]["policy_section_id"] === 32) {
        travel_auth_approvals_result.push(result[i]);
      } else if (result[i]["policy_section_id"] === 33) {
        business_travel_type_result.push(result[i]);
      } else if (result[i]["policy_section_id"] === 34) {
        accommodation_type_result.push(result[i]);
      } else if (result[i]["policy_section_id"] === 35) {
        daily_allowance_pa_result.push(result[i]);
      } else if (result[i]["policy_section_id"] === 36) {
        travel_class_result.push(result[i]);
      } else if (result[i]["policy_section_id"] === 37) {
        travel_daily_allowance_limit_result.push(result[i]);
      } else if (result[i]["policy_section_id"] === 38) {
        kilometer_allowance_result.push(result[i]);
      } else if (result[i]["policy_section_id"] === 39) {
        transfer_allowances_type_result.push(result[i]);
      }
    }

    res.status(200).json({
      status: true,
      statusCode: 200,
      message: "success",
      payload: {
        expenses_type,
        expense_auth_approvals,
        travel_auth_approvals,
        business_travel_type,
        accommodation_type,
        daily_allowance_pa,
        travel_class,
        travel_daily_allowance_limit,
        kilometer_allowance,
        transfer_allowances_type
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "internal failure",
      payload: null,
    });
  }
});

//to post expense and travel policy
router.post("/postExpenseTravelPolicy", async (req, res) => {
  try {
    //console.table(req.body);

    //parse request body
    let req_expenses_type = req.body.expenses_type;
    let req_expense_auth_approvals = req.body.expense_auth_approvals;
    let req_travel_auth_approvals = req.body.travel_auth_approvals;
    let req_business_travel_type = req.body.business_travel_type;
    let req_accommodation_type = req.body.accommodation_type;
    let req_daily_allowance_pa = req.body.daily_allowance_pa;
    let req_travel_class = req.body.travel_class;
    let req_travel_daily_allowance_limit =
      req.body.travel_daily_allowance_limit;
    let req_kilometer_allowance = req.body.kilometer_allowance;
    let req_transfer_allowances_type = req.body.transfer_allowances_type;

    let conn = req.conn;
    let pool = req.pool;

    //   console.table(req_expenses_type);
    //   console.table(req_expense_auth_approvals);
    //   console.table(req_travel_auth_approvals);
    //   console.table(req_business_travel_type);
    //   console.table(req_accommodation_type);

    let expenseTravelPolicyData = new ExpenseTravelPolicyData();

    let expenses_type_result = await expenseTravelPolicyData.postExpenseType(
      req_expenses_type,
      conn,
      pool
    );

    if (expenses_type_result.affectedRowsCount > 0) {
      //For posting expense authorization approvals
      let expense_auth_approvals_result = await expenseTravelPolicyData.postExpenseAuthApprovals(
        req_expense_auth_approvals,
        conn,
        pool
      );

      if (expense_auth_approvals_result.affectedRowsCount > 0) {
        //for posting travel authorization approvals
        let travel_auth_approvals_result = await expenseTravelPolicyData.postTravelAuthApprovals(
          req_travel_auth_approvals,
          conn,
          pool
        );

        if (travel_auth_approvals_result.affectedRowsCount > 0) {
          //for posting business travel type
          let business_travel_type_result = await expenseTravelPolicyData.postBusinessTravelType(
            req_business_travel_type,
            conn,
            pool
          );

          if (business_travel_type_result.affectedRowsCount > 0) {
            //for posting accommodation type
            let accommodation_type_result = await expenseTravelPolicyData.postAccommodationType(
              req_accommodation_type,
              conn,
              pool
            );

            if (accommodation_type_result.affectedRowsCount > 0) {
              //for posting daily allowance for personal arrangements
              let daily_allowance_pa_result = await expenseTravelPolicyData.postDailyAllowancePa(
                req_daily_allowance_pa,
                conn,
                pool
              );
              if (daily_allowance_pa_result.affectedRowsCount > 0) {
                //for posting travel class
                let travel_class_result = await expenseTravelPolicyData.postTravelClass(
                  req_travel_class,
                  conn,
                  pool
                );

                if (travel_class_result.affectedRowsCount > 0) {
                  //for posting daily allowance limit
                  let travel_daily_allowance_limit_result = await expenseTravelPolicyData.postTravelDailyAllowanceLimit(
                    req_travel_daily_allowance_limit,
                    conn,
                    pool
                  );

                  if (
                    travel_daily_allowance_limit_result.affectedRowsCount > 0
                  ) {
                    //for posting kilometer allowance
                    let kilometer_allowance_result = await expenseTravelPolicyData.postKilometerAllowance(
                      req_kilometer_allowance,
                      conn,
                      pool
                    );

                    if (kilometer_allowance_result.affectedRowsCount > 0) {
                      //for posting transfer allowances
                      let transfer_allowances_type_result = await expenseTravelPolicyData.postTransferAllowances(
                        req_transfer_allowances_type,
                        conn,
                        pool
                      );

                      if (
                        transfer_allowances_type_result.affectedRowsCount > 0
                      ) {
                        res.status(200).json({
                          status: true,
                          statusCode: 200,
                          message: "success",
                          payload: {
                            expenses_type_result,
                            expense_auth_approvals_result,
                            travel_auth_approvals_result,
                            business_travel_type_result,
                            accommodation_type_result,
                            daily_allowance_pa_result,
                            travel_class_result,
                            travel_daily_allowance_limit_result,
                            kilometer_allowance_result,
                            transfer_allowances_type_result,
                          },
                        });
                      } else {
                        console.log(
                          `error occured at transfer allowances section: ${transfer_allowances_type_result}`
                        );

                        res.status(400).json({
                          status: false,
                          statusCode: 400,
                          message: "Bad Request",
                          payload: null,
                        });
                      }
                    } else {
                      console.log(
                        `error occured at kilometer allowance section: ${kilometer_allowance_result}`
                      );

                      res.status(400).json({
                        status: false,
                        statusCode: 400,
                        message: "Bad Request",
                        payload: null,
                      });
                    }
                  } else {
                    console.log(
                      `error occured at travel daily allowance limit section: ${travel_daily_allowance_limit_result}`
                    );

                    res.status(400).json({
                      status: false,
                      statusCode: 400,
                      message: "Bad Request",
                      payload: null,
                    });
                  }
                } else {
                  console.log(
                    `error occured at travel class section: ${travel_class_result}`
                  );

                  res.status(400).json({
                    status: false,
                    statusCode: 400,
                    message: "Bad Request",
                    payload: null,
                  });
                }
              } else {
                console.log(
                  `error occured at daily allowance personal arrangements section: ${daily_allowance_pa_result}`
                );

                res.status(400).json({
                  status: false,
                  statusCode: 400,
                  message: "Bad Request",
                  payload: null,
                });
              }
            } else {
              console.log(
                `error occured at accommodation type section: ${accommodation_type_result}`
              );

              res.status(400).json({
                status: false,
                statusCode: 400,
                message: "Bad Request",
                payload: null,
              });
            }
          } else {
            console.log(
              `error occured at business travel type section ${business_travel_type_result}`
            );

            res.status(400).json({
              status: false,
              statusCode: 400,
              message: "Bad Request",
              payload: null,
            });
          }
        } else {
          console.log(
            `error occured at travel authorization approvals section: ${travel_auth_approvals_result}`
          );

          res.status(400).json({
            status: false,
            statusCode: 400,
            message: "Bad Request",
            payload: null,
          });
        }
      } else {
        console.log(
          `error occured at expense authorization approvals section: ${expense_auth_approvals_result}`
        );

        res.status(400).json({
          status: false,
          statusCode: 400,
          message: "Bad Request",
          payload: null,
        });
      }
    } else {
      res.status(500).json({
        status: false,
        statusCode: 500,
        message: "internal Error",
        payload: null,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "internal failure",
      payload: null,
    });
  }
});

//to update expense and travel policy
router.put("/updateExpenseTravelPolicy", async (req, res) => {
  try {
    //console.table(req.body);

    //parse request body
    let req_expenses_type = req.body.expenses_type;
    let req_expense_auth_approvals = req.body.expense_auth_approvals;
    let req_travel_auth_approvals = req.body.travel_auth_approvals;
    let req_business_travel_type = req.body.business_travel_type;
    let req_accommodation_type = req.body.accommodation_type;
    let req_daily_allowance_pa = req.body.daily_allowance_pa;
    let req_travel_class = req.body.travel_class;
    let req_travel_daily_allowance_limit =
      req.body.travel_daily_allowance_limit;
    let req_kilometer_allowance = req.body.kilometer_allowance;
    let req_transfer_allowances_type = req.body.transfer_allowances_type;

    let conn = req.conn;
    let pool = req.pool;

     //  console.table(req_expenses_type);
    //   console.table(req_expense_auth_approvals);
    //   console.table(req_travel_auth_approvals);
    //   console.table(req_business_travel_type);
    //   console.table(req_accommodation_type); 
    

    let expenseTravelPolicyData = new ExpenseTravelPolicyData();

    let expenses_type_result = await expenseTravelPolicyData.updateExpenseType(
      req_expenses_type,
      conn,
      pool
    );

    if (expenses_type_result.affectedRowsCount > 0) {
      //For updating expense authorization approvals
      let expense_auth_approvals_result = await expenseTravelPolicyData.updateExpenseAuthApprovals(
        req_expense_auth_approvals,
        conn,
        pool
      );

      if (expense_auth_approvals_result.affectedRowsCount > 0) {
        //for updating travel authorization approvals
        let travel_auth_approvals_result = await expenseTravelPolicyData.updateTravelAuthApprovals(
          req_travel_auth_approvals,
          conn,
          pool
        );

        if (travel_auth_approvals_result.affectedRowsCount > 0) {
          //for updating business travel type
          let business_travel_type_result = await expenseTravelPolicyData.updateBusinessTravelType(
            req_business_travel_type,
            conn,
            pool
          );

          if (business_travel_type_result.affectedRowsCount > 0) {
            //for updating accommodation type
            let accommodation_type_result = await expenseTravelPolicyData.updateAccommodationType(
              req_accommodation_type,
              conn,
              pool
            );

            if (accommodation_type_result.affectedRowsCount > 0) {
              //for updating daily allowance for personal arrangements
              let daily_allowance_pa_result = await expenseTravelPolicyData.updateDailyAllowancePa(
                req_daily_allowance_pa,
                conn,
                pool
              );
              if (daily_allowance_pa_result.affectedRowsCount > 0) {
                //for updating travel class
                let travel_class_result = await expenseTravelPolicyData.updateTravelClass(
                  req_travel_class,
                  conn,
                  pool
                );

                if (travel_class_result.affectedRowsCount > 0) {
                  //for updating daily allowance limit
                  let travel_daily_allowance_limit_result = await expenseTravelPolicyData.updateTravelDailyAllowanceLimit(
                    req_travel_daily_allowance_limit,
                    conn,
                    pool
                  );

                  if (
                    travel_daily_allowance_limit_result.affectedRowsCount > 0
                  ) {
                    //for updating kilometer allowance
                    let kilometer_allowance_result = await expenseTravelPolicyData.updateKilometerAllowance(
                      req_kilometer_allowance,
                      conn,
                      pool
                    );

                    if (kilometer_allowance_result.affectedRowsCount > 0) {
                      //for updating transfer allowances
                      let transfer_allowances_type_result = await expenseTravelPolicyData.updateTransferAllowances(
                        req_transfer_allowances_type,
                        conn,
                        pool
                      );

                      if (
                        transfer_allowances_type_result.affectedRowsCount > 0
                      ) {
                        res.status(200).json({
                          status: true,
                          statusCode: 200,
                          message: "success",
                          payload: {
                            expenses_type_result,
                            expense_auth_approvals_result,
                            travel_auth_approvals_result,
                            business_travel_type_result,
                            accommodation_type_result,
                            daily_allowance_pa_result,
                            travel_class_result,
                            travel_daily_allowance_limit_result,
                            kilometer_allowance_result,
                            transfer_allowances_type_result,
                          },
                        });
                      } else {
                        console.log(
                          `error occured at update update transfer allowances section: ${transfer_allowances_type_result}`
                        );

                        res.status(400).json({
                          status: false,
                          statusCode: 400,
                          message: "Bad Request",
                          payload: null,
                        });
                      }
                    } else {
                      console.log(
                        `error occured at update kilometer allowance section: ${kilometer_allowance_result}`
                      );

                      res.status(400).json({
                        status: false,
                        statusCode: 400,
                        message: "Bad Request",
                        payload: null,
                      });
                    }
                  } else {
                    console.log(
                      `error occured at update travel daily allowance limit section: ${travel_daily_allowance_limit_result}`
                    );

                    res.status(400).json({
                      status: false,
                      statusCode: 400,
                      message: "Bad Request",
                      payload: null,
                    });
                  }
                } else {
                  console.log(
                    `error occured at update travel class section: ${travel_class_result}`
                  );

                  res.status(400).json({
                    status: false,
                    statusCode: 400,
                    message: "Bad Request",
                    payload: null,
                  });
                }
              } else {
                console.log(
                  `error occured at update daily allowance personal arrangements section: ${daily_allowance_pa_result}`
                );

                res.status(400).json({
                  status: false,
                  statusCode: 400,
                  message: "Bad Request",
                  payload: null,
                });
              }
            } else {
              console.log(
                `error occured at update accommodation type section: ${accommodation_type_result}`
              );

              res.status(400).json({
                status: false,
                statusCode: 400,
                message: "Bad Request",
                payload: null,
              });
            }
          } else {
            console.log(
              `error occured at update business travel type section ${business_travel_type_result}`
            );

            res.status(400).json({
              status: false,
              statusCode: 400,
              message: "Bad Request",
              payload: null,
            });
          }
        } else {
          console.log(
            `error occured at update travel authorization approvals section: ${travel_auth_approvals_result}`
          );

          res.status(400).json({
            status: false,
            statusCode: 400,
            message: "Bad Request",
            payload: null,
          });
        }
      } else {
        console.log(
          `error occured at expense authorization approvals section: ${expense_auth_approvals_result}`
        );

        res.status(400).json({
          status: false,
          statusCode: 400,
          message: "Bad Request",
          payload: null,
        });
      }
    } else {
      res.status(500).json({
        status: false,
        statusCode: 500,
        message: "internal Error",
        payload: null,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "internal failure",
      payload: null,
    });
  }
});

module.exports = router;
