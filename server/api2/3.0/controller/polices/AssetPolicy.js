var express = require("express");
var router = express.Router();
var AssetPolicyData = require("../../model/polices/AssetPolicyData")
  .AssetPolicyData;

//to get asset policy
router.get("/getAssetPolicy", async (req, res) => {
  try {
    // console.table(req.body);

    let policy_id = req.body.policy_id;
    let conn = req.conn;
    let pool = req.pool;

    let assetPolicy = new AssetPolicyData();

    let result = await assetPolicy.getAssetPolicy(policy_id, conn, pool);

    let vehicle_asset_policy = [];
    let bear_vehicle_asset_run_cost = [];
    let providing_mobile_policy = [];
    let bear_mobile_run_cost = [];
    let providing_laptop_policy = [];
    let bear_laptop_run_cost = [];
    let asset_withdawl_policy = [];
    let asset_replace_disp_policy = [];

    //sectionize data based on their section id
    for (let i = 0; i < result.length; i++) {
      if (result[i]["policy_section_id"] === 40) {
        vehicle_asset_policy.push(result[i]);
      } else if (result[i]["policy_section_id"] === 41) {
        bear_vehicle_asset_run_cost.push(result[i]);
      } else if (result[i]["policy_section_id"] === 42) {
        providing_mobile_policy.push(result[i]);
      } else if (result[i]["policy_section_id"] === 43) {
        bear_mobile_run_cost.push(result[i]);
      } else if (result[i]["policy_section_id"] === 44) {
        providing_laptop_policy.push(result[i]);
      } else if (result[i]["policy_section_id"] === 45) {
        bear_laptop_run_cost.push(result[i]);
      } else if (result[i]["policy_section_id"] === 46) {
        asset_withdawl_policy.push(result[i]);
      } else if (result[i]["policy_section_id"] === 47) {
        asset_replace_disp_policy.push(result[i]);
      }
    }

    res.status(200).json({
      status: true,
      statusCode: 200,
      message: "success",
      payload: {
        vehicle_asset_policy,
        bear_vehicle_asset_run_cost,
        providing_mobile_policy,
        bear_mobile_run_cost,
        providing_laptop_policy,
        bear_laptop_run_cost,
        asset_withdawl_policy,
        asset_replace_disp_policy,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "internal failure",
      payload: null,
    });
  }
});

//to post asset policy
router.post("/postAssetPolicy", async (req, res) => {
  try {
    //console.table(req.body);

    //parse request body
    let req_vehicle_asset_policy = req.body.vehicle_asset_policy;
    let req_bear_vehicle_asset_run_cost = req.body.bear_vehicle_asset_run_cost;
    let req_providing_mobile_policy = req.body.providing_mobile_policy;
    let req_bear_mobile_run_cost = req.body.bear_mobile_run_cost;
    let req_providing_laptop_policy = req.body.providing_laptop_policy;
    let req_bear_laptop_run_cost = req.body.bear_laptop_run_cost;
    let req_asset_withdawl_policy = req.body.asset_withdawl_policy;
    let req_asset_replace_disp_policy = req.body.asset_replace_disp_policy;

    let conn = req.conn;
    let pool = req.pool;

    //   console.table(req_vehicle_asset_policy);
    //   console.table(req_bear_vehicle_asset_run_cost);
    //   console.table(req_providing_mobile_policy);
    //   console.table(req_bear_mobile_run_cost);

  

    let assetPolicy = new AssetPolicyData();

    let vehicle_asset_policy_result = await assetPolicy.postVehicleAssetPolicy(
      req_vehicle_asset_policy,
      conn,
      pool
    );

    if (vehicle_asset_policy_result.affectedRowsCount > 0) {
      //For posting bear vehicle asset running cost 
      let bear_vehicle_asset_run_cost_result = await assetPolicy.postBearVehicleAssetRc(
        req_bear_vehicle_asset_run_cost,
        conn,
        pool
      );

      if (bear_vehicle_asset_run_cost_result.affectedRowsCount > 0) {
        //for posting providing mobile policy
        let providing_mobile_policy_result = await assetPolicy.postProvideMobileAsset(
            req_providing_mobile_policy,
          conn,
          pool
        );

        if (providing_mobile_policy_result.affectedRowsCount > 0) {
          //for posting bear mobile asset running cost
          let bear_mobile_run_cost_result = await assetPolicy.postBearMobileAssetRc(
            req_bear_mobile_run_cost,
            conn,
            pool
          );

          if (bear_mobile_run_cost_result.affectedRowsCount > 0) {
            //for posting provide laptop asset
            let providing_laptop_policy_result = await assetPolicy.postProvideLaptopPolicy(
                req_providing_laptop_policy,
              conn,
              pool
            );

            if (providing_laptop_policy_result.affectedRowsCount > 0) {
              //for posting bear laptop asset running cost
              let bear_laptop_run_cost_result = await assetPolicy.postLaptopAssetRc(
                req_bear_laptop_run_cost,
                conn,
                pool
              );
              if (bear_laptop_run_cost_result.affectedRowsCount > 0) {
                //for posting asset withdrawl policy
                let asset_withdawl_policy_result = await assetPolicy.postAssetWithdrawlPolicy(
                  req_asset_withdawl_policy,
                  conn,
                  pool
                );

                if (asset_withdawl_policy_result.affectedRowsCount > 0) {
                  //for posting 
                  let asset_replace_disp_policy_result = await assetPolicy.postAssetReplaceDisposalPolicy(
                    req_asset_replace_disp_policy,
                    conn,
                    pool
                  );

                  if (
                    asset_replace_disp_policy_result.affectedRowsCount > 0
                  ) {
                    res.status(200).json({
                        status: true,
                        statusCode: 200,
                        message: "success",
                        payload: {
                            vehicle_asset_policy_result,
                            bear_vehicle_asset_run_cost_result,
                            providing_mobile_policy_result,
                            bear_mobile_run_cost_result,
                            providing_laptop_policy_result,
                            bear_laptop_run_cost_result,
                            asset_withdawl_policy_result,
                            asset_replace_disp_policy_result
                        },
                      });
                   
                  } else {
                    console.log(
                      `error occured at asset replace and disposal policy section: ${asset_replace_disp_policy_result}`
                    );

                    res.status(400).json({
                      status: false,
                      statusCode: 400,
                      message: "Bad Request",
                      payload: null,
                    });
                  }
                } else {
                  console.log(
                    `error occured at asset withdrawl policy section: ${asset_withdawl_policy_result}`
                  );

                  res.status(400).json({
                    status: false,
                    statusCode: 400,
                    message: "Bad Request",
                    payload: null,
                  });
                }
              } else {
                console.log(
                  `error occured at laptop asset running cost section: ${bear_laptop_run_cost_result}`
                );

                res.status(400).json({
                  status: false,
                  statusCode: 400,
                  message: "Bad Request",
                  payload: null,
                });
              }
            } else {
              console.log(
                `error occured at provide laptop asset policy section: ${providing_laptop_policy_result}`
              );

              res.status(400).json({
                status: false,
                statusCode: 400,
                message: "Bad Request",
                payload: null,
              });
            }
          } else {
            console.log(
              `error occured at bear mobile running cost section ${bear_mobile_run_cost_result}`
            );

            res.status(400).json({
              status: false,
              statusCode: 400,
              message: "Bad Request",
              payload: null,
            });
          }
        } else {
          console.log(
            `error occured at provide mobile asset section: ${providing_mobile_policy_result}`
          );

          res.status(400).json({
            status: false,
            statusCode: 400,
            message: "Bad Request",
            payload: null,
          });
        }
      } else {
        console.log(
          `error occured at bear vehicle asset running cost section: ${bear_vehicle_asset_run_cost_result}`
        );

        res.status(400).json({
          status: false,
          statusCode: 400,
          message: "Bad Request",
          payload: null,
        });
      }
    } else {
      res.status(500).json({
        status: false,
        statusCode: 500,
        message: "internal Error",
        payload: null,
      });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: 500,
      message: "internal failure",
      payload: null,
    });
  }
});


//to update asset policy
router.put("/updateAssetPolicy", async (req, res) => {
    try {
      //console.table(req.body);
  
      //parse request body
      let req_vehicle_asset_policy = req.body.vehicle_asset_policy;
      let req_bear_vehicle_asset_run_cost = req.body.bear_vehicle_asset_run_cost;
      let req_providing_mobile_policy = req.body.providing_mobile_policy;
      let req_bear_mobile_run_cost = req.body.bear_mobile_run_cost;
      let req_providing_laptop_policy = req.body.providing_laptop_policy;
      let req_bear_laptop_run_cost = req.body.bear_laptop_run_cost;
      let req_asset_withdawl_policy = req.body.asset_withdawl_policy;
      let req_asset_replace_disp_policy = req.body.asset_replace_disp_policy;
  
      let conn = req.conn;
      let pool = req.pool;
  
      //   console.table(req_vehicle_asset_policy);
      //   console.table(req_bear_vehicle_asset_run_cost);
      //   console.table(req_providing_mobile_policy);
      //   console.table(req_bear_mobile_run_cost);
  
    
  
      let assetPolicy = new AssetPolicyData();
  
      let vehicle_asset_policy_result = await assetPolicy.updateVehicleAssetPolicy(
        req_vehicle_asset_policy,
        conn,
        pool
      );
  
      if (vehicle_asset_policy_result.affectedRowsCount > 0) {
        //For updating bear vehicle asset running cost 
        let bear_vehicle_asset_run_cost_result = await assetPolicy.updateBearVehicleAssetRc(
          req_bear_vehicle_asset_run_cost,
          conn,
          pool
        );
  
        if (bear_vehicle_asset_run_cost_result.affectedRowsCount > 0) {
          //for updating providing mobile policy
          let providing_mobile_policy_result = await assetPolicy.updateProvideMobileAsset(
              req_providing_mobile_policy,
            conn,
            pool
          );
  
          if (providing_mobile_policy_result.affectedRowsCount > 0) {
            //for updating bear mobile asset running cost
            let bear_mobile_run_cost_result = await assetPolicy.updateBearMobileAssetRc(
              req_bear_mobile_run_cost,
              conn,
              pool
            );
  
            if (bear_mobile_run_cost_result.affectedRowsCount > 0) {
              //for updating provide laptop asset
              let providing_laptop_policy_result = await assetPolicy.updateProvideLaptopPolicy(
                  req_providing_laptop_policy,
                conn,
                pool
              );
  
              if (providing_laptop_policy_result.affectedRowsCount > 0) {
                //for updating bear laptop asset running cost
                let bear_laptop_run_cost_result = await assetPolicy.updateLaptopAssetRc(
                  req_bear_laptop_run_cost,
                  conn,
                  pool
                );
                if (bear_laptop_run_cost_result.affectedRowsCount > 0) {
                  //for updating asset withdrawl policy
                  let asset_withdawl_policy_result = await assetPolicy.updateAssetWithdrawlPolicy(
                    req_asset_withdawl_policy,
                    conn,
                    pool
                  );
  
                  if (asset_withdawl_policy_result.affectedRowsCount > 0) {
                    //for updating replace and disposal policy
                    let asset_replace_disp_policy_result = await assetPolicy.updateAssetReplaceDisposalPolicy(
                      req_asset_replace_disp_policy,
                      conn,
                      pool
                    );
  
                    if (
                      asset_replace_disp_policy_result.affectedRowsCount > 0
                    ) {
                      res.status(200).json({
                          status: true,
                          statusCode: 200,
                          message: "success",
                          payload: {
                              vehicle_asset_policy_result,
                              bear_vehicle_asset_run_cost_result,
                              providing_mobile_policy_result,
                              bear_mobile_run_cost_result,
                              providing_laptop_policy_result,
                              bear_laptop_run_cost_result,
                              asset_withdawl_policy_result,
                              asset_replace_disp_policy_result
                          },
                        });
                     
                    } else {
                      console.log(
                        `error occured at updating asset replace and disposal policy section: ${asset_replace_disp_policy_result}`
                      );
  
                      res.status(400).json({
                        status: false,
                        statusCode: 400,
                        message: "Bad Request",
                        payload: null,
                      });
                    }
                  } else {
                    console.log(
                      `error occured at updating asset withdrawl policy section: ${asset_withdawl_policy_result}`
                    );
  
                    res.status(400).json({
                      status: false,
                      statusCode: 400,
                      message: "Bad Request",
                      payload: null,
                    });
                  }
                } else {
                  console.log(
                    `error occured at updating laptop asset running cost section: ${bear_laptop_run_cost_result}`
                  );
  
                  res.status(400).json({
                    status: false,
                    statusCode: 400,
                    message: "Bad Request",
                    payload: null,
                  });
                }
              } else {
                console.log(
                  `error occured at updating provide laptop asset policy section: ${JSON.stringify(providing_laptop_policy_result)}`
                );
  
                res.status(400).json({
                  status: false,
                  statusCode: 400,
                  message: "Bad Request",
                  payload: null,
                });
              }
            } else {
              console.log(
                `error occured at updating bear mobile running cost section ${bear_mobile_run_cost_result}`
              );
  
              res.status(400).json({
                status: false,
                statusCode: 400,
                message: "Bad Request",
                payload: null,
              });
            }
          } else {
            console.log(
              `error occured at updating provide mobile asset section: ${providing_mobile_policy_result}`
            );
  
            res.status(400).json({
              status: false,
              statusCode: 400,
              message: "Bad Request",
              payload: null,
            });
          }
        } else {
          console.log(
            `error occured at updating bear vehicle asset running cost section: ${bear_vehicle_asset_run_cost_result}`
          );
  
          res.status(400).json({
            status: false,
            statusCode: 400,
            message: "Bad Request",
            payload: null,
          });
        }
      } else {
        res.status(500).json({
          status: false,
          statusCode: 500,
          message: "internal Error",
          payload: null,
        });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: false,
        statusCode: 500,
        message: "internal failure",
        payload: null,
      });
    }
  });
module.exports = router;
