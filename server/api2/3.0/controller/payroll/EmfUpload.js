var express = require("express");
var router = express.Router();
var emfUpload = require("../../model/payroll/EmfUpload").EmfUpload;
const fs = require("fs-extra");
var path = require("path");
const uploadFiles = require("../../helper/uploadFiles");
var appDir = path.dirname(require.main.filename);
const Papa = require("papaparse");
const { validate, ValidationError, Joi } = require("express-validation");
const validation = require("../../helper/Validation/ValidationPost");

router.post("/UploadingEmf",validate(validation.EmfUploding, {}, {}),async (req, res) => {
    const conn = req.conn;
    const pool = req.pool;
    const body = req.body;
    var emfupload = new emfUpload();
    let emfFileName;
    if (req.files != null) {
      var files = req.files.EmfFile;
      var resultEmfFile = await uploadFiles.uploadFile(files, "emf");
      if (!resultEmfFile.success) {
        res.status(200).json({ message: "error" });
      }
      emfFileName = resultEmfFile.assetNames[0].name;
    } else {
      emfFileName = "NA";
    }
    if (emfFileName != "NA") {
      const csvFilePath = `${appDir}/server/api2/3.0/uploads/emf/${emfFileName}`;
      const readCSV = async (filePath) => {
        const csvFile = fs.readFileSync(filePath);
        const csvData = csvFile.toString();
        return new Promise((resolve) => {
          Papa.parse(csvData, {
            header: true,
            complete: (results) => {
              // console.log("Complete", results.data.length - 1, "records.");
              resolve(results.data);
            },
          });
        });
      };

      const parseDataUploading = async () => {
        let parsedData = await readCSV(csvFilePath);
        console.log(parsedData.length);
        if (parsedData.length > 1) {
          let result = await emfupload.UploadEmf(parsedData,conn,body,emfFileName,pool);
          let status = result[0].status;
          delete result[0].status;
          res.status(status).send(result);
        } else {
          res.status(404).send("File Is Empty");
        }
      };

      parseDataUploading();
    } else {
      res.status(404).send("File Not Found");
    }
  }
);
router.get("/EmfList",validate(validation.EmfList, {}, {}),
  async (req, res) => {
    var emfupload = new emfUpload();
    let Date = req.body.Date;
    let Type = req.body.Type;
    const conn = req.conn;
    const pool = req.pool;
    try {
      emfupload
        .GetEmfList(conn, Date, Type, pool)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } catch (err) {
      res.status(500).send(err);
    }
  }
);
router.post("/UpdateEmf",validate(validation.EmfUpdate, {}, {}),
  async (req, res) => {
    var emfupload = new emfUpload();
    let EmfArray = JSON.parse(req.body.EmfArray);
    const conn = req.conn;
    const pool = req.pool;
    try {
      let result = await emfupload.UpdateEmf(conn, EmfArray, pool);
      let status = result[0].status;
      delete result[0].status;
      res.status(status).send(result);
    } catch (err) {
      res.status(500).send(err);
    }
  }
);
router.post("/EmfRevertToOrigin",validate(validation.EmfReverttoOrigin, {}, {}),
  async (req, res) => {
    var emfupload = new emfUpload();
    let Date = req.body.Date;
    let Type = req.body.Type;
    const conn = req.conn;
    const pool = req.pool;
    try {
      let result = await emfupload.EmfRevertToOrigin(conn,Date,Type,pool);
      let status = result[0].status;
      delete result[0].status;
      res.status(status).send(result);
    } catch (err) {
      res.status(500).send(err);
    }
  }
);

module.exports = router;
