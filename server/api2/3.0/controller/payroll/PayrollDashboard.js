var express = require("express");
var router = express.Router();
var prollDashboard = require("../../model/payroll/PayrollDashboardData")
  .PayrollDashboardData;

router.get("/checkMongo", (req, res, next) => {});
//////////////EMF Update as of today///////////////////////////////////////
router.get("/getNewHires", (req, res) => {
  var prolldashboard = new prollDashboard();
  let year = req.body.year;
  const conn = req.conn;
  const pool = req.pool;

  prolldashboard.getNewHires(year, conn, pool)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
router.get("/getLeavers", (req, res) => {
  var prolldashboard = new prollDashboard();
  let year = req.body.year;
  const conn = req.conn;
  const pool = req.pool;

  prolldashboard
    .getLeavers(year, conn, pool)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
router.get("/getSalaryChanges", (req, res) => {
  var prolldashboard = new prollDashboard();
  let year = req.body.year;
  const conn = req.conn;
  const pool = req.pool;

  prolldashboard
    .getSalaryChanges(year, conn, pool)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
router.get("/getExpenses", (req, res) => {
  var prolldashboard = new prollDashboard();
  let year = req.body.year;
  const conn = req.conn;
  const pool = req.pool;

  prolldashboard
    .getExpenses(year, conn, pool)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
router.get("/getAttendance", (req, res) => {
  var prolldashboard = new prollDashboard();
  let year = req.body.year;
  const conn = req.conn;
  const pool = req.pool;

  prolldashboard
    .getAttendance(year, conn, pool)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
router.get("/getLoanDeduction", (req, res) => {
  var prolldashboard = new prollDashboard();
  let year = req.body.year;
  const conn = req.conn;
  const pool = req.pool;

  prolldashboard
    .getLoanDeduction(year, conn, pool)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
////////////// PayRoll For /////////////////////////////////////////////////
router.get("/getHeadCount", (req, res) => {
  var prolldashboard = new prollDashboard();
  let year = req.body.year;
  const conn = req.conn;
  const pool = req.pool;

  prolldashboard
    .getHeadCount(year, conn, pool)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
router.get("/getSalaryCost", (req, res) => {
  var prolldashboard = new prollDashboard();
  let year = req.body.year;
  const conn = req.conn;
  const pool = req.pool;

  prolldashboard
    .getSalaryCost(year, conn, pool)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
router.get("/getStatutoryBenifits", (req, res) => {
  var prolldashboard = new prollDashboard();
  let year = req.body.year;
  const conn = req.conn;
  const pool = req.pool;

  prolldashboard
    .getStatutoryBenifits(year, conn, pool)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
router.get("/getOtherChanges", (req, res) => {
  var prolldashboard = new prollDashboard();
  let year = req.body.year;
  const conn = req.conn;
  const pool = req.pool;

  prolldashboard
    .getOtherChanges(year, conn, pool)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
///////// List For Add, Update and Delete on popup ////////////////////////
router.get("/getNewHiresList", (req, res) => {
  var prolldashboard = new prollDashboard();
  let DepartmentId = req.body.DepartmentId;
  let BandId = req.body.BandId;
  let CatDesc = req.body.CatDesc;
  let Year = req.body.Year;
  let Month = req.body.Month;
  const conn = req.conn;
  const pool = req.pool;
  //console.log(req.body);
 // return;
  try {
      prolldashboard.getNewHireslist(DepartmentId,BandId,CatDesc,Year,Month,conn,pool).then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        }); 
  } catch {
    res.status(500).send(err);
  }
});
router.get("/getLeaverList", (req, res) => {
  var prolldashboard = new prollDashboard();
  let DepartmentId = req.body.DepartmentId;
  let BandId = req.body.BandId;
  let CatDesc = req.body.CatDesc;
  const conn = req.conn;
  const pool = req.pool;
  try {
    if (
      DepartmentId != null ||
      DepartmentId != "" ||
      DepartmentId != "undefined"
    ) {
      prolldashboard
        .getLeaverlist(DepartmentId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (BandId != null || BandId != "" || BandId != "undefined") {
      prolldashboard
        .getLeaverlist(BandId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (CatDesc != null || CatDesc != "" || CatDesc != "undefined") {
      if (CatDesc == "NM") {
        prolldashboard
          .getLeaverlist(year)
          .then((result) => {
            res.status(200).send(CatDesc);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      } else {
        prolldashboard
          .getLeaverlist(CatDesc)
          .then((result) => {
            res.status(200).send(result);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      }
    } else {
      prolldashboard
        .getLeaverlist(null)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    }
  } catch {}
});
router.get("/getSalaryChangeslist", (req, res) => {
  var prolldashboard = new prollDashboard();
  let DepartmentId = req.body.DepartmentId;
  let BandId = req.body.BandId;
  let CatDesc = req.body.CatDesc;
  const conn = req.conn;
  const pool = req.pool;
  try {
    if (
      DepartmentId != null ||
      DepartmentId != "" ||
      DepartmentId != "undefined"
    ) {
      prolldashboard
        .getSalaryChangeslist(DepartmentId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (BandId != null || BandId != "" || BandId != "undefined") {
      prolldashboard
        .getSalaryChangeslist(BandId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (CatDesc != null || CatDesc != "" || CatDesc != "undefined") {
      if (CatDesc == "NM") {
        prolldashboard
          .getSalaryChangeslist(year)
          .then((result) => {
            res.status(200).send(CatDesc);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      } else {
        prolldashboard
          .getSalaryChangeslist(CatDesc)
          .then((result) => {
            res.status(200).send(result);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      }
    } else {
      prolldashboard
        .getSalaryChangeslist(null)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    }
  } catch {}
});
router.get("/getExpenseslist", (req, res) => {
  var prolldashboard = new prollDashboard();
  let DepartmentId = req.body.DepartmentId;
  let BandId = req.body.BandId;
  let CatDesc = req.body.CatDesc;
  const conn = req.conn;
  const pool = req.pool;
  try {
    if (
      DepartmentId != null ||
      DepartmentId != "" ||
      DepartmentId != "undefined"
    ) {
      prolldashboard
        .getExpenseslist(DepartmentId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (BandId != null || BandId != "" || BandId != "undefined") {
      prolldashboard
        .getExpenseslist(BandId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (CatDesc != null || CatDesc != "" || CatDesc != "undefined") {
      if (CatDesc == "NM") {
        prolldashboard
          .getExpenseslist(year)
          .then((result) => {
            res.status(200).send(CatDesc);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      } else {
        prolldashboard
          .getExpenseslist(CatDesc)
          .then((result) => {
            res.status(200).send(result);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      }
    } else {
      prolldashboard
        .getExpenseslist(null)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    }
  } catch {}
});
router.get("/getAttendancelist", (req, res) => {
  var prolldashboard = new prollDashboard();
  let DepartmentId = req.body.DepartmentId;
  let BandId = req.body.BandId;
  let CatDesc = req.body.CatDesc;
  const conn = req.conn;
  const pool = req.pool;
  try {
    if (
      DepartmentId != null ||
      DepartmentId != "" ||
      DepartmentId != "undefined"
    ) {
      prolldashboard
        .getAttendancelist(DepartmentId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (BandId != null || BandId != "" || BandId != "undefined") {
      prolldashboard
        .getAttendancelist(BandId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (CatDesc != null || CatDesc != "" || CatDesc != "undefined") {
      if (CatDesc == "NM") {
        prolldashboard
          .getAttendancelist(year)
          .then((result) => {
            res.status(200).send(CatDesc);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      } else {
        prolldashboard
          .getAttendancelist(CatDesc)
          .then((result) => {
            res.status(200).send(result);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      }
    } else {
      prolldashboard
        .getAttendancelist(null)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    }
  } catch {}
});
router.get("/getLoanDeductionlist", (req, res) => {
  var prolldashboard = new prollDashboard();
  let DepartmentId = req.body.DepartmentId;
  let BandId = req.body.BandId;
  let CatDesc = req.body.CatDesc;
  const conn = req.conn;
  const pool = req.pool;
  try {
    if (
      DepartmentId != null ||
      DepartmentId != "" ||
      DepartmentId != "undefined"
    ) {
      prolldashboard
        .getLoanDeductionlist(DepartmentId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (BandId != null || BandId != "" || BandId != "undefined") {
      prolldashboard
        .getLoanDeductionlist(BandId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (CatDesc != null || CatDesc != "" || CatDesc != "undefined") {
      if (CatDesc == "NM") {
        prolldashboard
          .getLoanDeductionlist(year)
          .then((result) => {
            res.status(200).send(CatDesc);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      } else {
        prolldashboard
          .getLoanDeductionlist(CatDesc)
          .then((result) => {
            res.status(200).send(result);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      }
    } else {
      prolldashboard
        .getLoanDeductionlist(null)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    }
  } catch {}
});
router.get("/test", (req, res) => {
  var prolldashboard = new prollDashboard();
  let DepartmentId = req.body.DepartmentId;
  let BandId = req.body.BandId;
  let CatDesc = req.body.CatDesc;
  const conn = req.conn;
  const pool = req.pool;
  try {
    if (
      DepartmentId != null ||
      DepartmentId != "" ||
      DepartmentId != "undefined"
    ) {
      prolldashboard
        .getLoanDeductionlist(DepartmentId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (BandId != null || BandId != "" || BandId != "undefined") {
      prolldashboard
        .getLoanDeductionlist(BandId)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } else if (CatDesc != null || CatDesc != "" || CatDesc != "undefined") {
      if (CatDesc == "NM") {
        prolldashboard
          .getLoanDeductionlist(year)
          .then((result) => {
            res.status(200).send(CatDesc);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      } else {
        prolldashboard
          .getLoanDeductionlist(CatDesc)
          .then((result) => {
            res.status(200).send(result);
          })
          .catch((err) => {
            res.status(500).send(err);
          });
      }
    } else {
      prolldashboard
        .getLoanDeductionlist(null)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    }
  } catch {}
});
module.exports = router;
