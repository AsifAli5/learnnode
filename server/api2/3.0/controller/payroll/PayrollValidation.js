var express = require("express");
var router = express.Router();
var payrollValidation = require("../../model/payroll/PayrollValidation").PayrollValidation;
const { validate, ValidationError, Joi } = require("express-validation");
const validation = require("../../helper/Validation/ValidationPost");

router.get("/GetPayrollDetail",
  validate(validation.payrollList, {}, {}),
  async (req, res) => {
    var payrollvalidation = new payrollValidation();
    let Date = req.body.Date;
    let Type = req.body.Type;
    const conn = req.conn;
    const pool = req.pool;
    try {
        payrollvalidation.GetPayrollDetail(conn, Date, Type, pool)
        .then((result) => {
          res.status(200).send(result);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    } catch (err) {
      res.status(500).send(err);
    }
  }
);
router.post("/UpdatePayroll",
    validate(validation.PayrollUpdate, {}, {}), 
    async (req, res) => {
        var payrollvalidation = new payrollValidation();
      let PayrollArray = JSON.parse(req.body.PayrollArray);
      const conn = req.conn;
      const pool = req.pool;
      try {
        let result = await payrollvalidation.UpdatePayroll(conn, PayrollArray, pool);
        let status = result[0].status;
        delete result[0].status;
        res.status(status).send(result);
      } catch (err) {
        res.status(500).send(err);
      }
    }
  );
module.exports = router;
