var express = require("express");
var router = express.Router();
var compAndBenDashboard = require("../../model/payroll/CompAndBenDashboard").CompAndBenDashboard;

router.get("/checkMongo", (req, res, next) => {});
router.get("/getStaffTurover", (req, res) => {
  var compandBenDashboard = new compAndBenDashboard();
  let count = req.body.count;
  let monthyn = req.body.monthyn;
  const conn = req.conn;
  const pool = req.pool;

  compandBenDashboard.getStaffTurover(count,monthyn)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
router.get("/getExpenses", (req, res) => {
  var compandBenDashboard = new compAndBenDashboard();
  let count = req.body.count;
  let monthyn = req.body.monthyn;
  const conn = req.conn;
  const pool = req.pool;

  compandBenDashboard.getExpenses(count,monthyn)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
router.get("/getsalaryChanges", (req, res) => {
  var compandBenDashboard = new compAndBenDashboard();
  let count = req.body.count;
  const conn = req.conn;
  const pool = req.pool;

  compandBenDashboard.getsalaryChanges(count)
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});
module.exports = router;
