var express = require("express");
var router = express.Router();
var payrollSummary = require("../../model/payroll/PayrollSummary")
  .PayrollSummary;
const { validate, ValidationError, Joi } = require("express-validation");
const validation = require("../../helper/Validation/ValidationPost");

router.get("/GetPayrollSummary",
  validate(validation.parollsummary, {}, {}),
  async (req, res) => {
    var payrollsummary = new payrollSummary();

    const conn = req.conn;
    const pool = req.pool;
    let infoId = req.body.InfoId;
    try {
      let result = await payrollsummary.GetPayrollSummary(conn, infoId, pool);
      let status = result[0].status;
      delete result[0].status;
      res.status(status).send(result);
    } catch (err) {
      res.status(500).send(err);
    }
  }
);
module.exports = router;
