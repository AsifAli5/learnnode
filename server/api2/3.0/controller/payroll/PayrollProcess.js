var express = require("express");
var router = express.Router();
var payrollProcess = require("../../model/payroll/PayrollProcess").PayrollProcess;
const fs = require("fs-extra");
var path = require("path");
const uploadFiles = require("../../helper/uploadFiles");
const { async } = require("rxjs/internal/scheduler/async");
var appDir = path.dirname(require.main.filename);
const Papa = require("papaparse");

router.post("/UploadingEmf", async (req, res) => {
  const conn = req.conn;
  var emfupload = new emfUpload();
  let emfFileName;
  if (req.files != null) {
    var files = req.files.EmfFile;
    var resultEmfFile = await uploadFiles.uploadFile(files, "emf");
    if (!resultEmfFile.success) {
      res.status(200).json({ message: "error" });
    }
    emfFileName = resultEmfFile.assetNames[0].name;
  } else {
    emfFileName = "NA";
  }
  if (emfFileName != "NA") {
    const csvFilePath = `${appDir}/server/api2/3.0/uploads/emf/${emfFileName}`;
    const readCSV = async (filePath) => {
      const csvFile = fs.readFileSync(filePath);
      const csvData = csvFile.toString();
      return new Promise((resolve) => {
        Papa.parse(csvData, {
          header: true,
          complete: (results) => {
           // console.log("Complete", results.data.length - 1, "records.");
            resolve(results.data);
          },
        });
      });
    };

    const parseDataUploading = async () => {
      let parsedData = await readCSV(csvFilePath);
      //console.log(parsedData);
      let result = await emfupload.uploadEmf(parsedData, conn);
      //console.log(result);
      res.status(200).json({ result });
    };

    parseDataUploading();
  } else {
    res.status(404).send("File Not Found");
  }
});


router.post("/GeneratePayroll", async (req, res) => {
  let payroll = new payrollProcess(req.conn);
  payroll.emfFileInfoId = 1;
  // let emfResult = await payroll.getEmfData(req.conn);
  // if (Object.keys(emfResult).length > 0) {
  //   let payrollResponse = await payroll.insertEmfData(req.conn, emfResult);
  //   res.status(200).json({payrollResponse});
  // } else {
  //   res.status(404).json({error: 'No record found'});
  // }
  //let d = await payroll.employeeGrossSalary(req.conn, 1);
  //let d = await payroll.salaryComponentSetup(req.conn);
  let d = await payroll.generatePayroll(  1);

  res.status(200);
  res.json(payroll.logArray);
  //res.json(d);
});

module.exports = router;
