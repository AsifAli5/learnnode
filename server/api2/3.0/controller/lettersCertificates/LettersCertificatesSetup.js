var express = require("express");
var router = express.Router();
var lettersCertificatesSetup= require('../../model/lettersCertificates/LettersCertificatesSetup').lettersCertificatesSetup;


//Get Letters api
router.get("/getLettersCertificatesCategories", (req, res) => {
    var lettersCertificatesSetup = new lettersCertificatesSetup();
    let client_id = req.body.client_id;
    const conn = req.conn;
    const pool = req.pool;
  
    lettersCertificatesSetup.getLettersCertificatesCategories(client_id, conn, pool)
      .then((result) => {
        res.status(200).send(result);
      })
      .catch((err) => {
        res.status(500).send(err);
      });
  });