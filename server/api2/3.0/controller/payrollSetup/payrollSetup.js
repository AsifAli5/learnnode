const payrollSetup = require('../../model/payrollSetup/payrollSetup').FilterScreen
const orm = require('../../model/payrollSetup/orm').Orm

const middleware = require('../../helper/validatePayrollSetup');
const getter = require('./getSet/operations/get');
const setter = require('./getSet/operations/set')
const recordValidators = require('./validations/index');
const defaultResponse = require('../../utils/defaultResponse');


const router = require('express').Router();
const {validate} = require('express-validation');


const STATUTORY_SETUP = 'cb_terminal_statutory_setup';
const STATUTORY_SETUP_ID = 'terminal_statutory_setup_id';
const STATUTORY_SETUP_DETAILS = 'cb_terminal_statutory_setup_details';
const STATUTORY_SETUP_DETAILS_ID = 'terminal_statutory_setup_detail_id';
const DEDUCTION_SETUP = 'cb_allowances_deduction_setup';
const DEDUCTION_SETUP_ID = 'allowances_deduction_setup_id';
const DEDUCTION_SETUP_DETAILS = 'cb_allowances_deduction_setup_detail';
const DEDUCTION_SETUP_DETAILS_ID = 'allowances_deduction_setup_detail_id';

const payrollSetupObj = new payrollSetup();

/**
 * @Author: ismail Raza
 * @Date: 26-02-2021
 */

router.post('/', validate(middleware.searchSetupValidation), async (req, res) => {
    try {
        let requestBody = Object.entries(req.body).length ? req.body : '';
        if (!requestBody) {
            defaultResponse().error({message: 'Invalid Body, Bad Request'}, res, 400)
        } else {
            await setter.formatDataToSearch(requestBody.payroll_setup.setupCategory, requestBody)
            const query = new payrollSetup();
            const data = await query.filterScreen(requestBody.payroll_setup, req.conn);
            const keys = ['payroll_setup', 'parent_table', 'child_table', 'change_log', 'headers'];
            const keyValuedData = [];
            for (let index = 0; index < data.length; index++) {
                let obj = {}
                obj[keys[index]] = data[index];
                keyValuedData.push(obj);
            }
            await getter.formatDataToFetch(requestBody.payroll_setup.setup_desc, keyValuedData);
            let screenData = {};
            keyValuedData.forEach(one => {
                screenData = {...screenData, ...one}
            })
            const dropDownValues = await fetchDropdownValues(requestBody.payroll_setup, req.conn)
            defaultResponse().success({message: 'Data is retrieved'}, {dropDownValues, screenData}, res, 200)
        }
    } catch (exception) {
        res.status(500).send(exception)
    }
})

/**
 * @Author: ismail Raza
 * @Date: 01-03-2021
 */

router.post('/search', async (req, res) => {
    try {
        const requestBody = (Object.entries(req.body)).length ? req.body : '';
        if (!requestBody) {
            defaultResponse().error({message: 'Invalid Body, Bad Request'}, res, 400)
        } else {
            let table;
            if (requestBody.parent) {
                if (requestBody.payroll_setup.setupScreen !== 'Compensation_Setup') {
                    table = STATUTORY_SETUP;
                } else {
                    table = DEDUCTION_SETUP;
                }
            } else {
                if (requestBody.payroll_setup.setupScreen !== 'Compensation_Setup') {
                    table = STATUTORY_SETUP_DETAILS;
                } else {
                    table = DEDUCTION_SETUP_DETAILS;
                }
            }
            const data = await searchFunction(table, requestBody.searchParams, requestBody.payroll_setup.setupCategory, requestBody.parent, req.conn);
            defaultResponse().success({message: 'Data is retrieved'}, data, res, 200)
        }
    } catch (e) {
        res.status(500).send(e)
    }
})

/**
 * @Author: ismail Raza
 * @Date: 01-03-2021
 */

router.post('/update', validate(middleware.updateSetupValidation), async (req, res) => {
    try {
        let requestBody = (Object.entries(req.body)).length ? req.body : '';
        let tables = {}, changeLogRecord;
        if (!requestBody) {
            defaultResponse().error({message: 'Invalid Body, Bad Request'}, res, 400)
        } else {

            // apply setter before saving data
            await setter.formatDataToSave(requestBody.payroll_setup.setupCategory, requestBody)

            // assigning tables names from constants
            if (requestBody.payroll_setup.setup_type !== 'Compensation_Setup') {
                tables.parentTable = STATUTORY_SETUP;
                tables.parentTableId = STATUTORY_SETUP_ID;
                tables.childTable = STATUTORY_SETUP_DETAILS;
                tables.childTableId = STATUTORY_SETUP_DETAILS_ID;
            } else {
                tables.parentTable = DEDUCTION_SETUP;
                tables.parentTableId = DEDUCTION_SETUP_ID;
                tables.childTable = DEDUCTION_SETUP_DETAILS;
                tables.childTableId = DEDUCTION_SETUP_DETAILS_ID;
            }
            // if tables names are not assigned
            if (!(Object.entries(tables)).length) {
                defaultResponse().error({message: 'Database tables are not assigned for Query'}, res, 500)

            } else {
                const record = new orm();

                // save parent year if not exist
                let yearSlab = await payrollSetupObj.validateExistingParentSlab(tables.parentTable, requestBody.parent_table, requestBody.parent_table.payroll_setup_id, req.conn);
                if (!yearSlab.length) {
                    yearSlab = await payrollSetupObj.addParentSlab(tables.parentTable, requestBody.parent_table, req.conn);
                } else {
                    yearSlab = yearSlab[0];
                    yearSlab.insertId = yearSlab[tables.parentTableId];
                }

                // check for duplicate slots before saving
                if (await recordValidators(requestBody.payroll_setup.setup_desc, tables, yearSlab.insertId, requestBody.child_table, req.conn)) {
                    defaultResponse().error({message: 'Data Slot already exist'}, res, 400)
                } else {

                    // filter slabs to update and insert
                    let {
                        slabIds,
                        slabsToUpdate,
                        slabsToInsert,
                        slabsToDelete
                    } = await filterSlabs(yearSlab, requestBody.child_table, tables);

                    // insert slabs into the tables
                    if ((slabsToUpdate && slabsToUpdate.length) || (slabsToInsert && slabsToInsert.length)) {
                        if (slabIds && slabIds.length) {
                            slabIds = await payrollSetupObj.selectMultipleByOneColumn(tables.childTable, tables.childTableId, slabIds, req.conn)
                        }
                        if (slabsToInsert && slabsToInsert.length)
                            for (let element of slabsToInsert) {
                                element[tables.parentTableId] = yearSlab.insertId;
                                await payrollSetupObj.insertSlabDetails(tables.childTable, element, req.conn);
                            }

                        // update slabs into the tables
                        if (slabsToUpdate && slabsToUpdate.length)
                            for (let [index, element] of slabsToUpdate.entries()) {
                                const cond = {};
                                changeLogRecord = slabIds && slabIds.length ? await findChanges(slabIds[index], element) : []
                                cond[tables.childTableId] = element[tables.childTableId];
                                delete element[tables.childTableId];
                                await record.update(tables.childTable, element, cond, req.conn);
                                const unused_variable_1 = changeLogRecord = changeLogRecord.length ? changeLogRecord.filter(ele => {
                                    ele[tables.childTableId] = cond[tables.childTableId];
                                    ele.created_by = element.created_by;
                                    ele.state_id = element.state_id;
                                    ele.payroll_setup_id = requestBody.parent_table.payroll_setup_id
                                    return ele;
                                }) : changeLogRecord
                            }

                        //soft deletion for the tables
                        if (slabsToDelete && slabsToDelete.length) {
                            for (let [index, one] of slabsToDelete.entries()) {
                                const cond = {};
                                changeLogRecord = slabIds && slabIds.length ? await findChanges(slabIds[index], one) : []
                                cond[tables.childTableId] = one[tables.childTableId];
                                await record.update(tables.childTable, {is_delete: 1}, cond, req.conn)
                                const unused_variable_2 = changeLogRecord = changeLogRecord.length ? changeLogRecord.filter(ele => {
                                    ele[tables.childTableId] = cond[tables.childTableId];
                                    ele.created_by = one.created_by;
                                    ele.state_id = one.state_id;
                                    ele.payroll_setup_id = requestBody.parent_table.payroll_setup_id
                                    return ele;
                                }) : changeLogRecord;
                            }
                        }

                        // create change log
                        const unused_variable_3 = changeLogRecord && changeLogRecord.length ? await insertIntoChangeLog(changeLogRecord, req.conn) : '';

                        // fetch data to respond to the frontend
                        const data = await payrollSetupObj.filterScreen(requestBody.payroll_setup, req.conn)
                        const keys = ['payroll_setup', 'parent_table', 'child_table', 'change_log', 'headers'];
                        const keyValuedData = [];
                        for (let index = 0; index < data.length; index++) {
                            let obj = {}
                            obj[keys[index]] = data[index];
                            keyValuedData.push(obj);
                        }
                        await getter.formatDataToFetch(requestBody.payroll_setup.setup_desc, keyValuedData);
                        let responseData = {};
                        keyValuedData.forEach(one => {
                            responseData = {...responseData, ...one}
                        })
                        defaultResponse().success({message: 'Data is updated'}, responseData, res, 200)
                    } else {
                        defaultResponse().error({message: 'Parent table conflict with child tables to save'}, res, 400)
                    }
                }
            }
        }
    } catch (exception) {
        res.status(500).send(exception)
    }
})

/**
 * @Author: ismail Raza
 * @Date: 24-03-2021
 */

router.get('/headers', async (req, res) => {
    try {

        const headers = await payrollSetupObj.displayHeaders(req.conn);
        if (!headers) {
            defaultResponse().success({message: 'Data not found'}, [], res, 404)
        } else {
            const data = await formatHeaderDataForResponse(headers)
            defaultResponse().success({message: 'Data is retrieved'}, data, res, 200)
        }
    } catch (exception) {
        defaultResponse().error({message: exception.message}, res, 500)
    }
})


/**
 * @Author: ismail Raza
 * @Date: 25-03-2021
 */

router.post('/headers/update', async (req, res) => {
    try {
        let requestBody = Object.entries(req.body.headers).length ? req.body : '';
        if (!requestBody)
            defaultResponse().error({message: 'Bad Request, Invalid Body'}, res, 400);
        else {

            const {toUpdate, toInsert} = await filterMisHeaders(requestBody.headers);
            if (toUpdate && toUpdate.length)
                for (let one of toUpdate) {
                    await payrollSetupObj.updateHeaders(one, req.conn)
                }
            if (toInsert && toInsert.length)
                for (let one of toInsert) {
                    await payrollSetupObj.insertHeaders(one, req.conn)
                }
            const headers = await payrollSetupObj.displayHeaders(req.conn);
            const data = await formatHeaderDataForResponse(headers)
            if (data)
                defaultResponse().success({message: 'Data is updated'}, data, res, 200)
        }
    } catch (exception) {
        defaultResponse().error({message: exception.message}, res, 500)
    }
})

/**
 * @Author: ismail Raza
 * @Date: 01-03-2021
 */

searchFunction = async (tbl, body, category, parent, conn) => {
    try {
        if (Object.entries(body).length) {
            let searchParams;
            const record = new orm();
            if (parent) {
                searchParams = await setter.formatDataToSearch(category, {'parent_table': body})
                let data = await record.searchSingleTable(tbl, searchParams.parent_table, conn);
                data = await getter.formatDataToFetch(category, [{'parent_table': data}], true)
                return data.parent_table;
            } else {
                searchParams = await setter.formatDataToSearch(category, {'child_table': [body]})
                let data = await record.searchSingleTable(tbl, searchParams.child_table[0], conn);
                data = await getter.formatDataToFetch(category, [{'child_table': data}], true)
                return data.child_table;
            }
        } else {
            throw {
                message: 'Bad Request, Invalid Body',
                error: true,
                data: null
            }
        }
    } catch (exception) {
        throw exception;
    }
}

/**
 * @Author: ismail Raza
 * @Date: 01-03-2021
 */

filterSlabs = async (parentSlab, slabs, tables) => {
    try {
        let slabsToUpdate, slabsToInsert, slabsToDelete;
        const slabIds = slabs.filter(one => one[tables.childTableId]).map(one => one[tables.childTableId]);
        if (parentSlab.is_consumed) {
            slabsToDelete = JSON.parse(JSON.stringify(slabs.filter(one => one[tables.childTableId] !== undefined)));
            slabsToInsert = slabs.map(one => {
                delete one[tables.parentTableId];
                delete one[tables.childTableId];
                return one
            })
        } else {
            slabsToUpdate = slabs.filter(one => one[tables.parentTableId] === parentSlab[tables.parentTableId]);
            slabsToInsert = slabs.filter(one => one[tables.parentTableId] === undefined);
        }
        return {slabIds, slabsToUpdate, slabsToInsert, slabsToDelete}
    } catch (exception) {
        throw exception
    }
}

/**
 * @Author: ismail Raza
 * @Date: 11-03-2021
 */

findChanges = async (oldRecord, newRecord) => {
    try {
        return await Object.keys(oldRecord).filter(key => oldRecord[key] !== newRecord[key] && newRecord[key] !== undefined).map(one => ({
            change_type: one,
            old_data: oldRecord[one],
            new_data: newRecord[one]
        }))
    } catch (exception) {
        throw exception
    }
}

/**
 * @Author: ismail Raza
 * @Date: 13-03-2021
 */

insertIntoChangeLog = async (arrayData, conn) => {
    try {

        for (let one of arrayData) {
            await payrollSetupObj.InsertChangeLog(one, conn)
        }
    } catch (exception) {
        throw exception;
    }
}

/**
 * @Author: ismail Raza
 * @Date: 25-03-2021
 */

filterMisHeaders = async (headers) => {
    try {
        let screens = [];

        headers.forEach(one => {
            for (let two of one.screens) {
                two.payroll_headers_id = one.payroll_headers_id
            }
            screens.push(...one.screens)
        })
        const toUpdate = screens.filter(one => one.headers_screen_filters_id)
        const toInsert = screens.filter(one => !one.headers_screen_filters_id)
        return {toUpdate, toInsert}
    } catch (exception) {
        throw exception
    }
}


formatHeaderDataForResponse = async (headers) => {
    try {
        const keys = ['headers', 'screens'];
        const keyValuedData = [];
        for (let index = 0; index < headers.length; index++) {
            let obj = {}
            obj[keys[index]] = headers[index];
            keyValuedData.push(obj);
        }
        let formattedData = {};
        keyValuedData.forEach(one => {
            formattedData = {...formattedData, ...one}
        })

        const output = [];

        for (let item of formattedData.headers) {
            let existing = output.filter((v, i) => {
                return v.payroll_headers_id === item.payroll_headers_id;
            })
            if (existing.length) {
                let existingIndex = output.indexOf(existing[0]);
                const obj = {
                    status: item.status,
                    payroll_setup_id: item.payroll_setup_id,
                    headers_screen_filters_id: item.headers_screen_filters_id
                }
                delete item.status;
                delete item.payroll_setup_id;
                delete item.headers_screen_filters_id;
                output[existingIndex].screens.push(obj)
            } else {
                if (item.headers_screen_filters_id) {
                    const obj = {
                        status: item.status,
                        payroll_setup_id: item.payroll_setup_id,
                        headers_screen_filters_id: item.headers_screen_filters_id
                    }
                    delete item.status;
                    delete item.payroll_setup_id;
                    delete item.headers_screen_filters_id;
                    item.screens = [obj];
                }
                output.push(item);
            }
        }
        return formattedData.headers = output;
    } catch (exception) {
        throw exception;
    }
}

fetchDropdownValues = async (screen, conn) => {
    try {

        const data = await payrollSetupObj.dropDownValues(screen, conn)
        const keys = ['years', 'regions', 'states', 'employee_type', 'contributor', 'calculation_type'];
        if (screen.setup_type === 'Compensation_Setup')
            keys.push('department', 'bands')
        else if (screen.setup_desc === 'Group_Life_Insurance' || screen.setup_type === 'Insurance') {
            keys.push('criteria')
        }
        let dropDownValues = [];
        for (let index = 0; index < data.length; index++) {
            let obj = {}
            obj[keys[index]] = data[index];
            dropDownValues.push(obj);
        }
        dropDownValues = Object.assign({}, ...dropDownValues)
        if (screen.setup_desc === 'Group_Life_Insurance' || screen.setup_type === 'Insurance') {
            dropDownValues.criteria = await formatCriteria(dropDownValues.criteria, conn)
        }
        return dropDownValues;
    } catch (exception) {
        throw exception
    }
}

/**
 * @Author: ismail Raza
 * @Date: 02-04-2021
 */

formatCriteria = async (body, conn) => {
    for (let one of body) {
        if (one.reference_data_code)
            one.values = await payrollSetupObj.valuesThroughRefTable(one.table_name, one.table_desc_col, one.table_pk_col, one.reference_data_code, conn)
        else
            one.values = await payrollSetupObj.valuesFromTable(one.table_name, one.table_desc_col, one.table_pk_col, conn)
        delete one.reference_data_code
        delete one.table_desc_col;
        delete one.table_pk_col;
        delete one.table_name;
    }
    return body;
}

module.exports = router
