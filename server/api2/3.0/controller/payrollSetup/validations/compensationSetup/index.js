const filterScreen = require('../../../../model/payrollSetup/payrollSetup').FilterScreen;

module.exports = async (tables, parentId, childSlabs, conn) => {
    const filterScreenObj = new filterScreen();
    const existingSlabs = await filterScreenObj.fetchExistingSlabs(tables, parentId, conn)
    const toUpdate = childSlabs.filter(one => one.allowances_deduction_setup_detail_id).map(one => one.allowances_deduction_setup_detail_id)
    const toFilter = toUpdate && toUpdate.length ? existingSlabs.filter(n => !toUpdate.includes(n.allowances_deduction_setup_detail_id)) : existingSlabs
    return await toFilter.some(o1 => childSlabs.some(o2 => (o1.payroll_headers_id === o2.payroll_headers_id && o1.employement_type_id === o2.employement_type_id && o1.department_id === o2.department_id && o1.band_id === o2.band_id)))
}