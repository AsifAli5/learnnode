const validations = {};

validations.Salary_Income_Tax = require('./statutorySetup/salaryIncomeTax')
validations.Consultancy_Income_Tax = require('./statutorySetup/consultancyIncomeTax')
validations.Social_Security = require('./statutorySetup/socialSecurity&EOBI&EducationCess')
validations.Education_Cess = require('./statutorySetup/socialSecurity&EOBI&EducationCess')
validations.EOBI = require('./statutorySetup/socialSecurity&EOBI&EducationCess')
validations.Provident_Fund = require('./terminalBenefitsSetup/index')
validations.Pension_Fund = require('./terminalBenefitsSetup/index')
validations.Gratuity = require('./terminalBenefitsSetup/index')
validations.Salary_Component = require('./compensationSetup/index')
validations.Variable_Allowance = require('./compensationSetup/index')
validations.Overtime = require('./compensationSetup/index')
validations.Deductions = require('./compensationSetup/deduction')

module.exports = async (category, tables, yearId, childSlabs, conn) => {
    try {
        return await validations[`${category}`](tables, yearId, childSlabs, conn)
    } catch (e) {
        throw e
    }
}
