const filterScreen = require('../../../../model/payrollSetup/payrollSetup').FilterScreen;

module.exports = async (tables, parentId, childSlabs, conn) => {
    const filterScreenObj = new filterScreen();
    const existingSlabs = await filterScreenObj.fetchExistingSlabs(tables, parentId, conn)
    const toUpdate = childSlabs.filter(one => one.terminal_statutory_setup_detail_id).map(one => one.terminal_statutory_setup_detail_id)
    const toFilter = toUpdate && toUpdate.length ? existingSlabs.filter(n => !toUpdate.includes(n.terminal_statutory_setup_detail_id)) : existingSlabs
    return await toFilter.some(o1 => childSlabs.some(o2 => (o1.contributor_id === o2.contributor_id && o1.employement_type_id === o2.employement_type_id && o1.state_id === o2.state_id && o1.gender === o2.gender)))
}