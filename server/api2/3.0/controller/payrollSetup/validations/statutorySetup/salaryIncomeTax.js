const filterScreen = require('../../../../model/payrollSetup/payrollSetup').FilterScreen;

module.exports = async (tables, parentId, childSlabs, conn) => {
    const filterScreenObj = new filterScreen();
    const existingSlabs = await filterScreenObj.fetchExistingSlabs(tables, parentId, conn)
    const toUpdate = childSlabs.filter(one => one.terminal_statutory_setup_detail_id).map(one => one.terminal_statutory_setup_detail_id)
    const toFilter = toUpdate && toUpdate.length ? existingSlabs.filter(n => !toUpdate.includes(n.terminal_statutory_setup_detail_id)) : existingSlabs;
    if (await toFilter.some(o1 => childSlabs.some(o2 => (o1.contributor_id === o2.contributor_id && o1.employement_type_id === o2.employement_type_id && o1.slab_start === o2.slab_start && o1.slab_end === o2.slab_end && o1.tax_payer_status === o2.tax_payer_status)))) {
        return true
    } else
        return await slabsValidation(toFilter, childSlabs)
}

slabsValidation = async (oldArr, newArr) => {
    const combinedData = oldArr.concat(newArr)
    if (await overlapTester(combinedData, combinedData)) {
        return true
    } else return !!await gapTester(combinedData);
}

overlapTester = async (first, second) => {
    return first.some(o1 => second.some(o2 => (((o2.slab_start >= o1.slab_start && o2.slab_start <= o1.slab_end) && first.indexOf(o1) !== second.indexOf(o2)) || ((o2.slab_end >= o1.slab_start && o2.slab_end <= o1.slab_end) && first.indexOf(o1) !== second.indexOf(o2)))))
}

gapTester = async (first) => {
    const c = first.sort((a, b) => a.slab_start > b.slab_start ? 1 : -1)
    let error = false;
    for (let [index, obj] of c.entries()) {
        if (c[index + 1]) {
            if (obj.slab_end + 1 !== c[index + 1].slab_start) {
                error = true
            }
        }
    }
    return error
}