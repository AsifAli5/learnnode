/**
 * @Author: Ismail Raza
 * @Date: 10-03-2021
 */

const deductionConst = require('./constatns');

module.exports = async (table, search = false) => {
    try {
        Object.keys(table).forEach(key => {
            if (key === 'payroll_setup') {
                table[key] = setKeys(table[key], deductionConst.payrollSetup)
            } else if (key === 'parent_table') {
                table[key] = setKeys(table[key], deductionConst.deductions)
            } else if (key === 'child_table') {
                let newScreen = [];
                for (let one of table[key]) {
                    newScreen.push(setKeys(one, deductionConst.deductionsDetails))
                }
                table[key] = newScreen;
            }
        })
        if (search)
            return table
    } catch (exception) {
        throw exception;
    }
}


setKeys = (obj, constants) => {
    let newObj = {};
    for (let key in obj) {
        newObj[constants[key]] = obj[key];
    }
    return newObj;
}