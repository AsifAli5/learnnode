/**
 * @Author: Ismail Raza
 * @Date: 11-03-2021
 */

module.exports.payrollSetup = {
    'id': 'payroll_setup_id',
    'setupScreen': 'setup_type',
    'setupCategory': 'setup_desc',
    'year': 'payroll_physical_years_id'
};

module.exports.child = {
    'id': 'allowances_deduction_setup_detail_id',
    'setupId': 'allowances_deduction_setup_id',
    'contributeType': 'payroll_headers_type_id',
    'contribute': 'payroll_headers_id',
    'employeeType': 'employment_type_id',
    'employerValue': 'employer_value',
    'employerCalculationType': 'employer_calculation_type',
    'employeeValue': 'employee_value',
    'employeeCalculationType': 'employee_calculation_type',
    'changedOn': 'changed_on',
    'status': 'status',
    'deleted': 'is_delete',
    'editable': 'is_editable',
    'createdDate': 'created_at',
    'createdBy': 'created_by',
    'updatedBy': 'updated_by',
};

module.exports.parent = {
    'id': 'allowances_deduction_setup_id',
    'year': 'payroll_physical_years_id',
    'setupScreen': 'setup_type',
    'setupCategory': 'setup_desc',
    'payrollSetupId': 'payroll_setup_id',
    'contributor': 'contributor_id',
    'employeeType': 'employment_type_id',
    'changedOn': 'changed_on',
    'updatedBy': 'updated_by',
    'createdBy': 'created_by',
    'status': 'status'
};
