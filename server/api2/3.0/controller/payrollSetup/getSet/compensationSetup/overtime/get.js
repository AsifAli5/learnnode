/**
 * @Author: Ismail Raza
 * @Date: 10-03-2021
 */

const overtimeConst = require('./constants');

module.exports = async (table, search = false) => {
    const key = Object.keys(table)[0];
    if (key === 'payroll_setup') {
        const setup = overtimeConst.payrollSetup
        let newScreen = [];
        for (let one of table[key]) {
            newScreen.push(getter(setup, one))
        }
        table[key] = newScreen;
    } else if (key === 'parent_table') {
        const setup = overtimeConst.overtime
        let newParents = [];
        for (let one of table[key]) {
            newParents.push(getter(setup, one))
        }
        table[key] = newParents
    } else if (key === 'child_table') {
        const setup = overtimeConst.overtimeDetails
        let newParents = [];
        for (let one of table[key]) {
            newParents.push(getter(setup, one))
        }
        table[key] = newParents
    }
    if (search)
        return table;
}

getter = function (req, data) {
    let obj = {};
    Object.keys(data).forEach(one => {
        const k = Object.keys(req).find(key => req[key] === one);
        if (k)
            obj[k] = data[one]
    })
    return obj;
}