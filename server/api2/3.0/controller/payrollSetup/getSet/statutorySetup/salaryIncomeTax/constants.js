/**
 * @Author: Ismail Raza
 * @Date: 05-03-2021
 */

module.exports.payrollSetup = {
    'id': 'payroll_setup_id',
    'setupScreen': 'setup_type',
    'setupCategory': 'setup_desc',
    'year': 'payroll_physical_years_id'
};

module.exports.child = {
    'id': 'terminal_statutory_setup_detail_id',
    'parentId': 'terminal_statutory_setup_id',
    'contributor': 'contributor_id',
    'employeeType': 'employement_type_id',
    'startSlabRange': 'slab_start',
    'endSlabRange': 'slab_end',
    'fixedAmount': 'fixed_amount',
    'taxRate': 'rate_percentage',
    'status': 'status',
    'deleted': 'is_delete',
    'editable': 'is_editable',
    'createdDate': 'created_at',
    'createdBy': 'created_by',
    'updatedBy': 'updated_by',
};

module.exports.parent = {
    'id': 'terminal_statutory_setup_id',
    'year': 'payroll_physical_years_id',
    'contributor': 'contributor_id',
    'payrollSetupId': 'payroll_setup_id',
    'employeeType': 'employement_type_id',
    'startSlabRange': 'slab_start',
    'endSlabRange': 'slab_end',
    'consumed':'is_consumed',
    'region': 'region_id',
    'province':'state_id',
    'changedOn': 'changed_on',
    'updatedBy': 'updated_by',
    'createdBy': 'created_by',
    'status': 'status'
};
