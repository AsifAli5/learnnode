/**
 * @Author: Ismail Raza
 * @Date: 01-04-2021
 */

module.exports.payrollSetup = {
    'id': 'payroll_setup_id',
    'setupScreen': 'setup_type',
    'setupCategory': 'setup_desc',
    'year': 'payroll_physical_years_id'
};

module.exports.child = {
    'id': 'id',
    'parentId': 'terminal_statutory_setup_id',
    'contributor': 'contributor_id',
    'insuranceType': 'type',
    'planType': 'plan',
    'policyAmount': 'policy_amount',
    'policyNumber': 'policy',
    'policyStartDate': 'policy_start_date',
    'policyEndDate': 'policy_end_date',
    'gender': 'gender',
    'minimumAge': 'min_age',
    'maximumAge': 'max_age',
    'frequency': 'frequency',
    'createdBy': 'created_by',
    'updatedBy': 'updated_by',
    'createdAt': 'created_at',
    'updatedAt': 'updated_at',
    'changedOn': 'changed_on'
};

module.exports.parent = {
    'id': 'terminal_statutory_setup_id',
    'year': 'payroll_physical_years_id',
    'contributor': 'contributor_id',
    'payrollSetupId': 'payroll_setup_id',
    'employeeType': 'employement_type_id',
    'consumed': 'is_consumed',
    'region': 'region_id',
    'province': 'state_id',
    'changedOn': 'changed_on',
    'updatedBy': 'updated_by',
    'createdBy': 'created_by',
    'status': 'status'
};
