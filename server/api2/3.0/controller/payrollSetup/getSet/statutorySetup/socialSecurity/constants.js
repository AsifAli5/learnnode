/**
 * @Author: Ismail Raza
 * @Date: 09-03-2021
 */

module.exports.payrollSetup = {
    'id': 'payroll_setup_id',
    'setupScreen': 'setup_type',
    'setupCategory': 'setup_desc',
    'year': 'payroll_physical_years_id'
};

module.exports.child = {
    'id': 'terminal_statutory_setup_detail_id',
    'parentId': 'terminal_statutory_setup_id',
    'contributor': 'contributor_id',
    'employeeType': 'employement_type_id',
    'province': 'state_id',
    'minimumLimit': 'slab_start',
    'maximumLimit': 'slab_end',
    'percentageRate': 'rate_percentage',
    'gender': 'gender',
    'ageLimit': 'age_limit',
    'calculationType': 'calculation_type',
    'maxSS': 'max_value',
    'lwop': 'lwop',
    'status': 'status',
    'deleted': 'is_delete',
    'editable': 'is_editable',
    'createdDate': 'created_at',
    'createdBy': 'created_by',
    'updatedBy': 'updated_by',
};

module.exports.parent = {
    'id': 'terminal_statutory_setup_id',
    'year': 'payroll_physical_years_id',
    'setupScreen': 'setup_type',
    'setupCategory': 'setup_desc',
    'contributor': 'contributor_id',
    'payrollSetupId': 'payroll_setup_id',
    'employeeType': 'employment_type_id',
    'region': 'region_id',
    'province':'state_id',
    'lowerLimit': 'lower_limit',
    'maxLimit': 'max_limit',
    'ssMaxAmount': 'ss_max_amount',
    'changedOn': 'changed_on',
    'updatedBy': 'updated_by',
    'createdBy': 'created_by',
    'status': 'status'
};
