/**
 * @Author: Ismail Raza
 * @Date: 09-03-2021
 */

module.exports.payrollSetup = {
    'id': 'payroll_setup_id',
    'setupScreen': 'setup_type',
    'setupCategory': 'setup_desc',
    'year': 'payroll_physical_years_id'
};

module.exports.child = {
    'id': 'terminal_statutory_setup_detail_id',
    'parentId': 'terminal_statutory_setup_id',
    'contributor': 'contributor_id',
    'employeeType': 'employement_type_id',
    'province': 'state_id',
    'lowerLimit': 'slab_start',
    'upperLimit': 'slab_end',
    'rate': 'rate_percentage',
    'gender': 'gender',
    'ageLimit': 'age_limit',
    'calculationType': 'calculation_type',
    'maxCount': 'max_value',
    'status': 'status',
    'deleted': 'is_delete',
    'editable': 'is_editable',
    'createdDate': 'created_at',
    'createdBy': 'created_by',
    'updatedBy': 'updated_by',
};

module.exports.parent = {
    'id': 'terminal_statutory_setup_id',
    'year': 'payroll_physical_years_id',
    'setupScreen': 'setup_type',
    'setupCategory': 'setup_desc',
    'payrollSetupId': 'payroll_setup_id',

    'contributor': 'contributor_id',
    'employeeType':'employment_type_id',
    'maxCount': 'max_count',
    'region': 'region_id',
    'province':'state_id',
    'changedOn': 'changed_on',
    'updatedBy': 'updated_by',
    'createdBy': 'created_by',
    'status': 'status'
};
