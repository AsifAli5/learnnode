/**
 * @Author: Ismail Raza
 * @Date: 15-03-2021
 */

const constants = {}
constants.Salary_Income_Tax = require('./statutorySetup/salaryIncomeTax/constants')
constants.Consultancy_Income_Tax = require('./statutorySetup/consltancyIncomeTax/constants')
constants.Social_Security = require('./statutorySetup/socialSecurity/constants')
constants.Education_Cess = require('./statutorySetup/educationCess/constants')
constants.EOBI = require('./statutorySetup/EOBI/constants')
constants.Variable_Allowance = require('./compensationSetup/variableAllowance/constants')
constants.Deductions = require('./compensationSetup/deduction/constatns')
constants.Salary_Component = require('./compensationSetup/salaryComponent/constants')
constants.Overtime = require('./compensationSetup/overtime/constants')
constants.Provident_Fund = require('./terminalBenefits/providentFunds/constants')
constants.Pension_Fund = require('./terminalBenefits/pensionFund/constants')
constants.Gratuity = require('./terminalBenefits/gratuity/constants')
constants.changeLog = require('./changeLog/constants')
constants.headers = require('./compensationSetup/headers/constants')

module.exports = async (table, category, search = false) => {
    try {
        const key = Object.keys(table)[0];
        if (key === 'payroll_setup') {
            const setup = constants[`${category}`].payrollSetup
            let newScreen = [];
            for (let one of table[key]) {
                newScreen.push(getter(setup, one))
            }
            table[key] = newScreen;
        } else if (key === 'parent_table') {
            const setup = constants[`${category}`].parent
            let newParents = [];
            for (let one of table[key]) {
                newParents.push(getter(setup, one))
            }
            table[key] = newParents
        } else if (key === 'child_table') {
            const setup = constants[`${category}`].child
            let newParents = [];
            for (let one of table[key]) {
                newParents.push(getter(setup, one))
            }
            table[key] = newParents
        } else if (key === 'change_log') {
            const setup = constants.changeLog
            let newParents = [];
            for (let one of table[key]) {
                newParents.push(getter(setup, one))
            }
            table[key] = newParents
        } else if (key === 'headers') {
            const setup = constants.headers
            let newParents = [];
            for (let one of table[key]) {
                newParents.push(getter(setup, one))
            }
            table[key] = newParents
        }
        if (search)
            return table;
    } catch (e) {
        throw e
    }
}

getter = function (req, data) {
    let obj = {};
    Object.keys(data).forEach(one => {
        const k = Object.keys(req).find(key => req[key] === one);
        if (k)
            obj[k] = data[one]
    })
    return obj;
}