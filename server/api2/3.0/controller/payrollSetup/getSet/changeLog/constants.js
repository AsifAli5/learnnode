/**
 * @Author: Ismail Raza
 * @Date: 15-03-2021
 */

module.exports = {
    'id': 'change_log_id',
    'setup_id': 'payroll_setup_id',
    'user': 'created_by',
    'change_type': 'change_type',
    'province': 'state_id',
    'old_data': 'old_data',
    'changed_data': 'new_data',
    'changed_on': 'created_at',
    'deleted': 'is_deleted',
    'terminal_detail_id': 'terminal_statutory_setup_detail_id',
    'compensation_detail_id': 'allowances_deduction_setup_detail_id'
}