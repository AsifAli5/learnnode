/**
 * @Author: Ismail Raza
 * @Date: 08-03-2021
 */

const getter = require('./getter')

exports.formatDataToFetch = async (category, data, search = false) => {
    try {
        if (search)
            for (let table of data) {
                return await getter(table, category, search)
            }
        else
            for (let table of data) {
                await getter(table, category)
            }
    } catch (e) {
        throw e
    }
}