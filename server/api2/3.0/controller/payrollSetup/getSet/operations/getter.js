/**
 * @Author: Ismail Raza
 * @Date: 15-03-2021
 */

const constants = require('./constantsObj');

module.exports = async (table, category, search = false) => {
    try {
        const key = Object.keys(table)[0];
        if (key === 'payroll_setup') {
            const setup = constants[`${category}`].payrollSetup
            let newScreen = [];
            for (let one of table[key]) {
                newScreen.push(getter(setup, one))
            }
            table[key] = newScreen;
        } else if (key === 'parent_table') {
            const setup = constants[`${category}`].parent
            let newParents = [];
            for (let one of table[key]) {
                newParents.push(getter(setup, one))
            }
            table[key] = newParents
        } else if (key === 'child_table') {
            const setup = constants[`${category}`].child
            let newParents = [];
            for (let one of table[key]) {
                newParents.push(getter(setup, one))
            }
            table[key] = newParents
        } else if (key === 'change_log') {
            const setup = constants.changeLog
            let newParents = [];
            for (let one of table[key]) {
                newParents.push(getter(setup, one))
            }
            table[key] = newParents
        } else if (key === 'headers') {
            const setup = constants.headers
            let newParents = [];
            for (let one of table[key]) {
                newParents.push(getter(setup, one))
            }
            table[key] = newParents
        }
        if (search)
            return table;
    } catch (e) {
        throw e
    }
}

getter = function (req, data) {
    let obj = {};
    Object.keys(data).forEach(one => {
        const k = Object.keys(req).find(key => req[key] === one);
        if (k)
            obj[k] = data[one]
    })
    return obj;
}