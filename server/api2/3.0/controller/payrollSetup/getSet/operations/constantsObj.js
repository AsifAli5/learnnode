/**
 * @Author: Ismail Raza
 * @Date: 03-04-2021
 */

const constants = {}
constants.Salary_Income_Tax = require('../statutorySetup/salaryIncomeTax/constants')
constants.Consultancy_Income_Tax = require('../statutorySetup/consltancyIncomeTax/constants')
constants.Social_Security = require('../statutorySetup/socialSecurity/constants')
constants.Education_Cess = require('../statutorySetup/educationCess/constants')
constants.EOBI = require('../statutorySetup/EOBI/constants')
constants.Variable_Allowance = require('../compensationSetup/variableAllowance/constants')
constants.Deductions = require('../compensationSetup/deduction/constatns')
constants.Salary_Component = require('../compensationSetup/salaryComponent/constants')
constants.Overtime = require('../compensationSetup/overtime/constants')
constants.Provident_Fund = require('../terminalBenefits/providentFunds/constants')
constants.Pension_Fund = require('../terminalBenefits/pensionFund/constants')
constants.Gratuity = require('../terminalBenefits/gratuity/constants')
constants.Group_Life_Insurance = require('../statutorySetup/groupLifeInsurance/constants')
constants.Group_Health_Insurance = require('../insurance/constants')
constants.GHI_Maternity = require('../insurance/constants')
constants.Cash_in_Transit = require('../insurance/constants')
constants.Contractual_Liability = require('../insurance/constants')

module.exports = constants