/**
 * @Author: Ismail Raza
 * @Date: 15-03-2021
 */

const constants = require('./constantsObj');

module.exports = async (table, category, search = false) => {
    try {
        Object.keys(table).forEach(key => {
            if (key === 'payroll_setup') {
                table[key] = setKeys(table[key], constants[`${category}`].payrollSetup)
            } else if (key === 'parent_table') {
                table[key] = setKeys(table[key], constants[`${category}`].parent)
            } else if (key === 'child_table') {
                let newScreen = [];
                for (let one of table[key]) {
                    newScreen.push(setKeys(one, constants[`${category}`].child))
                }
                table[key] = newScreen;
            }
        })
        if (search)
            return table
    } catch (exception) {
        throw exception;
    }
}


setKeys = (obj, constants) => {
    let newObj = {};
    for (let key in obj) {
        if (obj[key])
            newObj[constants[key]] = obj[key];
    }
    return newObj;
}