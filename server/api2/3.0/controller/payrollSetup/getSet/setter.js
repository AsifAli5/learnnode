/**
 * @Author: Ismail Raza
 * @Date: 15-03-2021
 */

const constants = {}
constants.Salary_Income_Tax = require('./statutorySetup/salaryIncomeTax/constants')
constants.Consultancy_Income_Tax = require('./statutorySetup/consltancyIncomeTax/constants')
constants.Social_Security = require('./statutorySetup/socialSecurity/constants')
constants.Education_Cess = require('./statutorySetup/educationCess/constants')
constants.EOBI = require('./statutorySetup/EOBI/constants')
constants.Variable_Allowance = require('./compensationSetup/variableAllowance/constants')
constants.Deductions = require('./compensationSetup/deduction/constatns')
constants.Salary_Component = require('./compensationSetup/salaryComponent/constants')
constants.Overtime = require('./compensationSetup/overtime/constants')
constants.Provident_Fund = require('./terminalBenefits/providentFunds/constants')
constants.Pension_Fund = require('./terminalBenefits/pensionFund/constants')
constants.Gratuity = require('./terminalBenefits/gratuity/constants')

module.exports = async (table, category, search = false) => {
    try {
        Object.keys(table).forEach(key => {
            if (key === 'payroll_setup') {
                table[key] = setKeys(table[key], constants[`${category}`].payrollSetup)
            } else if (key === 'parent_table') {
                table[key] = setKeys(table[key], constants[`${category}`].parent)
            } else if (key === 'child_table') {
                let newScreen = [];
                for (let one of table[key]) {
                    newScreen.push(setKeys(one, constants[`${category}`].child))
                }
                table[key] = newScreen;
            }
        })
        if (search)
            return table
    } catch (exception) {
        throw exception;
    }
}


setKeys = (obj, constants) => {
    let newObj = {};
    for (let key in obj) {
        newObj[constants[key]] = obj[key];
    }
    return newObj;
}