/**
 * @Author: Ismail Raza
 * @Date: 08-03-2021
 */
const setter = require('./setter')

exports.formatDataToSave = async (category, data) => {
    await setter(data, category)
}

exports.formatDataToSearch = async (category, data) => await setter(data, category, true);
