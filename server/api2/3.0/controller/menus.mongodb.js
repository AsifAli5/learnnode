const {
    MenusCacheSchema
} = require('../model/menus');

exports.getMenusCache = (req, res) => {
    const uid = req.params.uid;
    console.log(uid);

    MenusCacheSchema.find({
        // userid: uid
    }, (err, docs) => {
        console.log(docs);
        console.log(err);
        if (!err) {
            return res.status(400).send(err);
        } else {
            return res.send(docs);
        }
    })
}

exports.creatMenusCache = (req, res) => {
    const body = req.body;

    MenusCacheSchema.create(body)
        .then(resp => {
            return res.send(resp);
        }).catch((error) => {
            return res.status(400).send(error);
        });
}

exports.updateMenusCache = (req, res) => {
    const body = req.body;

    MenusCacheSchema.findOneAndUpdate({
            userid: body.uid
        }, body.updateObject)
        .then(resp => {
            return res.send(resp);
        }).catch((error) => {
            return res.status(400).send(error);
        });
}
