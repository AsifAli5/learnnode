const mongoose = require('mongoose');

// defining schema variable

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var employeeSchema = new Schema({
    id: ObjectId,
    employeeId : Number,
    token :  String
});

module.exports = mongoose.model('employee', employeeSchema);