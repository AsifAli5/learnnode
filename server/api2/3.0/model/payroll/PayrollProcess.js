var stringCheck = require("../../../../utils/StringCheck");
const config = require("../../../../../config");
const queryObj = require("../../helper/query");
var Database = require("../../../../config/database");
const {concatAll} = require("rxjs-compat/operator/concatAll");
var db = new Database();

class PayrollProcess {

    logArray = [];
    dbConnection = '';
    totalInsertedRecords = 0;
    totalUpdatedRecords = 0;
    emfFileInfoId = 0;
    empData = {
        rowId: 0,
        grossSalary: 0,
        employeeId: 0,
        employmentType: 0,
        departmentId: 0,
        bandId: 0,
        regionId: 0,
        stateId: 0,
        baseSalary: 0,
        basicSalary: 0,
        empName: '',
        empOvertimeType: '',
        empOvertimeHour: 0
    }

    constructor(conn) {
        this.dbConnection = conn;
    }

    async getEmfData(conn) {
        let emfQuery = `SELECT * FROM cb_payroll_emf WHERE payroll_emf_file_info_id = '1'`;
        return await queryObj.selectQuery(emfQuery, conn);
    }

    async insertEmfData(conn, emfResult) {
        emfResult = JSON.parse(JSON.stringify(emfResult));
        let dataLength = emfResult.length;
        if (dataLength > 0) {
            for (let index = 0; index < dataLength; index++) {
                let dataObj = emfResult[index];
                let emfId = dataObj.payroll_emf_id;
                let empCode = dataObj.people_code;
                let id = await this.checkEmfDataInPayroll(conn, emfId);
                if (id > 0) {
                    await this.payrollProcessUpdateObj(conn, dataObj);
                } else {
                    let insertId = await this.payrollProcessInsertObj(conn, dataObj);
                    if (insertId > 0) {
                        await this.updateEmployeeId(conn, insertId, empCode);
                    }
                }
            }
            let result = {
                total_inserted: this.totalInsertedRecords,
                total_updated: this.totalUpdatedRecords
            };
            return result;
        } else {
            let error = {
                error: 'Object length is zero.'
            };
            return error;
        }
    }

    async checkEmfDataInPayroll(conn, emfId) {
        let payrollQuery = `SELECT payroll_process_id FROM cb_payroll_process WHERE payroll_emf_id = '${emfId}'`;
        let data = await queryObj.selectQuery(payrollQuery, conn);
        if (data.length > 0) {
            return data[0].payroll_process_id;
        } else {
            return false;
        }
    }

    async payrollProcessInsertObj(conn, dataObj) {
        let insertQuery = `INSERT INTO cb_payroll_process (
        payroll_emf_id, payroll_emf_file_info_id, people_code, emp_id, basic_salary, housing_allow, utility_allow, medical_allow, car_allow, fuel_allow, mobile_allow, 
        food_allow, special_allow, school_allow, hardship_allow, transporation_allow, representation_allow, assignment_allow, security_allow, pension_allow, seniority_allow, general_fuel_allow, 
        electricity_allow, water_allow, domestic_allow, family_allow, furnishing_allow, t_e_allow, leave_allow, performance_allow, internet_allow, pam_allow, standby_allow, evening_allow, 
        gazetted_allow, abaya_allow, relationship_allow, bdu_allow, cf_fuel_allow, mobility_relocation_allow, dsa_allow, sra_allow, washing_allow, hajj_allow, retainer_allow, site_allow, 
        dearness_allow, other_allow, sunday_allow, saturday_allow, last_month_overtime_hours, last_month_overtime_amount, overtime_hours, overtime_amount, Arrears, Other_Additions, other_incentive, 
        collection_incentive, recovery_incentive, lsf_incentive, referral_incentive_for_disbursement, prepaid_card_incentive, kpi_incenitve, inbound_incentive, autos_incentive, 
        banca_scorecard_disbursement, bancassurance, reward, agri_fuel, phone_banking, rrm, pil, btf, barkat_auto_finance, barkat_housing_finance, mutual_fund_incentive, 
        rush_hour_campaign_winners_award, turbo_charge_sales_commission, cash_plus_commission, credit_card_commission, auto_commission, uad_commission, casa_commission, _13th_month_Pay, 
        bonus, referral_bonus, statutory_bonus, aip_bonus, laip_bonus, Inflation_Bonus, Utilization_Bonus, Goal_Achived_Bonus, Other_Bonus_Eid_Bonus, reimbursement, 
        maternity_reimbursment, airticket, transportation_travel_expense, opd_medical_expense, fuel_reimbursement, ta_da, mobile_bill_expense, meal_expense, commuting_mileage, PF_Withholdings, 
        vehicle_of_service, house_rent_advance, leave_salary_holidays_pay, loan_claim, security_deposit_return, Extra_Already_Paid, Extra_Not_Paid, Residency_, Mobile_Payment, Gratification, 
        Income_Tax_Refund, gratuity, end_of_service, notice_pay, exgratia_payment, severance_pay, leave_encashment, Out_of_Payroll_Mobile, Out_of_Payroll_LAIP, Out_of_Payroll_Others, 
        Out_of_Payroll_Relocation, Taxable_Benefit_Amount, Non_Cash_Taxable_Amount, lwop_days, lwop_deduction, other_deduction_from_gross, shortage_deduction, income_tax, l_s_t_deduction, 
        tax_adjustment, advance_leave_salary_deduction, recovery, other_deduction, gosi_deduction, pension_contribution_employee, medical_insurance_deduction, nssf_cnss, 
        provident_fund_contribution_employee, pf_arrears, eobi_deduction, eobi_arrears_deduction, advance_salary_loan_deduction, laundry_charges_deduction, extra_food_deduction, 
        excess_mobile_deduction, unserved_notice_period, hra_adjustments, espp_contribution, housing_loan_deduction, credit_union, data_bank_deduction, loan_repayment, nhif, 
        coop_contribution, coop_loan_repayment, christmas_bonus, utilities_electricity_security, travel_advance_recovery, education_support_deduction, helb_loan, kcb_loan_check_off, 
        scb_loan_check_off, bbk_loan_check_off, sacco_bonus_sch, ngid, safaricom_investment, nhf, sacco_deduction, gli_deduction, ghi_deduction, security_deposit_from_employee, 
        disbursement_deduction_from_employee, breakage_snatch_deduction, security_deduction_from_employee, id_card_deduction, uniform_deduction, penalty_deduction, commuting_deduction, 
        CLI_Deduction, PF_Loan_Ded, Online_Charges, BRC_Ded, emergency_fund, pension_contribution_employer, provident_fund_contribution_employer, PF_Arrears_Employer, eobi_contribution, 
        eobi_arrears_contribution, professional_fees, abattement, nef, nsitf, itf, wcf, sdl, training_tax_apprendiship_tax_fdfp, regime_general, expatries, retraite, accident_travail, 
        prest_familiale, social_security, social_security_arrears, education_cess, education_cess_arrears, gli_contritbution, gli_arrears, ghi_contribution, ghi_arrears, cli, cli_arrears, 
        cti, cti_arrears, gratuity_claimed, gratuity_claimed_arrears, pension_fund_contribution, laptop_security, uniform_charges, Bank_Charges_From_Client, CNIC_Verification, ID_Card_Charges, 
        Laptop_Charges, Executive_Search, Service_Charges, Sales_Tax, Tax_Add_Back_WHT, status, iteration, created_by, updated_by, created_at, updated_at
        ) VALUES (
        '${dataObj.payroll_emf_id}','${dataObj.payroll_emf_file_info_id}','${dataObj.people_code}','${dataObj.emp_id}','${dataObj.basic_salary}','${dataObj.housing_allow}',
        '${dataObj.utility_allow}','${dataObj.medical_allow}','${dataObj.car_allow}','${dataObj.fuel_allow}','${dataObj.mobile_allow}','${dataObj.food_allow}','${dataObj.special_allow}','${dataObj.school_allow}',
        '${dataObj.hardship_allow}','${dataObj.transporation_allow}','${dataObj.representation_allow}','${dataObj.assignment_allow}','${dataObj.security_allow}','${dataObj.pension_allow}',
        '${dataObj.seniority_allow}','${dataObj.general_fuel_allow}','${dataObj.electricity_allow}','${dataObj.water_allow}','${dataObj.domestic_allow}','${dataObj.family_allow}','${dataObj.furnishing_allow}',
        '${dataObj.t_e_allow}','${dataObj.leave_allow}','${dataObj.performance_allow}','${dataObj.internet_allow}','${dataObj.pam_allow}','${dataObj.standby_allow}','${dataObj.evening_allow}',
        '${dataObj.gazetted_allow}','${dataObj.abaya_allow}','${dataObj.relationship_allow}','${dataObj.bdu_allow}','${dataObj.cf_fuel_allow}','${dataObj.mobility_relocation_allow}','${dataObj.dsa_allow}',
        '${dataObj.sra_allow}','${dataObj.washing_allow}','${dataObj.hajj_allow}','${dataObj.retainer_allow}','${dataObj.site_allow}','${dataObj.dearness_allow}','${dataObj.other_allow}',
        '${dataObj.sunday_allow}','${dataObj.saturday_allow}','${dataObj.last_month_overtime_hours}','${dataObj.last_month_overtime_amount}','${dataObj.overtime_hours}','${dataObj.overtime_amount}',
        '${dataObj.Arrears}','${dataObj.Other_Additions}','${dataObj.other_incentive}','${dataObj.collection_incentive}','${dataObj.recovery_incentive}','${dataObj.lsf_incentive}','${dataObj.referral_incentive_for_disbursement}',
        '${dataObj.prepaid_card_incentive}','${dataObj.kpi_incenitve}','${dataObj.inbound_incentive}','${dataObj.autos_incentive}','${dataObj.banca_scorecard_disbursement}','${dataObj.bancassurance}',
        '${dataObj.reward}','${dataObj.agri_fuel}','${dataObj.phone_banking}','${dataObj.rrm}','${dataObj.pil}','${dataObj.btf}','${dataObj.barkat_auto_finance}','${dataObj.barkat_housing_finance}',
        '${dataObj.mutual_fund_incentive}','${dataObj.rush_hour_campaign_winners_award}','${dataObj.turbo_charge_sales_commission}','${dataObj.cash_plus_commission}','${dataObj.credit_card_commission}',
        '${dataObj.auto_commission}','${dataObj.uad_commission}','${dataObj.casa_commission}','${dataObj._13th_month_Pay}','${dataObj.bonus}','${dataObj.referral_bonus}','${dataObj.statutory_bonus}',
        '${dataObj.aip_bonus}','${dataObj.laip_bonus}','${dataObj.Inflation_Bonus}','${dataObj.Utilization_Bonus}','${dataObj.Goal_Achived_Bonus}','${dataObj.Other_Bonus_Eid_Bonus}',
        '${dataObj.reimbursement}','${dataObj.maternity_reimbursment}','${dataObj.airticket}','${dataObj.transportation_travel_expense}','${dataObj.opd_medical_expense}','${dataObj.fuel_reimbursement}',
        '${dataObj.ta_da}','${dataObj.mobile_bill_expense}','${dataObj.meal_expense}','${dataObj.commuting_mileage}','${dataObj.PF_Withholdings}','${dataObj.vehicle_of_service}','${dataObj.house_rent_advance}',
        '${dataObj.leave_salary_holidays_pay}','${dataObj.loan_claim}','${dataObj.security_deposit_return}','${dataObj.Extra_Already_Paid}','${dataObj.Extra_Not_Paid}','${dataObj.Residency_}',
        '${dataObj.Mobile_Payment}','${dataObj.Gratification}','${dataObj.Income_Tax_Refund}','${dataObj.gratuity}','${dataObj.end_of_service}','${dataObj.notice_pay}','${dataObj.exgratia_payment}',
        '${dataObj.severance_pay}','${dataObj.leave_encashment}','${dataObj.Out_of_Payroll_Mobile}','${dataObj.Out_of_Payroll_LAIP}','${dataObj.Out_of_Payroll_Others}','${dataObj.Out_of_Payroll_Relocation}',
        '${dataObj.Taxable_Benefit_Amount}','${dataObj.Non_Cash_Taxable_Amount}','${dataObj.lwop_days}','${dataObj.lwop_deduction}','${dataObj.other_deduction_from_gross}','${dataObj.shortage_deduction}',
        '${dataObj.income_tax}','${dataObj.l_s_t_deduction}','${dataObj.tax_adjustment}','${dataObj.advance_leave_salary_deduction}','${dataObj.recovery}','${dataObj.other_deduction}','${dataObj.gosi_deduction}',
        '${dataObj.pension_contribution_employee}','${dataObj.medical_insurance_deduction}','${dataObj.nssf_cnss}','${dataObj.provident_fund_contribution_employee}','${dataObj.pf_arrears}','${dataObj.eobi_deduction}',
        '${dataObj.eobi_arrears_deduction}','${dataObj.advance_salary_loan_deduction}','${dataObj.laundry_charges_deduction}','${dataObj.extra_food_deduction}','${dataObj.excess_mobile_deduction}',
        '${dataObj.unserved_notice_period}','${dataObj.hra_adjustments}','${dataObj.espp_contribution}','${dataObj.housing_loan_deduction}','${dataObj.credit_union}','${dataObj.data_bank_deduction}',
        '${dataObj.loan_repayment}','${dataObj.nhif}','${dataObj.coop_contribution}','${dataObj.coop_loan_repayment}','${dataObj.christmas_bonus}','${dataObj.utilities_electricity_security}','${dataObj.travel_advance_recovery}',
        '${dataObj.education_support_deduction}','${dataObj.helb_loan}','${dataObj.kcb_loan_check_off}','${dataObj.scb_loan_check_off}','${dataObj.bbk_loan_check_off}','${dataObj.sacco_bonus_sch}',
        '${dataObj.ngid}','${dataObj.safaricom_investment}','${dataObj.nhf}','${dataObj.sacco_deduction}','${dataObj.gli_deduction}','${dataObj.ghi_deduction}','${dataObj.security_deposit_from_employee}',
        '${dataObj.disbursement_deduction_from_employee}','${dataObj.breakage_snatch_deduction}','${dataObj.security_deduction_from_employee}','${dataObj.id_card_deduction}','${dataObj.uniform_deduction}',
        '${dataObj.penalty_deduction}','${dataObj.commuting_deduction}','${dataObj.CLI_Deduction}','${dataObj.PF_Loan_Ded}','${dataObj.Online_Charges}','${dataObj.BRC_Ded}','${dataObj.emergency_fund}',
        '${dataObj.pension_contribution_employer}','${dataObj.provident_fund_contribution_employer}','${dataObj.PF_Arrears_Employer}','${dataObj.eobi_contribution}','${dataObj.eobi_arrears_contribution}',
        '${dataObj.professional_fees}','${dataObj.abattement}','${dataObj.nef}','${dataObj.nsitf}','${dataObj.itf}','${dataObj.wcf}','${dataObj.sdl}','${dataObj.training_tax_apprendiship_tax_fdfp}',
        '${dataObj.regime_general}','${dataObj.expatries}','${dataObj.retraite}','${dataObj.accident_travail}','${dataObj.prest_familiale}','${dataObj.social_security}','${dataObj.social_security_arrears}',
        '${dataObj.education_cess}','${dataObj.education_cess_arrears}','${dataObj.gli_contritbution}','${dataObj.gli_arrears}','${dataObj.ghi_contribution}','${dataObj.ghi_arrears}','${dataObj.cli}',
        '${dataObj.cli_arrears}','${dataObj.cti}','${dataObj.cti_arrears}','${dataObj.gratuity_claimed}','${dataObj.gratuity_claimed_arrears}','${dataObj.pension_fund_contribution}','${dataObj.laptop_security}',
        '${dataObj.uniform_charges}','${dataObj.Bank_Charges_From_Client}','${dataObj.CNIC_Verification}','${dataObj.ID_Card_Charges}','${dataObj.Laptop_Charges}','${dataObj.Executive_Search}',
        '${dataObj.Service_Charges}','${dataObj.Sales_Tax}','${dataObj.Tax_Add_Back_WHT}','${dataObj.status}','${dataObj.iteration}','${dataObj.created_by}','${dataObj.updated_by}', NOW(), NOW()
        )`;
        let dbResponse = await queryObj.executeInsertQuery(insertQuery, conn);
        let length = Object.keys(dbResponse).length;
        if (length > 0 && dbResponse.affectedRows == 1) {
            this.totalInsertedRecords++;
            return dbResponse.insertId;
        } else {
            return 0;
        }
    }

    async payrollProcessUpdateObj(conn, dataObj) {
        let updateQuery = `UPDATE cb_payroll_process SET
        basic_salary = '${dataObj.basic_salary}',
        housing_allow = '${dataObj.housing_allow}',
        utility_allow = '${dataObj.utility_allow}',
        medical_allow = '${dataObj.medical_allow}',
        car_allow = '${dataObj.car_allow}',
        fuel_allow = '${dataObj.fuel_allow}',
        mobile_allow = '${dataObj.mobile_allow}',
        food_allow = '${dataObj.food_allow}',
        special_allow = '${dataObj.special_allow}',
        school_allow = '${dataObj.school_allow}',
        hardship_allow = '${dataObj.hardship_allow}',
        transporation_allow = '${dataObj.transporation_allow}',
        representation_allow = '${dataObj.representation_allow}',
        assignment_allow = '${dataObj.assignment_allow}',
        security_allow = '${dataObj.security_allow}',
        pension_allow = '${dataObj.pension_allow}',
        seniority_allow = '${dataObj.seniority_allow}',
        general_fuel_allow = '${dataObj.general_fuel_allow}',
        electricity_allow = '${dataObj.electricity_allow}',
        water_allow = '${dataObj.water_allow}',
        domestic_allow = '${dataObj.domestic_allow}',
        family_allow = '${dataObj.family_allow}',
        furnishing_allow = '${dataObj.furnishing_allow}',
        t_e_allow = '${dataObj.t_e_allow}',
        leave_allow = '${dataObj.leave_allow}',
        performance_allow = '${dataObj.performance_allow}',
        internet_allow = '${dataObj.internet_allow}',
        pam_allow = '${dataObj.pam_allow}',
        standby_allow = '${dataObj.standby_allow}',
        evening_allow = '${dataObj.evening_allow}',
        gazetted_allow = '${dataObj.gazetted_allow}',
        abaya_allow = '${dataObj.abaya_allow}',
        relationship_allow = '${dataObj.relationship_allow}',
        bdu_allow = '${dataObj.bdu_allow}',
        cf_fuel_allow = '${dataObj.cf_fuel_allow}',
        mobility_relocation_allow = '${dataObj.mobility_relocation_allow}',
        dsa_allow = '${dataObj.dsa_allow}',
        sra_allow = '${dataObj.sra_allow}',
        washing_allow = '${dataObj.washing_allow}',
        hajj_allow = '${dataObj.hajj_allow}',
        retainer_allow = '${dataObj.retainer_allow}',
        site_allow = '${dataObj.site_allow}',
        dearness_allow = '${dataObj.dearness_allow}',
        other_allow = '${dataObj.other_allow}',
        sunday_allow = '${dataObj.sunday_allow}',
        saturday_allow = '${dataObj.saturday_allow}',
        last_month_overtime_hours = '${dataObj.last_month_overtime_hours}',
        last_month_overtime_amount = '${dataObj.last_month_overtime_amount}',
        overtime_hours = '${dataObj.overtime_hours}',
        overtime_amount = '${dataObj.overtime_amount}',
        Arrears = '${dataObj.Arrears}',
        Other_Additions = '${dataObj.Other_Additions}',
        other_incentive = '${dataObj.other_incentive}',
        collection_incentive = '${dataObj.collection_incentive}',
        recovery_incentive = '${dataObj.recovery_incentive}',
        lsf_incentive = '${dataObj.lsf_incentive}',
        referral_incentive_for_disbursement = '${dataObj.referral_incentive_for_disbursement}',
        prepaid_card_incentive = '${dataObj.prepaid_card_incentive}',
        kpi_incenitve = '${dataObj.kpi_incenitve}',
        inbound_incentive = '${dataObj.inbound_incentive}',
        autos_incentive = '${dataObj.autos_incentive}',
        banca_scorecard_disbursement = '${dataObj.banca_scorecard_disbursement}',
        bancassurance = '${dataObj.bancassurance}',
        reward = '${dataObj.reward}',
        agri_fuel = '${dataObj.agri_fuel}',
        phone_banking = '${dataObj.phone_banking}',
        rrm = '${dataObj.rrm}',
        pil = '${dataObj.pil}',
        btf = '${dataObj.btf}',
        barkat_auto_finance = '${dataObj.barkat_auto_finance}',
        barkat_housing_finance = '${dataObj.barkat_housing_finance}',
        mutual_fund_incentive = '${dataObj.mutual_fund_incentive}',
        rush_hour_campaign_winners_award = '${dataObj.rush_hour_campaign_winners_award}',
        turbo_charge_sales_commission = '${dataObj.turbo_charge_sales_commission}',
        cash_plus_commission = '${dataObj.cash_plus_commission}',
        credit_card_commission = '${dataObj.credit_card_commission}',
        auto_commission = '${dataObj.auto_commission}',
        uad_commission = '${dataObj.uad_commission}',
        casa_commission = '${dataObj.casa_commission}',
        _13th_month_Pay = '${dataObj._13th_month_Pay}',
        bonus = '${dataObj.bonus}',
        referral_bonus = '${dataObj.referral_bonus}',
        statutory_bonus = '${dataObj.statutory_bonus}',
        aip_bonus = '${dataObj.aip_bonus}',
        laip_bonus = '${dataObj.laip_bonus}',
        Inflation_Bonus = '${dataObj.Inflation_Bonus}',
        Utilization_Bonus = '${dataObj.Utilization_Bonus}',
        Goal_Achived_Bonus = '${dataObj.Goal_Achived_Bonus}',
        Other_Bonus_Eid_Bonus = '${dataObj.Other_Bonus_Eid_Bonus}',
        reimbursement = '${dataObj.reimbursement}',
        maternity_reimbursment = '${dataObj.maternity_reimbursment}',
        airticket = '${dataObj.airticket}',
        transportation_travel_expense = '${dataObj.transportation_travel_expense}',
        opd_medical_expense = '${dataObj.opd_medical_expense}',
        fuel_reimbursement = '${dataObj.fuel_reimbursement}',
        ta_da = '${dataObj.ta_da}',
        mobile_bill_expense = '${dataObj.mobile_bill_expense}',
        meal_expense = '${dataObj.meal_expense}',
        commuting_mileage = '${dataObj.commuting_mileage}',
        PF_Withholdings = '${dataObj.PF_Withholdings}',
        vehicle_of_service = '${dataObj.vehicle_of_service}',
        house_rent_advance = '${dataObj.house_rent_advance}',
        leave_salary_holidays_pay = '${dataObj.leave_salary_holidays_pay}',
        loan_claim = '${dataObj.loan_claim}',
        security_deposit_return = '${dataObj.security_deposit_return}',
        Extra_Already_Paid = '${dataObj.Extra_Already_Paid}',
        Extra_Not_Paid = '${dataObj.Extra_Not_Paid}',
        Residency_ = '${dataObj.Residency_}',
        Mobile_Payment = '${dataObj.Mobile_Payment}',
        Gratification = '${dataObj.Gratification}',
        Income_Tax_Refund = '${dataObj.Income_Tax_Refund}',
        gratuity = '${dataObj.gratuity}',
        end_of_service = '${dataObj.end_of_service}',
        notice_pay = '${dataObj.notice_pay}',
        exgratia_payment = '${dataObj.exgratia_payment}',
        severance_pay = '${dataObj.severance_pay}',
        leave_encashment = '${dataObj.leave_encashment}',
        Out_of_Payroll_Mobile = '${dataObj.Out_of_Payroll_Mobile}',
        Out_of_Payroll_LAIP = '${dataObj.Out_of_Payroll_LAIP}',
        Out_of_Payroll_Others = '${dataObj.Out_of_Payroll_Others}',
        Out_of_Payroll_Relocation = '${dataObj.Out_of_Payroll_Relocation}',
        Taxable_Benefit_Amount = '${dataObj.Taxable_Benefit_Amount}',
        Non_Cash_Taxable_Amount = '${dataObj.Non_Cash_Taxable_Amount}',
        lwop_days = '${dataObj.lwop_days}',
        lwop_deduction = '${dataObj.lwop_deduction}',
        other_deduction_from_gross = '${dataObj.other_deduction_from_gross}',
        shortage_deduction = '${dataObj.shortage_deduction}',
        income_tax = '${dataObj.income_tax}',
        l_s_t_deduction = '${dataObj.l_s_t_deduction}',
        tax_adjustment = '${dataObj.tax_adjustment}',
        advance_leave_salary_deduction = '${dataObj.advance_leave_salary_deduction}',
        recovery = '${dataObj.recovery}',
        other_deduction = '${dataObj.other_deduction}',
        gosi_deduction = '${dataObj.gosi_deduction}',
        pension_contribution_employee = '${dataObj.pension_contribution_employee}',
        medical_insurance_deduction = '${dataObj.medical_insurance_deduction}',
        nssf_cnss = '${dataObj.nssf_cnss}',
        provident_fund_contribution_employee = '${dataObj.provident_fund_contribution_employee}',
        pf_arrears = '${dataObj.pf_arrears}',
        eobi_deduction = '${dataObj.eobi_deduction}',
        eobi_arrears_deduction = '${dataObj.eobi_arrears_deduction}',
        advance_salary_loan_deduction = '${dataObj.advance_salary_loan_deduction}',
        laundry_charges_deduction = '${dataObj.laundry_charges_deduction}',
        extra_food_deduction = '${dataObj.extra_food_deduction}',
        excess_mobile_deduction = '${dataObj.excess_mobile_deduction}',
        unserved_notice_period = '${dataObj.unserved_notice_period}',
        hra_adjustments = '${dataObj.hra_adjustments}',
        espp_contribution = '${dataObj.espp_contribution}',
        housing_loan_deduction = '${dataObj.housing_loan_deduction}',
        credit_union = '${dataObj.credit_union}',
        data_bank_deduction = '${dataObj.data_bank_deduction}',
        loan_repayment = '${dataObj.loan_repayment}',
        nhif = '${dataObj.nhif}',
        coop_contribution = '${dataObj.coop_contribution}',
        coop_loan_repayment = '${dataObj.coop_loan_repayment}',
        christmas_bonus = '${dataObj.christmas_bonus}',
        utilities_electricity_security = '${dataObj.utilities_electricity_security}',
        travel_advance_recovery = '${dataObj.travel_advance_recovery}',
        education_support_deduction = '${dataObj.education_support_deduction}',
        helb_loan = '${dataObj.helb_loan}',
        kcb_loan_check_off = '${dataObj.kcb_loan_check_off}',
        scb_loan_check_off = '${dataObj.scb_loan_check_off}',
        bbk_loan_check_off = '${dataObj.bbk_loan_check_off}',
        sacco_bonus_sch = '${dataObj.sacco_bonus_sch}',
        ngid = '${dataObj.ngid}',
        safaricom_investment = '${dataObj.safaricom_investment}',
        nhf = '${dataObj.nhf}',
        sacco_deduction = '${dataObj.sacco_deduction}',
        gli_deduction = '${dataObj.gli_deduction}',
        ghi_deduction = '${dataObj.ghi_deduction}',
        security_deposit_from_employee = '${dataObj.security_deposit_from_employee}',
        disbursement_deduction_from_employee = '${dataObj.disbursement_deduction_from_employee}',
        breakage_snatch_deduction = '${dataObj.breakage_snatch_deduction}',
        security_deduction_from_employee = '${dataObj.security_deduction_from_employee}',
        id_card_deduction = '${dataObj.id_card_deduction}',
        uniform_deduction = '${dataObj.uniform_deduction}',
        penalty_deduction = '${dataObj.penalty_deduction}',
        commuting_deduction = '${dataObj.commuting_deduction}',
        CLI_Deduction = '${dataObj.CLI_Deduction}',
        PF_Loan_Ded = '${dataObj.PF_Loan_Ded}',
        Online_Charges = '${dataObj.Online_Charges}',
        BRC_Ded = '${dataObj.BRC_Ded}',
        emergency_fund = '${dataObj.emergency_fund}',
        pension_contribution_employer = '${dataObj.pension_contribution_employer}',
        provident_fund_contribution_employer = '${dataObj.provident_fund_contribution_employer}',
        PF_Arrears_Employer = '${dataObj.PF_Arrears_Employer}',
        eobi_contribution = '${dataObj.eobi_contribution}',
        eobi_arrears_contribution = '${dataObj.eobi_arrears_contribution}',
        professional_fees = '${dataObj.professional_fees}',
        abattement = '${dataObj.abattement}',
        nef = '${dataObj.nef}',
        nsitf = '${dataObj.nsitf}',
        itf = '${dataObj.itf}',
        wcf = '${dataObj.wcf}',
        sdl = '${dataObj.sdl}',
        training_tax_apprendiship_tax_fdfp = '${dataObj.training_tax_apprendiship_tax_fdfp}',
        regime_general = '${dataObj.regime_general}',
        expatries = '${dataObj.expatries}',
        retraite = '${dataObj.retraite}',
        accident_travail = '${dataObj.accident_travail}',
        prest_familiale = '${dataObj.prest_familiale}',
        social_security = '${dataObj.social_security}',
        social_security_arrears = '${dataObj.social_security_arrears}',
        education_cess = '${dataObj.education_cess}',
        education_cess_arrears = '${dataObj.education_cess_arrears}',
        gli_contritbution = '${dataObj.gli_contritbution}',
        gli_arrears = '${dataObj.gli_arrears}',
        ghi_contribution = '${dataObj.ghi_contribution}',
        ghi_arrears = '${dataObj.ghi_arrears}',
        cli = '${dataObj.cli}',
        cli_arrears = '${dataObj.cli_arrears}',
        cti = '${dataObj.cti}',
        cti_arrears = '${dataObj.cti_arrears}',
        gratuity_claimed = '${dataObj.gratuity_claimed}',
        gratuity_claimed_arrears = '${dataObj.gratuity_claimed_arrears}',
        pension_fund_contribution = '${dataObj.pension_fund_contribution}',
        laptop_security = '${dataObj.laptop_security}',
        uniform_charges = '${dataObj.uniform_charges}',
        Bank_Charges_From_Client = '${dataObj.Bank_Charges_From_Client}',
        CNIC_Verification = '${dataObj.CNIC_Verification}',
        ID_Card_Charges = '${dataObj.ID_Card_Charges}',
        Laptop_Charges = '${dataObj.Laptop_Charges}',
        Executive_Search = '${dataObj.Executive_Search}',
        Service_Charges = '${dataObj.Service_Charges}',
        Sales_Tax = '${dataObj.Sales_Tax}',
        Tax_Add_Back_WHT = '${dataObj.Tax_Add_Back_WHT}',
        status = '${dataObj.status}',
        iteration = '${dataObj.iteration}',
        created_by = '${dataObj.created_by}',
        updated_by = '${dataObj.updated_by}',
        updated_at = NOW()
        WHERE payroll_emf_id = '${dataObj.payroll_emf_id}'`;
        let dbResponse = await queryObj.executeInsertQuery(updateQuery, conn);
        let length = Object.keys(dbResponse).length;
        if (length > 0 && dbResponse.affectedRows == 1) {
            this.totalUpdatedRecords++;
        }
    }

    async updateEmployeeId(conn, insertId, empCode) {
        let updateEmployeeIdQuery = `UPDATE cb_payroll_process SET proll_employee_id = (SELECT id FROM proll_employee WHERE empcode = '${empCode}') WHERE payroll_process_id = '${insertId}'`;
        await queryObj.executeInsertQuery(updateEmployeeIdQuery, conn);
    }

    async employeeGrossSalary() {
        const source = 'Emp_Profile';
        const salaryView = 'New_Salary';
        try {
            let employeeGrossSalary = `
            SELECT 
                cb_payroll_process.proll_employee_id, 
                proll_employee_salary_summary.gross_salary,
                proll_employee.contract_status,
                proll_employee.dept_id,
                proll_employee.emp_band,
                proll_client_location.country_id,
                cities.state_id,
                proll_employee.name,
                cb_payroll_process.payroll_process_id AS row_id,
                cb_payroll_process.overtime_type,
                cb_payroll_process.overtime_hours
            FROM cb_payroll_process
            LEFT JOIN proll_employee_salary_summary
                ON proll_employee_salary_summary.emp_id = cb_payroll_process.proll_employee_id AND proll_employee_salary_summary.source = '${source}' AND proll_employee_salary_summary.salary_view = '${salaryView}'
            LEFT JOIN proll_employee
                ON cb_payroll_process.proll_employee_id = proll_employee.id
            LEFT JOIN proll_client_location
                ON proll_client_location.loc_id = proll_employee.loc_id
            LEFT JOIN cities 
                ON cities.id = proll_client_location.city_id            
            WHERE 
                cb_payroll_process.payroll_emf_file_info_id = '${this.emfFileInfoId}' AND cb_payroll_process.proll_employee_id > 0
            `;
            return await queryObj.selectQuery(employeeGrossSalary, this.dbConnection);
        } catch (error) {
            return error;
        } finally {
        }
    }

    async compensationSetup(setupType, setupDesc) {
        let salaryComponentSetup = `
        SELECT
            cb_allowances_deduction_setup.region_id,
            cb_allowances_deduction_setup.state_id,
            cb_allowances_deduction_setup.payroll_physical_years_id,  
            cb_payroll_headers.header_key, 
            cb_allowances_deduction_setup_detail.employment_type_id, 
            cb_allowances_deduction_setup_detail.department_id, 
            cb_allowances_deduction_setup_detail.band_id, 
            cb_payroll_calculations_type.match_key AS calculation_type, 
            cb_allowances_deduction_setup_detail.value, 
            cb_allowances_deduction_setup_detail.is_taxable, 
            cb_allowances_deduction_setup_detail.non_avg_tax, 
            cb_allowances_deduction_setup_detail.exclude_for_ss, 
            cb_allowances_deduction_setup_detail.changed_on,
            ct.match_key AS feature,
            cb_allowances_deduction_setup_detail.hrs_per_month, 
            cb_allowances_deduction_setup_detail.ot_rate, 
            cb_allowances_deduction_setup_detail.frequency 
        FROM cb_allowances_deduction_setup
        INNER JOIN cb_payroll_setup 
            ON cb_allowances_deduction_setup.payroll_setup_id = cb_payroll_setup.payroll_setup_id
        INNER JOIN cb_allowances_deduction_setup_detail 
            ON cb_allowances_deduction_setup.allowances_deduction_setup_id = cb_allowances_deduction_setup_detail.allowances_deduction_setup_id
        LEFT JOIN cb_payroll_headers 
            ON cb_allowances_deduction_setup_detail.payroll_headers_id = cb_payroll_headers.payroll_headers_id
        LEFT JOIN proll_department 
            ON cb_allowances_deduction_setup_detail.department_id = proll_department.id
        LEFT JOIN employee_bands 
            ON cb_allowances_deduction_setup_detail.band_id = employee_bands.id
        LEFT JOIN cb_payroll_calculations_type 
            ON cb_allowances_deduction_setup_detail.payroll_calculations_type_id = cb_payroll_calculations_type.payroll_calculations_type_id
        LEFT JOIN cb_payroll_calculations_type AS ct 
            ON cb_allowances_deduction_setup_detail.feature = ct.payroll_calculations_type_id
        LEFT JOIN proll_dates 
            ON proll_dates.id = cb_allowances_deduction_setup.payroll_physical_years_id
        WHERE 
            cb_allowances_deduction_setup.status = '1' AND 
            cb_payroll_setup.setup_type = '${setupType}' AND 
            cb_payroll_setup.setup_desc = '${setupDesc}' AND 
            cb_payroll_setup.status = '1' AND
            proll_dates.status = '1'
        `;
        return await queryObj.selectQuery(salaryComponentSetup, this.dbConnection);
    }

    async generatePayroll(emfFileId) {
        let employeeGrossSalaryData = await this.employeeGrossSalary();
        let salaryDataLength = Object.keys(employeeGrossSalaryData).length;
        switch (true) {
            case (salaryDataLength > 0):
                let salaryComponentData = await this.compensationSetup('Compensation_Setup', 'Salary_Component');
                let variableAllowancesData = await this.compensationSetup('Compensation_Setup', 'Variable_Allowance');
                let OvertimeData = await this.compensationSetup('Compensation_Setup', 'Overtime');
                for (let i = 0; i < salaryDataLength; i++) {
                    await this.getEmpData(employeeGrossSalaryData[i]);
                    await this.compensationDefaultState();
                    switch (true) {
                        case (this.empData.grossSalary > 0):
                            await this.calculateCompensation(salaryComponentData, 'above_base');
                            await this.calculateCompensation(salaryComponentData, 'base_salary');
                            await this.calculateCompensation(variableAllowancesData, 'variable_allowance');
                            await this.calculateCompensation(variableAllowancesData, 'reimbursement');
                            await this.calculateCompensationOvertime(OvertimeData);
                            break;
                        default:
                            this.logArray.push({
                                title: 'Gross Salary',
                                query: `Gross Salary is 0 for employee: ${employeeGrossSalaryData[i].name}. Payroll calculation skipped.`,
                                data: null
                            });

                    }
                }
                break;
            default:
                this.logArray.push('Object "employeeGrossSalaryData" is empty.');
        }
        console.log('--------------------');
    }

    async calculateCompensation(dataObj, method) {
        if (Object.keys(dataObj).length > 0) {
            let totalAboveBase = 0;
            let finalVal = 0;
            for (let i = 0; i < Object.keys(dataObj).length; i++) {
                let regionIdSetup = dataObj[i].region_id;
                let stateIdSetup = dataObj[i].state_id;
                let feature = dataObj[i].feature;
                let calculationType = dataObj[i].calculation_type;
                let value = dataObj[i].value;
                let employmentTypeIdSetup = dataObj[i].employment_type_id;
                let departmentIdSetup = dataObj[i].department_id;
                let bandIdSetup = dataObj[i].band_id;
                let component = dataObj[i].header_key;
                let isTaxable = dataObj[i].is_taxable;
                switch (true) {
                    case (component):
                        switch (true) {
                            case (employmentTypeIdSetup == 0 && departmentIdSetup == 0 && bandIdSetup == 0 && regionIdSetup == 0 && stateIdSetup == 0):
                                finalVal = await this.calculateValues(feature, calculationType, component, value, method);
                                await this.updateTaxableIncome(isTaxable, finalVal, feature, method);
                                await this.updateVariableAllowances(finalVal, feature, component);
                                totalAboveBase += finalVal;
                                break;
                            case ((employmentTypeIdSetup == 0 || employmentTypeIdSetup == this.empData.employmentType) && (departmentIdSetup == 0 || departmentIdSetup == this.empData.departmentId) && (bandIdSetup == 0 || bandIdSetup == this.empData.bandId) && (regionIdSetup == 0 || regionIdSetup == this.empData.regionId) && (stateIdSetup == 0 || stateIdSetup == this.empData.stateId)):
                                finalVal = await this.calculateValues(feature, calculationType, component, value, method);
                                await this.updateTaxableIncome(isTaxable, finalVal, feature, method);
                                await this.updateVariableAllowances(finalVal, feature, component, method);
                                await this.updateTotalReimbursement(finalVal, feature, component, method);
                                totalAboveBase += finalVal;
                                break;
                            default:
                                this.logArray.push({
                                    title: 'Header Key Empty',
                                    query: `Header Key Empty. Payroll calculation skipped`,
                                    data: null
                                });
                        }
                }

                switch (true) {
                    case (method == 'base_salary' && component == 'basic_salary'):
                        this.empData.basicSalary = finalVal;
                        this.logArray.push({
                            title: 'Basic Salary',
                            query: `Basic Salary of employee: ${this.empData.empName} is ${this.empData.basicSalary}`,
                            data: null
                        });
                        break;
                }
            }

            switch (true) {
                case (method == 'above_base'):
                    this.empData.baseSalary = this.empData.grossSalary - totalAboveBase;
                    this.logArray.push({
                        title: 'Base Salary',
                        query: `Base Salary of employee: ${this.empData.empName} is ${this.empData.baseSalary}`,
                        data: null
                    });
                    break;
            }
        }
    }

    async updatePayrollComponent(component, value) {
        let updatePayrollComponentQuery = `UPDATE cb_payroll_process SET ${component} = '${value}' WHERE payroll_process_id = '${this.empData.rowId}'`;
        let data = await queryObj.executeInsertQuery(updatePayrollComponentQuery, this.dbConnection);
        this.logArray.push({
            title: 'Query Executed',
            query: updatePayrollComponentQuery,
            data: data
        });
    }

    async updateTaxableIncome(taxable, value, feature, method) {
        try {
            if (taxable && feature == method) {
                let updateTaxableIncomeQuery = `UPDATE cb_payroll_process SET taxable_income = IFNULL(taxable_income, 0) + ${value} WHERE payroll_process_id = ${this.empData.rowId}`;
                await queryObj.executeInsertQuery(updateTaxableIncomeQuery, this.dbConnection);
                this.logArray.push(`Execute query: ${updateTaxableIncomeQuery}`);
            }
        } catch (error) {
            return error
        } finally {
        }
    }

    async updateVariableAllowances(value, feature, component, method) {
        try {
            if (feature == 'variable_allowance' && method == 'variable_allowance') {
                let updateVariableAllowancesQuery = `UPDATE cb_payroll_process SET variable_allowances = IFNULL(variable_allowances, 0) + ${value} WHERE payroll_process_id = ${this.empData.rowId}`;
                await queryObj.executeInsertQuery(updateVariableAllowancesQuery, this.dbConnection);
                this.logArray.push(`Execute query: ${updateVariableAllowancesQuery}, Component: ${component}, Method: ${method}`);
            }
        } catch (error) {
            return error
        } finally {
        }
    }

    async updateTotalReimbursement(value, feature, component, method) {
        try {
            if (feature == 'reimbursement' && method == 'reimbursement') {
                let updateTotalReimbursementQuery = `UPDATE cb_payroll_process SET total_reimbursement = IFNULL(total_reimbursement, 0) + ${value} WHERE payroll_process_id = ${this.empData.rowId}`;
                await queryObj.executeInsertQuery(updateTotalReimbursementQuery, this.dbConnection);
                this.logArray.push(`Execute query: ${updateTotalReimbursementQuery}, Component: ${component}, Method: ${method}`);
            }
        } catch (error) {
            return error
        } finally {
        }
    }

    async calculateValues(feature, calculationType, component, value, method) {
        switch (true) {
            case (feature == method && calculationType == 'fixed'):
                await this.updatePayrollComponent(component, value);
                break;
            case (feature == method && calculationType == '%_of_gross_salary'):
                value = this.empData.grossSalary / 100 * value;
                await this.updatePayrollComponent(component, value);
                break;
            case (feature == method && calculationType == 'division_of_gross_salary'):
                value = this.empData.grossSalary / value;
                await this.updatePayrollComponent(component, value);
                break;
            case (feature == method && calculationType == '%_of_base_salary'):
                value = this.empData.baseSalary / 100 * value;
                await this.updatePayrollComponent(component, value);
                break;
            case (feature == method && calculationType == 'division_of_base_salary'):
                value = this.empData.baseSalary / value;
                await this.updatePayrollComponent(component, value);
                break;
            case (feature == method && calculationType == 'manual'):
                value = await this.getManualComponentValue(component);
                break;
            case (feature == method && calculationType == '%_of_basic_salary'):
                value = this.empData.basicSalary / 100 * value;
                await this.updatePayrollComponent(component, value);
                break;
            default:
                value = 0;
        }
        return value;
    }

    async getEmpData(dataObj) {
        this.empData.rowId = dataObj.row_id;
        this.empData.grossSalary = dataObj.gross_salary;
        this.empData.employeeId = dataObj.proll_employee_id;
        this.empData.employmentType = dataObj.contract_status;
        this.empData.departmentId = dataObj.dept_id;
        this.empData.bandId = dataObj.emp_band;
        this.empData.regionId = dataObj.country_id;
        this.empData.stateId = dataObj.state_id;
        this.empData.empName = dataObj.name;
        this.empData.empOvertimeType = dataObj.overtime_type.toLowerCase().replace(" ", "_"),
        this.empData.empOvertimeHour = dataObj.overtime_hours
    }

    async compensationDefaultState() {
        try {
            let compensationDefaultStateQuery = `UPDATE cb_payroll_process SET variable_allowances = 0, taxable_income = 0 WHERE payroll_process_id = ${this.empData.rowId}`;
            let data = await queryObj.executeInsertQuery(compensationDefaultStateQuery, this.dbConnection);
            this.logArray.push({
                title: 'Query Executed',
                query: compensationDefaultStateQuery,
                data: data
            });
        } catch (error) {
            return error
        } finally {
        }
    }

    async getManualComponentValue(component) {
        try {
            let getManualComponentValueQuery = `SELECT ${component} FROM cb_payroll_process WHERE payroll_process_id = ${this.empData.rowId}`;
            let data = await queryObj.executeInsertQuery(getManualComponentValueQuery, this.dbConnection);
            this.logArray.push(`Execute query: ${getManualComponentValueQuery}`);
            return Object.values(data.length ? data[0] : {})[0];
        } catch (error) {
            return error
        } finally {
        }
    }

    async calculateCompensationOvertime(dataObj) {
        if (Object.keys(dataObj).length > 0) {
            for (let i = 0; i < Object.keys(dataObj).length; i++) {
                let regionIdSetup = dataObj[i].region_id;
                let stateIdSetup = dataObj[i].state_id;
                let allowanceType = dataObj[i].header_key;
                let calculationType = dataObj[i].calculation_type;
                let employmentTypeIdSetup = dataObj[i].employment_type_id;
                let departmentIdSetup = dataObj[i].department_id;
                let bandIdSetup = dataObj[i].band_id;
                let hoursPerMonth = dataObj[i].hrs_per_month;
                let otRate = dataObj[i].ot_rate;
                let frequency = dataObj[i].frequency;
                let perHourCost = 0;
                let overtimeAmount = 0;
                switch (true) {
                    case (allowanceType == this.empData.empOvertimeType && (employmentTypeIdSetup == 0 || employmentTypeIdSetup == this.empData.employmentType) && (departmentIdSetup == 0 || departmentIdSetup == this.empData.departmentId) && (bandIdSetup == 0 || bandIdSetup == this.empData.bandId) && (regionIdSetup == 0 || regionIdSetup == this.empData.regionId) && (stateIdSetup == 0 || stateIdSetup == this.empData.stateId)):
                        switch (true) {
                            case (calculationType == 'division_of_gross_salary'):
                                perHourCost = this.empData.grossSalary / hoursPerMonth;
                                overtimeAmount = otRate / 100 * perHourCost * this.empData.empOvertimeHour;
                                await this.updatePayrollComponent('overtime_amount', overtimeAmount);
                                //================= Log =================
                                this.logArray.push({
                                    title: `Overtime Formula When Calculation Type is 'division_of_gross_salary'`,
                                    exp_1: `${perHourCost} = ${this.empData.grossSalary} / ${hoursPerMonth}`,
                                    exp_2: `${overtimeAmount} = ${otRate} / 100 * ${perHourCost} * ${this.empData.empOvertimeHour}`,
                                    data: null
                                });
                                break;
                            case (calculationType == 'division_of_base_salary'):
                                perHourCost = this.empData.baseSalary / hoursPerMonth;
                                overtimeAmount = otRate / 100 * perHourCost * this.empData.empOvertimeHour;
                                await this.updatePayrollComponent('overtime_amount', overtimeAmount);
                                //================= Log =================
                                this.logArray.push({
                                    title: `Overtime Formula When Calculation Type is 'division_of_base_salary'`,
                                    exp_1: `${perHourCost} = ${this.empData.baseSalary} / ${hoursPerMonth}`,
                                    exp_2: `${overtimeAmount} = ${otRate} / 100 * ${perHourCost} * ${this.empData.empOvertimeHour}`,
                                    data: null
                                });
                                break;
                            case (calculationType == 'division_of_basic_salary'):
                                perHourCost = this.empData.basicSalary / hoursPerMonth;
                                overtimeAmount = otRate / 100 * perHourCost * this.empData.empOvertimeHour;
                                await this.updatePayrollComponent('overtime_amount', overtimeAmount);
                                //================= Log =================
                                this.logArray.push({
                                    title: `Overtime Formula When Calculation Type is 'division_of_basic_salary'`,
                                    exp_1: `${perHourCost} = ${this.empData.basicSalary} / ${hoursPerMonth}`,
                                    exp_2: `${overtimeAmount} = ${otRate} / 100 * ${perHourCost} * ${this.empData.empOvertimeHour}`,
                                    data: null
                                });
                                break;
                            case (calculationType == 'fixed'):
                                overtimeAmount = hoursPerMonth * this.empData.empOvertimeHour;
                                await this.updatePayrollComponent('overtime_amount', overtimeAmount);
                                //================= Log =================
                                this.logArray.push({
                                    title: `Overtime Formula When Calculation Type is 'fixed'`,
                                    exp_1: `${overtimeAmount} = ${hoursPerMonth} * ${this.empData.empOvertimeHour}`,
                                    data: null
                                });
                                break;
                        }
                        break;
                }
            }
        }
    }

}

exports.PayrollProcess = PayrollProcess;
