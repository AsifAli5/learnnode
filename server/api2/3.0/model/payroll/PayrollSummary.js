const queryObj = require("../../helper/query");
class PayrollSummary {
  constructor() {}

  async GetPayrollSummary(conn, infoId, pool) {
    try {
      let FinalResult = [];
      //////////////////Salary Wages/////////////////////////////
      let SalaryWagesSelection = "";
      let TotalGross = "";
      let SalaryWages = `SELECT 
      ph.header_desc, ph.header_key
  FROM
      cb_payroll_headers ph
          LEFT JOIN
      cb_headers_screen_filters hsf ON ph.payroll_headers_id = hsf.payroll_headers_id
          LEFT JOIN
      cb_payroll_headers_link phl ON ph.payroll_headers_id = phl.payroll_headers_id
          LEFT JOIN
      cb_payroll_setup ps ON hsf.payroll_setup_id = ps.payroll_setup_id
          LEFT JOIN
      cb_payroll_headers_type pht ON phl.payroll_headers_type_id = pht.payroll_headers_type_id
  WHERE
      ps.setup_type = 'Payroll_Summary'
          AND pht.header_type = 'Salary_Wages'`;
      let SalaryWagesResult = await queryObj.executeInsertQuery(SalaryWages,conn);
      for (let i = 0; i < SalaryWagesResult.length; i++) {
        let header_key = SalaryWagesResult[i]["header_key"];
        SalaryWagesSelection =
          SalaryWagesSelection + `COALESCE(SUM(${header_key}),0) as ${header_key},`;
        if (i === 0) {
          TotalGross = TotalGross + ` (COALESCE(SUM(${header_key}),0) +`;
        } else if (i === SalaryWagesResult.length - 1) {
          TotalGross = TotalGross + ` COALESCE(SUM(${header_key}),0)) `;
          SalaryWagesSelection =
            SalaryWagesSelection + `COALESCE(SUM(${header_key}),0) as ${header_key}`;
        } else {
          TotalGross = TotalGross + ` COALESCE(SUM(${header_key}),0) +`;
        }
      }
      let SalaryWagesAmountQuery = `select count(payroll_process_id) AS HeadCount, ${SalaryWagesSelection} , ${TotalGross} As GrossSalary
        from cb_payroll_process where payroll_emf_file_info_id = ${infoId} `;
      let SalaryWagesAmountQueryResult = await queryObj.executeInsertQuery(
        SalaryWagesAmountQuery,
        conn
      );
      //////////////////Deduction////////////////////////////////////////
      let DeductionSelection = "";
      let TotalDeduction = "";
      let Deduction = `SELECT 
        ph.header_desc, ph.header_key
    FROM
        cb_payroll_headers ph
            LEFT JOIN
        cb_headers_screen_filters hsf ON ph.payroll_headers_id = hsf.payroll_headers_id
            LEFT JOIN
        cb_payroll_headers_link phl ON ph.payroll_headers_id = phl.payroll_headers_id
            LEFT JOIN
        cb_payroll_setup ps ON hsf.payroll_setup_id = ps.payroll_setup_id
            LEFT JOIN
        cb_payroll_headers_type pht ON phl.payroll_headers_type_id = pht.payroll_headers_type_id
    WHERE
        ps.setup_type = 'Payroll_Summary'
            AND pht.header_type = 'Employee_Deduction'`;
      let DeductionResult = await queryObj.executeInsertQuery(Deduction, conn);
      for (let i = 0; i < DeductionResult.length; i++) {
        let header_key = DeductionResult[i]["header_key"];
        DeductionSelection =
          DeductionSelection + `COALESCE(SUM(${header_key}),0) as ${header_key},`;
        if (i === 0) {
          TotalDeduction = TotalDeduction + ` (COALESCE(SUM(${header_key}),0) +`;
        } else if (i === DeductionResult.length - 1) {
          TotalDeduction =
            TotalDeduction + ` COALESCE(SUM(${header_key}),0)) `;
          DeductionSelection =
            DeductionSelection + `COALESCE(SUM(${header_key}),0) as ${header_key}`;
        } else {
          TotalDeduction = TotalDeduction + `COALESCE(SUM(${header_key}),0) +`;
        }
      }
      let DeductionAmountQuery = `select ${DeductionSelection} , ${TotalDeduction} As TotalDeduction
          from cb_payroll_process where payroll_emf_file_info_id = ${infoId} `;
      let DeductionAmountResult = await queryObj.executeInsertQuery(
        DeductionAmountQuery,
        conn
      );
      //////////////////Contribution////////////////////////
      let ContributionSelection = "";
      let TotalContribution = "";
      let Contribution = `SELECT 
        ph.header_desc, ph.header_key
    FROM
        cb_payroll_headers ph
            LEFT JOIN
        cb_headers_screen_filters hsf ON ph.payroll_headers_id = hsf.payroll_headers_id
            LEFT JOIN
        cb_payroll_headers_link phl ON ph.payroll_headers_id = phl.payroll_headers_id
            LEFT JOIN
        cb_payroll_setup ps ON hsf.payroll_setup_id = ps.payroll_setup_id
            LEFT JOIN
        cb_payroll_headers_type pht ON phl.payroll_headers_type_id = pht.payroll_headers_type_id
    WHERE
        ps.setup_type = 'Payroll_Summary'
            AND pht.header_type = 'Employee_Contribitions'`;
      let ContributionResult = await queryObj.executeInsertQuery(
        Contribution,
        conn
      );
      for (let i = 0; i < ContributionResult.length; i++) {
        let header_key = ContributionResult[i]["header_key"];
        ContributionSelection =
          ContributionSelection + `COALESCE(SUM(${header_key}),0) as ${header_key},`;
        if (i === 0) {
          TotalContribution = TotalContribution + ` (COALESCE(SUM(${header_key}),0) +`;
        } else if (i === ContributionResult.length - 1) {
          TotalContribution =
            TotalContribution + ` COALESCE(SUM(${header_key}),0)) `;
          ContributionSelection =
            ContributionSelection + `COALESCE(SUM(${header_key}),0) as ${header_key}`;
        } else {
          TotalContribution = TotalContribution + ` COALESCE(SUM(${header_key}),0) +`;
        }
      }
      let ContributionAmountQuery = `select ${ContributionSelection} , ${TotalContribution} As TotalContribution
          from cb_payroll_process where payroll_emf_file_info_id = ${infoId} `;
      let ContributionAmountResult = await queryObj.executeInsertQuery(
        ContributionAmountQuery,
        conn
      );
      let CostSummaryQuery = `select count(payroll_process_id) AS HeadCount,${TotalGross} As GrossSalary ,${TotalContribution} As TotalContribution,((${TotalGross}) + (${TotalContribution})) as PayrollCost from cb_payroll_process where payroll_emf_file_info_id = ${infoId}`;
      let CostSummaryResult = await queryObj.executeInsertQuery(
        CostSummaryQuery,
        conn
      );
      FinalResult.push({
        status: 200,
        SalaryWages: SalaryWagesAmountQueryResult,
        Deduction: DeductionAmountResult,
        Contribution:ContributionAmountResult,
        PayrollCost:CostSummaryResult
      });
      return FinalResult;
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
}
exports.PayrollSummary = PayrollSummary;
