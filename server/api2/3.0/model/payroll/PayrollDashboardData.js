var stringCheck = require("../../../../utils/StringCheck");
const config = require("../../../../../config");
const queryObj = require("../../helper/query");

class PayrollDashboardData {
  constructor() {}
  //////////////EMF Update as of today///////////////////////////////////////
  getNewHires(year, conn, pool) {
    try {
      // if( year == '' || year == 'undefined' || year == null) {

      //     let query = ``;

      //     return queryObj.executeQuery(query);

      // }
      // else {

      let query = `
        SELECT 
            COUNT(emp.id) AS joiner
        FROM
            proll_employee emp
        WHERE
            YEAR(emp.doj) = '${year}';
            SELECT 
            COUNT(emp.department_id) AS DeptCount,
            dept.department_name AS DeptName,
            dept.id AS DepartmentId
        FROM
            proll_employee emp
                LEFT JOIN
            department_hierarchy dept ON dept.id = emp.department_id
        WHERE
            YEAR(emp.doj) = '${year}'
        GROUP BY emp.department_id;
        SELECT 
            COUNT(emp.emp_band) AS BandCount,
            ban.band_desc AS BandName,
            ban.id AS BandId
        FROM
            proll_employee emp
                LEFT JOIN
            employee_bands ban ON ban.id = emp.emp_band
        WHERE
            YEAR(emp.doj) = '${year}'
        GROUP BY emp.emp_band;
        SELECT 
            (SELECT 
                    COUNT(Semp.id)
                FROM
                    proll_employee Semp
                        LEFT JOIN
                    employee_bands Sban ON Sban.id = Semp.emp_band
                WHERE
                    YEAR(Semp.doj) = '${year}'
                        AND Sban.band_desc = 'NM') AS Non_Managerial,
            (SELECT 
                    COUNT(Semp.id)
                FROM
                    proll_employee Semp
                        LEFT JOIN
                    employee_bands Sban ON Sban.id = Semp.emp_band
                WHERE
                    YEAR(Semp.doj) = '${year}'
                        AND Sban.band_desc != 'NM') AS Managerial
        FROM
            proll_employee emp
        WHERE
            YEAR(emp.doj) = '${year}'
        LIMIT 1;`;

      return queryObj.executeInsertQuery(query, conn);
      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getLeavers(year, conn, pool) {
    try {
      // if( year == '' || year == 'undefined' || year == null) {

      //     let query = ``;

      //     return queryObj.executeQuery(query);

      // }
      // else {

      let query = `SELECT 
    COUNT(emp.id) AS Leaver
FROM
    proll_employee emp
        LEFT JOIN
    proll_employee_detail empd ON empd.empid = emp.id
WHERE
    YEAR(empd.lastworkingdate) = '${year}'
        AND emp.status != 1;
        SELECT 
        COUNT(emp.department_id) AS DeptCount,
               dept.department_name AS DeptName,
               dept.id AS DepartmentId
   FROM
       proll_employee emp
           LEFT JOIN
       department_hierarchy dept ON dept.id = emp.department_id
           LEFT JOIN
       proll_employee_detail empd ON empd.empid = emp.id
   WHERE
       YEAR(empd.lastworkingdate) = '${year}'
           AND emp.status != 1
   GROUP BY emp.department_id;
SELECT 
    COUNT(emp.emp_band) AS BandCount,
    ban.band_desc AS BandName,
    ban.id AS BandId
FROM
    proll_employee emp
        LEFT JOIN
    employee_bands ban ON ban.id = emp.emp_band
        LEFT JOIN
    proll_employee_detail empd ON empd.empid = emp.id
WHERE
    YEAR(empd.lastworkingdate) = '${year}'
        AND emp.status != 1
GROUP BY emp.emp_band;
SELECT 
    (SELECT 
            COUNT(Semp.id)
        FROM
            proll_employee Semp
                LEFT JOIN
            employee_bands Sban ON Sban.id = Semp.emp_band
                LEFT JOIN
            proll_employee_detail empd ON empd.empid = Semp.id
        WHERE
            YEAR(empd.lastworkingdate) = '${year}'
                AND Sban.band_desc = 'NM'
                AND Semp.status != 1) AS Non_Managerial,
    (SELECT 
            COUNT(Semp.id)
        FROM
            proll_employee Semp
                LEFT JOIN
            employee_bands Sban ON Sban.id = Semp.emp_band
                LEFT JOIN
            proll_employee_detail empd ON empd.empid = Semp.id
        WHERE
            YEAR(empd.lastworkingdate) = '${year}'
                AND Sban.band_desc != 'NM'
                AND Semp.status != 1) AS Managerial
FROM
    proll_employee emp
        LEFT JOIN
    proll_employee_detail empd ON empd.empid = emp.id
WHERE
    YEAR(empd.lastworkingdate) = '${year}'
        AND emp.status != 1
LIMIT 1;`;

      return queryObj.executeInsertQuery(query, conn);

      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getSalaryChanges(year, conn, pool) {
    try {
      // if( year == '' || year == 'undefined' || year == null) {

      //     let query = ``;

      //     return queryObj.executeQuery(query);

      // }
      // else {

      let query = `SELECT 
    COUNT(pres.salary_view) AS Increments
FROM
    proll_employee_salary_summary pres
WHERE
    (YEAR(pres.updated_at) = '${year}'
        OR (pres.updated_at = NULL
        AND YEAR(pres.created_at) = '${year}'));
    SELECT 
        COUNT(prem.department_id) AS DeptCount,
                dept.department_name AS DeptName,
                dept.id AS DepartmentId
    FROM
        proll_employee_salary_summary pres
            LEFT JOIN
        proll_employee prem ON prem.id = pres.emp_id
            LEFT JOIN
        department_hierarchy dept ON dept.id = prem.department_id
    WHERE
        (YEAR(pres.updated_at) = '${year}'
            OR (pres.updated_at = NULL
            AND YEAR(pres.created_at) = '${year}'))
    GROUP BY prem.department_id;
SELECT 
    COUNT(pres.salary_view) AS BandCount,
    ban.band_desc AS BandName,
    ban.id AS BandId
FROM
    proll_employee_salary_summary pres
        LEFT JOIN
    proll_employee prem ON prem.id = pres.emp_id
        LEFT JOIN
    employee_bands ban ON ban.id = prem.emp_band
WHERE
    (YEAR(pres.updated_at) = '${year}'
        OR (pres.updated_at = NULL
        AND YEAR(pres.created_at) = '${year}'))
GROUP BY prem.emp_band;
SELECT 
    (SELECT 
            COUNT(pres.salary_view)
        FROM
            proll_employee_salary_summary pres
                LEFT JOIN
            proll_employee prem ON prem.id = pres.emp_id
                LEFT JOIN
            employee_bands ban ON ban.id = prem.emp_band
        WHERE
            (YEAR(pres.updated_at) = '${year}'
                OR (pres.updated_at = NULL
                AND YEAR(pres.created_at) = '${year}'))
                AND ban.band_desc != 'NM') AS Managerial,
    (SELECT 
            COUNT(pres.salary_view)
        FROM
            proll_employee_salary_summary pres
                LEFT JOIN
            proll_employee prem ON prem.id = pres.emp_id
                LEFT JOIN
            employee_bands ban ON ban.id = prem.emp_band
        WHERE
            (YEAR(pres.updated_at) = '${year}'
                OR (pres.updated_at = NULL
                AND YEAR(pres.created_at) = '${year}'))
                AND ban.band_desc = 'NM') AS Non_Managerial
FROM
    proll_employee_salary_summary
LIMIT 1`;

      return queryObj.executeInsertQuery(query, conn);

      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getExpenses(year, conn, pool) {
    try {
      // if( year == '' || year == 'undefined' || year == null) {

      //     let query = ``;

      //     return queryObj.executeQuery(query);

      // }
      // else {

      let query = `SELECT 
    COUNT(prexp.eid) AS Expenses
FROM
    proll_expense prexp
        LEFT JOIN
    proll_employee prep ON prep.id = prexp.eid
WHERE
    YEAR(prexp.exp_date) = '${year}';
    SELECT 
    COUNT(prexp.eid) AS DeptCount,
    prdept.department_name AS DeptName,
    prdept.id AS DepartmentId
FROM
    proll_expense prexp
        LEFT JOIN
    proll_employee prep ON prep.id = prexp.eid
        LEFT JOIN
    department_hierarchy prdept ON prdept.id = prep.department_id
WHERE
    YEAR(prexp.exp_date) = '${year}'
GROUP BY prep.department_id;
SELECT 
    COUNT(prexp.eid) AS BandCount,
    ban.band_desc AS BandName,
    ban.id AS BandId
FROM
    proll_expense prexp
        LEFT JOIN
    proll_employee prep ON prep.id = prexp.eid
        LEFT JOIN
    employee_bands ban ON ban.id = prep.emp_band
WHERE
    YEAR(prexp.exp_date) = '${year}'
GROUP BY prep.emp_band;
SELECT 
    (SELECT 
            COUNT(prexp.eid)
        FROM
            proll_expense prexp
                LEFT JOIN
            proll_employee prep ON prep.id = prexp.eid
                LEFT JOIN
            employee_bands ban ON ban.id = prep.emp_band
        WHERE
            YEAR(prexp.exp_date) = '${year}'
                AND band_desc = 'NM') AS Non_Managerial,
    (SELECT 
            COUNT(prexp.eid)
        FROM
            proll_expense prexp
                LEFT JOIN
            proll_employee prep ON prep.id = prexp.eid
                LEFT JOIN
            employee_bands ban ON ban.id = prep.emp_band
        WHERE
            YEAR(prexp.exp_date) = '${year}'
                AND band_desc != 'NM') AS Managerial
FROM
    proll_expense prexp
        LEFT JOIN
    proll_employee prep ON prep.id = prexp.eid
        LEFT JOIN
    employee_bands ban ON ban.id = prep.emp_band
WHERE
    YEAR(prexp.exp_date) = '${year}'
LIMIT 1`;

      return queryObj.executeInsertQuery(query, conn);

      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getAttendance(year, conn, pool) {
    try {
      // if( year == '' || year == 'undefined' || year == null) {

      //     let query = ``;

      //     return queryObj.executeQuery(query);

      // }
      // else {

      let query = `SELECT 
    COUNT(prle.leaveid) AS LWOP
FROM
    proll_leave prle
        LEFT JOIN
    proll_employee prem ON prem.id = prle.empid
        LEFT JOIN
    proll_leave_type_c prlet ON prlet.id = prle.type_id
        LEFT JOIN
    al_roster alr ON alr.emp_id = prem.id
WHERE
    '${year}' BETWEEN YEAR(prle.fdate) AND YEAR(prle.tdate)
        AND (prle.appstatus = 3
        OR prlet.type = 'lwop')
        AND YEAR(alr.actual_shift_time_in) = '0000'
        AND YEAR(alr.roster_date) = '${year}'
        AND prem.job_status = 1;
    SELECT 
        COUNT(prle.leaveid) AS DeptCount,
        dept.department_name AS DeptName,
        dept.id AS DepartmentId
    FROM
        proll_leave prle
            LEFT JOIN
        proll_employee prem ON prem.id = prle.empid
            LEFT JOIN
        proll_leave_type_c prlet ON prlet.id = prle.type_id
            LEFT JOIN
        al_roster alr ON alr.emp_id = prem.id
            LEFT JOIN
        department_hierarchy dept ON dept.id = prem.department_id
    WHERE
        '${year}' BETWEEN YEAR(prle.fdate) AND YEAR(prle.tdate)
            AND (prle.appstatus = 3
            OR prlet.type = 'lwop')
            AND YEAR(alr.actual_shift_time_in) = '0000'
            AND YEAR(alr.roster_date) = '${year}'
            AND prem.job_status = 1
    GROUP BY prem.department_id;
SELECT 
    COUNT(prle.leaveid) AS BandCount,
    ban.band_desc AS BandName,
    ban.id AS BandId
FROM
    proll_leave prle
        LEFT JOIN
    proll_employee prem ON prem.id = prle.empid
        LEFT JOIN
    proll_leave_type_c prlet ON prlet.id = prle.type_id
        LEFT JOIN
    al_roster alr ON alr.emp_id = prem.id
        LEFT JOIN
    employee_bands ban ON ban.id = prem.emp_band
WHERE
    '${year}' BETWEEN YEAR(prle.fdate) AND YEAR(prle.tdate)
        AND (prle.appstatus = 3
        OR prlet.type = 'lwop')
        AND YEAR(alr.actual_shift_time_in) = '0000'
        AND YEAR(alr.roster_date) = '${year}'
        AND prem.job_status = 1
GROUP BY prem.emp_band;
SELECT 
    (SELECT 
            COUNT(prle.leaveid) AS BandCount
        FROM
            proll_leave prle
                LEFT JOIN
            proll_employee prem ON prem.id = prle.empid
                LEFT JOIN
            proll_leave_type_c prlet ON prlet.id = prle.type_id
                LEFT JOIN
            al_roster alr ON alr.emp_id = prem.id
                LEFT JOIN
            employee_bands ban ON ban.id = prem.emp_band
        WHERE
            '${year}' BETWEEN YEAR(prle.fdate) AND YEAR(prle.tdate)
                AND (prle.appstatus = 3
                OR prlet.type = 'lwop')
                AND YEAR(alr.actual_shift_time_in) = '0000'
                AND YEAR(alr.roster_date) = '${year}'
                AND prem.job_status = 1
                AND ban.band_desc != 'NM') AS Managerial,
    (SELECT 
            COUNT(prle.leaveid) AS BandCount
        FROM
            proll_leave prle
                LEFT JOIN
            proll_employee prem ON prem.id = prle.empid
                LEFT JOIN
            proll_leave_type_c prlet ON prlet.id = prle.type_id
                LEFT JOIN
            al_roster alr ON alr.emp_id = prem.id
                LEFT JOIN
            employee_bands ban ON ban.id = prem.emp_band
        WHERE
            '${year}' BETWEEN YEAR(prle.fdate) AND YEAR(prle.tdate)
                AND (prle.appstatus = 3
                OR prlet.type = 'lwop')
                AND YEAR(alr.actual_shift_time_in) = '0000'
                AND YEAR(alr.roster_date) = '${year}'
                AND prem.job_status = 1
                AND ban.band_desc = 'NM') AS Non_Managerial
FROM
    proll_leave prle
LIMIT 1`;

      return queryObj.executeInsertQuery(query, conn);

      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getLoanDeduction(year, conn, pool) {
    try {
      // if( year == '' || year == 'undefined' || year == null) {

      //     let query = ``;

      //     return queryObj.executeQuery(query);

      // }
      // else {

      let query = `SELECT 
    COUNT(lains.installment_id) AS LOANS
FROM
    la_installments lains
WHERE
    YEAR(lains.payroll_month) = '${year}'
        AND lains.status = 2;
    SELECT 
        COUNT(lains.installment_id) AS DeptCount,
        dept.department_name AS DeptName,
        dept.id AS DepartmentId
    FROM
        la_installments lains
            LEFT JOIN
        proll_employee prem ON prem.id = lains.empid
            LEFT JOIN
        department_hierarchy dept ON dept.id = prem.department_id
    WHERE
        YEAR(lains.payroll_month) = '2010'
            AND lains.status = 2
    GROUP BY prem.department_id;
SELECT 
    COUNT(lains.installment_id) AS BandCount,
    ban.band_desc AS BandName,
    ban.id AS BandId
FROM
    la_installments lains
        LEFT JOIN
    proll_employee prem ON prem.id = lains.empid
        LEFT JOIN
    employee_bands ban ON ban.id = prem.emp_band
WHERE
    YEAR(lains.payroll_month) = '${year}'
        AND lains.status = 2
GROUP BY prem.emp_band;
SELECT 
    (SELECT 
            COUNT(lains.installment_id)
        FROM
            la_installments lains
                LEFT JOIN
            proll_employee prem ON prem.id = lains.empid
                LEFT JOIN
            employee_bands ban ON ban.id = prem.emp_band
        WHERE
            YEAR(lains.payroll_month) = '${year}'
                AND lains.status = 2
                AND ban.band_desc != 'NM') AS Managerial,
    (SELECT 
            COUNT(lains.installment_id)
        FROM
            la_installments lains
                LEFT JOIN
            proll_employee prem ON prem.id = lains.empid
                LEFT JOIN
            employee_bands ban ON ban.id = prem.emp_band
        WHERE
            YEAR(lains.payroll_month) = '${year}'
                AND lains.status = 2
                AND ban.band_desc = 'NM') AS Non_Managerial
FROM
    la_installments lains
WHERE
    lains.status = 2
LIMIT 1
`;

      return queryObj.executeInsertQuery(query, conn);

      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  ////////////// PayRoll for /////////////////////////////////////////////////
  getHeadCount(year, conn, pool) {
    try {
      // if( year == '' || year == 'undefined' || year == null) {

      //     let query = ``;

      //     return queryObj.executeQuery(query);

      // }
      // else {

      let query = `SELECT 
    (SELECT 
            COUNT(cbs.id)
        FROM
            cb_salary_details cbs
        WHERE
            (YEAR(cbs.payroll_date) = '2020'
                AND MONTH(cbs.payroll_date) = '12')) AS Headcount,
    (SELECT 
            COUNT(cbs.id)
        FROM
            cb_salary_details cbs
        WHERE
            (YEAR(cbs.payroll_date) = '2020'
                AND MONTH(cbs.payroll_date) = '11')) AS Previous_Headcount
FROM
    cb_salary_details cbs
LIMIT 1;
SELECT 
    COUNT(cbs.id) AS GanderCount,
    prem.name_salute
    
FROM
    cb_salary_details cbs
        LEFT JOIN
    proll_employee prem ON cbs.people_code = prem.empcode
WHERE
    (YEAR(cbs.payroll_date) = '2020'
        AND MONTH(cbs.payroll_date) = '12')
GROUP BY prem.name_salute;
SELECT 
    COUNT(cbs.id) AS DeptCount,
    dept.department AS DeptName,
    dept.id AS DepartmentId
FROM
    cb_salary_details cbs
        LEFT JOIN
    proll_employee prem ON cbs.people_code = prem.empcode
        LEFT JOIN
    proll_department dept ON prem.dept_id = dept.id
WHERE
    (YEAR(cbs.payroll_date) = '2020'
        AND MONTH(cbs.payroll_date) = '12')
GROUP BY prem.dept_id;
SELECT 
    TRUNCATE((SELECT 
                AVG(cbs.basic_salary)
            FROM
                cb_salary_details cbs
            WHERE
                (YEAR(cbs.payroll_date) = '2020'
                    AND MONTH(cbs.payroll_date) = '12')),
        3) AS Avgsalary,
    TRUNCATE((SELECT 
                AVG(cbs.basic_salary)
            FROM
                cb_salary_details cbs
            WHERE
                (YEAR(cbs.payroll_date) = '2020'
                    AND MONTH(cbs.payroll_date) = '11')),
        3) AS previous_Avgsalary
FROM
    cb_salary_details cbs
LIMIT 1;
SELECT 
    TRUNCATE((SELECT 
                MIN(cbs.basic_salary)
            FROM
                cb_salary_details cbs
            WHERE
                (YEAR(cbs.payroll_date) = '2020'
                    AND MONTH(cbs.payroll_date) = '12')),
        3) AS Avgsalary,
    TRUNCATE((SELECT 
                MIN(cbs.basic_salary)
            FROM
                cb_salary_details cbs
            WHERE
                (YEAR(cbs.payroll_date) = '2020'
                    AND MONTH(cbs.payroll_date) = '11')),
        3) AS previous_Avgsalary
FROM
    cb_salary_details cbs
LIMIT 1
`;

      return queryObj.executeInsertQuery(query, conn);

      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getSalaryCost(year, conn, pool) {
    try {
      // if( year == '' || year == 'undefined' || year == null) {

      //     let query = ``;

      //     return queryObj.executeQuery(query);

      // }
      // else {

      let query = ``;

      return queryObj.executeInsertQuery(query, conn);

      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getStatutoryBenifits(year, conn, pool) {
    try {
      // if( year == '' || year == 'undefined' || year == null) {

      //     let query = ``;

      //     return queryObj.executeQuery(query);

      // }
      // else {

      let query = ``;

      return queryObj.executeInsertQuery(query, conn);

      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getOtherChanges(year, conn, pool) {
    try {
      // if( year == '' || year == 'undefined' || year == null) {

      //     let query = ``;

      //     return queryObj.executeQuery(query);

      // }
      // else {

      let query = `SELECT 
    (SELECT 
            COUNT(emp.id)
        FROM
            proll_employee emp
        WHERE
            YEAR(emp.doj) = '${year}') AS joiner,
    (SELECT 
            COUNT(emp.id)
        FROM
            proll_employee emp
                LEFT JOIN
            proll_employee_detail empd ON empd.empid = emp.id
        WHERE
            YEAR(empd.lastworkingdate) = '${year}'
                AND emp.status != 1) AS Leaver
FROM
    proll_employee
LIMIT 1;
SELECT 
    COUNT(emp.dept_id) AS DeptCount,
    dept.department AS DeptName,
    dept.id AS DepartmentId,
    SUM(prems.basic_salary) AS Amount
FROM
    proll_employee emp
        LEFT JOIN
    proll_department dept ON dept.id = emp.dept_id
        LEFT JOIN
    proll_employee_salary_summary prems ON prems.emp_id = emp.id
WHERE
    YEAR(emp.doj) = '${year}'
        AND prems.source = 'Payroll'
GROUP BY emp.dept_id;
SELECT 
    COUNT(emp.dept_id) AS DeptCount,
    dept.department AS DeptName,
    dept.id AS DepartmentId,
    COALESCE(SUM(prems.basic_salary), 0) AS Amount
FROM
    proll_employee emp
        LEFT JOIN
    proll_department dept ON dept.id = emp.dept_id
        LEFT JOIN
    proll_employee_detail empd ON empd.empid = emp.id
        LEFT JOIN
    proll_employee_salary_summary prems ON prems.emp_id = emp.id
WHERE
    YEAR(empd.lastworkingdate) = '${year}'
        AND emp.status != 1
        AND prems.source = 'Payroll'
GROUP BY emp.dept_id;
SELECT 
    Count(d.id) Increments
   
FROM
    cb_salary_details d
        LEFT JOIN
    cb_salary_details s ON s.people_code = d.people_code
         and (year(s.payroll_date) = '2020' and month(s.payroll_date) = '11')
WHERE
    (year(d.payroll_date) = '2020' and month(d.payroll_date) = '12')
        AND (IFNULL(d.basic_salary, 0) - IFNULL(s.basic_salary, 0)) != 0;
        SELECT 
    Count(d.id) Promotions
   
FROM
    cb_salary_details d
        LEFT JOIN
    cb_salary_details s ON s.people_code = d.people_code
       -- AND DATE_FORMAT(s.payroll_date, '%Y-%m') = '2020-11'
        and (year(s.payroll_date) = '2020' and month(s.payroll_date) = '11')
WHERE
  --  DATE_FORMAT(d.payroll_date, '%Y-%m') = '2020-12'
   (year(d.payroll_date) = '2020' and month(d.payroll_date) = '12')
        AND (IFNULL(d.band, 0) != IFNULL(s.band, 0)) != 0;
        SELECT 
    (IFNULL(d.basic_salary, 0) - IFNULL(s.basic_salary, 0)) AS Amount,
    dept.department AS DeptName,
    dept.id AS DepartmentId
    
FROM
    cb_salary_details d
        LEFT JOIN
    cb_salary_details s ON s.people_code = d.people_code
      and (year(s.payroll_date) = '2020' and month(s.payroll_date) = '11')
        left join proll_employee prem
        on
        prem.empcode = d.people_code
        left join proll_department dept
        on dept.id =  prem.dept_id
WHERE
    (year(d.payroll_date) = '2020' and month(d.payroll_date) = '12')
        AND (IFNULL(d.basic_salary, 0) - IFNULL(s.basic_salary, 0)) != 0
        group by prem.dept_id`;

      return queryObj.executeInsertQuery(query, conn);

      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  ///////// List For Add, Update and Delete on popup ////////////////////////
  getNewHireslist(DepartmentId,BandId,CatDesc,Year,Month, conn, pool) {
    try {
        if (DepartmentId != null && DepartmentId != '' && DepartmentId != 'undefined') {
            let query = `SELECT 
            emp.empcode Emp_ID,
            emp.name Employee_Name,
            emp.department_id,
            dpt.department_name,
            emp.designation desid,
            dsi.designation_name Designation,
            emp.doj DOJ,
            pemps.gross_salary
           FROM
            proll_employee emp
           LEFT JOIN
            proll_employee_salary_summary pemps ON emp.id = pemps.emp_id
           LEFT JOIN
            department_hierarchy dpt ON emp.department_id = dpt.id
           LEFT JOIN
            proll_client_designation dsi ON emp.designation = dsi.designation_id
           WHERE YEAR(emp.doj) = '${Year}' AND MONTH(emp.doj)= '${Month}'
           AND emp.department_id =  ${DepartmentId}`;

            return queryObj.executeInsertQuery(query, conn);
            
          } else if (BandId != null && BandId != '' && BandId != "undefined") {
            let query = `SELECT 
            emp.id,
            emp.empcode Emp_ID,
            emp.name Employee_Name,
            emp.emp_band,
            bnd.band_desc Band,
            emp.designation desid,
            dsi.designation_name Designation,
            emp.doj DOJ,
            pemps.gross_salary Salary
           FROM
            proll_employee emp
           LEFT JOIN
            proll_employee_salary_summary pemps ON emp.id = pemps.emp_id
           LEFT JOIN
            employee_bands bnd ON emp.emp_band = bnd.id
           LEFT JOIN
            proll_client_designation dsi ON emp.designation = dsi.designation_id
           WHERE YEAR(emp.doj) = '${Year}' AND MONTH(emp.doj)= '${Month}'
           AND emp.emp_band = ${BandId}`;

            return queryObj.executeInsertQuery(query, conn);
          
          } else if (CatDesc != null && CatDesc != "" && CatDesc != "undefined") {
            if (CatDesc == "NM") {
                let query = `SELECT 
                emp.id,
                emp.empcode Emp_ID,
                emp.name Employee_Name,
                emp.emp_band,
                COALESCE(bnd.band_description,'Non-Managerial') Category,
                emp.designation desid,
                dsi.designation_name Designation,
                emp.doj DOJ,
                pemps.gross_salary Salary
               FROM
                proll_employee emp
               LEFT JOIN
                proll_employee_salary_summary pemps ON emp.id = pemps.emp_id
               LEFT JOIN
                employee_bands bnd ON emp.emp_band = bnd.id
               LEFT JOIN
                proll_client_designation dsi ON emp.designation = dsi.designation_id
               WHERE YEAR(emp.doj) = '${Year}' AND MONTH(emp.doj)= '${Month}'
               AND bnd.band_desc = '${CatDesc}'`;

      return queryObj.executeInsertQuery(query, conn);
             
            } else {
                let query = `SELECT 
                emp.id,
                emp.empcode Emp_ID,
                emp.name Employee_Name,
                emp.emp_band,
                COALESCE(bnd.band_description,'Managerial') Category,
                emp.designation desid,
                dsi.designation_name Designation,
                emp.doj DOJ,
                pemps.gross_salary Salary
               FROM
                proll_employee emp
               LEFT JOIN
                proll_employee_salary_summary pemps ON emp.id = pemps.emp_id
               LEFT JOIN
                employee_bands bnd ON emp.emp_band = bnd.id
               LEFT JOIN
                proll_client_designation dsi ON emp.designation = dsi.designation_id
               WHERE YEAR(emp.doj) = '${Year}' AND MONTH(emp.doj)= '${Month}'
               AND bnd.band_desc != '${CatDesc}'`;

      return queryObj.executeInsertQuery(query, conn);
              
            }
          } else {
            let Message = 'No Option Select';

            return Message;
            
          }  
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getLeaverlist(parm, conn, pool) {
    try {
      if (parm == "" || parm == "undefined" || parm == null) {
        let query = ``;

        return queryObj.executeInsertQuery(query, conn);
      } else {
        let query = ``;

        return queryObj.executeInsertQuery(query, conn);
      }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getSalaryChangeslist(parm, conn, pool) {
    try {
      if (parm == "" || parm == "undefined" || parm == null) {
        let query = ``;

        return queryObj.executeInsertQuery(query, conn);
      } else {
        let query = ``;

        return queryObj.executeInsertQuery(query, conn);
      }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getExpenseslist(parm, conn, pool) {
    try {
      if (parm == "" || parm == "undefined" || parm == null) {
        let query = ``;

        return queryObj.executeInsertQuery(query, conn);
      } else {
        let query = ``;

        return queryObj.executeInsertQuery(query, conn);
      }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getAttendancelist(parm, conn, pool) {
    try {
      if (parm == "" || parm == "undefined" || parm == null) {
        let query = ``;

        return queryObj.executeInsertQuery(query, conn);
      } else {
        let query = ``;

        return queryObj.executeInsertQuery(query, conn);
      }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  getLoanDeductionlist(parm, conn, pool) {
    try {
      if (parm == "" || parm == "undefined" || parm == null) {
        let query = ``;

        return queryObj.executeInsertQuery(query, conn);
      } else {
        let query = ``;

        return queryObj.executeInsertQuery(query, conn);
      }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
}

exports.PayrollDashboardData = PayrollDashboardData;
