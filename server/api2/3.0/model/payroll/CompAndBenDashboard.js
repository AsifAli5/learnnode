var stringCheck = require("../../../../utils/StringCheck");
const config = require("../../../../../config");
const queryObj = require("../../helper/query");

class CompAndBenDashboard {
  constructor() {}
//////////////EMF Update as of today///////////////////////////////////////
  getStaffTurover(count,monthyn) {
    if( count == '' || count == 'undefined' || count == null) {

        let query = `SELECT 
        (SELECT 
                COUNT(emp.id)
            FROM
                proll_employee emp
            WHERE
                DATE_FORMAT(emp.doj, '%Y-%m-%d') BETWEEN (CURDATE() - INTERVAL 3 MONTH) AND CURDATE()) AS joiner,
        (SELECT 
                COUNT(emp.id)
            FROM
                proll_employee emp
                    LEFT JOIN
                proll_employee_detail empd ON empd.empid = emp.id
            WHERE
                DATE_FORMAT(empd.lastworkingdate, '%Y-%m-%d') BETWEEN (CURDATE() - INTERVAL 3 MONTH) AND CURDATE()
                    AND emp.status != 1) AS Leaver
    FROM
        proll_employee
    LIMIT 1;
    SELECT 
        IFNULL(SUM(prems.basic_salary), 0) AS Amount,
        DATE_FORMAT(emp.doj, '%b-%y') AS groupBy
    FROM
        proll_employee emp
            LEFT JOIN
        proll_employee_salary_summary prems ON emp.id = prems.emp_id
    WHERE
        DATE_FORMAT(emp.doj, '%Y-%m-%d') BETWEEN (CURDATE() - INTERVAL 3 MONTH) AND CURDATE()
    GROUP BY MONTH(emp.doj);`;

        return queryObj.executeSelectQuery(query);

    }
    else {
        let interval ;
        if(monthyn == 1)
        {
             interval = 'MONTH';

        }
        else
        {
            interval = 'YEAR';

        }

    let query = `SELECT 
    (SELECT 
            COUNT(emp.id)
        FROM
            proll_employee emp
        WHERE
            DATE_FORMAT(emp.doj, '%Y-%m-%d') BETWEEN (CURDATE() - INTERVAL ${count} ${interval}) AND CURDATE()) AS joiner,
    (SELECT 
            COUNT(emp.id)
        FROM
            proll_employee emp
                LEFT JOIN
            proll_employee_detail empd ON empd.empid = emp.id
        WHERE
            DATE_FORMAT(empd.lastworkingdate, '%Y-%m-%d') BETWEEN (CURDATE() - INTERVAL ${count} ${interval}) AND CURDATE()
                AND emp.status != 1) AS Leaver
FROM
    proll_employee
LIMIT 1;
SELECT 
    IFNULL(SUM(prems.basic_salary), 0) AS Amount,
    DATE_FORMAT(emp.doj, '%b-%y') AS groupBy
FROM
    proll_employee emp
        LEFT JOIN
    proll_employee_salary_summary prems ON emp.id = prems.emp_id
WHERE
    DATE_FORMAT(emp.doj, '%Y-%m-%d') BETWEEN (CURDATE() - INTERVAL ${count} ${interval}) AND CURDATE()
GROUP BY ${interval}(emp.doj);`;

    return queryObj.executeSelectQuery(query);

     }
  }
  getExpenses(count,monthyn) {
    if( count == '' || count == 'undefined' || count == null) {

        let query = `SELECT 
        prext.type AS ExpenseName,
        IFNULL(SUM(prexd.amount), 0) AS Amount
    FROM
        proll_expense prex
            LEFT JOIN
        proll_expense_detail prexd ON prex.id = prexd.exp_id
            LEFT JOIN
        proll_expense_type prext ON prext.id = prexd.type_id
    WHERE
        DATE_FORMAT(prex.exp_date, '%Y-%m-%d') BETWEEN (CURDATE() - INTERVAL 3 MONTH) AND CURDATE()
    GROUP BY prexd.type_id;
        
        
    SELECT 
        IFNULL(SUM(prexd.amount), 0) AS Amount,
        DATE_FORMAT(prex.exp_date, '%b-%y') AS groupBy
    FROM
        proll_expense prex
            LEFT JOIN
        proll_expense_detail prexd ON prex.id = prexd.exp_id
            LEFT JOIN
        proll_expense_type prext ON prext.id = prexd.type_id
    WHERE
        DATE_FORMAT(prex.exp_date, '%Y-%m-%d') BETWEEN (CURDATE() - INTERVAL 3 MONTH) AND CURDATE()
    GROUP BY MONTH(prex.exp_date);`;

        return queryObj.executeSelectQuery(query);

    }
    else {
        let interval ;
        if(monthyn == 1)
        {
             interval = 'MONTH';

        }
        else
        {
            interval = 'YEAR';

        }

    let query = `SELECT 
    prext.type AS ExpenseName,
    IFNULL(SUM(prexd.amount), 0) AS Amount
FROM
    proll_expense prex
        LEFT JOIN
    proll_expense_detail prexd ON prex.id = prexd.exp_id
        LEFT JOIN
    proll_expense_type prext ON prext.id = prexd.type_id
WHERE
    DATE_FORMAT(prex.exp_date, '%Y-%m-%d') BETWEEN (CURDATE() - INTERVAL ${count} ${interval}) AND CURDATE()
GROUP BY prexd.type_id;
    
    
SELECT 
    IFNULL(SUM(prexd.amount), 0) AS Amount,
    DATE_FORMAT(prex.exp_date, '%b-%y') AS groupBy
FROM
    proll_expense prex
        LEFT JOIN
    proll_expense_detail prexd ON prex.id = prexd.exp_id
        LEFT JOIN
    proll_expense_type prext ON prext.id = prexd.type_id
WHERE
    DATE_FORMAT(prex.exp_date, '%Y-%m-%d') BETWEEN (CURDATE() - INTERVAL ${count} ${interval}) AND CURDATE()
GROUP BY ${interval}(prex.exp_date);`;

    return queryObj.executeSelectQuery(query);

     }
  }
  getsalaryChanges(count) {
    // if( count == '' || count == 'undefined' || count == null) {

    //     let query = ``;

    //     return queryObj.executeQuery(query);

    // }
    // else {

    let query = `SELECT 
    COUNT(P.Increments), SUM(P.Amount), P.payroll_date
FROM
    (SELECT 
        d.people_code AS Increments,
            (d.basic_salary - s.basic_salary) AS Amount,
            d.payroll_date
    FROM
        cb_salary_details d
    LEFT JOIN cb_salary_details s ON s.people_code = d.people_code
        AND DATE_FORMAT(s.payroll_date, '%Y-%m-%d') BETWEEN YEAR(CURDATE() - INTERVAL 2 YEAR) AND YEAR(CURDATE())
    WHERE
        DATE_FORMAT(d.payroll_date, '%Y-%m-%d') BETWEEN YEAR(CURDATE() - INTERVAL 2 YEAR) AND YEAR(CURDATE())
            AND (IFNULL(d.basic_salary, 0) - IFNULL(s.basic_salary, 0)) != 0
            AND (IFNULL(d.basic_salary, 0) - IFNULL(s.basic_salary, 0)) > 0
    GROUP BY d.people_code) P
GROUP BY YEAR(P.payroll_date);
	    
SELECT 
    COUNT(DISTINCT (d.people_code)) AS Promotions,
    SUM(d.basic_salary - s.basic_salary) AS Amount
FROM
    cb_salary_details d
        LEFT JOIN
    cb_salary_details s ON s.people_code = d.people_code
        AND DATE_FORMAT(s.payroll_date, '%Y-%m-%d') BETWEEN YEAR(CURDATE() - INTERVAL 3 YEAR) AND YEAR(CURDATE())
WHERE
    DATE_FORMAT(d.payroll_date, '%Y-%m-%d') BETWEEN YEAR(CURDATE() - INTERVAL 3 YEAR) AND YEAR(CURDATE())
        AND (IFNULL(d.band, 0) != IFNULL(s.band, 0)) != 0
        AND (IFNULL(d.band, 0) != IFNULL(s.band, 0)) > 0
GROUP BY YEAR(d.payroll_date);`;

    return queryObj.executeSelectQuery(query);

    // }
  }
}

exports.CompAndBenDashboard = CompAndBenDashboard;
