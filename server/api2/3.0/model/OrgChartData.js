var stringCheck = require('../../../utils/StringCheck');
 
var Database = require('../../../config/database');
var db = new Database();
const config = require('../../../../config');

class OrgChartData {

    constructor () {
    }

    async getOrgChartResults(option, userId = null, filters = '') {

        let data = null;

        try {
                    
            if(option == 1 && userId == null) {

                data = await this.approvalMatrixRecord(filters);

            }
            else if(option == 2) {

                data = await this.getEmployeeHeadCountRecord(filters);

            }
            else if(option == 3) {

                data = await this.getEmployeeByDepartment(filters);

                data = this.manipulateEmployeeDepartData( data );

            }
            else if(option == 4) {

                data = await this.getEmployeeByDepartmentLM();

                data = this.manipulateEmpDepartLmData( data );

            }
            else if(option == 1 && userId != null) {

                data = await this.getDirectReportingLM( userId );

                data = this.manipulateDirectReportingLMData( data );

            }
            else {

                data = [];

            }

            

        }
        catch(e) {
            console.log('catch an error ', e);
        }

        let balkanGraphData =  [];

        let total_data = data.length;

        if( total_data > 0 ) {

                let i = 0;

                do {

                    const site_url = config.site.SITE_URL || "";
                    const ppm_url = config.site.PPM_URL || "";
                    

                    let image = '';
                    if( data[i]['picture'] != '' || data[i]['picture'] != null || data[i]['picture'] != 'undefined' ) {
                        image = site_url+ppm_url+"emp_pictures/"+data[i]['picture'];
                    }
                    else {

                        let placeHolder = '';

                        if ( data[i]['name_salute'].toLowerCase() == "mr.") {

                            placeHolder = site_url+ppm_url+'assets/orgchart/img/male.png';

                        }
                        else {

                            placeHolder = site_url+ppm_url+'assets/orgchart/img/female.png';

                        }

                        image = placeHolder;

                    }

                    balkanGraphData.push( this.generateTemplateData( option, data, i, image ) );

                    i++;

                }while(i < total_data);

                let finalArray = [];

                if( (option != '' || option != null || option != 'undefined') && 
                    (userId == '' || userId == null || userId == 'undefined') 
                ) {

                    let tempParentArray = {};
    
                    if( option == 3 ) {
    
                        tempParentArray =  {
                            "id"            :  "000000",
                            "name"          :  "",
                            "title"         :  "",
                            "image"         :  "",
                            "parent"        :  null,
                            "level_id"      :  "0",
                            "band"          :  "_",
                            "department_id" :  "",
                            "reporting_id"  :  "",
                            "line_manager"  :  "",
                            "companyname"   :  "",
                            "group_id"      :  "_",
                            "group_image"   :  "|",
                        };
    
                    }
                    else if( option == 4 ) {
    
                        tempParentArray =  {
                            "id"            :  "000000",
                            "name"          :  "",
                            "title"         :  "",
                            "image"         :  "",
                            "parent"        :  null,
                            "band"          :  "_",
                            "department_id" :  "",
                            "reporting_id"  :  "",
                            "line_manager"  :  "",
                            "companyname"   :  "",
                            "group_id"      :  "_",
                            "group_image"   :  "|",
                            "level_id"      :  "0",
                        };
    
                    }
                    else {
    
                        tempParentArray =  {
                            "id"            :  "000000",
                            "name"          :  "",
                            "title"         :  "",
                            "image"         :  "",
                            "parent"        :  null,
                            "band"          :  "",
                            "department_id" :  "",
                            "reporting_id"  :  "",
                            "line_manager"  :  "",
                            "companyname"   :  "",
                            "level_id"      :  "0",
                        };
    
                    }
    
                    finalArray.push(tempParentArray);

                    let arrayLength = balkanGraphData.length;
                    let arrayCounter = 0;

                    do {

                        if( balkanGraphData[arrayCounter]['parent'] == null || 
                            balkanGraphData[arrayCounter]['parent'] == '' ||
                            balkanGraphData[arrayCounter]['parent'] == 'undefined') 
                        {
                            balkanGraphData[arrayCounter]['parent'] = "000000";
                        }

                        finalArray.push(balkanGraphData[arrayCounter]);

                        arrayCounter++;
                    }while(arrayCounter < arrayLength);
        
                }
                else {
                        
                    finalArray = balkanGraphData;

                }

                let dataMapOrgMaster = this.dataMapOrgMaster(finalArray);

                this.stripEmptyArray(dataMapOrgMaster);
                
                return dataMapOrgMaster;
                

        }

    }

    stripEmptyArray( arry ) {

        for (var k in arry) {
            
            if( !arry[k]['children'] || typeof arry[k]['children'] !== "object") {
                continue;
            }
            this.stripEmptyArray(arry[k]['children']);

            if( Object.keys(arry[k]['children']).length === 0) {
                delete arry[k]['children'];
            }

        }
    }

    pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    dataMapOrgMaster( arr ) {

        var tree = [],
          mappedArr = {},
          arrElem,
          mappedElem;

      // First map the nodes of the array to an object -> create a hash table.
      for(var i = 0, len = arr.length; i < len; i++) {
        arrElem = arr[i];
        mappedArr[arrElem.id] = arrElem;
        mappedArr[arrElem.id]['children'] = [];
      }


      for (var id in mappedArr) {
        if (mappedArr.hasOwnProperty(id)) {
          mappedElem = mappedArr[id];
          // If the element is not at the root level, add it to its parent array of children.
          if (mappedElem.parent) {
            mappedArr[mappedElem['parent']]['children'].push(mappedElem);
          }
          // If the element is at the root level, add it to first level elements array.
          else {
            tree.push(mappedElem);
          }
        }
      }
      return tree;

    }


    generateTemplateData( option, data, loopCounter, image ) {

        var tempArray = {};

        if(option == 1) {

            if( data[loopCounter]['parent_id'] == '' || data[loopCounter]['parent_id'] == null
                || data[loopCounter]['parent_id'] == 'undefined' ) {

                tempArray = {
                        'id'            :  data[loopCounter]['id'],
                        'name'          :  data[loopCounter]['name'],
                        'title'         :  data[loopCounter]['designation_name'],
                        'image'         :  '<span class="black_image" style="background-image: url('+image+')"></span>',
                        'parent'        :  null,
                        'band'          :  '',
                        'department_id' :  data[loopCounter]['dept_id'],
                        'reporting_id'  :  data[loopCounter]['reporting_to_id'],
                        'line_manager'  :  null,
                        'companyname'   :  data[loopCounter]['companyname'],
                        'level_id'      :  data[loopCounter]['main_lm'],
                };

            }
            else {

                tempArray = {
                        'id'            :  data[loopCounter]['id'],
                        'parent'        :  data[loopCounter]['parent_id'],
                        'name'          :  data[loopCounter]['name'],
                        'title'         :  data[loopCounter]['designation_name'],
                        'image'         :  '<span class="black_image" style="background-image: url('+image+')"></span>',
                        'band'          :  '',
                        'department_id' :  data[loopCounter]['dept_id'],
                        'reporting_id'  :  data[loopCounter]['reporting_to_id'],
                        'line_manager'  :  null,
                        'companyname'   :  data[loopCounter]['companyname'],
                        'level_id'      :  data[loopCounter]['main_lm'],
                };

            }

        }
        else if(option == 2) {
            
            if( data[loopCounter]['parent_id'] == '' || data[loopCounter]['parent_id'] == null
            || data[loopCounter]['parent_id'] == 'undefined' ) {

                tempArray = {
                    'id'            :  data[loopCounter]['group_id'],
                    'name'          :  ((data[loopCounter]['line_manager'] != '' 
                                        || data[loopCounter]['line_manager'] != 'undefined'
                                        || data[loopCounter]['line_manager'] != null) && data[loopCounter]['line_manager'] == '0' ) ? data[loopCounter]['designation_name'] : data[loopCounter]['name'],
                    'title'         :  data[loopCounter]['designation_name'],
                    'image'         :  '<p class="number_image">'+this.pad(data[loopCounter]['total_employee'], 2)+'</p>',//'<span class="black_image" style="background-image: url('.image.')"></span>',
                    'parent'        :  null,
                    'band'          :  '',
                    'department_id' :  data[loopCounter]['dept_id'],
                    'reporting_id'  :  data[loopCounter]['reporting_to_id'],
                    'line_manager'  :  data[loopCounter]['line_manager'],
                    'companyname'   :  data[loopCounter]['companyname'],
                };

            }
            else {

                tempArray = {
                    'id'            :  data[loopCounter]['group_id'],
                    'parent'        :  data[loopCounter]['parent_id'],
                    'name'          :  ((data[loopCounter]['line_manager'] != '' 
                                        || data[loopCounter]['line_manager'] != 'undefined'
                                        || data[loopCounter]['line_manager'] != null) || data[loopCounter]['line_manager'] == '0' ) ? data[loopCounter]['designation_name'] : data[loopCounter]['name'],
                    'title'         :  data[loopCounter]['designation_name'],
                    'image'         :  '<p class="number_image">'+this.pad(data[loopCounter]['total_employee'], 2)+'</p>',//(stringCheck.isset(data[loopCounter]['line_manager']) && data[loopCounter]['line_manager'] > '0' ) ? '<span class="black_image" style="background-image: url('.image.')"></span>' : '<p class="number_image">'.data[loopCounter]['total_employee'].'</p>',
                    'band'          :  '',//data[loopCounter]['band_id'],
                    'department_id' :  data[loopCounter]['dept_id'],
                    'reporting_id'  :  data[loopCounter]['reporting_to_id'],
                    'line_manager'  :  data[loopCounter]['line_manager'],
                    'companyname'   :  data[loopCounter]['companyname'],
                };

            }

        }
        else if(option == 3) {

            if( data[loopCounter]['parent_id'] == '' || data[loopCounter]['parent_id'] == null
            || data[loopCounter]['parent_id'] == 'undefined' ) {

                tempArray = {
                    'id'            :  data[loopCounter]['id'],
                    'name'          :  (data[loopCounter]['group_name'] != '' || data[loopCounter]['group_name'] != null 
                                        || data[loopCounter]['group_name'] != 'undefined') ? data[loopCounter]['group_name'] : data[loopCounter]['name'],
                    'title'         :  (data[loopCounter]['group_designation_name'] != '' || data[loopCounter]['group_designation_name'] != null 
                                        || data[loopCounter]['group_designation_name'] != 'undefined') ? data[loopCounter]['group_designation_name'] : data[loopCounter]['designation_name'],
                    'image'         :  '<span class="black_image" style="background-image: url('+image+')"></span>',
                    'parent'        :  null,
                    'level_id'      :  data[loopCounter]['level_id'],
                    'band'          :  data[loopCounter]['band_id'],
                    'department_id' :  data[loopCounter]['dept_id'],
                    'reporting_id'  :  data[loopCounter]['reporting_to_id'],
                    'line_manager'  :  data[loopCounter]['line_manager'],
                    'companyname'   :  data[loopCounter]['department'],
                    'group_image'   :  (data[loopCounter]['group_picture'] != '' || data[loopCounter]['group_picture'] != null 
                                        || data[loopCounter]['group_picture'] != 'undefined') ? data[loopCounter]['group_picture'] : data[loopCounter]['picture'],
                    'group_id'      :  (data[loopCounter]['group_id'] != '' || data[loopCounter]['group_id'] != null 
                                        || data[loopCounter]['group_id'] != 'undefined') ? data[loopCounter]['group_id'] : data[loopCounter]['id'],
                };

            }
            else {

                tempArray = {
                    'id'            :  data[loopCounter]['id'],
                    'parent'        :  data[loopCounter]['parent_id'],
                    'name'          :  (data[loopCounter]['group_name'] != '' || data[loopCounter]['group_name'] != null 
                                        || data[loopCounter]['group_name'] != 'undefined') ? data[loopCounter]['group_name'] : data[loopCounter]['name'],
                    'title'         :  (data[loopCounter]['group_designation_name'] != '' || data[loopCounter]['group_designation_name'] != null 
                                        || data[loopCounter]['group_designation_name'] != 'undefined') ? data[loopCounter]['group_designation_name'] : data[loopCounter]['designation_name'],
                    'image'         :  '<span class="black_image" style="background-image: url('+image+')"></span>',
                    'band'          :  data[loopCounter]['band_id'],
                    'level_id'      :  data[loopCounter]['level_id'],
                    'department_id' :  data[loopCounter]['dept_id'],
                    'reporting_id'  :  data[loopCounter]['reporting_to_id'],
                    'line_manager'  :  data[loopCounter]['line_manager'],
                    'companyname'   :  data[loopCounter]['department'],
                    'group_image'   :  (data[loopCounter]['group_picture'] != '' || data[loopCounter]['group_picture'] != null 
                                        || data[loopCounter]['group_picture'] != 'undefined') ? data[loopCounter]['group_picture'] : data[loopCounter]['picture'],
                    'group_id'      :  (data[loopCounter]['group_id'] != '' || data[loopCounter]['group_id'] != null 
                                        || data[loopCounter]['group_id'] != 'undefined') ? data[loopCounter]['group_id'] : data[loopCounter]['id'],
                };

            }

        }
        else if( option == 4 ) {

            if( data[loopCounter]['parent_id'] == '' && data[loopCounter]['parent_id'] == null
            && data[loopCounter]['parent_id'] == 'undefined' ) {

                tempArray = {
                    'id'            :  data[loopCounter]['id'],
                    'name'          :  (data[loopCounter]['group_name'] != '' && data[loopCounter]['group_name'] != null
                                        && data[loopCounter]['group_name'] != 'undefined') ? data[loopCounter]['group_name'] : data[loopCounter]['name'],
                    'title'         :  (data[loopCounter]['group_designation_name'] != '' && data[loopCounter]['group_designation_name'] != null
                                        && data[loopCounter]['group_designation_name'] != 'undefined') ? data[loopCounter]['group_designation_name'] : data[loopCounter]['designation_name'],
                    'image'         :  '<span class="black_image" style="background-image: url('+image+')"></span>',
                    'parent'        :  null,
                    'band'          :  data[loopCounter]['band_id'],
                    'department_id' :  data[loopCounter]['dept_id'],
                    'reporting_id'  :  data[loopCounter]['reporting_to_id'],
                    'line_manager'  :  data[loopCounter]['line_manager'],
                    'companyname'   :  data[loopCounter]['department'],
                    'level_id'      :  (data[loopCounter]['level_id'] != '' && data[loopCounter]['level_id'] != null
                                        && data[loopCounter]['level_id'] != 'undefined') ? data[loopCounter]['level_id'] : data[loopCounter]['group_level_id'],
                    'group_image'   :  (data[loopCounter]['group_picture'] != '' && data[loopCounter]['group_picture'] != null
                                        && data[loopCounter]['group_picture'] != 'undefined') ? data[loopCounter]['group_picture'] : data[loopCounter]['picture'],
                    'group_id'      :  (!stringCheck.empty(data[loopCounter]['group_id'])) ? data[loopCounter]['group_id'] : data[loopCounter]['id'],
                };

            }
            else {

                tempArray = {
                    'id'            :  data[loopCounter]['id'],
                    'parent'        :  data[loopCounter]['parent_id'],
                    'name'          :  (data[loopCounter]['group_name'] != '' && data[loopCounter]['group_name'] != null
                                        && data[loopCounter]['group_name'] != 'undefined') ? data[loopCounter]['group_name'] : data[loopCounter]['name'],
                    'title'         :  (data[loopCounter]['group_designation_name'] != '' && data[loopCounter]['group_designation_name'] != null
                                        && data[loopCounter]['group_designation_name'] != 'undefined') ? data[loopCounter]['group_designation_name'] : data[loopCounter]['designation_name'],
                    'image'         :  '<span class="black_image" style="background-image: url('+image+')"></span>',
                    'band'          :  data[loopCounter]['band_id'],
                    'department_id' :  data[loopCounter]['dept_id'],
                    'reporting_id'  :  data[loopCounter]['reporting_to_id'],
                    'line_manager'  :  data[loopCounter]['line_manager'],
                    'companyname'   :  data[loopCounter]['department'],
                    'level_id'      :  (data[loopCounter]['level_id'] != '' && data[loopCounter]['level_id'] != null
                                        && data[loopCounter]['level_id'] != 'undefined') ? data[loopCounter]['level_id'] : data[loopCounter]['group_level_id'],
                    'group_image'   :  (data[loopCounter]['group_picture'] != '' && data[loopCounter]['group_picture'] != null
                                        && data[loopCounter]['group_picture'] != 'undefined') ? data[loopCounter]['group_picture'] : data[loopCounter]['picture'],
                    'group_id'      :  (!stringCheck.empty(data[loopCounter]['group_id'])) ? data[loopCounter]['group_id'] : data[loopCounter]['id'],
                };

            }

        }

        return tempArray;
    }

    approvalMatrixRecord(filters) {

        let filter = this.getFilterQuery(filters);

        let query = `SELECT 
                    DISTINCT(pe.id), pe.name, pe.dept_id, pe.reporting_to_id, pe.name_salute, 
                    ( CASE WHEN pe.picture IS NULL OR pe.picture = '' THEN 'n' ELSE pe.picture END ) AS picture,
                    (
                    SELECT 
                    (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id 
                    FROM proll_department 
                    INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    INNER JOIN (
                        SELECT 
                            DISTINCT(pe.id) AS id
                            FROM proll_employee pe
                            INNER JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                            LEFT JOIN proll_client pc ON pc.id = pe.cid
                            LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                            LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                            LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                            LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                            LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                            LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                            WHERE pe.status = 1 AND pc.id = '48' `+filter+`
                    ) AS pj ON pj.id = proll_employee.id
                    WHERE proll_department.id IN (
                    (CASE WHEN pe.reporting_to_id != 0 AND pe.reporting_to_id != pe.dept_id THEN pe.reporting_to_id ELSE pe.dept_id END) 
                    )
                    )
                    AS parent_id,
                    eb.band_desc,
                    eb.id AS band_id,
                    pcd.designation_name,
                    pe.main_lm,
                    (SELECT department FROM proll_department WHERE proll_department.id = pe.dept_id) AS companyname
                    FROM proll_employee pe
                    INNER JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND pc.id = '48' `+filter+`
                    ORDER BY eb.band_desc ASC, parent_id ASC, pe.id ASC`;

        return this.executeQuery( query );

    }

    getEmployeeHeadCountRecord(filters) {

        let filter = this.getFilterQuery(filters);

        let query = `SELECT 
                    DISTINCT(pe.id), 
                    pe.id AS group_id,
                    pe.name, pe.dept_id, 
                    ( 1 ) AS total_employee,
                    pe.reporting_to_id, pe.name_salute, 
                    ( CASE WHEN pe.picture IS NULL OR pe.picture = '' THEN 'n' ELSE pe.picture END ) AS picture,
                    (
                    SELECT 
                    (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id 
                    FROM proll_department 
                    INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    INNER JOIN (
                        SELECT 
                                DISTINCT(pe.id) AS id
                            FROM proll_employee pe
                            INNER JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                            LEFT JOIN proll_client pc ON pc.id = pe.cid
                            LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                            LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                            LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                            LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                            LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                            LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                            WHERE pe.status = 1 AND pc.id = '48' `+filter+`
                    ) AS pj ON pj.id = proll_employee.id
                    WHERE proll_department.id IN (
                    (CASE WHEN pe.reporting_to_id != 0 AND pe.reporting_to_id != pe.dept_id THEN pe.reporting_to_id ELSE pe.dept_id END) 
                    )
                    )
                    AS parent_id,
                    (
                    SELECT 
                    SUM(( CASE WHEN proll_employee.id = pe.id THEN 1 ELSE 0 END )) AS id
                        FROM proll_department
                        INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    )
                    AS line_manager,
                    eb.band_desc,
                    pcd.designation_name,
                    (SELECT fd.department FROM proll_department fd INNER JOIN proll_employee fe ON fe.dept_id = fd.id WHERE fe.id = pe.id) AS companyname
                    FROM proll_employee pe
                    INNER JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND (
                    SELECT 
                    SUM(( CASE WHEN proll_employee.id = pe.id THEN 1 ELSE 0 END )) AS id
                        FROM proll_department
                        INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    ) > 0 AND pc.id = '48' `+filter+`
                    
                    UNION
                
                SELECT 
                    DISTINCT(pe.id),
                    GROUP_CONCAT(DISTINCT pe.id SEPARATOR '-') AS group_id,
                    GROUP_CONCAT(pe.name SEPARATOR '_') AS name, 
                    pe.dept_id, 
                    COUNT(DISTINCT(pe.id)) AS total_employee,
                    pe.reporting_to_id, pe.name_salute, 
                    ( CASE WHEN pe.picture IS NULL OR pe.picture = '' THEN 'n' ELSE pe.picture END ) AS picture,
                    (
                    SELECT 
                    (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id 
                    FROM proll_department 
                    INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    INNER JOIN (
                        SELECT 
                            DISTINCT(pe.id) AS id
                            FROM proll_employee pe
                            INNER JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                            LEFT JOIN proll_client pc ON pc.id = pe.cid
                            LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                            LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                            LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                            LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                            LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                            LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                            WHERE pe.status = 1 AND pc.id = '48' `+filter+`
                    ) AS pj ON pj.id = proll_employee.id
                    WHERE proll_department.id IN (
                    (CASE WHEN pe.reporting_to_id != 0 AND pe.reporting_to_id != pe.dept_id THEN pe.dept_id ELSE pe.dept_id END) 
                    )
                    )
                    AS parent_id,
                    (
                    SELECT 
                    SUM(( CASE WHEN proll_employee.id = pe.id THEN 1 ELSE 0 END )) AS id
                        FROM proll_department
                        INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    )
                    AS line_manager,
                    eb.band_desc,
                    pcd.designation_name AS designation_name,
                    (SELECT fd.department FROM proll_department fd INNER JOIN proll_employee fe ON fe.dept_id = fd.id WHERE fe.id = pe.id) AS companyname
                    FROM proll_employee pe
                    INNER JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND (
                    SELECT 
                    SUM(( CASE WHEN proll_employee.id = pe.id THEN 1 ELSE 0 END )) AS id
                        FROM proll_department
                        INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    ) < 1 AND pc.id = '48' `+filter+`
                    GROUP BY pcd.designation_id
                    ORDER BY band_desc ASC, line_manager DESC, parent_id ASC`;

        return this.executeQuery( query );

    }

    getEmployeeByDepartment(filters) {

        let filter = this.getFilterQuery(filters);

        let query = `SELECT DISTINCT(pe.id) AS id,
                    (
                    SELECT 
                    (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id 
                    FROM proll_department 
                    INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    INNER JOIN (
                        SELECT 
                                DISTINCT(pe.id) AS id
                            FROM proll_employee pe
                            INNER JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                            LEFT JOIN proll_client pc ON pc.id = pe.cid
                            LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                            LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                            LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                            LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                            LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                            LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                            WHERE pe.status = 1 AND pe.cid = '48' `+filter+`
                    ) AS pj ON pj.id = proll_employee.id
                    WHERE proll_department.id IN (
                    (CASE WHEN pe.reporting_to_id != 0 AND pe.reporting_to_id != pe.dept_id THEN pe.reporting_to_id ELSE pe.dept_id END) 
                    )
                    ) AS parent_id,
                    pe.main_lm AS level_id,
                    ( CASE WHEN pe.picture IS NULL OR pe.picture = '' THEN 'n' ELSE pe.picture END ) AS picture,
                    pe.name,
                    pd.department,
                    pd.id AS dept_id,
                    pcd.designation_name,
                    eb.band_desc AS band_id,
                    ( SELECT ( CASE WHEN GROUP_CONCAT(
                        (CASE WHEN pe2.name IS NULL OR pe2.name = '' THEN '' ELSE pe2.name END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) IS NOT NULL THEN 
                                    GROUP_CONCAT(
                    (CASE WHEN pe2.name IS NULL OR pe2.name = '' THEN '' ELSE pe2.name END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) ELSE '' END)
                    FROM proll_employee pe2
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    ) AS group_name,
                    ( SELECT ( CASE WHEN GROUP_CONCAT(pe2.id ORDER BY eb2.band_desc ASC SEPARATOR '_' ) IS NOT NULL THEN
                                    GROUP_CONCAT(pe2.id ORDER BY eb2.band_desc ASC SEPARATOR '_' ) ELSE '' END )
                    FROM proll_employee pe2
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    ) AS group_id,
                    ( SELECT ( CASE WHEN GROUP_CONCAT(
                        (CASE WHEN pe2.picture IS NULL OR pe2.picture = '' THEN 'n' ELSE pe2.picture END)  ORDER BY eb2.band_desc ASC SEPARATOR '|' ) IS NOT NULL THEN
                                    GROUP_CONCAT(
                    (CASE WHEN pe2.picture IS NULL OR pe2.picture = '' THEN 'n' ELSE pe2.picture END) ORDER BY eb2.band_desc ASC SEPARATOR '|' ) ELSE '' END )
                    FROM proll_employee pe2
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    ) AS group_picture,
                    ( SELECT MAX(pe2.id) FROM proll_employee pe2
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    ) AS group_view_id,
                    ( SELECT ( CASE WHEN GROUP_CONCAT( 
                        (CASE WHEN eb2.band_desc IS NULL OR eb2.band_desc = '' THEN '' ELSE eb2.band_desc END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) IS NOT NULL THEN
                                    GROUP_CONCAT( 
                    (CASE WHEN eb2.band_desc IS NULL OR eb2.band_desc = '' THEN '' ELSE eb2.band_desc END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) ELSE '' END )
                    FROM proll_employee pe2
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    )  AS group_band,
                    ( SELECT ( CASE WHEN GROUP_CONCAT( 
                    (CASE WHEN pcd2.designation_name IS NULL OR pcd2.designation_name = '' THEN '' ELSE pcd2.designation_name END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) IS NOT NULL THEN
                                    GROUP_CONCAT( 
                    (CASE WHEN pcd2.designation_name IS NULL OR pcd2.designation_name = '' THEN '' ELSE pcd2.designation_name END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) ELSE '' END )
                    FROM proll_employee pe2
                    LEFT JOIN proll_client_designation pcd2 ON pcd2.designation_id = pe2.designation
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    )  AS group_designation_name,
                    ( SELECT ( CASE WHEN GROUP_CONCAT(pe2.main_lm ORDER BY eb2.band_desc ASC SEPARATOR '_' ) IS NOT NULL THEN
                                GROUP_CONCAT(pe2.main_lm ORDER BY eb2.band_desc ASC SEPARATOR '_' ) ELSE '' END )
                    FROM proll_employee pe2
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    ) AS group_level_id
                    FROM proll_department pd
                    INNER JOIN proll_employee pe ON pe.loginname = pd.email
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND pe.cid = '48' `+filter+`
                    ORDER BY eb.band_desc ASC`;

        return this.executeQuery( query );

    }

    getEmployeeCountByDesignation(filters) {

        let filter = this.getFilterQuery(filters);

        let query = `SELECT DISTINCT(pcd.designation_id), 
                    ( SELECT count(proll_employee.id) FROM proll_employee 
                        WHERE proll_employee.designation = pcd.designation_id 
                    ) AS total_employee,
                    pcd.designation_name
                    FROM proll_employee pe
                    INNER JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    LEFT JOIN proll_client_location pcl ON pcl.loc_id = pe.loc_id
                    WHERE pe.status = 1 AND pc.id = '48' `+filter+`
                    ORDER BY pcd.designation_name ASC`;

        return this.executeQuery( query );

    }

    getFilterQuery(filters) {

        let filter = '';

        if(typeof filters.filter != 'undefined' && filters.filter == '1') {

            if(typeof filters.country != 'undefined' && filters.country != '') {

                filter += ' AND pcl.country_id IN('+filters.country+') ';

            }

            if(typeof filters.city != 'undefined' && filters.city != '') {

                filter += ' AND pcl.city_id IN('+filters.city+') ';

            }

            if( typeof filters.branch != 'undefined' && filters.branch != '' ){

                filter += ' AND pcl.loc_id IN('+filters.branch+') ';

            }

            if( typeof filters.group != 'undefined' && filters.group != '' ){

                filter += ' AND dg2.department_group2_id IN('+filters.group+') ';

            }

            if( typeof filters.family != 'undefined' && filters.family != ''  ){

                filter += ' AND dg3.department_group3_id IN('+filters.family+') ';

            }

            if( typeof filters.department != 'undefined' && filters.department != ''  ){

                filter += ' AND pd.id IN('+filters.department+') ';

            }

            if( typeof filters.designation != 'undefined' && filters.designation != ''  ){

                filter += ' AND pcd.designation_id IN('+filters.designation+')';

            }

            if( typeof filters.band != 'undefined' && filters.band != ''  ){

                filter += ' AND eb.id IN('+filters.band+')';

            }

        }
        else {

            filter = '';

        }

        return filter;
    }

    manipulateEmployeeDepartData( data ) {

        let result = [];

        let arrayLength = data.length;

        let i = 0;

        const site_url = config.site.SITE_URL || "";

        const ppm_url = config.site.PPM_URL || "";

        var placeHolder = 'micro_dummy_pic.jpg';

        if( arrayLength > 0 ) {

            do{

                let groupId         =   data[i]['group_id'];
                let group_name      =   data[i]['group_name'];
                let group_picture   =   data[i]['group_picture'];
                let group_view_id   =   data[i]['group_view_id'];
                let parentId        =   data[i]['id'];
                let group_level_id  =   data[i]['group_level_id'];

                let tempArray = {

                    'id'                        :   data[i]['id'],
                    'parent_id'                 :   data[i]['parent_id'],
                    'level_id'                  :   data[i]['level_id']+"_"+group_level_id,
                    'group_id'                  :   data[i]['id']+"_"+groupId,
                    'group_name'                :   data[i]['name']+"_"+group_name,
                    'group_picture'             :   data[i]['picture']+"|"+group_picture,
                    'department'                :   data[i]['department'],
                    'group_designation_name'    :   data[i]['designation_name']+"_"+data[i]['group_designation_name'],
                    'band_id'                   :   data[i]['band_id']+"_"+data[i]['group_band'],

                };

                delete data[i]['group_id'];
                delete data[i]['group_name'];
                delete data[i]['group_picture'];
                delete data[i]['group_view_id'];
                delete data[i]['group_designation_name'];
                delete data[i]['group_level_id'];

                result.push( tempArray );

                i++;
            }while(i < arrayLength);

        }

        return result;

    }

    getEmployeeByDepartmentLM() {

        let query = `SELECT DISTINCT(pe.id) AS id,
                    (
                    SELECT 
                    (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id 
                    FROM proll_department 
                    INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    WHERE proll_department.id IN (
                    (CASE WHEN pe.reporting_to_id != 0 AND pe.reporting_to_id != pe.dept_id THEN pe.reporting_to_id ELSE pe.dept_id END) 
                    )
                    ) AS parent_id,
                    pe.main_lm AS level_id,
                    ( CASE WHEN pe.picture IS NULL OR pe.picture = '' THEN 'n' ELSE pe.picture END ) AS picture,
                    pe.name,
                    pd.department,
                    pd.id AS dept_id,
                    pcd.designation_name,
                    eb.band_desc AS band_id,
                    ( SELECT ( CASE WHEN GROUP_CONCAT(
                        (CASE WHEN pe2.name IS NULL OR pe2.name = '' THEN '' ELSE pe2.name END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) IS NOT NULL THEN 
                                    GROUP_CONCAT(
                        (CASE WHEN pe2.name IS NULL OR pe2.name = '' THEN '' ELSE pe2.name END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) ELSE '' END)
                    FROM proll_employee pe2
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    ) AS group_name,
                    ( SELECT ( CASE WHEN GROUP_CONCAT(pe2.id ORDER BY eb2.band_desc ASC SEPARATOR '_' ) IS NOT NULL THEN
                                    GROUP_CONCAT(pe2.id ORDER BY eb2.band_desc ASC SEPARATOR '_' ) ELSE '' END )
                    FROM proll_employee pe2
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    ) AS group_id,
                    ( SELECT ( CASE WHEN GROUP_CONCAT(
                        (CASE WHEN pe2.picture IS NULL OR pe2.picture = '' THEN 'n' ELSE pe2.picture END)  ORDER BY eb2.band_desc ASC SEPARATOR '|' ) IS NOT NULL THEN
                                    GROUP_CONCAT(
                        (CASE WHEN pe2.picture IS NULL OR pe2.picture = '' THEN 'n' ELSE pe2.picture END) ORDER BY eb2.band_desc ASC SEPARATOR '|' ) ELSE '' END )
                    FROM proll_employee pe2
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    ) AS group_picture,
                    ( SELECT MAX(pe2.id) FROM proll_employee pe2
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    ) AS group_view_id,
                    ( SELECT ( CASE WHEN GROUP_CONCAT( 
                        (CASE WHEN eb2.band_desc IS NULL OR eb2.band_desc = '' THEN '' ELSE eb2.band_desc END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) IS NOT NULL THEN
                                    GROUP_CONCAT( 
                        (CASE WHEN eb2.band_desc IS NULL OR eb2.band_desc = '' THEN '' ELSE eb2.band_desc END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) ELSE '' END )
                    FROM proll_employee pe2
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    )  AS group_band,
                    ( SELECT ( CASE WHEN GROUP_CONCAT( 
                    (CASE WHEN pcd2.designation_name IS NULL OR pcd2.designation_name = '' THEN '' ELSE pcd2.designation_name END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) IS NOT NULL THEN
                                    GROUP_CONCAT( 
                        (CASE WHEN pcd2.designation_name IS NULL OR pcd2.designation_name = '' THEN '' ELSE pcd2.designation_name END) ORDER BY eb2.band_desc ASC SEPARATOR '_' ) ELSE '' END )
                    FROM proll_employee pe2
                    LEFT JOIN proll_client_designation pcd2 ON pcd2.designation_id = pe2.designation
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    )  AS group_designation_name,
                    ( SELECT ( CASE WHEN GROUP_CONCAT(pe2.main_lm ORDER BY eb2.band_desc ASC SEPARATOR '_' ) IS NOT NULL THEN
                                GROUP_CONCAT(pe2.main_lm ORDER BY eb2.band_desc ASC SEPARATOR '_' ) ELSE '' END )
                    FROM proll_employee pe2
                    LEFT JOIN employee_bands eb2 ON eb2.id = pe2.emp_band
                    WHERE pe2.dept_id = pd.id AND pe2.status = 1 AND pe2.id != pe.id 
                    ) AS group_level_id
                    FROM proll_department pd
                    INNER JOIN proll_employee pe ON pe.loginname = pd.email
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    WHERE pe.status = 1 AND pe.cid = '48'
                    ORDER BY eb.band_desc ASC`;

        return this.executeQuery( query );

    }

    manipulateEmpDepartLmData( data ) {

        let result = [];

        let arrayLength = data.length;

        let i = 0;

        if( arrayLength > 0 ) {

            do{

                let groupId         =   data[i]['group_id'];
                let group_name      =   data[i]['group_name'];
                let group_picture   =   data[i]['group_picture'];
                let group_view_id   =   data[i]['group_view_id'];
                let group_bands     =   data[i]['group_band'];
                let parentId        =   data[i]['id'];
                let group_level_id  =   data[i]['group_level_id'];

               let tempArray = {
                   'id'                     :   group_view_id,
                   'parent_id'              :   parentId,
                   'groupId'                :   groupId,
                   'group_name'             :   group_name,
                   'group_picture'          :   group_picture,
                   'department'             :   data[i]['department'],
                   'group_designation_name' :   data[i]['group_designation_name'],
                   'band_id'                :   group_bands,
                   'group_id'               :   groupId,
                   'group_level_id'         :   group_level_id
               };

               delete data[i]['group_id'] ;
               delete data[i]['group_name'];
               delete data[i]['group_picture'];
               delete data[i]['group_view_id'];
               delete data[i]['group_designation_name'];
               delete data[i]['group_band'];
               delete data[i]['group_level_id'];

               array_push( result, data[i] );
               
               if(  (groupId != null || groupId != 'undefined') && 
                    (group_view_id != null || group_view_id != 'undefined') ) {

                    array_push( result, tempArray );
            
                }

                i++;
            }while( i < arrayLength);

        }

        return result;
    }

    getDirectReportingLM( userId ) {

        let query = `SELECT 
                    DISTINCT(pe.id), pe.name, pe.dept_id, pe.reporting_to_id, pe.name_salute, pe.picture,
                    (
                    SELECT 
                    (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id 
                    FROM proll_department 
                    INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    WHERE proll_department.id IN (
                    (CASE WHEN pe.reporting_to_id != 0 AND pe.reporting_to_id != pe.dept_id THEN pe.reporting_to_id ELSE pe.dept_id END) 
                    )
                    )
                    AS parent_id,
                    eb.band_desc,
                    eb.id AS band_id,
                    pcd.designation_name,
                    (SELECT department FROM proll_department WHERE proll_department.id = pe.dept_id) AS companyname
                    FROM proll_employee pe
                    INNER JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    WHERE pe.status = 1 AND pc.id = '48' AND pe.id IN ( `+userId+`, ( SELECT 
                    DISTINCT (
                    SELECT 
                    (CASE WHEN proll_employee.id = pe.id THEN 0 ELSE proll_employee.id END) AS id 
                    FROM proll_department 
                    INNER JOIN proll_employee ON proll_employee.loginname = proll_department.email
                    WHERE proll_department.id IN (
                    (CASE WHEN pe.reporting_to_id != 0 AND pe.reporting_to_id != pe.dept_id THEN pe.reporting_to_id ELSE pe.reporting_to_id END) 
                    )
                    )
                    AS parent_id
                    FROM proll_employee pe
                    INNER JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    WHERE pe.status = 1 AND pc.id = '48' AND pe.id IN (`+ userId +`)
                    ORDER BY eb.band_desc ASC, parent_id ASC, pe.id ASC ) )
                    ORDER BY parent_id ASC, pe.id ASC, eb.band_desc ASC`;

        return this.executeQuery( query );

    }

    manipulateDirectReportingLMData( data ) {

        let finalArray = [];

        if(count(data) > 0) {

            let firstNodeId         =   data['0']['id'];
            let firstNodeParentId   =   data['0']['parent_id'];

            let secondNodeId        =   data['1']['id'];
            let secondNodeParentId  =   data['1']['parent_id'];

            if( firstNodeId === secondNodeParentId ) {

                finalArray = data;

            }
            else if( secondNodeId === firstNodeParentId ) {

                finalArray = data.reverse();

            }

            finalArray['0']['parent_id'] = null;

        }

        return finalArray;
    }

    async getSingleEmployeeData( userId ) {

        let exploded_id = userId.split("_")[1];

        if( exploded_id.length > 0 ) {

            let id = exploded_id;

            let actId = null;

            if( id.includes( "-" ) !== false ) {

                actId = id.split("-").join(",");

            }
            else {

                actId = id;

            }

            if( actId != '' || actId != null || actId != 'undefined' ){

                let data = null;

                try {
                    
                    data = await this.singleEmployeeDataQuery(actId);

                }
                catch(e) {
                    console.log('catch an error ', e);
                }

                return data;

            }
            else {

                return [];

            }

        }
        else {

            return [];

        }

    }

    async getOrgLocations(filters = '') {

        let filter = this.getFilterQuery(filters);

        let query = `SELECT
                    DISTINCT(pcl.loc_id),
                    pcl.latitude,
                    pcl.longitude,
                    pcl.loc_id,
                    pcl.loc_desc,
                    pcl.country_id,
                    pcl.city_id
                    FROM proll_client_location pcl
                    LEFT JOIN proll_employee pe ON pe.loc_id = pcl.loc_id
                    LEFT JOIN proll_department pd ON (pd.id = pe.reporting_to_id OR pd.id = pe.dept_id)
                    LEFT JOIN proll_client pc ON pc.id = pe.cid
                    LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
                    LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
                    LEFT JOIN department_group3 dg3 ON dg3.department_group3_id = pd.department_group3_id
                    LEFT JOIN department_group2 dg2 ON dg2.department_group2_id = dg3.department_group2_id
                    LEFT JOIN department_group1 dg1 ON dg1.department_group1_id = dg2.department_group1_id
                    WHERE pcl.latitude IS NOT NULL AND pcl.longitude IS NOT NULL
                    AND pe.status = 1 AND pc.id = '48' `+filter;

        return this.executeQuery( query );

    }

    singleEmployeeDataQuery(actId, clientId = '48') {

        let query = `SELECT 
        (CASE WHEN pe.name IS NULL OR pe.name = '' THEN 'N/A' ELSE pe.name END) AS 'name',
        (CASE WHEN ped.gender IS NULL OR ped.gender = '' THEN 'N/A' ELSE ped.gender END) AS 'gender',
        (CASE WHEN pe.doj IS NULL OR pe.doj = '' THEN 'N/A' ELSE DATE_FORMAT(pe.doj, '%d-%m-%Y') END) AS 'doj',
        (CASE WHEN pe.empcode IS NULL OR pe.empcode = '' THEN 'N/A' ELSE pe.empcode END) AS 'empcode',
        (CASE WHEN eb.band_desc IS NULL OR eb.band_desc = '' THEN 'N/A' ELSE eb.band_desc END) AS 'band',
        (CASE WHEN pd.department IS NULL OR pd.department = '' THEN 'N/A' ELSE pd.department END) AS 'department',
        (pe.picture) AS 'picture',
        (CASE WHEN pe.name_salute IS NULL OR pe.name_salute = '' THEN 'N/A' ELSE pe.name_salute END) AS 'name_salute',
        (CASE WHEN pcd.designation_name IS NULL OR pcd.designation_name = '' THEN 'N/A' ELSE pcd.designation_name END) AS 'designation_name',
        pe.id,
        (CASE WHEN dg3.department_group3 IS NULL OR dg3.department_group3 = '' THEN 'N/A' ELSE dg3.department_group3 END) AS 'job_families',
        (CASE WHEN dg2.department_group2 IS NULL OR dg2.department_group2 = '' THEN 'N/A' ELSE dg2.department_group2 END) AS 'job_grouop',
        dg1.department_group1,
        (CASE WHEN sdl1.sub_department_level1 IS NULL OR sdl1.sub_department_level1 = '' THEN 'N/A' ELSE sdl1.sub_department_level1 END ) AS 'sub_department',
        sdl2.sub_department_level2
        FROM proll_employee pe
        LEFT JOIN employee_bands eb ON eb.id = pe.emp_band
        LEFT JOIN proll_department pd ON pd.id = pe.dept_id
        LEFT JOIN proll_client_designation pcd ON pcd.designation_id = pe.designation
        LEFT JOIN proll_employee_detail ped ON ped.empid = pe.id
        LEFT JOIN department_group3 dg3 ON dg3.department_group3_id=pd.department_group3_id
        LEFT JOIN department_group2 dg2 ON dg2.department_group2_id=dg3.department_group2_id
        LEFT JOIN department_group1 dg1 ON dg1.department_group1_id=dg2.department_group1_id
        LEFT JOIN sub_department_level1 sdl1 ON sdl1.department_id=pd.id
        LEFT JOIN sub_department_level2 sdl2 ON sdl2.sub_department_level2_id = sdl1.sub_department_level1_id
        WHERE pe.id IN (`+actId+`) AND pe.cid =`+clientId;

        return this.executeQuery( query );

    }

    async checkUser(username, password) {

        let checkUser = await this.validateEmail(username);
        let checkPassword = await this.validatePassword(username, password);

        if(!checkUser || checkUser.length == 0) {
            return {
                "response": false,
                "err": "Invalid Username Provided",
            };
        }
        else if(!checkPassword || checkPassword.length == 0) {
            return {
                "response": false,
                "err": "Invalid Password Provided",
            };
        }
        else {
            return {
                "response": true,
                "email": checkPassword[0].loginname,
                "code": checkPassword[0].pass_word,
                "id": checkPassword[0].id,
                "empcode": checkPassword[0].empcode,
                "emp_name": checkPassword[0].name,
            }

        }
        

    }

    async validateEmail( username ) {
        
        let query = `SELECT * FROM proll_employee pe WHERE pe.loginname LIKE '`+username+`'`;

        return this.executeQuery( query );

    }

    async validatePassword( username, password ) {

        let query = `SELECT * FROM proll_employee pe WHERE pe.loginname LIKE '`+username+`'
                    AND pe.e_pass_word LIKE '`+password+`'`;

        return this.executeQuery( query );

    }

    accessUser(userLoginId) {

        let query = `SELECT pe.loginname AS username, pe.e_pass_word AS secret, 
        mll.destination,
        (
            SELECT
            CASE 
                WHEN (mll.portal = 'hr' AND mll.role = 'hr') THEN mll.portal
                WHEN (mll.portal = 'hr' AND mll.role = 'lm') THEN mll.role
                WHEN (mll.portal = 'hr' AND mll.role = 'em') THEN mll.role
                WHEN (mll.portal = 'lm' AND mll.role = 'lm') THEN mll.portal
                WHEN (mll.portal = 'lm' AND mll.role = 'hr') THEN mll.role
                WHEN (mll.portal = 'lm' AND mll.role = 'em') THEN mll.role
                WHEN (mll.portal = 'em' AND mll.role = 'em') THEN mll.portal
                WHEN (mll.portal = 'em' AND mll.role = 'lm') THEN mll.role
                WHEN (mll.portal = 'em' AND mll.role = 'hr') THEN mll.role
                WHEN (mll.portal = 'ceo' AND mll.role = 'ceo') THEN mll.portal
                WHEN (mll.portal = 'ceo' AND mll.role = 'hr') THEN mll.portal
                WHEN (mll.portal = 'ceo' AND mll.role = 'lm') THEN mll.role
                WHEN (mll.portal = 'ceo' AND mll.role = 'em') THEN mll.role
                WHEN (mll.portal = 'hr' AND mll.role = 'ceo') THEN mll.role
                WHEN (mll.portal = 'lm' AND mll.role = 'ceo') THEN mll.role
                WHEN (mll.portal = 'em' AND mll.role = 'ceo') THEN mll.role
            ELSE
            mll.role
            END
            ) portal,
        mll.id FROM multi_login ml
        INNER JOIN proll_employee pe ON pe.id = ml.employee_id
        INNER JOIN multi_login_log mll ON mll.multi_login_id = ml.id
        WHERE ml.id = '`+userLoginId+`' AND mll.des_portal LIKE 'angular_ppi_people' 
        ORDER BY mll.id DESC LIMIT 0, 1`;

        return this.executeQuery( query );
    }

    executeQuery( query ) {

        return new Promise( (resolve, reject) => {
    
            db.query(query, (result, err) => {
    
                if (!err) { 
                            resolve(JSON.parse(JSON.stringify(result)));
                        }
                else {
                        console.log(err);
                        reject(err);
                    }
        
            });
    
        });

    }

}

exports.OrgChartData = OrgChartData;