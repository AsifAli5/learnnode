const mongoose = require('mongoose');
const employeeSchema = require('../schema/employee.js');
const EmployeeModel = mongoose.model('employee');

class Employee {

    constructor() {}

    checkEmployee() {
        
    }

    async saveEmployee(employeeId, token, callback) {

        const employee = new EmployeeModel({ 
              employeeId: employeeId,
              token: token
        });

        employee.save( (err) => {
            callback( err );
        });

    }

}

exports.Employee = Employee;