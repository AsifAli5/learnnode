var Database = require('../../../config/database');
var db = new Database();

class SeamlessLogin {

    constructor() { }

    async checkSeamlessLogin( employeeId ) {

        if( Number.isInteger(employeeId) ) {

            let query = "SELECT * FROM `multi_login` WHERE employee_id = "+employeeId;

            return this.executeQuery(query);

        }
        else {
            return false;
        }

    }

    async insertUserData(source, destination, portal, remoteAddr, agent, 
                        employeeId, isLogin, loginTime, extraParams) {

        let loginData  = await this.insertMultiLoginData(employeeId, isLogin, loginTime);
        let insertLoginLog = await this.insertMultiLoginLogData(employeeId, source, 
                                                                destination, remoteAddr, agent, portal,
                                                                extraParams);
        let loginId = await this.checkSeamlessLogin(employeeId);

        if(loginData.affectedRows > 0 && insertLoginLog.affectedRows > 0) {
        
          return {
            'status' : '200',
            'loginId' : loginId[0].id
          };
  
        }
        else {
          
          return {
            'status' : '500',
            'loginId' : false
          };
        }
        
    }

    async updateUserData(source, destination, portal, remoteAddr, agent,
                  employeeId, isLogin, loginTime, extraParams) {

      let loginData  = await this.updateMultiLoginData(employeeId, isLogin, loginTime);
      let insertLoginLog = await this.insertMultiLoginLogData(employeeId, source, 
                                                              destination, remoteAddr, agent, portal,
                                                              extraParams);
      let loginId = await this.checkSeamlessLogin(employeeId);
      
      if(loginData.affectedRows > 0 && insertLoginLog.affectedRows > 0) {
        
        return {
          'status' : '200',
          'loginId' : loginId[0].id
        };

      }
      else {
        
        return {
          'status' : '500',
          'loginId' : false
        };

      }
      
    }


    async insertMultiLoginData(employeeId, isLogin, loginTime) {

      let query = "INSERT INTO `multi_login` (`employee_id`, `is_login`, `login_time`) \n\
                  VALUES ('"+employeeId+"', '"+isLogin+"', "+loginTime+")";

      return this.executeQuery(query);

    }

    async updateMultiLoginData(employeeId, isLogin, loginTime) {

      let query = "UPDATE `multi_login` \n\
                  SET `is_login` = '"+isLogin+"', `login_time`= "+loginTime+" \n\
                  WHERE `employee_id` = "+employeeId;
      
      return this.executeQuery(query);

    }

    async insertMultiLoginLogData(employeeId, source, destination, addr, agent, portal, extraParams) {

      let loginUser = await this.checkSeamlessLogin(employeeId);

      let params = '';

      if(extraParams == '') {

        params = extraParams;

      }
      else {

        params = decodeURIComponent(extraParams);

      }

      if(loginUser.length > 0) {

        let query = "INSERT INTO `multi_login_log` (`multi_login_id`, `source`, `destination`, `client_ip`, `client_details`, `portal`, `is_login`, `src_portal`, `des_portal`, `params`)\n\
        VALUES ('"+loginUser[0].id+"', '"+source+"', '"+destination+"', '"+addr+"', '"+agent+"', '"+portal+"', '1', 'angular_ppi_people', 'php_ppi_people', '"+params+"')";

        return this.executeQuery(query);

      }
      else {

        return false;

      }

    }

    executeQuery(query) {
  
      return new Promise((resolve, reject) => {
  
        db.query(query, (result, err) => {
  
          if (!err) {
            resolve(JSON.parse(JSON.stringify(result)));
          }
          else {
            console.log(err);
            reject(err);
          }
  
        });
  
      });
  
    }
  
  }
  
  exports.SeamlessLogin = SeamlessLogin;