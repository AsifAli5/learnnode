var stringCheck = require("../../../../utils/StringCheck");
const config = require("../../../../../config");
const queryObj = require("../../helper/query");

class lettersCertificatesSetup {
  constructor() {}

  getLettersCertificatesCategories(client_id, conn, pool) {
    try {
      
      let query = `SELECT id, category_name                        
      FROM lc_categories WHERE cid=48 AND STATUS=${client_id};`;

      return queryObj.executeInsertQuery(query, conn);
      // }
    } catch (e) {
      console.log("entering catch block");
      console.log(e);
      return e;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }
  

}