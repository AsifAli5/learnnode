const queryObj = require("../../helper/query");
const date = require('../../utils/fomateDateForMySql')

class Orm {
    constructor() {
    }

    /**
     * @Author: ismail Raza
     * @Date: 23-02-2021
     */
    async create(tbl, obj, conn) {
        try {
            const keys = Object.keys(obj);
            const values = Object.values(obj)
            let rawQuery = `INSERT INTO ${tbl} (`;
            for (let i = 0; i < keys.length; i++) {
                if (keys[i + 1]) {
                    rawQuery += `${keys[i]}, `
                } else {
                    rawQuery += `${keys[i]} `
                }
            }
            rawQuery += `)VALUES(`
            for (let i = 0; i < keys.length; i++) {
                if (keys[i + 1]) {
                    rawQuery += `'${values[i]}', `
                } else {
                    rawQuery += `'${values[i]}') `
                }
            }
            return await queryObj.executeInsertQuery(rawQuery, conn);
        } catch (exception) {
            throw {message:exception.sqlMessage}
        }
    }

    /**
     * @Author: ismail Raza
     * @Date: 23-02-2021
     */
    async update(tbl, obj, cond, conn) {
        try {
            const currentDate = await date();
            obj = {...obj, updated_at: currentDate};
            const objKeys = Object.keys(obj);
            const objValues = Object.values(obj);
            let query = `UPDATE ${tbl} SET `
            for (let i = 0; i < objKeys.length; i++) {
                if (objKeys[i + 1]) {
                    query += `${objKeys[i]}='${objValues[i]}', `;
                } else {
                    query += `${objKeys[i]}='${objValues[i]}'`;
                }
            }
            query += ` where `;
            const condKeys = Object.keys(cond);
            const condValues = Object.values(cond);
            for (let i = 0; i < condKeys.length; i++) {
                if (condKeys[i + 1]) {
                    query += `${condKeys[i]}='${condValues[i]}' AND `;
                } else {
                    query += `${condKeys[i]}='${condValues[i]}';`;
                }
            }

            return await queryObj.executeInsertQuery(query, conn);
        } catch (exception) {
            throw {message:exception.sqlMessage}
        }
    }

    /**
     * @Author: ismail Raza
     * @Date: 23-02-2021
     */
    async searchSingleTable(tbl, obj, conn) {
        try {
            let query = `SELECT * FROM ${tbl} where `;
            const keys = Object.keys(obj);
            const values = Object.values(obj);
            for (let i = 0; i < keys.length; i++) {
                if (keys[i + 1]) {
                    query += `${keys[i]}='${values[i]}' and `;
                } else {
                    query += `${keys[i]}='${values[i]}'`;
                }
            }
            return await queryObj.executeSelectQuery(query, conn);
        } catch (exception) {
            throw {message:exception.sqlMessage}
        }
    }

    /**
     * @Author: ismail Raza
     * @Date: 23-02-2021
     */
    async findAll(tbl) {
        try {
            let query = `SELECT * FROM ${tbl}`;
            return await queryObj.executeSelectQuery(query);
        } catch (exception) {
            throw {message:exception.sqlMessage}
        }
    }
}

exports.Orm = Orm