const queryObj = require('../../helper/query');
const orm = require('./orm').Orm;
const dateFormat = require('../../utils/fomateDateForMySql');

class PayrollSetup {
    constructor() {
    }

    record = new orm()

    /**
     * @Author: Ismail Raza
     * @Date: 25-02-2021
     */

    async filterScreen(body, conn) {
        try {
            let query = `SELECT ps.setup_type, ps.setup_desc, ps.payroll_setup_id FROM cb_payroll_setup ps
                       WHERE ps.setup_type='${body.setup_type}' AND ps.setup_desc='${body.setup_desc}';`
            // if (body.setup_type === 'Statutory_Setup' && body.setup_desc === 'Group_Life_Insurance') {   && body.setup_desc !== 'Group_Life_Insurance'
            if (body.setup_type === 'Statutory_Setup' || body.setup_type==='Insurance') {
                query += `SELECT *, MIN(tssd.slab_start) AS slab_start ,MAX(tssd.slab_end) AS slab_end FROM cb_terminal_statutory_setup tss
                        INNER JOIN cb_payroll_setup ps
                        ON ps.payroll_setup_id= tss.payroll_setup_id
                        INNER JOIN cb_terminal_statutory_setup_details tssd
                        ON tssd.terminal_statutory_setup_id=tss.terminal_statutory_setup_id
                        WHERE ps.setup_type='${body.setup_type}' AND ps.setup_desc='${body.setup_desc}'
                        GROUP BY tss.terminal_statutory_setup_id
                        ORDER BY tss.payroll_physical_years_id DESC;
                        SELECT  tssd.*, ps.setup_type, ps.setup_desc, ps.payroll_setup_id
                        FROM cb_payroll_setup ps
                        INNER JOIN cb_terminal_statutory_setup tss ON ps.payroll_setup_id= tss.payroll_setup_id
                        INNER JOIN cb_terminal_statutory_setup_details tssd
                        ON tssd.terminal_statutory_setup_id= tss.terminal_statutory_setup_id
                        AND tss.payroll_physical_years_id = (
                        SELECT MAX(tss1.payroll_physical_years_id) FROM cb_terminal_statutory_setup tss1
                        WHERE tss1.payroll_setup_id = tss.payroll_setup_id) 
                        WHERE ps.setup_type='${body.setup_type}' AND ps.setup_desc='${body.setup_desc}';
                        SELECT cl.* FROM cb_payroll_setup ps 
                        INNER JOIN cb_change_log cl
                        ON cl.payroll_setup_id= ps.payroll_setup_id
                        WHERE ps.setup_type='${body.setup_type}' AND ps.setup_desc='${body.setup_desc}'
                        ORDER By cl.change_log_id DESC;`
            } else if (body.setup_type === 'Compensation_Setup') {
                query += `SELECT ads.* FROM cb_payroll_setup ps
                        INNER JOIN cb_allowances_deduction_setup ads
                        ON ads.payroll_setup_id=ps.payroll_setup_id
                        WHERE ps.setup_type='${body.setup_type}' AND ps.setup_desc='${body.setup_desc}';
                        SELECT adsd.*, ps.setup_type, ps.setup_desc, ps.payroll_setup_id FROM cb_payroll_setup ps
                        INNER JOIN cb_allowances_deduction_setup ads
                        ON ps.payroll_setup_id=ads.payroll_setup_id
                        INNER JOIN cb_allowances_deduction_setup_detail adsd
                        ON adsd.allowances_deduction_setup_id=ads.allowances_deduction_setup_id
                        AND ads.payroll_physical_years_id = (
                        SELECT MAX(ads1.payroll_physical_years_id) FROM cb_allowances_deduction_setup ads1
                        WHERE ads1.payroll_setup_id = ads.payroll_setup_id) 
                        WHERE ps.setup_type='${body.setup_type}' AND ps.setup_desc='${body.setup_desc}';
                        SELECT cl.* FROM cb_payroll_setup ps 
                        INNER JOIN cb_change_log cl
                        ON cl.payroll_setup_id= ps.payroll_setup_id
                        WHERE ps.setup_type='${body.setup_type}' AND ps.setup_desc='${body.setup_desc}'
                        ORDER By cl.change_log_id DESC;
                        SELECT ph.* FROM cb_payroll_setup ps
                        INNER JOIN cb_allowances_deduction_setup ads
                        ON ads.payroll_setup_id= ps.payroll_setup_id
                        INNER JOIN cb_allowances_deduction_setup_detail adsd
                        ON adsd.allowances_deduction_setup_id= ads.allowances_deduction_setup_id
                        INNER JOIN cb_payroll_headers ph
                        ON ph.payroll_headers_id= adsd.payroll_headers_id
                        WHERE ps.setup_type='${body.setup_type}' AND ps.setup_desc='${body.setup_desc}';`
            }
            return await queryObj.executeSelectQuery(query, conn);
        } catch (exception) {
            throw {message: exception.sqlMessage}
        }
    }

    async validateExistingParentSlab(tbl, parent, setupId, conn) {
        try {
            return await queryObj.executeSelectQuery(`SELECT * FROM ${tbl} WHERE payroll_physical_years_id like ${parent.payroll_physical_years_id} AND payroll_setup_id like ${setupId};`, conn)
        } catch (exception) {
            throw {message: exception.sqlMessage}
        }
    }

    async addParentSlab(tbl, obj, conn) {
        try {
            const date = await dateFormat()
            const newObj = {
                ...obj,
                status: 1,
                changed_on: date,
                created_at: date,
                updated_at: date,
            }
            return await this.record.create(tbl, newObj, conn);
        } catch (e) {
            throw e
        }
    }

    async insertSlabDetails(tbl, obj, conn) {
        try {
            const date = await dateFormat();
            const newObj = {
                ...obj,
                changed_on: date,
                created_at: date,
                updated_at: date,
            };
            return await this.record.create(tbl, newObj, conn);
        } catch (e) {
            throw e
        }
    }

    async selectMultipleByOneColumn(tbl, col, values, conn) {
        try {
            let query = `SELECT * FROM ${tbl} WHERE ${col} IN (`;
            for (let index = 0; values[index]; index++) {
                if (values[index + 1]) {
                    query += `${values[index]}, `
                } else
                    query += `${values[index]});`
            }
            return await queryObj.executeSelectQuery(query, conn)
        } catch (exception) {
            throw {message: exception.sqlMessage}
        }
    }

    async InsertChangeLog(obj, conn) {
        try {
            const date = await dateFormat();
            const newObj = {...obj, created_at: date}
            await this.record.create('cb_change_log', newObj, conn)
        } catch (exception) {
            throw exception
        }
    }

    async fetchExistingSlabs(tbl, parentId, conn) {
        try {
            let query = `SELECT child.* FROM ${tbl.parentTable} parent
                    INNER JOIN ${tbl.childTable} child
                    ON child.${tbl.parentTableId}= parent.${tbl.parentTableId}
                    WHERE parent.${tbl.parentTableId}= ${parentId}`

            return await queryObj.executeSelectQuery(query, conn)
        } catch (exception) {
            throw {message: exception.sqlMessage}
        }
    }

    async displayHeaders(conn) {
        try {
            let query = `SELECT ph.payroll_headers_id, ph.header_desc, hsf.specific_lable, hsf.headers_screen_filters_id, hsf.status, ps.payroll_setup_id FROM cb_payroll_headers ph
                    LEFT JOIN cb_payroll_headers_link phl
                    ON ph.payroll_headers_id=phl.payroll_headers_id
                    LEFT JOIN cb_payroll_headers_type pht
                    ON phl.payroll_headers_type_id= pht.payroll_headers_type_id
                    LEFT JOIN cb_headers_screen_filters hsf
                    ON ph.payroll_headers_id=hsf.payroll_headers_id
                    LEFT JOIN cb_payroll_setup ps
                    ON hsf.payroll_setup_id= ps.payroll_setup_id;
                    SELECT ps.payroll_setup_id, ps.setup_desc FROM cb_payroll_setup ps;`;
            return await queryObj.executeSelectQuery(query, conn)
        } catch (excetpion) {
            throw {message: exception.sqlMessage}
        } finally {
            conn.release();
        }
    }

    async updateHeaders(body, conn) {
        try {
            let query = `UPDATE cb_headers_screen_filters hsf SET status=${body.status}
             WHERE hsf.headers_screen_filters_id=${body.headers_screen_filters_id};`
            return await queryObj.executeInsertQuery(query, conn)
        } catch (exception) {
            throw {message: exception.sqlMessage}
        }
    }

    async insertHeaders(body, conn) {
        try {
            const date = await dateFormat()
            const obj = {
                updated_by: 1,
                created_by: 1,
                created_at: date,
                updated_at: date,
                status: body.status,
                payroll_headers_id: body.payroll_headers_id,
                payroll_setup_id: body.payroll_setup_id,
                is_non_zero_flag: 0,
            };
            return await this.record.create('cb_headers_screen_filters', obj, conn)
        } catch (exception) {
            throw exception
        }
    }

    async dropDownValues(screen, conn) {
        try {
            let query = ` SELECT * FROM proll_dates pd
            WHERE pd.form='Payroll_Physical_Year';
            SELECT c.country_id, c.country FROM countries c;
            SELECT GROUP_CONCAT(s.id) AS ids, GROUP_CONCAT(s.name) AS states, s.country_id AS countery FROM states s
            GROUP BY s.country_id;
            SELECT ph.payroll_headers_id ,ph.header_desc FROM cb_payroll_headers ph
            LEFT JOIN cb_headers_screen_filters hsf
            ON ph.payroll_headers_id=hsf.payroll_headers_id
            LEFT JOIN cb_payroll_headers_link phl
            ON ph.payroll_headers_id=phl.payroll_headers_id
            LEFT JOIN cb_payroll_headers_type pht
            ON phl.payroll_headers_type_id= pht.payroll_headers_type_id
            WHERE pht.header_type='Contributor';
            SELECT * FROM proll_reference_data prd
            LEFT JOIN proll_reference_data_code prdc
            ON prd.ref_id=prdc.ref_id
            WHERE prdc.reference_code='Eligibilty' AND prd.status=1;
            SELECT pct.payroll_calculations_type_id, pct.calulations_type_desc
            FROM cb_payroll_calculations_type pct
            WHERE pct.status= 1;`
            if (screen.setup_type === 'Statutory_Setup' || screen.setup_type === 'Compensation_Setup' || screen.setup_type === 'Insurance') {
                if (screen.setup_type === 'Compensation_Setup') {
                    query += `SELECT d.id, d.department_name FROM department_hierarchy d;
                             SELECT eb.id, eb.band_desc FROM employee_bands eb;`
                } else if (screen.setup_desc === 'Group_Life_Insurance' || screen.setup_type === 'Insurance') {
                    query += `SELECT csf.id, csf.label, csf.table_name, csf.table_pk_col, csf.table_desc_col, csf.reference_data_code
                              FROM hr_setup_types st
                              INNER JOIN cb_payroll_setup ps
                              ON ps.hr_setup_type_id=st.id
                              LEFT JOIN hr_setup_type_criteria_fields stcf
                              ON st.id=stcf.hr_setup_type_id
                              LEFT JOIN hr_criteria_setup_fields csf
                              ON stcf.criteria_setup_field_id= csf.id;`
                }
                return await queryObj.executeSelectQuery(query, conn)
            } else {
                return {message: 'Invalid Screen Selection'}
            }
        } catch (exception) {
            throw {message: exception.sqlMessage}
        } finally {
            conn.release();
        }
    }

    async valuesFromTable(tbl, id, col, conn) {
        let query = `SELECT tbl.${id}, tbl.${col} FROM ${tbl} tbl;`
        return await queryObj.executeSelectQuery(query, conn)
    }

    async valuesThroughRefTable(tbl, id, col, ref, conn) {
        let query = `SELECT tbl.${id}, tbl.${col} FROM ${tbl} tbl
                    LEFT JOIN proll_reference_data_code prdc
                    ON tbl.ref_id=prdc.ref_id
                    WHERE prdc.reference_code='${ref}';`;
        return await queryObj.executeSelectQuery(query, conn)
    }

}

exports.FilterScreen = PayrollSetup