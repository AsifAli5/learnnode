const mongoose = require('mongoose');

// Menus Cache Schema as per User
const menusCacheSchema = mongoose.Schema({
    e_number: Number,
    link: String,

    levelOne: Number,
    levelTwo: Number,
    levelThree: Number,
});

// Menus Schema for all portals, this will be static and will be updated when any menu portal is updated
const menusSchema = mongoose.Schema({
    client_id: Number,
    component_name: String,
    created_at: String,
    created_by: Number,
    description: String,
    file_name: String,
    icon: String,
    id: Number,
    img: String,
    level: Number,
    menu_order: Number,
    menu_title: String,
    menu_type_id: Number,
    page_menu_title: String,
    parent_id: Number,
    status: Number,
    subMenu: [{
        client_id: Number,
        component_name: String,
        created_at: String,
        created_by: Number,
        description: String,
        file_name: String,
        icon: String,
        id: Number,
        img: String,
        level: Number,
        menu_order: Number,
        menu_title: String,
        menu_type_id: Number,
        page_menu_title: String,
        parent_id: Number,
        status: Number,
        subMenu: [{
            client_id: Number,
            component_name: String,
            created_at: String,
            created_by: Number,
            description: String,
            file_name: String,
            icon: String,
            id: Number,
            img: String,
            level: Number,
            menu_order: Number,
            menu_title: String,
            menu_type_id: Number,
            page_menu_title: String,
            parent_id: Number,
            status: Number,
            subMenu: [],
            updated_at: String
        }],
        updated_at: String
    }],
    updated_at: String
});

const MenusCacheSchema = mongoose.model('MenusCache', menusCacheSchema);
const MenusSchema = mongoose.model('MenusCache', menusSchema);

module.exports = {
    MenusCacheSchema,
    MenusSchema
}