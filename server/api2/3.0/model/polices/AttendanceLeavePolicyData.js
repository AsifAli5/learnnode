var stringCheck = require('../../../../utils/StringCheck');
const config = require('../../../../../config');
const queryObj = require('../../helper/query');

class AttendanceLeavePolicyData {

    constructor() {
    }

    //to get attence leave policy
    async getAttendanceLeavePolicy(policy_id,conn,pool){
        try {
            let query = `SELECT *
            FROM policy_section_values psv
            WHERE psv.policy_id = ${policy_id};`;

            let result = queryObj.executeQueryConn(query,conn);
            return result;
            
        } catch (err) {
            console.log(err);
            return err;
            
        }finally{
            conn.release();
            //console.log(pool._freeConntections.indexOf(conn));
        }
    }

    /************************* Post Section ***************************** */


    //to post leave types section data
    async postLeaveTypes(req_leave_types, conn, pool){
        try {
            //console.log(req_leave_types);
            //console.log(conn);
            //console.log(pool);            
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_leave_types.length; i++) {
              let record = req_leave_types[i];

              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
            
        }

    }

    //to post leave authorization approvals section data
    async postLeaveAuthApprovals(req_leave_auth_approvals,conn,pool){
        try {
            //console.log(req_leave_auth_approvals);
            //console.log(conn);
            //console.log(pool);            
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_leave_auth_approvals.length; i++) {
              let record = req_leave_auth_approvals[i];

              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
            
        }
    }

    //to post leave encashment claim
    async postLeaveEncashClaim(
        req_leave_encash_claim,
        conn,
        pool
      ){
        try {
            //console.log(req_leave_encash_claim);
            //console.log(conn);
            //console.log(pool);            
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_leave_encash_claim.length; i++) {
              let record = req_leave_encash_claim[i];

              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
            
        }

      }

      //to post working days and office timings
      async postWorkingDaysOfficeTimings(
        req_working_days_office_timings,
        conn,
        pool
      ){
        try {
            //console.log(req_working_days_office_timings);
            //console.log(conn);
            //console.log(pool);            
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_working_days_office_timings.length; i++) {
              let record = req_working_days_office_timings[i];

              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
            
        }

      }

      // to post employee attendance mark
      async postEmpAttendaceMark(
        req_emp_attendance_mark,
        conn,
        pool
      ){
        try {
            //console.log(req_emp_attendance_mark);
            //console.log(conn);
            //console.log(pool);            
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_emp_attendance_mark.length; i++) {
              let record = req_emp_attendance_mark[i];

              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
            
        }

      }

      //to post extra hours compensation
      async postExtraHoursCompensation(
        req_extra_hours_compen,
        conn,
        pool
      ){
        try {
            //console.log(req_extra_hours_compen);
            //console.log(conn);
            //console.log(pool);            
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_extra_hours_compen.length; i++) {
              let record = req_extra_hours_compen[i];

              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
            
        }

      }

      //to post working shifts 
      async postWorkingShifts(
        req_working_shifts,
        conn,
        pool
      ){
        try {
            //console.log(req_working_shifts);
            //console.log(conn);
            //console.log(pool);            
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_working_shifts.length; i++) {
              let record = req_working_shifts[i];

              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
            
        }

      }

      //to post late arrivals for regular employees
      async postLateArrivalsRegEmp(
        req_treat_late_arrivals_reg_emp,
        conn,
        pool
      ){
        try {
            //console.log(req_treat_late_arrivals_reg_emp);
            //console.log(conn);
            //console.log(pool);            
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_treat_late_arrivals_reg_emp.length; i++) {
              let record = req_treat_late_arrivals_reg_emp[i];

              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
            
        }

      }

      // to post late arrivals for roster employees
      async postPostLateArrivalsRosEmp(
        req_treat_late_arrivals_ros_emp,
        conn,
        pool
      ){
        try {
            //console.log(req_treat_late_arrivals_ros_emp);
            //console.log(conn);
            //console.log(pool);            
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_treat_late_arrivals_ros_emp.length; i++) {
              let record = req_treat_late_arrivals_ros_emp[i];

              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
            
        }finally{
            conn.release();
        }

      }





       /************************* Update Section ***************************** */

       //to update leave types section data
    async updateLeaveTypes(req_leave_types, conn, pool){
        try {
            //console.table(req_leave_types);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
            
      
            for (let i = 0; i < req_leave_types.length; i++) {
              const record = req_leave_types[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
             
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
            
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
             
            }
      
            return {affectedRowsCount,changedRowsCount};
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
            
          } catch (error) {
            console.log(error);
            return error;
          } 

    }

    //to update leave authorization approvals section data
    async updateLeaveAuthApprovals(req_leave_auth_approvals,conn,pool){
        try {
            //console.table(req_leave_auth_approvals);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
            
      
            for (let i = 0; i < req_leave_auth_approvals.length; i++) {
              const record = req_leave_auth_approvals[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
             
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
            
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
             
            }
      
            return {affectedRowsCount,changedRowsCount};
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
            
          } catch (error) {
            console.log(error);
            return error;
          } 
    }

    //to update leave encashment claim
    async updateLeaveEncashClaim(
        req_leave_encash_claim,
        conn,
        pool
      ){
        try {
            //console.table(req_leave_encash_claim);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
            
      
            for (let i = 0; i < req_leave_encash_claim.length; i++) {
              const record = req_leave_encash_claim[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
             
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
            
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
             
            }
      
            return {affectedRowsCount,changedRowsCount};
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
            
          } catch (error) {
            console.log(error);
            return error;
          } 

      }

      //to update working days and office timings
      async updateWorkingDaysOfficeTimings(
        req_working_days_office_timings,
        conn,
        pool
      ){
        try {
            //console.table(req_working_days_office_timings);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
            
      
            for (let i = 0; i < req_working_days_office_timings.length; i++) {
              const record = req_working_days_office_timings[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
             
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
            
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
             
            }
      
            return {affectedRowsCount,changedRowsCount};
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
            
          } catch (error) {
            console.log(error);
            return error;
          } 

      }

      // to update employee attendance mark
      async updateEmpAttendaceMark(
        req_emp_attendance_mark,
        conn,
        pool
      ){
        try {
            //console.table(req_emp_attendance_mark);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
            
      
            for (let i = 0; i < req_emp_attendance_mark.length; i++) {
              const record = req_emp_attendance_mark[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
             
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
            
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
             
            }
      
            return {affectedRowsCount,changedRowsCount};
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
            
          } catch (error) {
            console.log(error);
            return error;
          } 

      }

      //to update extra hours compensation
      async updateExtraHoursCompensation(
        req_extra_hours_compen,
        conn,
        pool
      ){
        try {
            //console.table(req_extra_hours_compen);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
            
      
            for (let i = 0; i < req_extra_hours_compen.length; i++) {
              const record = req_extra_hours_compen[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
             
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
            
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
             
            }
      
            return {affectedRowsCount,changedRowsCount};
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
            
          } catch (error) {
            console.log(error);
            return error;
          } 

      }

      //to update working shifts 
      async updateWorkingShifts(
        req_working_shifts,
        conn,
        pool
      ){
        try {
            //console.table(req_working_shifts);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
            
      
            for (let i = 0; i < req_working_shifts.length; i++) {
              const record = req_working_shifts[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
             
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
            
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
             
            }
      
            return {affectedRowsCount,changedRowsCount};
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
            
          } catch (error) {
            console.log(error);
            return error;
          } 

      }

      //to update late arrivals for regular employees
      async updateLateArrivalsRegEmp(
        req_treat_late_arrivals_reg_emp,
        conn,
        pool
      ){
        try {
            //console.table(req_treat_late_arrivals_reg_emp);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
            
      
            for (let i = 0; i < req_treat_late_arrivals_reg_emp.length; i++) {
              const record = req_treat_late_arrivals_reg_emp[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
             
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
            
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
             
            }
      
            return {affectedRowsCount,changedRowsCount};
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
            
          } catch (error) {
            console.log(error);
            return error;
          } 

      }

      // to update late arrivals for roster employees
      async updateLateArrivalsRosEmp(
        req_treat_late_arrivals_ros_emp,
        conn,
        pool
      ){
        try {
            //console.table(req_treat_late_arrivals_ros_emp);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
            
      
            for (let i = 0; i < req_treat_late_arrivals_ros_emp.length; i++) {
              const record = req_treat_late_arrivals_ros_emp[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
             
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
            
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
             
            }
      
            return {affectedRowsCount,changedRowsCount};
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
            
          } catch (error) {
            console.log(error);
            return error;
          } finally{
            conn.release();
        }

      }



}
exports.AttendanceLeavePolicyData = AttendanceLeavePolicyData;