var stringCheck = require('../../../../utils/StringCheck');
const config = require('../../../../../config');
const queryObj = require('../../helper/query');

class AssetPolicyData {

    constructor() {
    }

    // to get asset policy
    async getAssetPolicy(
        policy_id,
        conn,
        pool
      ){
        try {
            let query = `SELECT *
                  FROM policy_section_values psv
                  WHERE psv.policy_id = ${policy_id};`;
      
            let result = queryObj.executeQueryConn(query, conn);
            return result;
          } catch (err) {
            console.log(err);
            return err;
          } finally {
            conn.release();
            //console.log(pool._freeConntections.indexOf(conn));
          }
      }

      /********************** Post section ******************************* */

      //to post vehicle asset policy
      async postVehicleAssetPolicy(
        req_vehicle_asset_policy,
        conn,
        pool
      ){
        try {
            //console.log(req_vehicle_asset_policy);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_vehicle_asset_policy.length; i++) {
              let record = req_vehicle_asset_policy[i];
      
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              let query = `INSERT INTO policy_section_values
                              (
                              policy_id,
                              policy_section_id,
                              policy_section_types_id,
                              policy_status_id,
                              section_type_val_name,
                              sec_value
                              ) VALUES
                              (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
          }

      }

      //to post bear vehicle asset running cost 
      async postBearVehicleAssetRc( 
        req_bear_vehicle_asset_run_cost,
        conn,
        pool){
            try {
                //console.log(req_bear_vehicle_asset_run_cost);
                //console.log(conn);
                //console.log(pool);
                let insertIds = [];
                let affectedRowsCount = 0;
          
                for (let i = 0; i < req_bear_vehicle_asset_run_cost.length; i++) {
                  let record = req_bear_vehicle_asset_run_cost[i];
          
                  let policy_id = record["policy_id"];
                  let policy_section_id = record["policy_section_id"];
                  let policy_section_types_id = record["policy_section_types_id"];
                  let policy_status_id = record["policy_status_id"];
                  let section_type_val_name = record["section_type_val_name"];
                  let sec_value = record["sec_value"];
          
                  let query = `INSERT INTO policy_section_values
                                  (
                                  policy_id,
                                  policy_section_id,
                                  policy_section_types_id,
                                  policy_status_id,
                                  section_type_val_name,
                                  sec_value
                                  ) VALUES
                                  (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
          
                  let result = await queryObj.executeInsertQuery(query, conn);
                  affectedRowsCount = affectedRowsCount + result.affectedRows;
                  insertIds.push(result.insertId);
                }
          
                return { affectedRowsCount, insertIds };
              } catch (error) {
                console.log(error);
                return error;
              }
    

      }

      //to post provide mobile phone asset
      async postProvideMobileAsset(
        req_providing_mobile_policy,
      conn,
      pool
    ){
        try {
            //console.log(req_providing_mobile_policy);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_providing_mobile_policy.length; i++) {
              let record = req_providing_mobile_policy[i];
      
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              let query = `INSERT INTO policy_section_values
                              (
                              policy_id,
                              policy_section_id,
                              policy_section_types_id,
                              policy_status_id,
                              section_type_val_name,
                              sec_value
                              ) VALUES
                              (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
          }

    }

    //to post bear mobile asset running cost 
    async postBearMobileAssetRc(
        req_bear_mobile_run_cost,
        conn,
        pool
      ){
        try {
            //console.log(req_bear_mobile_run_cost);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_bear_mobile_run_cost.length; i++) {
              let record = req_bear_mobile_run_cost[i];
      
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              let query = `INSERT INTO policy_section_values
                              (
                              policy_id,
                              policy_section_id,
                              policy_section_types_id,
                              policy_status_id,
                              section_type_val_name,
                              sec_value
                              ) VALUES
                              (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
          }

      }

      //to post provide laptop asset policy
      async postProvideLaptopPolicy(
        req_providing_laptop_policy,
      conn,
      pool
    ){
        try {
            //console.log(req_providing_laptop_policy);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_providing_laptop_policy.length; i++) {
              let record = req_providing_laptop_policy[i];
      
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              let query = `INSERT INTO policy_section_values
                              (
                              policy_id,
                              policy_section_id,
                              policy_section_types_id,
                              policy_status_id,
                              section_type_val_name,
                              sec_value
                              ) VALUES
                              (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
          }

    }

    //to post bear laptop asset running cost
    async postLaptopAssetRc(
        req_bear_laptop_run_cost,
        conn,
        pool
      ){
        try {
            //console.log(req_bear_laptop_run_cost);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_bear_laptop_run_cost.length; i++) {
              let record = req_bear_laptop_run_cost[i];
      
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              let query = `INSERT INTO policy_section_values
                              (
                              policy_id,
                              policy_section_id,
                              policy_section_types_id,
                              policy_status_id,
                              section_type_val_name,
                              sec_value
                              ) VALUES
                              (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
          }

      }

      //to post asset withdrawl policy
      async postAssetWithdrawlPolicy(
        req_asset_withdawl_policy,
        conn,
        pool
      ){
        try {
            //console.log(req_asset_withdawl_policy);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_asset_withdawl_policy.length; i++) {
              let record = req_asset_withdawl_policy[i];
      
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              let query = `INSERT INTO policy_section_values
                              (
                              policy_id,
                              policy_section_id,
                              policy_section_types_id,
                              policy_status_id,
                              section_type_val_name,
                              sec_value
                              ) VALUES
                              (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
          }

      }

      //to post asset replace and disposal policy
      async postAssetReplaceDisposalPolicy(
        req_asset_replace_disp_policy,
        conn,
        pool
      ){
        try {
            //console.log(req_asset_replace_disp_policy);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_asset_replace_disp_policy.length; i++) {
              let record = req_asset_replace_disp_policy[i];
      
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              let query = `INSERT INTO policy_section_values
                              (
                              policy_id,
                              policy_section_id,
                              policy_section_types_id,
                              policy_status_id,
                              section_type_val_name,
                              sec_value
                              ) VALUES
                              (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (error) {
            console.log(error);
            return error;
          }finally{
              conn.release();
            //   console.log(pool._freeConnections.indexOf(conn));
          }

      }


        /********************** Update section ******************************* */

      //to update vehicle asset policy
      async updateVehicleAssetPolicy(
        req_vehicle_asset_policy,
        conn,
        pool
      ){
        try {
            //console.table(req_vehicle_asset_policy);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
      
            for (let i = 0; i < req_vehicle_asset_policy.length; i++) {
              const record = req_vehicle_asset_policy[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
                    SET 	 psv.policy_id = ${policy_id},
                    psv.policy_section_id = ${policy_section_id},
                    psv.policy_section_types_id = ${policy_section_types_id},
                    psv.policy_status_id = ${policy_status_id},
                    psv.section_type_val_name = '${section_type_val_name}',
                    psv.sec_value = '${sec_value}'
                    WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
      
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
            }
      
            return { affectedRowsCount, changedRowsCount };
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
          } catch (error) {
            console.log(error);
            return error;
          }

      }

      //to update bear vehicle asset running cost 
      async updateBearVehicleAssetRc( 
        req_bear_vehicle_asset_run_cost,
        conn,
        pool){
            try {
                //console.table(req_bear_vehicle_asset_run_cost);
                // console.log(conn);
                // console.log(pool);
          
                let affectedRowsCount = 0;
                let changedRowsCount = 0;
          
                for (let i = 0; i < req_bear_vehicle_asset_run_cost.length; i++) {
                  const record = req_bear_vehicle_asset_run_cost[i];
          
                  let id = record["id"];
                  let policy_id = record["policy_id"];
                  let policy_section_id = record["policy_section_id"];
                  let policy_section_types_id = record["policy_section_types_id"];
                  let policy_status_id = record["policy_status_id"];
                  let section_type_val_name = record["section_type_val_name"];
                  let sec_value = record["sec_value"];
          
                  //console.table(record);
          
                  let query = `UPDATE policy_section_values psv 
                        SET 	 psv.policy_id = ${policy_id},
                        psv.policy_section_id = ${policy_section_id},
                        psv.policy_section_types_id = ${policy_section_types_id},
                        psv.policy_status_id = ${policy_status_id},
                        psv.section_type_val_name = '${section_type_val_name}',
                        psv.sec_value = '${sec_value}'
                        WHERE psv.id = ${id}`;
          
                  let result = await queryObj.executeQueryConn(query, conn);
          
                  affectedRowsCount = affectedRowsCount + result.affectedRows;
                  changedRowsCount = changedRowsCount + result.changedRows;
                }
          
                return { affectedRowsCount, changedRowsCount };
          
                // console.log(affectedRowsCount);
                // console.log(changedRowsCount);
              } catch (error) {
                console.log(error);
                return error;
              }
    

      }

      //to update provide mobile phone asset
      async updateProvideMobileAsset(
        req_providing_mobile_policy,
      conn,
      pool
    ){
        try {
            //console.table(req_providing_mobile_policy);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
      
            for (let i = 0; i < req_providing_mobile_policy.length; i++) {
              const record = req_providing_mobile_policy[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
                    SET 	 psv.policy_id = ${policy_id},
                    psv.policy_section_id = ${policy_section_id},
                    psv.policy_section_types_id = ${policy_section_types_id},
                    psv.policy_status_id = ${policy_status_id},
                    psv.section_type_val_name = '${section_type_val_name}',
                    psv.sec_value = '${sec_value}'
                    WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
      
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
            }
      
            return { affectedRowsCount, changedRowsCount };
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
          } catch (error) {
            console.log(error);
            return error;
          }

    }

    //to update bear mobile asset running cost 
    async updateBearMobileAssetRc(
        req_bear_mobile_run_cost,
        conn,
        pool
      ){
        try {
            //console.table(req_bear_mobile_run_cost);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
      
            for (let i = 0; i < req_bear_mobile_run_cost.length; i++) {
              const record = req_bear_mobile_run_cost[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
                    SET 	 psv.policy_id = ${policy_id},
                    psv.policy_section_id = ${policy_section_id},
                    psv.policy_section_types_id = ${policy_section_types_id},
                    psv.policy_status_id = ${policy_status_id},
                    psv.section_type_val_name = '${section_type_val_name}',
                    psv.sec_value = '${sec_value}'
                    WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
      
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
            }
      
            return { affectedRowsCount, changedRowsCount };
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
          } catch (error) {
            console.log(error);
            return error;
          }

      }

      //to update provide laptop asset policy
      async updateProvideLaptopPolicy(
        req_providing_laptop_policy,
      conn,
      pool
    ){
        try {
            //console.table(req_providing_laptop_policy);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
      
            for (let i = 0; i < req_providing_laptop_policy.length; i++) {
              const record = req_providing_laptop_policy[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
                    SET 	 psv.policy_id = ${policy_id},
                    psv.policy_section_id = ${policy_section_id},
                    psv.policy_section_types_id = ${policy_section_types_id},
                    psv.policy_status_id = ${policy_status_id},
                    psv.section_type_val_name = '${section_type_val_name}',
                    psv.sec_value = '${sec_value}'
                    WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
      
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
            }
      
            return { affectedRowsCount, changedRowsCount };
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
          } catch (error) {
            console.log(error);
            return error;
          }

    }

    //to update bear laptop asset running cost
    async updateLaptopAssetRc(
        req_bear_laptop_run_cost,
        conn,
        pool
      ){
        try {
            //console.table(req_bear_laptop_run_cost);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
      
            for (let i = 0; i < req_bear_laptop_run_cost.length; i++) {
              const record = req_bear_laptop_run_cost[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
                    SET 	 psv.policy_id = ${policy_id},
                    psv.policy_section_id = ${policy_section_id},
                    psv.policy_section_types_id = ${policy_section_types_id},
                    psv.policy_status_id = ${policy_status_id},
                    psv.section_type_val_name = '${section_type_val_name}',
                    psv.sec_value = '${sec_value}'
                    WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
      
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
            }
      
            return { affectedRowsCount, changedRowsCount };
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
          } catch (error) {
            console.log(error);
            return error;
          }

      }

      //to update asset withdrawl policy
      async updateAssetWithdrawlPolicy(
        req_asset_withdawl_policy,
        conn,
        pool
      ){
        try {
            //console.table(req_asset_withdawl_policy);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
      
            for (let i = 0; i < req_asset_withdawl_policy.length; i++) {
              const record = req_asset_withdawl_policy[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
                    SET 	 psv.policy_id = ${policy_id},
                    psv.policy_section_id = ${policy_section_id},
                    psv.policy_section_types_id = ${policy_section_types_id},
                    psv.policy_status_id = ${policy_status_id},
                    psv.section_type_val_name = '${section_type_val_name}',
                    psv.sec_value = '${sec_value}'
                    WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
      
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
            }
      
            return { affectedRowsCount, changedRowsCount };
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
          } catch (error) {
            console.log(error);
            return error;
          }

      }

      //to update asset replace and disposal policy
      async updateAssetReplaceDisposalPolicy(
        req_asset_replace_disp_policy,
        conn,
        pool
      ){
        try {
            //console.table(req_asset_replace_disp_policy);
            // console.log(conn);
            // console.log(pool);
      
            let affectedRowsCount = 0;
            let changedRowsCount = 0;
      
            for (let i = 0; i < req_asset_replace_disp_policy.length; i++) {
              const record = req_asset_replace_disp_policy[i];
      
              let id = record["id"];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
      
              //console.table(record);
      
              let query = `UPDATE policy_section_values psv 
                    SET 	 psv.policy_id = ${policy_id},
                    psv.policy_section_id = ${policy_section_id},
                    psv.policy_section_types_id = ${policy_section_types_id},
                    psv.policy_status_id = ${policy_status_id},
                    psv.section_type_val_name = '${section_type_val_name}',
                    psv.sec_value = '${sec_value}'
                    WHERE psv.id = ${id}`;
      
              let result = await queryObj.executeQueryConn(query, conn);
      
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              changedRowsCount = changedRowsCount + result.changedRows;
            }
      
            return { affectedRowsCount, changedRowsCount };
      
            // console.log(affectedRowsCount);
            // console.log(changedRowsCount);
          } catch (error) {
            console.log(error);
            return error;
          }finally{
              conn.release();
            //   console.log(pool._freeConnections.indexOf(conn));
          }

      }

}
exports.AssetPolicyData = AssetPolicyData;