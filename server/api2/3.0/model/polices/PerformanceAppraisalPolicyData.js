var stringCheck = require("../../../../utils/StringCheck");
const config = require("../../../../../config");
const queryObj = require("../../helper/query");

class PerformanceAppraisalPolicyData {
  constructor() {}

  //to get performance appraisal policy
  async getPerformanceAppraisalPolicy(policy_id, conn, pool) {
    try {
        let query = `SELECT *
              FROM policy_section_values psv
              WHERE psv.policy_id = ${policy_id};`;
  
        let result = queryObj.executeQueryConn(query, conn);
        return result;
      } catch (err) {
        console.log(err);
        return err;
      } finally {
        conn.release();
        //console.log(pool._freeConntections.indexOf(conn));
      }
  }

  /******************************* Post Section ************************************ */

  //For posting performance launch frequency
  async postPerformLaunchFrequency(req_perform_launch_frequency, conn, pool) {
    try {
      //console.log(req_perform_launch_frequency);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_perform_launch_frequency.length; i++) {
        let record = req_perform_launch_frequency[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                      (
                      policy_id,
                      policy_section_id,
                      policy_section_types_id,
                      policy_status_id,
                      section_type_val_name,
                      sec_value
                      ) VALUES
                      (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //For posting performance goals authority
  async postPerformGoalsAuthority(req_perform_goals_authority, conn, pool) {
    try {
      //console.log(req_perform_goals_authority);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_perform_goals_authority.length; i++) {
        let record = req_perform_goals_authority[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting performance appraisal objectives
  async postPerformAppraisObjectives(
    req_perform_apprais_objectives,
    conn,
    pool
  ) {
    try {
      //console.log(req_perform_apprais_objectives);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_perform_apprais_objectives.length; i++) {
        let record = req_perform_apprais_objectives[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                          (
                          policy_id,
                          policy_section_id,
                          policy_section_types_id,
                          policy_status_id,
                          section_type_val_name,
                          sec_value
                          ) VALUES
                          (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting employees involved in evaluation process
  async postEvalProcessInvolveEmp(req_eval_process_involve_emp, conn, pool) {
    try {
      //console.log(req_eval_process_involve_emp);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_eval_process_involve_emp.length; i++) {
        let record = req_eval_process_involve_emp[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                          (
                          policy_id,
                          policy_section_id,
                          policy_section_types_id,
                          policy_status_id,
                          section_type_val_name,
                          sec_value
                          ) VALUES
                          (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting staff performance evaluation basis
  async postStaffPerformEvalBasis(req_staff_perform_eval_basis, conn, pool) {
    try {
      //console.log(req_staff_perform_eval_basis);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_staff_perform_eval_basis.length; i++) {
        let record = req_staff_perform_eval_basis[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                          (
                          policy_id,
                          policy_section_id,
                          policy_section_types_id,
                          policy_status_id,
                          section_type_val_name,
                          sec_value
                          ) VALUES
                          (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting rating system and its definition
  async postRatingSystemAndDef(req_rating_system_and_def, conn, pool) {
    try {
      //console.log(req_rating_system_and_def);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_rating_system_and_def.length; i++) {
        let record = req_rating_system_and_def[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                          (
                          policy_id,
                          policy_section_id,
                          policy_section_types_id,
                          policy_status_id,
                          section_type_val_name,
                          sec_value
                          ) VALUES
                          (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting appraisal authorization approvals
  async postAppraisAuthApprovals(req_apprais_auth_approvals, conn, pool) {
    try {
      //console.log(req_apprais_auth_approvals);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_apprais_auth_approvals.length; i++) {
        let record = req_apprais_auth_approvals[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                          (
                          policy_id,
                          policy_section_id,
                          policy_section_types_id,
                          policy_status_id,
                          section_type_val_name,
                          sec_value
                          ) VALUES
                          (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting manage poor performers
  async postManagePoorPerformers(req_manage_poor_performers, conn, pool) {
    try {
      //console.log(req_manage_poor_performers);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_manage_poor_performers.length; i++) {
        let record = req_manage_poor_performers[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                          (
                          policy_id,
                          policy_section_id,
                          policy_section_types_id,
                          policy_status_id,
                          section_type_val_name,
                          sec_value
                          ) VALUES
                          (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting employee performance cycle for appraisal
  async postEmpPerformCycleForApprais(
    req_emp_perform_cycle_for_apprais,
    conn,
    pool
  ) {
    try {
      //console.log(req_emp_perform_cycle_for_apprais);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_emp_perform_cycle_for_apprais.length; i++) {
        let record = req_emp_perform_cycle_for_apprais[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                          (
                          policy_id,
                          policy_section_id,
                          policy_section_types_id,
                          policy_status_id,
                          section_type_val_name,
                          sec_value
                          ) VALUES
                          (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting probation evaluation policy
  async postProbationEvalPolicy(req_probation_eval_policy, conn, pool) {
    try {
      //console.log(req_probation_eval_policy);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_probation_eval_policy.length; i++) {
        let record = req_probation_eval_policy[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                          (
                          policy_id,
                          policy_section_id,
                          policy_section_types_id,
                          policy_status_id,
                          section_type_val_name,
                          sec_value
                          ) VALUES
                          (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    } finally {
      conn.release();
      //   console.log(pool._freeConnections.indexOf(conn));
    }
  }

  /******************************* Update Section ************************************ */

  //For updating performance launch frequency
  async updatePerformLaunchFrequency(req_perform_launch_frequency, conn, pool) {
    try {
        //console.table(req_perform_launch_frequency);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
  
        for (let i = 0; i < req_perform_launch_frequency.length; i++) {
          const record = req_perform_launch_frequency[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
                SET 	 psv.policy_id = ${policy_id},
                psv.policy_section_id = ${policy_section_id},
                psv.policy_section_types_id = ${policy_section_types_id},
                psv.policy_status_id = ${policy_status_id},
                psv.section_type_val_name = '${section_type_val_name}',
                psv.sec_value = '${sec_value}'
                WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
  
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
  
        return { affectedRowsCount, changedRowsCount };
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //For updating performance goals authority
  async updatePerformGoalsAuthority(req_perform_goals_authority, conn, pool) {
    try {
        //console.table(req_perform_goals_authority);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
  
        for (let i = 0; i < req_perform_goals_authority.length; i++) {
          const record = req_perform_goals_authority[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
                SET 	 psv.policy_id = ${policy_id},
                psv.policy_section_id = ${policy_section_id},
                psv.policy_section_types_id = ${policy_section_types_id},
                psv.policy_status_id = ${policy_status_id},
                psv.section_type_val_name = '${section_type_val_name}',
                psv.sec_value = '${sec_value}'
                WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
  
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
  
        return { affectedRowsCount, changedRowsCount };
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //for updating performance appraisal objectives
  async updatePerformAppraisObjectives(
    req_perform_apprais_objectives,
    conn,
    pool
  ) {
    try {
        //console.table(req_perform_apprais_objectives);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
  
        for (let i = 0; i < req_perform_apprais_objectives.length; i++) {
          const record = req_perform_apprais_objectives[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
                SET 	 psv.policy_id = ${policy_id},
                psv.policy_section_id = ${policy_section_id},
                psv.policy_section_types_id = ${policy_section_types_id},
                psv.policy_status_id = ${policy_status_id},
                psv.section_type_val_name = '${section_type_val_name}',
                psv.sec_value = '${sec_value}'
                WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
  
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
  
        return { affectedRowsCount, changedRowsCount };
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //for updating employees involved in evaluation process
  async updateEvalProcessInvolveEmp(req_eval_process_involve_emp, conn, pool) {
    try {
        //console.table(req_eval_process_involve_emp);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
  
        for (let i = 0; i < req_eval_process_involve_emp.length; i++) {
          const record = req_eval_process_involve_emp[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
                SET 	 psv.policy_id = ${policy_id},
                psv.policy_section_id = ${policy_section_id},
                psv.policy_section_types_id = ${policy_section_types_id},
                psv.policy_status_id = ${policy_status_id},
                psv.section_type_val_name = '${section_type_val_name}',
                psv.sec_value = '${sec_value}'
                WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
  
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
  
        return { affectedRowsCount, changedRowsCount };
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //for updating staff performance evaluation basis
  async updateStaffPerformEvalBasis(req_staff_perform_eval_basis, conn, pool) {
    try {
        //console.table(req_staff_perform_eval_basis);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
  
        for (let i = 0; i < req_staff_perform_eval_basis.length; i++) {
          const record = req_staff_perform_eval_basis[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
                SET 	 psv.policy_id = ${policy_id},
                psv.policy_section_id = ${policy_section_id},
                psv.policy_section_types_id = ${policy_section_types_id},
                psv.policy_status_id = ${policy_status_id},
                psv.section_type_val_name = '${section_type_val_name}',
                psv.sec_value = '${sec_value}'
                WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
  
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
  
        return { affectedRowsCount, changedRowsCount };
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //for updating rating system and its definition
  async updateRatingSystemAndDef(req_rating_system_and_def, conn, pool) {
    try {
        //console.table(req_rating_system_and_def);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
  
        for (let i = 0; i < req_rating_system_and_def.length; i++) {
          const record = req_rating_system_and_def[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
                SET 	 psv.policy_id = ${policy_id},
                psv.policy_section_id = ${policy_section_id},
                psv.policy_section_types_id = ${policy_section_types_id},
                psv.policy_status_id = ${policy_status_id},
                psv.section_type_val_name = '${section_type_val_name}',
                psv.sec_value = '${sec_value}'
                WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
  
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
  
        return { affectedRowsCount, changedRowsCount };
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //for updating appraisal authorization approvals
  async updateAppraisAuthApprovals(req_apprais_auth_approvals, conn, pool) {
    try {
        //console.table(req_apprais_auth_approvals);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
  
        for (let i = 0; i < req_apprais_auth_approvals.length; i++) {
          const record = req_apprais_auth_approvals[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
                SET 	 psv.policy_id = ${policy_id},
                psv.policy_section_id = ${policy_section_id},
                psv.policy_section_types_id = ${policy_section_types_id},
                psv.policy_status_id = ${policy_status_id},
                psv.section_type_val_name = '${section_type_val_name}',
                psv.sec_value = '${sec_value}'
                WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
  
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
  
        return { affectedRowsCount, changedRowsCount };
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //for updating manage poor performers
  async updateManagePoorPerformers(req_manage_poor_performers, conn, pool) {
    try {
        //console.table(req_manage_poor_performers);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
  
        for (let i = 0; i < req_manage_poor_performers.length; i++) {
          const record = req_manage_poor_performers[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
                SET 	 psv.policy_id = ${policy_id},
                psv.policy_section_id = ${policy_section_id},
                psv.policy_section_types_id = ${policy_section_types_id},
                psv.policy_status_id = ${policy_status_id},
                psv.section_type_val_name = '${section_type_val_name}',
                psv.sec_value = '${sec_value}'
                WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
  
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
  
        return { affectedRowsCount, changedRowsCount };
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //for updating employee performance cycle for appraisal
  async updateEmpPerformCycleForApprais(
    req_emp_perform_cycle_for_apprais,
    conn,
    pool
  ) {
    try {
        //console.table(req_emp_perform_cycle_for_apprais);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
  
        for (let i = 0; i < req_emp_perform_cycle_for_apprais.length; i++) {
          const record = req_emp_perform_cycle_for_apprais[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
                SET 	 psv.policy_id = ${policy_id},
                psv.policy_section_id = ${policy_section_id},
                psv.policy_section_types_id = ${policy_section_types_id},
                psv.policy_status_id = ${policy_status_id},
                psv.section_type_val_name = '${section_type_val_name}',
                psv.sec_value = '${sec_value}'
                WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
  
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
  
        return { affectedRowsCount, changedRowsCount };
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //for updating probation evaluation policy
  async updateProbationEvalPolicy(req_probation_eval_policy, conn, pool) {
    try {
        //console.table(req_probation_eval_policy);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
  
        for (let i = 0; i < req_probation_eval_policy.length; i++) {
          const record = req_probation_eval_policy[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
                SET 	 psv.policy_id = ${policy_id},
                psv.policy_section_id = ${policy_section_id},
                psv.policy_section_types_id = ${policy_section_types_id},
                psv.policy_status_id = ${policy_status_id},
                psv.section_type_val_name = '${section_type_val_name}',
                psv.sec_value = '${sec_value}'
                WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
  
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
  
        return { affectedRowsCount, changedRowsCount };
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
      } catch (error) {
        console.log(error);
        return error;
      } finally {
      conn.release();
      //   console.log(pool._freeConnections.indexOf(conn));
    }
  }










}
exports.PerformanceAppraisalPolicyData = PerformanceAppraisalPolicyData;
