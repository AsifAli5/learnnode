var stringCheck = require('../../../../utils/StringCheck');
const config = require('../../../../../config');
const queryObj = require('../../helper/query');

class IncentiveBonusPolicyData {

    constructor() {
    }

    // get incentives and bonuses policy
    async getIncentiveBonusPolicy(policy_id,conn,pool){
        try {
            // console.log(req_incentives_type);
            // console.log(conn);
            // console.log(pool);

            let query = `SELECT * FROM policy_section_values psv
            WHERE psv.policy_id = ${policy_id};`;

        

            let result = await queryObj.executeQueryConn(query,conn);
            //console.log(result);
            return result;
            

            
        } catch (error) {
            console.log(error);
            return error;
            
        }finally{
            conn.release();
            console.log(pool._freeConnections.indexOf(conn));
            
        }

    }


     /****************************  Post Section  **********************************/

    //to post incentives type section data
    async postIncentivesType(req_incentives_type, conn, pool){
        try {
            //console.log(req_incentives_type);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_incentives_type.length; i++) {
              let record = req_incentives_type[i];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (e) {
            console.log(e);
            return e;
          }

    }

    //to post bonuses type section data
    async postBonusesType(req_bonuses_type,conn,pool){
        try {
            //console.log(req_bonuses_type);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_bonuses_type.length; i++) {
              let record = req_bonuses_type[i];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (e) {
            console.log(e);
            return e;
          }
    }

    //to post statutory bonus section data
    async postStatutoryBonus(req_statutory_bonus,conn,pool){
        try {
            //console.log(req_statutory_bonus);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_statutory_bonus.length; i++) {
              let record = req_statutory_bonus[i];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (e) {
            console.log(e);
            return e;
          }
    }

    //to post festival bonus section data
    async postFestivalBonus(req_festival_bonus,conn,pool){
        try {
            //console.log(req_festival_bonus);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_festival_bonus.length; i++) {
              let record = req_festival_bonus[i];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (e) {
            console.log(e);
            return e;
          }
    }

    //to post long service award section data
    async postLongServiceAward(req_long_service_award,conn,pool){
        try {
            //console.log(req_long_service_award);
            //console.log(conn);
            //console.log(pool);
            let insertIds = [];
            let affectedRowsCount = 0;
      
            for (let i = 0; i < req_long_service_award.length; i++) {
              let record = req_long_service_award[i];
              let policy_id = record["policy_id"];
              let policy_section_id = record["policy_section_id"];
              let policy_section_types_id = record["policy_section_types_id"];
              let policy_status_id = record["policy_status_id"];
              let section_type_val_name = record["section_type_val_name"];
              let sec_value = record["sec_value"];
              
      
              let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
      
              let result = await queryObj.executeInsertQuery(query, conn);
              affectedRowsCount = affectedRowsCount + result.affectedRows;
              insertIds.push(result.insertId);
            }
      
            return { affectedRowsCount, insertIds };
          } catch (e) {
            console.log(e);
            return e;
          }finally{
              conn.release();
              console.log(pool._freeConnections.indexOf(conn));
          }
    }

  /**************************** Update Section *******************************/

  //to update incentives type section data
  async updateIncentivesType(req_incentives_type, conn, pool){
    try {
        // console.log(req_incentives_type);
        // console.log(conn);
        // console.log(pool);
    
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
    
        for (let i = 0; i < req_incentives_type.length; i++) {
          const record = req_incentives_type[i];
          //console.log(record);
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
    
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
    
          let result = await queryObj.executeQueryConn(query,conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
        return { affectedRowsCount, changedRowsCount };
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //to update bonuses type section data
  async updateBonusesType(req_bonuses_type,conn,pool){
    try {
        // console.log(req_bonuses_type);
        // console.log(conn);
        // console.log(pool);
    
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
    
        for (let i = 0; i < req_bonuses_type.length; i++) {
          const record = req_bonuses_type[i];
          //console.log(record);
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
    
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
    
          let result = await queryObj.executeQueryConn(query,conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
        return { affectedRowsCount, changedRowsCount };
      } catch (error) {
        console.log(error);
        return error;
      }
  }

  //to update statutory bonus section data
  async updateStatutoryBonus(req_statutory_bonus,conn,pool){
    try {
        // console.log(req_statutory_bonus);
        // console.log(conn);
        // console.log(pool);
    
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
    
        for (let i = 0; i < req_statutory_bonus.length; i++) {
          const record = req_statutory_bonus[i];
          //console.log(record);
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
    
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
    
          let result = await queryObj.executeQueryConn(query,conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
        return { affectedRowsCount, changedRowsCount };
      } catch (error) {
        console.log(error);
        return error;
      } 
  }

  //to update festival/occasional bonus section data
  async updateFestivalBonus(req_festival_bonus,conn,pool){
    try {
        // console.log(req_festival_bonus);
        // console.log(conn);
        // console.log(pool);
    
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
    
        for (let i = 0; i < req_festival_bonus.length; i++) {
          const record = req_festival_bonus[i];
          //console.log(record);
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
    
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
    
          let result = await queryObj.executeQueryConn(query,conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
        return { affectedRowsCount, changedRowsCount };
      } catch (error) {
        console.log(error);
        return error;
      } 
  }

  //to update long server award section data
  async updateLongServiceAward(req_long_service_award,conn,pool) {
    try {
        // console.log(req_long_service_award);
        // console.log(conn);
        // console.log(pool);
    
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
    
        for (let i = 0; i < req_long_service_award.length; i++) {
          const record = req_long_service_award[i];
          //console.log(record);
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
    
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
    
          let result = await queryObj.executeQueryConn(query,conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
        }
        return { affectedRowsCount, changedRowsCount };
      } catch (error) {
        console.log(error);
        return error;
      }finally{
          conn.release();
          console.log(pool._freeConnections.indexOf(conn));
      }
  }


}
exports.IncentiveBonusPolicyData = IncentiveBonusPolicyData;