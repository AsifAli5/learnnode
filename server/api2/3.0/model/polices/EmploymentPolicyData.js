var stringCheck = require('../../../../utils/StringCheck');
const config = require('../../../../../config');
const queryObj = require('../../helper/query');
// const { connect } = require('../../controller/polices/EmploymentPolicy');


class EmploymentPolicyData {
  constructor() {}

  async postHiringStaff(req_staff_category, conn, pool) {
    try {
      //console.log(req_staff_category);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_staff_category.length; i++) {
        let record = req_staff_category[i];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
        

        let query = `INSERT INTO policy_section_values
                  (
                  policy_id,
                  policy_section_id,
                  policy_section_types_id,
                  policy_status_id,
                  section_type_val_name,
                  sec_value
                  ) VALUES
                  (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  //for posting hiring approval authority
  async postHiringApprovalAuth(req_approval_auth, conn, pool) {
    try {
      // console.log(req_approval_auth);
      // console.log(conn);
      // console.log(pool);

      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_approval_auth.length; i++) {
        const record = req_approval_auth[i];
        //console.log(record);
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
   

        let query = `INSERT INTO policy_section_values
                (
                policy_id,
                policy_section_id,
                policy_section_types_id,
                policy_status_id,
                section_type_val_name,
                sec_value
                ) VALUES
                (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }
      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting probationary period
  async postProbationPeriod(req_probation_period, conn, pool) {
    try {
      // console.log(req_probation_period);
      // console.log(conn);
      // console.log(pool);

      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_probation_period.length; i++) {
        const record = req_probation_period[i];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
       

        let query = `INSERT INTO policy_section_values
                (
                policy_id,
                policy_section_id,
                policy_section_types_id,
                policy_status_id,
                section_type_val_name,
                sec_value
                ) VALUES
                (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }
      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting notice period
  async postNoticePeriod(req_notice_period, conn, pool) {
    try {
      // console.log(req_notice_period);
      // console.log(conn);
      // console.log(pool);

      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_notice_period.length; i++) {
        const record = req_notice_period[i];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
        

        let query = `INSERT INTO policy_section_values
                (
                policy_id,
                policy_section_id,
                policy_section_types_id,
                policy_status_id,
                section_type_val_name,
                sec_value
                ) VALUES
                (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting separation process
  async postSeparationProcess(req_separation_process, conn, pool) {
    // console.log(req_separation_process);
    // console.log(conn);
    // console.log(pool);

    try {
      let affectedRowsCount = 0;
      let insertIds = [];

      for (let i = 0; i < req_separation_process.length; i++) {
        const record = req_separation_process[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
        
        let query = `INSERT INTO policy_section_values
                (
                policy_id,
                policy_section_id,
                policy_section_types_id,
                policy_status_id,
                section_type_val_name,
                sec_value
                ) VALUES
                (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //for posting resignation documents
  async postResigDocuments(req_resig_documents, conn, pool) {
    try {
      // console.log(req_resig_documents);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let insertIds = [];

      for (let i = 0; i < req_resig_documents.length; i++) {
        const record = req_resig_documents[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
        

        let query = `INSERT INTO policy_section_values
                (
                policy_id,
                policy_section_id,
                policy_section_types_id,
                policy_status_id,
                section_type_val_name,
                sec_value
                ) VALUES
               
                (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    } finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }
  }

  //to get all sections data for particular policy
  async getEmploymentPolicy(policy_id, conn, pool) {
    try {
      let query = `SELECT *
            FROM policy_section_values psv
            WHERE psv.policy_id = ${policy_id};`;

      let result = queryObj.executeSelectQuery(query, conn);

      //console.log(result);
      return result;
    } catch (error) {
      console.log(error);
      return error;
    } finally {
      conn.release();
     // console.log(pool._freeConnections.indexOf(conn));
    }
  }



  /*********************** Update Section ********************************* */

  //to update staff hiring
  async updateHiringStaff(req_staff_category, conn, pool) {
    try {
      //console.table(req_staff_category);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;
      

      for (let i = 0; i < req_staff_category.length; i++) {
        const record = req_staff_category[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
       

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
        SET 	 psv.policy_id = ${policy_id},
        psv.policy_section_id = ${policy_section_id},
        psv.policy_section_types_id = ${policy_section_types_id},
        psv.policy_status_id = ${policy_status_id},
        psv.section_type_val_name = '${section_type_val_name}',
        psv.sec_value = '${sec_value}'
        WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);
      
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
       
      }

      return {affectedRowsCount,changedRowsCount};

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
      
    } catch (error) {
      console.log(error);
      return error;
    } 
  }

  //to update hiring approval authority
  async updateHiringApprovalAuth(req_approval_auth, conn, pool) {
    try {
      // console.log(req_approval_auth);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_approval_auth.length; i++) {
        const record = req_approval_auth[i];
        //console.log(record);
        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
      

        let query = `UPDATE policy_section_values psv 
        SET 	 psv.policy_id = ${policy_id},
        psv.policy_section_id = ${policy_section_id},
        psv.policy_section_types_id = ${policy_section_types_id},
        psv.policy_status_id = ${policy_status_id},
        psv.section_type_val_name = '${section_type_val_name}',
        psv.sec_value = '${sec_value}'
        WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query,conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }
      return { affectedRowsCount, changedRowsCount };
    } catch (error) {
      console.log(error);
      return error;
    }
  }


   //to update probationary period
   async updateProbationPeriod(req_probation_period, conn, pool) {
    try {
      // console.log(req_probation_period);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_probation_period.length; i++) {
        const record = req_probation_period[i];
        //console.log(record);
        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
        

        let query = `UPDATE policy_section_values psv 
        SET 	 psv.policy_id = ${policy_id},
        psv.policy_section_id = ${policy_section_id},
        psv.policy_section_types_id = ${policy_section_types_id},
        psv.policy_status_id = ${policy_status_id},
        psv.section_type_val_name = '${section_type_val_name}',
        psv.sec_value = '${sec_value}'
        WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query,conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }
      return { affectedRowsCount, changedRowsCount };
    } catch (error) {
      console.log(error);
      return error;
    }
   
  }

  //to update notice period details
  async updateNoticePeriod(req_notice_period,conn,pool){
    try {
      // console.log(req_notice_period);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_notice_period.length; i++) {
        const record = req_notice_period[i];
        //console.log(record);
        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
       

        let query = `UPDATE policy_section_values psv 
        SET 	 psv.policy_id = ${policy_id},
        psv.policy_section_id = ${policy_section_id},
        psv.policy_section_types_id = ${policy_section_types_id},
        psv.policy_status_id = ${policy_status_id},
        psv.section_type_val_name = '${section_type_val_name}',
        psv.sec_value = '${sec_value}'
        WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query,conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }
      return { affectedRowsCount, changedRowsCount };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to update separation process detail
 async updateSeparationProcess(req_separation_process,conn,pool){
  try {
    // console.log(req_separation_process);
    // console.log(conn);
    // console.log(pool);

    let affectedRowsCount = 0;
    let changedRowsCount = 0;

    for (let i = 0; i < req_separation_process.length; i++) {
      const record = req_separation_process[i];
      //console.log(record);
      let id = record["id"];
      let policy_id = record["policy_id"];
      let policy_section_id = record["policy_section_id"];
      let policy_section_types_id = record["policy_section_types_id"];
      let policy_status_id = record["policy_status_id"];
      let section_type_val_name = record["section_type_val_name"];
      let sec_value = record["sec_value"];

      let query = `UPDATE policy_section_values psv 
      SET 	 psv.policy_id = ${policy_id},
      psv.policy_section_id = ${policy_section_id},
      psv.policy_section_types_id = ${policy_section_types_id},
      psv.policy_status_id = ${policy_status_id},
      psv.section_type_val_name = '${section_type_val_name}',
      psv.sec_value = '${sec_value}'
      WHERE psv.id = ${id}`;

      let result = await queryObj.executeQueryConn(query,conn);
      affectedRowsCount = affectedRowsCount + result.affectedRows;
      changedRowsCount = changedRowsCount + result.changedRows;
    }
    return { affectedRowsCount, changedRowsCount };
  } catch (error) {
    console.log(error);
    return error;
  }
  }

  async updateResigDocuments(req_resig_documents,conn,pool){
    try {
      // console.log(req_resig_documents);
      // console.log(conn);
      // console.log(pool);
  
      let affectedRowsCount = 0;
      let changedRowsCount = 0;
  
      for (let i = 0; i < req_resig_documents.length; i++) {
        const record = req_resig_documents[i];
        //console.log(record);
        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
        
  
        let query = `UPDATE policy_section_values psv 
        SET 	 psv.policy_id = ${policy_id},
        psv.policy_section_id = ${policy_section_id},
        psv.policy_section_types_id = ${policy_section_types_id},
        psv.policy_status_id = ${policy_status_id},
        psv.section_type_val_name = '${section_type_val_name}',
        psv.sec_value = '${sec_value}'
        WHERE psv.id = ${id}`;
  
        let result = await queryObj.executeQueryConn(query,conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }
      return { affectedRowsCount, changedRowsCount };
    } catch (error) {
      console.log(error);
      return error;
    }finally {
      conn.release();
      console.log(pool._freeConnections.indexOf(conn));
    }


  }



}
exports.EmploymentPolicyData = EmploymentPolicyData;