var stringCheck = require('../../../../utils/StringCheck');
const config = require('../../../../../config');
const queryObj = require('../../helper/query');

class CompensationPolicyData {

    constructor() {
    }

    async getCompensationPolicy(policy_id,conn,pool){
        try {
            let query = `SELECT *
                  FROM policy_section_values psv
                  WHERE psv.policy_id = ${policy_id};`;
      
            let result = queryObj.executeSelectQuery(query, conn);
      
            //console.log(result);
            return result;
          } catch (error) {
            console.log(error);
            return error;
          } finally {
            conn.release();
            console.log(pool._freeConnections.indexOf(conn));
          }
    }


    /*************************** Post Section ************************************** */

    //to post monthly salary breakup section data
    async postMonthlySalaryBreakup(req_monthly_salary_breakup, conn, pool){
      try {
        //console.log(req_monthly_salary_breakup);
        //console.log(conn);
        //console.log(pool);

        let insertIds = [];
        let affectedRowsCount = 0;

        for (let i = 0; i < req_monthly_salary_breakup.length; i++) {
          const record = req_monthly_salary_breakup[i];
          //console.log(record);
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
     
  
          let query = `INSERT INTO policy_section_values
                  (
                  policy_id,
                  policy_section_id,
                  policy_section_types_id,
                  policy_status_id,
                  section_type_val_name,
                  sec_value
                  ) VALUES
                  (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
  
          let result = await queryObj.executeInsertQuery(query, conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          insertIds.push(result.insertId);
        }
  
        return { affectedRowsCount, insertIds };
      } catch (e) {
        console.log(e);
        return e;
      }  

    }

    //to post job bands and salary ranges section data
    async postJobBandsSalaryRanges(req_job_bands_salary_ranges,conn,pool){
      try {
        //console.log(req_job_bands_salary_ranges);
        //console.log(conn);
        //console.log(pool);

        let insertIds = [];
        let affectedRowsCount = 0;

        for (let i = 0; i < req_job_bands_salary_ranges.length; i++) {
          const record = req_job_bands_salary_ranges[i];
          //console.log(record);
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
     
  
          let query = `INSERT INTO policy_section_values
                  (
                  policy_id,
                  policy_section_id,
                  policy_section_types_id,
                  policy_status_id,
                  section_type_val_name,
                  sec_value
                  ) VALUES
                  (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
  
          let result = await queryObj.executeInsertQuery(query, conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          insertIds.push(result.insertId);
        }
  
        return { affectedRowsCount, insertIds };
      } catch (e) {
        console.log(e);
        return e;
      }  

    }

    //to post basis unpaid leave calculation section data
   async postUnpaidLeaveCalculation(req_unpaid_leave_calculation,conn,pool){
    try {
      //console.log(req_unpaid_leave_calculation);
      //console.log(conn);
      //console.log(pool);

      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_unpaid_leave_calculation.length; i++) {
        const record = req_unpaid_leave_calculation[i];
        //console.log(record);
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
   

        let query = `INSERT INTO policy_section_values
                (
                policy_id,
                policy_section_id,
                policy_section_types_id,
                policy_status_id,
                section_type_val_name,
                sec_value
                ) VALUES
                (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (e) {
      console.log(e);
      return e;
    }  
    }

    //to post montly cut off time section data
    async postMonthlyCutoffTime(req_monthly_cutoff_time,conn,pool){
      try {
        //console.log(req_monthly_cutoff_time);
        //console.log(conn);
        //console.log(pool);
  
        let insertIds = [];
        let affectedRowsCount = 0;
  
        for (let i = 0; i < req_monthly_cutoff_time.length; i++) {
          const record = req_monthly_cutoff_time[i];
          //console.log(record);
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
     
  
          let query = `INSERT INTO policy_section_values
                  (
                  policy_id,
                  policy_section_id,
                  policy_section_types_id,
                  policy_status_id,
                  section_type_val_name,
                  sec_value
                  ) VALUES
                  (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
  
          let result = await queryObj.executeInsertQuery(query, conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          insertIds.push(result.insertId);
        }
  
        return { affectedRowsCount, insertIds };
      } catch (e) {
        console.log(e);
        return e;
      } 


    }

    //to post over time calculation criteria section data
    async postOvertimeCalculationCriteria(req_overtime_calculation_criteria,conn,pool){
      try {
        //console.log(req_overtime_calculation_criteria);
        //console.log(conn);
        //console.log(pool);
  
        let insertIds = [];
        let affectedRowsCount = 0;
  
        for (let i = 0; i < req_overtime_calculation_criteria.length; i++) {
          const record = req_overtime_calculation_criteria[i];
          //console.log(record);
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
     
  
          let query = `INSERT INTO policy_section_values
                  (
                  policy_id,
                  policy_section_id,
                  policy_section_types_id,
                  policy_status_id,
                  section_type_val_name,
                  sec_value
                  ) VALUES
                  (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
  
          let result = await queryObj.executeInsertQuery(query, conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          insertIds.push(result.insertId);
        }
  
        return { affectedRowsCount, insertIds };
      } catch (e) {
        console.log(e);
        return e;
      } 
    }

    //to post terminal benefits section data
    async postTerminalBenefits(req_terminal_benefits,conn,pool){
      try {
        //console.log(req_terminal_benefits);
        //console.log(conn);
        //console.log(pool);
  
        let insertIds = [];
        let affectedRowsCount = 0;
  
        for (let i = 0; i < req_terminal_benefits.length; i++) {
          const record = req_terminal_benefits[i];
          //console.log(record);
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
     
  
          let query = `INSERT INTO policy_section_values
                  (
                  policy_id,
                  policy_section_id,
                  policy_section_types_id,
                  policy_status_id,
                  section_type_val_name,
                  sec_value
                  ) VALUES
                  (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
  
          let result = await queryObj.executeInsertQuery(query, conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          insertIds.push(result.insertId);
        }
  
        return { affectedRowsCount, insertIds };
      } catch (e) {
        console.log(e);
        return e;
      } 

    }

    //to post group life and disability insurance data
    async postGroupLifeDisInsurnace(req_gl_dis_insurance,conn,pool){
      try {
        //console.log(req_gl_dis_insurance);
        //console.log(conn);
        //console.log(pool);
  
        let insertIds = [];
        let affectedRowsCount = 0;
  
        for (let i = 0; i < req_gl_dis_insurance.length; i++) {
          const record = req_gl_dis_insurance[i];
          //console.log(record);
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
     
  
          let query = `INSERT INTO policy_section_values
                  (
                  policy_id,
                  policy_section_id,
                  policy_section_types_id,
                  policy_status_id,
                  section_type_val_name,
                  sec_value
                  ) VALUES
                  (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
  
          let result = await queryObj.executeInsertQuery(query, conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          insertIds.push(result.insertId);
        }
  
        return { affectedRowsCount, insertIds };
      } catch (e) {
        console.log(e);
        return e;
      } 

    }

    //to post group life health insurance data
    async postGroupLifeHealthInsurance(req_gl_health_insurance,conn,pool){
      try {
        //console.log(req_gl_health_insurance);
        //console.log(conn);
        //console.log(pool);
  
        let insertIds = [];
        let affectedRowsCount = 0;
  
        for (let i = 0; i < req_gl_health_insurance.length; i++) {
          const record = req_gl_health_insurance[i];
          //console.log(record);
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
     
  
          let query = `INSERT INTO policy_section_values
                  (
                  policy_id,
                  policy_section_id,
                  policy_section_types_id,
                  policy_status_id,
                  section_type_val_name,
                  sec_value
                  ) VALUES
                  (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
  
          let result = await queryObj.executeInsertQuery(query, conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          insertIds.push(result.insertId);
        }
  
        return { affectedRowsCount, insertIds };
      } catch (e) {
        console.log(e);
        return e;
      } 

    }

    //to post other benefits data
    async postOtherBenefits(req_other_benefits,conn,pool){
      try {
        //console.log(req_other_benefits);
        //console.log(conn);
        //console.log(pool);
  
        let insertIds = [];
        let affectedRowsCount = 0;
  
        for (let i = 0; i < req_other_benefits.length; i++) {
          const record = req_other_benefits[i];
          //console.log(record);
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
     
  
          let query = `INSERT INTO policy_section_values
                  (
                  policy_id,
                  policy_section_id,
                  policy_section_types_id,
                  policy_status_id,
                  section_type_val_name,
                  sec_value
                  ) VALUES
                  (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;
  
          let result = await queryObj.executeInsertQuery(query, conn);
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          insertIds.push(result.insertId);
        }
  
        return { affectedRowsCount, insertIds };
      } catch (e) {
        console.log(e);
        return e;
      }finally{
        conn.release();
        // console.log(pool._freeConnections.indexOf(conn));
      }

    }

    /************************* Update Section *************************** */


    async updateMonthlySalaryBreakup( req_monthly_salary_breakup, conn, pool ){
      try {
        //console.table(req_monthly_salary_breakup);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
        
  
        for (let i = 0; i < req_monthly_salary_breakup.length; i++) {
          const record = req_monthly_salary_breakup[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
         
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
        
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
         
        }
  
        return {affectedRowsCount,changedRowsCount};
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
        
      } catch (error) {
        console.log(error);
        return error;
      } 
      
    }

    async updateJobBandsSalaryRanges( req_job_bands_salary_ranges, conn, pool ){
      
      try {
        //console.table(req_job_bands_salary_ranges);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
        
  
        for (let i = 0; i < req_job_bands_salary_ranges.length; i++) {
          const record = req_job_bands_salary_ranges[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
         
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
        
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
         
        }
  
        return {affectedRowsCount,changedRowsCount};
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
        
      } catch (error) {
        console.log(error);
        return error;
      } 
      
    }

    async updateUnpaidLeaveCalculation(
      req_unpaid_leave_calculation,
      conn,
      pool
    ){
      try {
        //console.table(req_unpaid_leave_calculation);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
        
  
        for (let i = 0; i < req_unpaid_leave_calculation.length; i++) {
          const record = req_unpaid_leave_calculation[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
         
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
        
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
         
        }
  
        return {affectedRowsCount,changedRowsCount};
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
        
      } catch (error) {
        console.log(error);
        return error;
      } 
    }

    async updateMonthlyCutoffTime(
      req_monthly_cutoff_time,
      conn,
      pool
    ){
      try {
        //console.table(req_monthly_cutoff_time);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
        
  
        for (let i = 0; i < req_monthly_cutoff_time.length; i++) {
          const record = req_monthly_cutoff_time[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
         
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
        
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
         
        }
  
        return {affectedRowsCount,changedRowsCount};
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
        
      } catch (error) {
        console.log(error);
        return error;
      } 

    }

    async updateOvertimeCalculationCriteria(
      req_overtime_calculation_criteria,
      conn,
      pool
    ){
      try {
        //console.table(req_overtime_calculation_criteria);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
        
  
        for (let i = 0; i < req_overtime_calculation_criteria.length; i++) {
          const record = req_overtime_calculation_criteria[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
         
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
        
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
         
        }
  
        return {affectedRowsCount,changedRowsCount};
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
        
      } catch (error) {
        console.log(error);
        return error;
      } 

    }

    async updateTerminalBenefits(
      req_terminal_benefits,
      conn,
      pool
    ){
      try {
        //console.table(req_terminal_benefits);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
        
  
        for (let i = 0; i < req_terminal_benefits.length; i++) {
          const record = req_terminal_benefits[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
         
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
        
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
         
        }
  
        return {affectedRowsCount,changedRowsCount};
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
        
      } catch (error) {
        console.log(error);
        return error;
      } 

    }

    async updateGroupLifeDisInsurnace(
      req_gl_dis_insurance,
      conn,
      pool
    ){
      try {
        //console.table(req_gl_dis_insurance);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
        
  
        for (let i = 0; i < req_gl_dis_insurance.length; i++) {
          const record = req_gl_dis_insurance[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
         
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
        
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
         
        }
  
        return {affectedRowsCount,changedRowsCount};
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
        
      } catch (error) {
        console.log(error);
        return error;
      }

    }


    async updateGroupLifeHealthInsurance(
      req_gl_health_insurance,
      conn,
      pool
    ){
      try {
        //console.table(req_gl_health_insurance);
        // console.log(conn);
        // console.log(pool);
  
        let affectedRowsCount = 0;
        let changedRowsCount = 0;
        
  
        for (let i = 0; i < req_gl_health_insurance.length; i++) {
          const record = req_gl_health_insurance[i];
  
          let id = record["id"];
          let policy_id = record["policy_id"];
          let policy_section_id = record["policy_section_id"];
          let policy_section_types_id = record["policy_section_types_id"];
          let policy_status_id = record["policy_status_id"];
          let section_type_val_name = record["section_type_val_name"];
          let sec_value = record["sec_value"];
         
  
          //console.table(record);
  
          let query = `UPDATE policy_section_values psv 
          SET 	 psv.policy_id = ${policy_id},
          psv.policy_section_id = ${policy_section_id},
          psv.policy_section_types_id = ${policy_section_types_id},
          psv.policy_status_id = ${policy_status_id},
          psv.section_type_val_name = '${section_type_val_name}',
          psv.sec_value = '${sec_value}'
          WHERE psv.id = ${id}`;
  
          let result = await queryObj.executeQueryConn(query, conn);
        
          affectedRowsCount = affectedRowsCount + result.affectedRows;
          changedRowsCount = changedRowsCount + result.changedRows;
         
        }
  
        return {affectedRowsCount,changedRowsCount};
  
        // console.log(affectedRowsCount);
        // console.log(changedRowsCount);
        
      } catch (error) {
        console.log(error);
        return error;
      } 


    }

    async updateOtherBenefits(
      req_other_benefits,
      conn,
      pool
    ){

      
    try {
      //console.table(req_other_benefits);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;
      

      for (let i = 0; i < req_other_benefits.length; i++) {
        const record = req_other_benefits[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];
       

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
        SET 	 psv.policy_id = ${policy_id},
        psv.policy_section_id = ${policy_section_id},
        psv.policy_section_types_id = ${policy_section_types_id},
        psv.policy_status_id = ${policy_status_id},
        psv.section_type_val_name = '${section_type_val_name}',
        psv.sec_value = '${sec_value}'
        WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);
      
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
       
      }

      return {affectedRowsCount,changedRowsCount};

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
      
    } catch (error) {
      console.log(error);
      return error;
    } finally{
      conn.release();
      // console.log(pool._freeConnections.indexOf(conn));
    }
    }




}
exports.CompensationPolicyData = CompensationPolicyData;