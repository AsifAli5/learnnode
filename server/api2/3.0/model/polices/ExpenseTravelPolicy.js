var stringCheck = require("../../../../utils/StringCheck");
const config = require("../../../../../config");
const queryObj = require("../../helper/query");

class ExpenseTravelPolicyData {
  constructor() {}

  //to get expense and travel policy
  async getExpenseTravelPolicy(policy_id, conn, pool) {
    try {
      let query = `SELECT *
            FROM policy_section_values psv
            WHERE psv.policy_id = ${policy_id};`;

      let result = queryObj.executeQueryConn(query, conn);
      return result;
    } catch (err) {
      console.log(err);
      return err;
    } finally {
      conn.release();
      //console.log(pool._freeConntections.indexOf(conn));
    }
  }

  /**************************** Post Section ****************************** */

  //to post expense type
  async postExpenseType(req_expenses_type, conn, pool) {
    try {
      //console.log(req_expenses_type);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_expenses_type.length; i++) {
        let record = req_expenses_type[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to post expense authorization approvals
  async postExpenseAuthApprovals(req_expense_auth_approvals, conn, pool) {
    try {
      //console.log(req_expense_auth_approvals);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_expense_auth_approvals.length; i++) {
        let record = req_expense_auth_approvals[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to post travel authorization approvals
  async postTravelAuthApprovals(req_travel_auth_approvals, conn, pool) {
    try {
      //console.log(req_travel_auth_approvals);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_travel_auth_approvals.length; i++) {
        let record = req_travel_auth_approvals[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to post business travel type
  async postBusinessTravelType(req_business_travel_type, conn, pool) {
    try {
      //console.log(req_business_travel_type);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_business_travel_type.length; i++) {
        let record = req_business_travel_type[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to post accommodation type
  async postAccommodationType(req_accommodation_type, conn, pool) {
    try {
      //console.log(req_accommodation_type);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_accommodation_type.length; i++) {
        let record = req_accommodation_type[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to post daily allowance for personal arrangements
  async postDailyAllowancePa(req_daily_allowance_pa, conn, pool) {
    try {
      //console.log(req_daily_allowance_pa);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_daily_allowance_pa.length; i++) {
        let record = req_daily_allowance_pa[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to post travel class
  async postTravelClass(req_travel_class, conn, pool) {
    try {
      //console.log(req_travel_class);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_travel_class.length; i++) {
        let record = req_travel_class[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  // to post travel daily allowance limit
  async postTravelDailyAllowanceLimit(
    req_travel_daily_allowance_limit,
    conn,
    pool
  ) {
    try {
      //console.log(req_travel_daily_allowance_limit);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_travel_daily_allowance_limit.length; i++) {
        let record = req_travel_daily_allowance_limit[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to post kilometer allowance
  async postKilometerAllowance(req_kilometer_allowance, conn, pool) {
    try {
      //console.log(req_kilometer_allowance);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_kilometer_allowance.length; i++) {
        let record = req_kilometer_allowance[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to post transfere allowances
  async postTransferAllowances(req_transfer_allowances_type, conn, pool) {
    try {
      //console.log(req_transfer_allowances_type);
      //console.log(conn);
      //console.log(pool);
      let insertIds = [];
      let affectedRowsCount = 0;

      for (let i = 0; i < req_transfer_allowances_type.length; i++) {
        let record = req_transfer_allowances_type[i];

        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        let query = `INSERT INTO policy_section_values
                        (
                        policy_id,
                        policy_section_id,
                        policy_section_types_id,
                        policy_status_id,
                        section_type_val_name,
                        sec_value
                        ) VALUES
                        (${policy_id},${policy_section_id},${policy_section_types_id},${policy_status_id},'${section_type_val_name}','${sec_value}');`;

        let result = await queryObj.executeInsertQuery(query, conn);
        affectedRowsCount = affectedRowsCount + result.affectedRows;
        insertIds.push(result.insertId);
      }

      return { affectedRowsCount, insertIds };
    } catch (error) {
      console.log(error);
      return error;
    } finally {
      conn.release();
      // console.log(pool._freeConnections.indexOf(conn));
    }
  }

  /**************************** Update Section ****************************** */

  //to update expense type
  async updateExpenseType(req_expenses_type, conn, pool) {
    try {
      //console.table(req_expenses_type);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_expenses_type.length; i++) {
        const record = req_expenses_type[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }

      return { affectedRowsCount, changedRowsCount };

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to update expense authorization approvals
  async updateExpenseAuthApprovals(req_expense_auth_approvals, conn, pool) {
    try {
      //console.table(req_expense_auth_approvals);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_expense_auth_approvals.length; i++) {
        const record = req_expense_auth_approvals[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }

      return { affectedRowsCount, changedRowsCount };

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to update travel authorization approvals
  async updateTravelAuthApprovals(req_travel_auth_approvals, conn, pool) {
    try {
      //console.table(req_travel_auth_approvals);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_travel_auth_approvals.length; i++) {
        const record = req_travel_auth_approvals[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }

      return { affectedRowsCount, changedRowsCount };

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to update business travel type
  async updateBusinessTravelType(req_business_travel_type, conn, pool) {
    try {
      //console.table(req_business_travel_type);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_business_travel_type.length; i++) {
        const record = req_business_travel_type[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }

      return { affectedRowsCount, changedRowsCount };

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to update accommodation type
  async updateAccommodationType(req_accommodation_type, conn, pool) {
    try {
      //console.table(req_accommodation_type);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_accommodation_type.length; i++) {
        const record = req_accommodation_type[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }

      return { affectedRowsCount, changedRowsCount };

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to update daily allowance for personal arrangements
  async updateDailyAllowancePa(req_daily_allowance_pa, conn, pool) {
    try {
      //console.table(req_daily_allowance_pa);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_daily_allowance_pa.length; i++) {
        const record = req_daily_allowance_pa[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }

      return { affectedRowsCount, changedRowsCount };

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to update travel class
  async updateTravelClass(req_travel_class, conn, pool) {
    try {
      //console.table(req_travel_class);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_travel_class.length; i++) {
        const record = req_travel_class[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }

      return { affectedRowsCount, changedRowsCount };

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  // to update travel daily allowance limit
  async updateTravelDailyAllowanceLimit(
    req_travel_daily_allowance_limit,
    conn,
    pool
  ) {
    try {
      //console.table(req_travel_daily_allowance_limit);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_travel_daily_allowance_limit.length; i++) {
        const record = req_travel_daily_allowance_limit[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }

      return { affectedRowsCount, changedRowsCount };

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to update kilometer allowance
  async updateKilometerAllowance(req_kilometer_allowance, conn, pool) {
    try {
      //console.table(req_kilometer_allowance);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_kilometer_allowance.length; i++) {
        const record = req_kilometer_allowance[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }

      return { affectedRowsCount, changedRowsCount };

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  //to update transfere allowances
  async updateTransferAllowances(req_transfer_allowances_type, conn, pool) {
    try {
      //console.table(req_transfer_allowances_type);
      // console.log(conn);
      // console.log(pool);

      let affectedRowsCount = 0;
      let changedRowsCount = 0;

      for (let i = 0; i < req_transfer_allowances_type.length; i++) {
        const record = req_transfer_allowances_type[i];

        let id = record["id"];
        let policy_id = record["policy_id"];
        let policy_section_id = record["policy_section_id"];
        let policy_section_types_id = record["policy_section_types_id"];
        let policy_status_id = record["policy_status_id"];
        let section_type_val_name = record["section_type_val_name"];
        let sec_value = record["sec_value"];

        //console.table(record);

        let query = `UPDATE policy_section_values psv 
              SET 	 psv.policy_id = ${policy_id},
              psv.policy_section_id = ${policy_section_id},
              psv.policy_section_types_id = ${policy_section_types_id},
              psv.policy_status_id = ${policy_status_id},
              psv.section_type_val_name = '${section_type_val_name}',
              psv.sec_value = '${sec_value}'
              WHERE psv.id = ${id}`;

        let result = await queryObj.executeQueryConn(query, conn);

        affectedRowsCount = affectedRowsCount + result.affectedRows;
        changedRowsCount = changedRowsCount + result.changedRows;
      }

      return { affectedRowsCount, changedRowsCount };

      // console.log(affectedRowsCount);
      // console.log(changedRowsCount);
    } catch (error) {
      console.log(error);
      return error;
    } finally {
      conn.release();
      // console.log(pool._freeConnections.indexOf(conn));
    }
  }
}
exports.ExpenseTravelPolicyData = ExpenseTravelPolicyData;
