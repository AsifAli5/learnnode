var mysql = require('mysql');
const config = require('../../config');
class Database {

  connect() {
    if (typeof poolNumber !== "number") {

      const host =config.host ||"localhost";
      const user = config.user||"root";
      const password = config.password||"";
      const port =config.port|| "3306";
      const database = config.database||"policies_db";
      const multipleStatements = true;

      this.Connection = mysql.createConnection({
        host: host,
        user: user,
        password: password,
        port: port,
        database: database,
        multipleStatements: multipleStatements,
      });

      let connection = this.Connection;
      connection.connect(function (err) {
        if (err) {
          console.error('error connecting: ' + err.stack);
          return;
        }
        console.log('connected as id ' + connection.threadId);
      });
    } else {
      this.Connection = mysql.createPool({
        host: host,
        user: user,
        password: password,
        port: port,
        database: database,
        multipleStatements: multipleStatements,
        connectionLimit: poolNumber,
      });

      this.Connection.on('acquire', function (connection) {
        console.log('Connection %d acquired', connection.threadId);
      });
      this.Connection.on('enqueue', function () {
        console.log('Waiting for available connection slot');
      });
      this.Connection.on('release', function (connection) {
        console.log('Connection %d released', connection.threadId);
      });
    }
  }

  query(queryString, callback) {
    this.connect();
    this.Connection.query(queryString, function (error, results, fields) {
      if (error) {
        callback(null, error);
      } else {
        callback(results, false);
      }
    });
    this.Connection.end();
  }

}

module.exports = Database;
