const mongoose = require('mongoose');

mongoose.connect(
        'mongodb://localhost:27017/peoplei', {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
    ).then(() => {
        console.log('Successfuly connected');
    })
    .catch(() => {
        console.log('Connection not found');
       // process.exit();
    });