class StringCheck {

    constructor() {
    }

    empty( string ) {

        if ( (string == '') && (string == null) && 
            (string == 'undefined') && (typeof string == 'undefined')
        ) {

            return true;

        }else {

            return false;

        }

    }

    isset( string ) {

        if (typeof string !== 'undefined') {
            return true;
        }
        else {
            return false;
        }

    }

}

exports.StringCheck = StringCheck;