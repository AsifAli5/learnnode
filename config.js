var config  =   {};

// server export configurations
config.server       =   {};
config.server.HOST  =   'localhost';
config.server.PORT  =   '3000';

//For Local Envoirement 
config.db                           =   {};
config.db.HOST                      =   'localhost';
config.db.USER                      =   'root';
config.db.PASSWORD                  =   '';
config.db.PORT                      =   '3306';
config.db.NAME                      =   'ppi_people_3.0.0';
config.db.ALLOW_MULTIPLE_STATEMENTS =   true;

// // db export configurations
// config.db                           =   {};
// config.db.HOST                      =   '192.168.99.240';
// config.db.USER                      =   'khalil';
// config.db.PASSWORD                  =   'khalil786';
// config.db.PORT                      =   '3306';
// config.db.NAME                      =   'people';
// config.db.ALLOW_MULTIPLE_STATEMENTS =   true;

// site assets and local assets configurations
config.site             =   {};
config.site.SITE_URL    =   'http://localhost:81/';
config.site.PPM_ROOT    =   '/ppi_people/';
config.site.PPM_URL     =   'ppi_people/';
config.site.P_LOCAL     =   'http://localhost:81/';

module.exports  =   config;